---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
author: team / organisatie(onderdeel)
status: draft
version: v0.1
capabilities: []
categories: []
description: >
  Vul hier een korte beschrijving in (deze wordt als eerste alinea zichtbaar op de pagina)
---
Inleidende alinea, dit benoemt de volgende onderdelen:
1. Aanleiding voor schrijven van deze pagina
2. Doel van deze pagina 
3. Daar waar mogelijk, beschrijving van de totstandkoming van de pagina

## Hoofdstuk 1

...

### Paragraaf 1

...

### Paragraaf 2

...

## Hoofdstuk 2

...
