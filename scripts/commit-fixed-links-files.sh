#!/bin/sh

# if internal links are fixed by the script these changes should be commit to the branch

# check input in pipeline
echo "- (DEBUG) FDS_PUSH_TOKEN: $FDS_PUSH_TOKEN"

config_git_and_push() {
    echo '- commit and push!'
    git config user.email "gitlab-ci@federatief.datastelsel.nl"
    git config user.name "ci-bot"
    git remote add gitlab_origin https://$CI_PROJECT_NAME:$FDS_PUSH_TOKEN@gitlab.com/datastelsel.nl/federatief/website.git
    git remote show gitlab_origin
    git add .
    git commit -m "push back from pipeline"
    git push gitlab_origin HEAD:$CI_COMMIT_REF_NAME -o ci.skip # prevent triggering pipeline again
}

check_for_changed_files() {
    HAS_CHANGES=true
    echo '- checking for changed files'
    git status --porcelain
    if [ "`git status --porcelain`" != "" ]; then
        echo Local changes: We need to commit and push!
        HAS_CHANGES=true
    else
        echo Nothing to commit
        HAS_CHANGES=false
    fi
}

check_for_changed_files
echo "- (DEBUG) HAS_CHANGES: $HAS_CHANGES"

if [ "$HAS_CHANGES" = "true" ] && [ "$FDS_PUSH_TOKEN" != "" ]; then
    echo "- (DEBUG) do commit magic ..."
    config_git_and_push
fi