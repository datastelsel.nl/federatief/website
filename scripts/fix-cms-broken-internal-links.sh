#!/bin/sh
echo 'Fixing internal links broken by the CMS (Decap)'

echo '- found:'
find ./content -name "*.md" -exec grep -rl "(<>)\|\[/\[" {} \;

echo '- fixing ...'
find ./content -name "*.md" -exec sed -i -e 's/\[\\\[/\[\[/g' {} \; -exec sed -i -e 's/\(<>\)//g' {} \;

echo '- checking ...'
if [[ `find ./content -name "*.md" -exec grep -rl "(<>)\|\[/\[" {} \;` ]]; then
    find ./content -name "*.md" -exec grep -rl "(<>)\|\[/\[" {} \;
    echo 'ERROR still found broken links!'
    exit 1 
fi
