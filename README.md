# federatief.datastelsel.nl | Website

Dit is de start van het platform voor de ontwikkeling van het federatief datastelsel!

In deze repository staan de bronbestanden in [Markdown](https://www.markdownguide.org/). Daarvan
wordt een site gegenereerd die de publicatie is van deze bronbestanden:

**[federatief.datastelsel.nl](https://federatief.datastelsel.nl/)**

## Management

For the management of this project there're a few tools available:

- Website: [f.ds.nl](https://federatief.datastelsel.nl/) |
  [CMS](https://federatief.datastelsel.nl/cms/) | [CMS
  Workflow](https://federatief.datastelsel.nl/cms/#/workflow)
- GitLab: [repo](https://gitlab.com/datastelsel.nl/federatief/website) |
  [MRs](https://gitlab.com/datastelsel.nl/federatief/website/-/merge_requests) | [Issue
  board](https://gitlab.com/datastelsel.nl/federatief/website/-/boards)
- Statistics: [Goatcounter](https://fds.goatcounter.com/) | [GitLab MR stats](https://gitlab.com/datastelsel.nl/federatief/website/-/analytics/merge_request_analytics)

## Usage

### Hugo with Docsy

Deze site maakt gebruik [Hugo](https://gohugo.io/) met de [Docsy](https://www.docsy.dev/) theme. De
basis komt dan ook uit de [Docsy example site](https://example.docsy.dev/)
([github](https://github.com/google/docsy-example)).

### Running the website locally

Building and running the site locally requires a recent `extended` version of
[Hugo](https://gohugo.io). You can find out more about how to install Hugo for your environment in
our [Getting started](https://www.docsy.dev/docs/getting-started/#prerequisites-and-installation)
guide.

Once you've made your working copy of the site repo, from the repo root folder, run:

```bash
hugo server
```

### Developing the Docsy FDS theme locally

Uncomment the comments about developing the Docsy FDS theme locally in the following files:

- go.mod
- docker-compose.yaml

Then start docker compose with `docker compose up`

### Cleanup

To stop Docker Compose, on your terminal window, press **Ctrl + C**.

To remove the produced images run:

```bash
docker-compose rm
```

For more information see the [Docker Compose documentation][].

## Check broken links

Live dead link check:
```bash
wget --spider --connect-timeout=5 -t 2 -o f.ds.nl.log -e robots=off -rp https://federatief.datastelsel.nl/kennisbank/
```

Local dead link check:
```bash
wget --spider --connect-timeout=5 -t 2 -o f.ds.nl.log -e robots=off -rp http://localhost:8080/kennisbank/
```

## Support

For support use our [Mattermost
~samenwerkingsplatform](https://digilab.overheid.nl/chat/fds/channels/samenwerkingsplatform)
channel.

## Contributing

Contributions are most welcome! See the published [contribution
guide](https://federatief.datastelsel.nl/docs/contribution/) for how to.

## Authors and acknowledgment

This project is authored by the
[FDS](https://realisatieibds.nl/cms/view/8852ee2a-a28a-4b91-9f3e-aab229bbe07f/federatief-datastelsel)
Architects with help from [Digilab](https://digilab.overheid.nl/)

## License

This project is [CC0](./LICENSE.md) licensed. This enables maximal freedom for others to re-use this
content.
