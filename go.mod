module gitlab.com/datastelsel.nl/federatief/website

go 1.20

// replace gitlab.com/datastelsel.nl/federatief/docsy-fds => ../docsy-fds // uncomment to develop local docsy-fds theme

require (
	github.com/google/docsy v0.11.0 // indirect
	gitlab.com/datastelsel.nl/federatief/docsy-fds v0.0.27 // indirect
	gitlab.com/datastelsel.nl/federatief/puzzels v0.0.0-20240704070216-00c7530eebf8 // indirect
)
