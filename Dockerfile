FROM docker.io/hugomods/hugo:exts AS build

# Copy the repo content to /site
WORKDIR /site

COPY package.json package-lock.json ./

# Run npm install to build the dependencies
RUN npm ci

COPY . .

# Run Hugo to build the static files
# RUN hugo --gc
RUN hugo build --gc

# Serve the files using nginx (preferred over serving them with the Hugo server)
FROM nginx:1.27-alpine3.20

COPY --from=build /site/public /usr/share/nginx/html

COPY ./nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 8080
