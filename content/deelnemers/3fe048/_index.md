---
title: Kamer van Koophandel
date: 2024-12-11
description: >
  De deelnemerkenmerken van de Kamer van Koophandel
---

<div about="#deelnemer" prefix="rdfs: http://www.w3.org/2000/01/rdf-schema# fds: http://federatief.datastelsel.nl/deelnemers/ontology/#">

| Kenmerk         | Waarde                                                                           |
|-----------------|----------------------------------------------------------------------------------|
| Label           | <span property="rdfs:label">Kamer van Koophandel</span>                          |
| KvK Nummer      | <span property="fds:kvk">59581883</span>                                         |
| OIN             | <span property="fds:oin">00000003595818830000</span>                             |
| TOOI            | <a rel="fds:tooi" href="https://identifier.overheid.nl/tooi/id/zbo/zb000184">Kamer van Koophandel</a>  |
| Toegelaten door | <a rel="fds:admitted-by" href="../12416e/#deelnemer">Ministerie van Binnenlandse Zaken en Koninkrijksrelaties</a>  |
| FDS aanbod      | <a rel="fds:catalog" href="dcat/#cat">Het FDS aanbod van de Kamer van Koophandel</a>  |

</div>

Deze pagina bevat linked data in [RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa) formaat, ook beschikbaar in [Turtle](index.ttl) en [JSONLD](index.jsonld). De linked data is opgezet volgens een CONCEPT [ontologie](../ontology/).

