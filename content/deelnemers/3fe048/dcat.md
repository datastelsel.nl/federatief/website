---
title: FDS Aanbod Kamer van Koophandel
date: 2024-12-11
description: >
  Het FDS aanbod van de Kamer van Koophandel
---
<div about="#cat" prefix="dct: http://purl.org/dc/terms/" >

## Het aanbod 
{{% pageinfo color="info" %}}
Binnen FDS is het de bedoeling dat deze informatie door de aanbieder zelf wordt gepubliceerd. Hierop vooruitlopend
publiceren we hier alvast een gedeelte om een indruk te geven hoe deze informatie eruit zou kunnen zien.
{{% /pageinfo %}}

| Kenmerk          | Waarde                                                                           |
|------------------|----------------------------------------------------------------------------------|
| Naam             | <span property="dct:title">Het FDS aanbod van de Kamer van Koophandel</span>     |
| Aanbieder        | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/zbo/zb000184">Kamer van Koophandel</a> (TOOI link)  |
|                  | <a rel="dct:creator" href="../#deelnemer">Kamer van Koophandel</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/zbo/zb000184">Kamer van Koophandel</a> (TOOI link)  |
|                  | <a rel="dct:publisher" href="../#deelnemer">Kamer van Koophandel</a> (FDS deelnemer link)  |
| Wijzigingsdatum  | <span property="dct:modified" datatype="xsd:date">2024-12-11</span>              |
| Homepage         | <a rel="foaf:homepage" href=".">deze pagina</a>                                  |

<div property='dcat:dataset' resource="#hr-dataset"></div>

</div> <!-- about #cat-->

## De dataset(s) 

### HR Dataset <!-- Niet aanwezig van aanbieder - Wel record van KOOP -->

<div about="#hr-dataset" prefix="dct: http://purl.org/dc/terms/">

| Kenmerk                  | Waarde                                                                           |
|--------------------------|----------------------------------------------------------------------------------|
| Naam                     | <span property="dct:title">Handelsregister</span>                                |
| Aanbieder                | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/zbo/zb000184">Kamer van Koophandel</a> (TOOI link)  |
|                          | <a rel="dct:creator" href="../#deelnemer">Kamer van Koophandel</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/zbo/zb000184">Kamer van Koophandel</a> (TOOI link)  |
|                          | <a rel="dct:publisher" href="../#deelnemer">Kamer van Koophandel</a> (FDS deelnemer link)  |
| Toegang                  | <a property="dct:accessRights" href="http://publications.europa.eu/resource/authority/access-right/RESTRICTED">Beperkt</a>  |
| Thema                    | <a property='dcat:theme' href="http://standaarden.overheid.nl/owms/terms/Bestuur">Bestuur</a>  |

<div property="dct:description">

In het Handelsregister staan alle bedrijven en organisaties. Je ziet met wie je te maken hebt en wie namens een bedrijf contracten mag tekenen of aansprakelijk is. Je vindt er adresgegevens en ziet of een bedrijf failliet is. Zo kun je veilig zakendoen.
</div>

</div> <!-- about #hr-dataset-->


</div> <!-- prefixes-->

--------------------------------------------

Deze pagina bevat linked data in [RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa) formaat, ook beschikbaar in [Turtle](index.ttl) en [JSONLD](index.jsonld). De linked data is opgezet volgens de [DCAT-AP-NL 3.0](https://docs.geostandaarden.nl/dcat/dcat-ap-nl30/) ontologie.
