---
title: FDS Aanbod TNO
date: 2024-12-11
description: >
  Het FDS aanbod van Nederlandse organisatie voor toegepast-natuurwetenschappelijk onderzoek (TNO)
---
<div about="#cat" prefix="dct: http://purl.org/dc/terms/" >

## Het aanbod 
{{% pageinfo color="info" %}}
Binnen FDS is het de bedoeling dat deze informatie door de aanbieder zelf wordt gepubliceerd. Hierop vooruitlopend
publiceren we hier alvast een gedeelte om een indruk te geven hoe deze informatie eruit zou kunnen zien.
{{% /pageinfo %}}

| Kenmerk          | Waarde                                                                           |
|------------------|----------------------------------------------------------------------------------|
| Naam             | <span property="dct:title">Het FDS aanbod van de Nederlandse organisatie voor toegepast-natuurwetenschappelijk onderzoek</span>                |
| Aanbieder        | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/zbo/zb000162">Nederlandse organisatie voor toegepast-natuurwetenschappelijk onderzoek</a> (TOOI link)  |
|                  | <a rel="dct:creator" href="../#deelnemer">Nederlandse organisatie voor toegepast-natuurwetenschappelijk onderzoek</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/zbo/zb000162">Nederlandse organisatie voor toegepast-natuurwetenschappelijk onderzoek</a> (TOOI link)  |
|                  | <a rel="dct:publisher" href="../#deelnemer">Nederlandse organisatie voor toegepast-natuurwetenschappelijk onderzoek</a> (FDS deelnemer link)  |
| Wijzigingsdatum  | <span property="dct:modified" datatype="xsd:date">2024-12-11</span>              |
| Homepage         | <a rel="foaf:homepage" href=".">deze pagina</a>                               |

<div property='dcat:dataset' resource="#bro-dataset"></div>

</div> <!-- about #cat-->

## De dataset(s) 

### BRO Dataset <!-- Niet aanwezig op nivo BRO - Wel de fijnere onderdelen aanwezig maar hier niet genoemd -->

<div about="#bro-dataset" prefix="dct: http://purl.org/dc/terms/">

| Kenmerk                  | Waarde                                                                           |
|--------------------------|----------------------------------------------------------------------------------|
| Naam                     | <span property="dct:title">Basisregistratie: Ondergrond (BRO)</span>     |
| Aanbieder                | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/zbo/zb000162">Nederlandse organisatie voor toegepast-natuurwetenschappelijk onderzoek</a> (TOOI link)  |
|                          | <a rel="dct:creator" href="../#deelnemer">Nederlandse organisatie voor toegepast-natuurwetenschappelijk onderzoek</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/zbo/zb000162">Nederlandse organisatie voor toegepast-natuurwetenschappelijk onderzoek</a> (TOOI link)  |
|                          | <a rel="dct:publisher" href="../#deelnemer">Nederlandse organisatie voor toegepast-natuurwetenschappelijk onderzoek</a> (FDS deelnemer link)  |
| Toegang                  | <a property="dct:accessRights" href="http://publications.europa.eu/resource/authority/access-right/PUBLIC">Publiek</a>  |
| Thema                    | <a property='dcat:theme' href="http://standaarden.overheid.nl/owms/terms/Ruimtelijke_ordening">Ruimtelijke Ordening</a>  |

<div property="dct:description">

De Basisregistratie Ondergrond (BRO) is een centrale registratie met eenduidige, betrouwbare data en informatie over de Nederlandse ondergrond. Overheden leggen voor dezelfde objecten dezelfde, betrouwbare, algemene gegevens vast. Vanuit één centrale digitale plek, de landelijke voorziening, kunnen gebruikers gegevens opvragen voor informatie over bodem en ondergrond. Op de datasets kunnen ook rekenprogramma’s en modellen uitgevoerd worden.
</div>

</div> <!-- about #bro-dataset-->


</div> <!-- prefixes-->

--------------------------------------------

Deze pagina bevat linked data in [RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa) formaat, ook beschikbaar in [Turtle](index.ttl) en [JSONLD](index.jsonld). De linked data is opgezet volgens de [DCAT-AP-NL 3.0](https://docs.geostandaarden.nl/dcat/dcat-ap-nl30/) ontologie.
