---
title: Ontologie
weight: 20
date: 2024-12-11
version: v0.1
status: Concept
description: >
  De ontologie van de deelnemerslijst van het FDS
---

Deze beschrijft bevat een CONCEPT linked data ontologie document in [RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa) formaat, ook beschikbaar in [Turtle](index.ttl) en [JSONLD](index.jsonld).

De aanbevolen prefix voor deze ontologie:

<pre>
  <code class="language-turtle">fds: http://federatief.datastelsel.nl/deelnemers/#</code>
</pre>

<div property="rdf:type" resource="owl:Ontology"></div>

### Deelnemer

<pre><code class="language-turtle"><div about="#participant">fds:participant
  a <span rel="rdf:type" resource="rdfs:Class">rdfs:Class</span>, <span rel="rdf:type" resource="owl:Class">owl:Class</span> ;
  rdfs:label '<span property="rdfs:label">Deelnemer</span>'@nl ;
  rdfs:comment '<span property="rdfs:comment">Een deelnemer van FDS.</span>'@nl .
</div>
</code></pre>

<pre><code class="language-turtle"><div about="#kvk">fds:kvk
  a <span rel="rdf:type" resource="rdf:Property">rdf:Property</span>, <span rel="rdf:type" resource="owl:DatatypeProperty">owl:DatatypeProperty</span> ;
  rdfs:domain <span rel="rdfs:domain" resource="#participant">fds:participant</span> ;
  rdfs:range <span rel="rdfs:range" resource="xsd:string">xsd:string</span> ;
  rdfs:label '<span property="rdfs:label">kvk</span>'@nl ;
  rdfs:comment '<span property="rdfs:comment">KvK nummer van de deelnemer.</span>'@nl .
</div>
</code></pre>

<pre><code class="language-turtle"><div about="#oin">fds:oin
  a <span rel="rdf:type" resource="rdf:Property">rdf:Property</span>, <span rel="rdf:type" resource="owl:DatatypeProperty">owl:DatatypeProperty</span> ;
  rdfs:domain <span rel="rdfs:domain" resource="#participant">fds:participant</span> ;
  rdfs:range <span rel="rdfs:range" resource="xsd:string">xsd:string</span> ;
  rdfs:label '<span property="rdfs:label">oin</span>'@nl ;
  rdfs:comment '<span property="rdfs:comment">Overheidsidentificatienummer (oin) van de deelnemer.</span>'@nl .
</div>
</code></pre>

<pre><code class="language-turtle"><div about="#tooi">fds:tooi
  a <span rel="rdf:type" resource="rdf:Property">rdf:Property</span>, <span rel="rdf:type" resource="owl:ObjectProperty">owl:ObjectProperty</span> ;
  rdfs:domain <span rel="rdfs:domain" resource="#participant">fds:participant</span> ;
  rdfs:range <a rel="rdfs:range" resource="https://identifier.overheid.nl/tooi/def/ont#Overheidsorganisatie" href="https://standaarden.overheid.nl/tooi/doc/tooi-ontologie-1.4.1/">tooiont:Overheidsorganisatie</a> ;
  rdfs:label '<span property="rdfs:label">tooi</span>'@nl ;
  rdfs:comment '<span property="rdfs:comment">Verwijzing naar de organisatie in TOOI.</span>'@nl .
</div>
</code></pre>

<pre><code class="language-turtle"><div about="#admitted-by">fds:admitted-by
  a <span rel="rdf:type" resource="rdf:Property">rdf:Property</span>, <span rel="rdf:type" resource="owl:ObjectProperty">owl:ObjectProperty</span> ;
  rdfs:domain <span rel="rdfs:domain" resource="#participant">fds:participant</span> ;
  rdfs:range <span rel="rdfs:range" resource="#admin">fds:admin</span> ;
  rdfs:label '<span property="rdfs:label">toegelaten door</span>'@nl ;
  rdfs:comment '<span property="rdfs:comment">De deelname-adminstrateur die de deelnemer heeft toegelaten.</span>'@nl .
</div>
</code></pre>

<pre><code class="language-turtle"><div about="#catalog">fds:catalog
  a <span rel="rdf:type" resource="rdf:Property">rdf:Property</span>, <span rel="rdf:type" resource="owl:ObjectProperty">owl:ObjectProperty</span> ;
  rdfs:domain <span rel="rdfs:domain" resource="#participant">fds:participant</span> ;
  rdfs:range <span rel="rdfs:range" resource="dcat:catalog">dcat:catalog</span> ;
  rdfs:label '<span property="rdfs:label">FDS aanbod</span>'@nl ;
  rdfs:comment '<span property="rdfs:comment">De FDS dcat-catalog met het aanbod van de deelnemer als aanbieder.</span>'@nl .
</div>
</code></pre>

### Admin

<pre><code class="language-turtle"><div about="#admin">fds:admin
  a <span rel="rdf:type" resource="rdfs:Class">rdfs:Class</span>, <span rel="rdf:type" resource="owl:Class">owl:Class</span> ;
  rdfs:subClassOf <span rel="rdfs:subClassOf" resource="#participant">fds:participant</span> ;
  rdfs:label '<span property="rdfs:label">Deelname administrateur</span>'@nl ;
  rdfs:comment '<span property="rdfs:comment">De deelname administrateur is een deelnemer die andere deelnemers kan toelaten tot FDS.</span>'@nl .
</div>
</code></pre>


-- (nog) geen aanvullende properties ---
