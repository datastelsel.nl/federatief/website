---
title: Deelnemers
date: 2024-12-11
menu: {main: {weight: 80}}
weight: 20
description: >
  De lijst met deelnemers aan FDS
---

<div about="#lijst" prefix="fds: http://federatief.datastelsel.nl/deelnemers/ontology/#">
{{% pageinfo color="info" %}}
Deze sectie bevat een CONCEPT deelnemerslijst van FDS. De opgenomen aanbieders hebben op de stelseldag 
van 11 december 2024 een intentieverklaringen getekend om te gaan deelnemen aan FDS. Deze informatie is vooral bedoeld
om inzichtelijk te maken wat de intentie is om informatie over de deelnemers en het aanbod _machineleesbaar_ beschikbaar te
maken. 
{{% /pageinfo %}}

De deelnemers aan FDS zijn:

- <a rel="rdfs:member" href="12416e/#deelnemer">Ministerie van Binnenlandse Zaken en Koninkrijksrelaties</a>
- <a rel="rdfs:member" href="c882eb/#deelnemer">Dienst voor het kadaster en de openbare registers</a>
- <a rel="rdfs:member" href="2ca2de/#deelnemer">Het Waterschapshuis</a>
- <a rel="rdfs:member" href="3e512d/#deelnemer">Rijksdienst voor Identiteitsgegevens</a>
- <a rel="rdfs:member" href="3fe048/#deelnemer">Kamer van Koophandel</a>
- <a rel="rdfs:member" href="aefd89/#deelnemer">Nederlandse organisatie voor toegepast-natuurwetenschappelijk onderzoek (TNO)</a>
- <a rel="rdfs:member" href="bed120/#deelnemer">Ministerie van Financiën</a>
- <a rel="rdfs:member" href="cf5bec/#deelnemer">Centraal Bureau voor de Statistiek</a>

</div>

Deze pagina bevat linked data in [RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa) formaat, ook beschikbaar in [Turtle](index.ttl) en [JSONLD](index.jsonld). De linked data is opgezet volgens een CONCEPT [ontologie](ontology/).

