---
title: FDS Aanbod Centraal Bureau voor de Statistiek
date: 2024-12-11
description: >
  Het FDS aanbod van het Centraal Bureau voor de Statistiek
---
<div about="#cat" prefix="dct: http://purl.org/dc/terms/" >

## Het aanbod
{{% pageinfo color="info" %}}
Binnen FDS is het de bedoeling dat deze informatie door de aanbieder zelf wordt gepubliceerd. Hierop vooruitlopend
publiceren we hier alvast een gedeelte om een indruk te geven hoe deze informatie eruit zou kunnen zien.
{{% /pageinfo %}}

| Kenmerk          | Waarde                                                                           |
|------------------|----------------------------------------------------------------------------------|
| Naam             | <span property="dct:title">Het FDS aanbod van het Centraal Bureau voor de Statistiek</span>                |
| Aanbieder        | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/zbo/zb000193">Directeur-generaal van de Statistiek</a> (TOOI link)  |
|                  | <a rel="dct:creator" href="../#deelnemer">Centraal Bureau voor de Statistiek</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/zbo/zb000193">Directeur-generaal van de Statistiek</a> (TOOI link)  |
|                  | <a rel="dct:publisher" href="../#deelnemer">Centraal Bureau voor de Statistiek</a> (FDS deelnemer link)  |
| Wijzigingsdatum  | <span property="dct:modified" datatype="xsd:date">2024-12-11</span>              |
| Homepage         | <a rel="foaf:homepage" href=".">deze pagina</a>                                  |

<div property='dcat:catalog' resource="#cbsopendata-dataset"></div>
<div property='dcat:catalog' resource="#cbsgeslotendata-dataset"></div>

</div> <!-- about #cat-->

## De dataset(s) <!-- als Catalog ivm > 7000 datasets -->

### Catalogus met open data sets

<div about="#cbsopendata-dataset" prefix="dct: http://purl.org/dc/terms/">

| Kenmerk                  | Waarde                                                                           |
|--------------------------|----------------------------------------------------------------------------------|
| Naam                     | <span property="dct:title">De open datasets in het FDS aanbod van het CBS</span>     |
| Aanbieder                | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/zbo/zb000193">Directeur-generaal van de Statistiek</a> (TOOI link)  |
|                          | <a rel="dct:creator" href="../#deelnemer">Centraal Bureau voor de Statistiek</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/zbo/zb000193">Directeur-generaal van de Statistiek</a> (TOOI link)  |
|                          | <a rel="dct:publisher" href="../#deelnemer">Centraal Bureau voor de Statistiek</a> (FDS deelnemer link)  |
| Toegang                  | <a property="dct:accessRights" href="http://publications.europa.eu/resource/authority/access-right/PUBLIC">Publiek</a>  |
| Thema                    | <a property='dcat:theme' href="http://standaarden.overheid.nl/owms/terms/Water">Bestuur</a>  |



<div property="dct:description">

Open [hier](https://data.overheid.nl/datasets?facet_authority%5B0%5D=http%3A//standaarden.overheid.nl/owms/terms/Centraal_Bureau_voor_de_Statistiek&sort=score%20desc%2C%20sys_modified%20desc&facet_catalog%5B0%5D=https%3A//opendata.cbs.nl/ODataCatalog/) de catalogus met open datasets.
</div>

</div> <!-- about #cbsopendata-dataset-->



### Catalogus met gesloten datasets


<div about="#cbsgeslotendata-dataset" prefix="dct: http://purl.org/dc/terms/">

| Kenmerk                  | Waarde                                                                           |
|--------------------------|----------------------------------------------------------------------------------|
| Naam                     | <span property="dct:title">De gesloten datasets in het FDS aanbod van het CBS</span>     |
| Aanbieder                | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/zbo/zb000193">Directeur-generaal van de Statistiek</a> (TOOI link)  |
|                          | <a rel="dct:creator" href="../#deelnemer">Centraal Bureau voor de Statistiek</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/zbo/zb000193">Directeur-generaal van de Statistiek</a> (TOOI link)  |
|                          | <a rel="dct:publisher" href="../#deelnemer">Centraal Bureau voor de Statistiek</a> (FDS deelnemer link)  |
| Toegang                  | <a property="dct:accessRights" href="http://publications.europa.eu/resource/authority/access-right/NON_PUBLIC">Niet Publiek</a>  |
| Thema                    | <a property='dcat:theme' href="http://standaarden.overheid.nl/owms/terms/Water">Bestuur</a>  |

<div property="dct:description">

Open [hier](https://data.overheid.nl/datasets?facet_authority%5B0%5D=http%3A//standaarden.overheid.nl/owms/terms/Centraal_Bureau_voor_de_Statistiek&sort=score%20desc%2C%20sys_modified%20desc&facet_catalog%5B0%5D=https%3A//portal.odissei.nl/dataverse/cbs) de catalogus met gesloten datasets.
</div>

</div> <!-- about #cbsgeslotendata-dataset-->





</div> <!-- prefixes-->

--------------------------------------------

Deze pagina bevat linked data in [RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa) formaat, ook beschikbaar in [Turtle](index.ttl) en [JSONLD](index.jsonld). De linked data is opgezet volgens de [DCAT-AP-NL 3.0](https://docs.geostandaarden.nl/dcat/dcat-ap-nl30/) ontologie.
