---
title: FDS Aanbod (totaal)
date: 2024-12-11
weight: 10
description: >
  Het totale FDS aanbod vanuit de verschillende aanbieders
---
<div about="#cat" prefix="dct: http://purl.org/dc/terms/" >

## Het aanbod

| Kenmerk          | Waarde                                                                           |
|------------------|----------------------------------------------------------------------------------|
| Naam             | <span property="dct:title">Het total FDS aanbod vanuit de verschillende aanbieders</span>                |
| Aanbieder        | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/ministerie/mnre1034">Ministerie van BZK</a> (TOOI link)  |
|                  | <a rel="dct:creator" href="../12416e/#deelnemer">Ministerie van BZK</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/zbo/zb000193">Ministerie van BZK</a> (TOOI link)  |
|                  | <a rel="dct:publisher" href="../12416e/#deelnemer">Ministerie van BZK</a> (FDS deelnemer link)  |
| Wijzigingsdatum  | <span property="dct:modified" datatype="xsd:date">2024-12-11</span>              |
| Homepage         | <a rel="foaf:homepage" href=".">deze pagina</a>                               |


<a rel="dcat:catalog" href="../2ca2de/dcat/#cat">Het FDS aanbod van Het Waterschapshuis</a>

<a rel="dcat:catalog" href="../3e512d/dcat/#cat">Het FDS aanbod van de Rijksdienst voor Identiteitsgegevens</a>

<a rel="dcat:catalog" href="../3fe048/dcat/#cat">Het FDS aanbod van de Kamer van Koophandel</a>

<a rel="dcat:catalog" href="../aefd89/dcat/#cat">Het FDS aanbod van de Nederlandse organisatie voor toegepast-natuurwetenschappelijk onderzoek (TNO)</a>

<a rel="dcat:catalog" href="../bed120/dcat/#cat">Het FDS aanbod van het Ministerie van Financiën</a>

<a rel="dcat:catalog" href="../c882eb/dcat/#cat">Het FDS aanbod van het Kadaster</a>

<a rel="dcat:catalog" href="../cf5bec/dcat/#cat">Het FDS aanbod van het Centraal Bureau voor de Statistiek</a>


</div> <!-- about #cat-->

--------------------------------------------

Deze pagina bevat linked data in [RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa) formaat, ook beschikbaar in [Turtle](index.ttl) en [JSONLD](index.jsonld). De linked data is opgezet volgens de [DCAT-AP-NL 3.0](https://docs.geostandaarden.nl/dcat/dcat-ap-nl30/) ontologie.
