---
title: FDS Aanbod Kadaster
date: 2024-12-11
description: >
  Het FDS aanbod van het Kadaster
---
<div about="#cat" prefix="dct: http://purl.org/dc/terms/" >

## Het aanbod 
{{% pageinfo color="info" %}}
Binnen FDS is het de bedoeling dat deze informatie door de aanbieder zelf wordt gepubliceerd. Hierop vooruitlopend
publiceren we hier alvast een gedeelte om een indruk te geven hoe deze informatie eruit zou kunnen zien.
{{% /pageinfo %}}

| Kenmerk          | Waarde                                                                           |
|------------------|----------------------------------------------------------------------------------|
| Naam             | <span property="dct:title">Het FDS aanbod van het Kadaster</span>                |
| Aanbieder        | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/zbo/zb000195">Dienst voor het kadaster en de openbare registers</a> (TOOI link)  |
|                  | <a rel="dct:creator" href="../#deelnemer">Dienst voor het kadaster en de openbare registers</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/zbo/zb000195">Dienst voor het kadaster en de openbare registers</a> (TOOI link)  |
|                  | <a rel="dct:publisher" href="../#deelnemer">Dienst voor het kadaster en de openbare registers</a> (FDS deelnemer link)  |
| Wijzigingsdatum  | <span property="dct:modified" datatype="xsd:date">2024-12-11</span>              |
| Homepage         | <a rel="foaf:homepage" href=".">deze pagina</a>                               |

<div property='dcat:dataset' resource="#brt-dataset"></div>
<div property='dcat:dataset' resource="#brk-dataset"></div>
<div property='dcat:dataset' resource="#bgt-dataset"></div>
<div property='dcat:dataset' resource="#woz-dataset"></div>
<div property='dcat:dataset' resource="#bag-dataset"></div>

</div> <!-- about #cat-->

## De dataset(s) 

### BRT Dataset 
<!-- Merk op bestaat niet als dataset wel als TOP X bestanden - Hier alleen TOP10NL genomen -->

<div about="#brt-dataset" prefix="dct: http://purl.org/dc/terms/">

| Kenmerk                  | Waarde                                                                           |
|--------------------------|----------------------------------------------------------------------------------|
| Naam                     | <span property="dct:title">Basisregistratie Topografie</span>     |
| Aanbieder                | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/zbo/zb000195">Dienst voor het kadaster en de openbare registers</a> (TOOI link)  |
|                          | <a rel="dct:creator" href="../#deelnemer">Dienst voor het kadaster en de openbare registers</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/zbo/zb000195">Dienst voor het kadaster en de openbare registers</a> (TOOI link)  |
|                          | <a rel="dct:publisher" href="../#deelnemer">Dienst voor het kadaster en de openbare registers</a> (FDS deelnemer link)  |
| Toegang                  | <a property="dct:accessRights" href="http://publications.europa.eu/resource/authority/access-right/PUBLIC">Publiek</a>  |
| Thema                    | <a property='dcat:theme' href="http://standaarden.overheid.nl/owms/terms/Ruimtelijke_ordening">Ruimtelijke ordening</a>  |

<div property="dct:description">

De BRT bestaat uit digitale topografische bestanden op verschillende schaalniveaus. Zowel de opgemaakte kaarten als de objectgerichte bestanden zijn beschikbaar als open data. Dat betekent dat het Kadaster deze gegevensbestanden gratis en met minimale leveringsvoorwaarden ter beschikking stelt.

Zie ook de [website](https://www.kadaster.nl/zakelijk/registraties/basisregistraties/brt/brt-producten) van het kadaster.
</div>

</div> <!-- about #brt-dataset-->

### BRK Dataset 
<!-- Bestaat niet bij aanbieder - wel een record van publisher KOOP -->

<div about="#brk-dataset" prefix="dct: http://purl.org/dc/terms/">

| Kenmerk                  | Waarde                                                                           |
|--------------------------|----------------------------------------------------------------------------------|
| Naam                     | <span property="dct:title">Basisregistratie: Kadaster (BRK)</span>     |
| Aanbieder                | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/zbo/zb000195">Dienst voor het kadaster en de openbare registers</a> (TOOI link)  |
|                          | <a rel="dct:creator" href="../#deelnemer">Dienst voor het kadaster en de openbare registers</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/zbo/zb000195">Dienst voor het kadaster en de openbare registers</a> (TOOI link)  |
|                          | <a rel="dct:publisher" href="../#deelnemer">Dienst voor het kadaster en de openbare registers</a> (FDS deelnemer link)  |
| Toegang                  | <a property="dct:accessRights" href="http://publications.europa.eu/resource/authority/access-right/RESTRICTED">Beperktk</a>  |
| Thema                    | <a property='dcat:theme' href="http://standaarden.overheid.nl/owms/terms/Bestuur">Bestuur</a>  |

<div property="dct:description">

De Basisregistratie Kadaster (BRK) bestaat uit:

-	de kadastrale registratie van onroerende zaken en zakelijke rechten 
-	de kadastrale kaart. Hierop ziet u de ligging van de kadastrale percelen (inclusief perceelnummer) en de grenzen van het rijk, de provincies en de gemeenten.

Ook leidingen en kabelnetwerken zijn onroerende zaken. Een geregistreerd netwerk maakt daarom onderdeel uit van de BRK. 

Zie ook de [website](https://www.kadaster.nl/zakelijk/registraties/basisregistraties/brk) van het kadaster.
</div>

</div> <!-- about #brk-dataset-->

### BGT Dataset

<div about="#bgt-dataset" prefix="dct: http://purl.org/dc/terms/">

| Kenmerk                  | Waarde                                                                           |
|--------------------------|----------------------------------------------------------------------------------|
| Naam                     | <span property="dct:title">Basisregistratie Grootschalige Topografie (BGT)</span>     |
| Aanbieder                | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/zbo/zb000195">Dienst voor het kadaster en de openbare registers</a> (TOOI link)  |
|                          | <a rel="dct:creator" href="../#deelnemer">Dienst voor het kadaster en de openbare registers</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/zbo/zb000195">Dienst voor het kadaster en de openbare registers</a> (TOOI link)  |
|                          | <a rel="dct:publisher" href="../#deelnemer">Dienst voor het kadaster en de openbare registers</a> (FDS deelnemer link)  |
| Toegang                  | <a property="dct:accessRights" href="http://publications.europa.eu/resource/authority/access-right/PUBLIC">Publiek</a>  |
| Thema                    | <a property='dcat:theme' href="http://standaarden.overheid.nl/owms/terms/Ruimtelijke_ordening">Ruimtelijke ordening</a>  |

<div property="dct:description">

De Basisregistratie Grootschalige Topografie (BGT) is een gedetailleerde digitale kaart van Nederland. In de BGT worden objecten zoals gebouwen, wegen, water, spoorlijnen en groen op eenduidige manier vastgelegd.

Zie ook de [website](https://www.kadaster.nl/zakelijk/registraties/basisregistraties/bgt) van het kadaster.
</div>

</div> <!-- about #bgt-dataset-->

### WOZ Dataset 
<!-- bestaat niet bij aanbieder - wel een record van publisher KOOP -->

<div about="#woz-dataset" prefix="dct: http://purl.org/dc/terms/">

| Kenmerk                  | Waarde                                                                           |
|--------------------------|----------------------------------------------------------------------------------|
| Naam                     | <span property="dct:title">Basisregistratie Waardering Onroerende Zaken (WOZ)</span>     |
| Aanbieder                | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/zbo/zb000195">Dienst voor het kadaster en de openbare registers</a> (TOOI link)  |
|                          | <a rel="dct:creator" href="../#deelnemer">Dienst voor het kadaster en de openbare registers</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/zbo/zb000195">Dienst voor het kadaster en de openbare registers</a> (TOOI link)  |
|                          | <a rel="dct:publisher" href="../#deelnemer">Dienst voor het kadaster en de openbare registers</a> (FDS deelnemer link)  |
| Toegang                  | <a property="dct:accessRights" href="http://publications.europa.eu/resource/authority/access-right/NON_PUBLIC">Niet publiek</a>  |
| Thema                    | <a property='dcat:theme' href="http://standaarden.overheid.nl/owms/terms/Bestuur">Bestuur</a>  |

<div property="dct:description">
In de Basisregistratie WOZ zijn onder andere de WOZ-waarde, de kadastrale aanduiding, adresgegevens en de belanghebbende gegevens van de WOZ-objecten in Nederland opgenomen. 

Gemeenten bepalen de WOZ-waarde van huizen, winkels, kantoren en bouwpercelen. Deze WOZ-waarde heeft invloed op de hoogte van belastingen, zoals de onroerendezaakbelasting (OZB), de waterschapsbelasting en de inkomstenbelasting. 

De Basisregsitratie WOZ is niet openbaar. De WOZ-waarden van woningen zijn openbaar in te zien via het [WOZ-waardeloket](https://www.wozwaardeloket.nl/).
</div>

</div> <!-- about #woz-dataset-->

### BAG Dataset

<div about="#bag-dataset" prefix="dct: http://purl.org/dc/terms/">

| Kenmerk                  | Waarde                                                                           |
|--------------------------|----------------------------------------------------------------------------------|
| Naam                     | <span property="dct:title">Basisregistratie Adressen en gebouwen (BAG)</span>     |
| Aanbieder                | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/zbo/zb000195">Dienst voor het kadaster en de openbare registers</a> (TOOI link)  |
|                          | <a rel="dct:creator" href="../#deelnemer">Dienst voor het kadaster en de openbare registers</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/zbo/zb000195">Dienst voor het kadaster en de openbare registers</a> (TOOI link)  |
|                          | <a rel="dct:publisher" href="../#deelnemer">Dienst voor het kadaster en de openbare registers</a> (FDS deelnemer link)  |
| Toegang                  | <a property="dct:accessRights" href="http://publications.europa.eu/resource/authority/access-right/PUBLIC">Publiek</a>  |
| Thema                    | <a property='dcat:theme' href="http://standaarden.overheid.nl/owms/terms/Ruimtelijke_ordening">Ruimtelijke ordening</a>  |

<div property="dct:description">
De gegevens bestaan uit BAG-panden en een deelselectie van BAG-gegevens van deze panden en de zich daarin bevindende verblijfsobjecten. Ook de ligplaatsen en standplaatsen zijn hierin opgenomen met een deelselectie van BAG-gegevens.

De gegevens van de nummeraanduiding zijn in deze services onderdeel van de adresseerbare objecten, hierbij wordt slechts 1 adres opgenomen, dus objecten met meerdere adressen (hoofd- en nevenadressen) zijn niet compleet. In deze services zitten dus niet alle BAG adressen.

Aangezien niet alle BAG gegevens worden geleverd, adviseren wij u om de actuele gegevens in één van de BAG producten te controleren.
Raadpleeg de BAG Viewer voor enkele bevragingen van BAG gegevens.
Een overzicht van alle beschikbare producten kunt u vinden op de [website](https://www.kadaster.nl/zakelijk/registraties/basisregistraties/bag).

De BAG WMS, BAG WFS en BAG API Individuele bevragingen zijn expliciet niet bedoeld voor bulkbevragingen waarmee veel gegevens in één keer worden opgevraagd en in een database worden verwerkt. Wilt u in één keer meer gegevens opvragen, dan raden wij u aan om gebruik te maken van onze 
[BAG Geopackage](https://www.kadaster.nl/zakelijk/producten/adressen-en-gebouwen/bag-geopackage) of 
[BAG extract](https://www.kadaster.nl/zakelijk/producten/adressen-en-gebouwen/bag-2.0-extract).
De BAG Geopackage en de BAG extract kunt u downloaden via de [Atom downloadservice](https://service.pdok.nl/lv/bag/atom/bag.xml).

Deze dataset wordt ook gebruikt voor het ontsluiten van het INSPIRE thema Gebouwen. Het betreft gebouwcontouren, constructieve onderdelen van gebouwen en ruimtelijke barrieres. Dit betreft niet-geharmoniseerde data uit de basisregistratie Adressen en Gebouwen (BAG).

De service wordt dagelijks geactualiseerd.
</div>

</div> <!-- about #bag-dataset-->




--------------------------------------------

Deze pagina bevat linked data in [RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa) formaat, ook beschikbaar in [Turtle](index.ttl) en [JSONLD](index.jsonld). De linked data is opgezet volgens de [DCAT-AP-NL 3.0](https://docs.geostandaarden.nl/dcat/dcat-ap-nl30/) ontologie.
