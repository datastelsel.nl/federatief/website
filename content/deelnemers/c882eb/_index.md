---
title: Kadaster
date: 2024-12-11
description: >
  De deelnemerkenmerken van het Kadaster
---

<div about="#deelnemer" prefix="rdfs: http://www.w3.org/2000/01/rdf-schema# fds: http://federatief.datastelsel.nl/deelnemers/ontology/#">

| Kenmerk         | Waarde                                                                           |
|-----------------|----------------------------------------------------------------------------------|
| Label           | <span property="rdfs:label">Dienst voor het kadaster en de openbare registers</span>  |
| KvK Nummer      | <span property="fds:kvk">08215619</span>                                         |
| OIN             | <span property="fds:oin">00000003082156190000</span>                             |
| TOOI            | <a rel="fds:tooi" href="https://identifier.overheid.nl/tooi/id/zbo/zb000195">Dienst voor het kadaster en de openbare registers</a>  |
| Toegelaten door | <a rel="fds:admitted-by" href="../12416e/#deelnemer">Ministerie van Binnenlandse Zaken en Koninkrijksrelaties</a>  |
| FDS aanbod      | <a rel="fds:catalog" href="dcat/#cat">Het FDS aanbod van het Kadaster</a>        |

</div>

Deze pagina bevat linked data in [RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa) formaat, ook beschikbaar in [Turtle](index.ttl) en [JSONLD](index.jsonld). De linked data is opgezet volgens een CONCEPT [ontologie](../ontology/).

