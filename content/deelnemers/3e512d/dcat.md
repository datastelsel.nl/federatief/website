---
title: FDS Aanbod Rijksdienst voor Identiteitsgegevens
date: 2024-12-11
description: >
  Het FDS aanbod van de Rijksdienst voor Identiteitsgegevens
---
<div about="#cat" prefix="dct: http://purl.org/dc/terms/" >

## Het aanbod 
{{% pageinfo color="info" %}}
Binnen FDS is het de bedoeling dat deze informatie door de aanbieder zelf wordt gepubliceerd. Hierop vooruitlopend
publiceren we hier alvast een gedeelte om een indruk te geven hoe deze informatie eruit zou kunnen zien.
{{% /pageinfo %}}

| Kenmerk          | Waarde                                                                           |
|------------------|----------------------------------------------------------------------------------|
| Naam             | <span property="dct:title">Het FDS aanbod van de Rijksdienst voor Identiteitsgegevens</span>                |
| Aanbieder        | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/oorg/oorg10103">Rijksdienst voor Identiteitsgegevens</a> (TOOI link)  |
|                  | <a rel="dct:creator" href="../#deelnemer">Rijksdienst voor Identiteitsgegevens</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/oorg/oorg10103">Rijksdienst voor Identiteitsgegevens</a> (TOOI link)  |
|                  | <a rel="dct:publisher" href="../#deelnemer">Rijksdienst voor Identiteitsgegevens</a> (FDS deelnemer link)  |
| Wijzigingsdatum  | <span property="dct:modified" datatype="xsd:date">2024-12-11</span>              |
| Homepage         | <a rel="foaf:homepage" href=".">deze pagina</a>                               |

<div property='dcat:dataset' resource="#brp-dataset"></div>

</div> <!-- about #cat-->

## De dataset(s) 

### BRP Dataset <!-- Niet aanwezig van RvIG - wel record van KOOP -->

<div about="#brp-dataset" prefix="dct: http://purl.org/dc/terms/">

| Kenmerk                  | Waarde                                                                           |
|--------------------------|----------------------------------------------------------------------------------|
| Naam                     | <span property="dct:title">Basisregistratie Personen</span>     |
| Aanbieder                | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/oorg/oorg10103">Rijksdienst voor Identiteitsgegevens</a>  |
|                          | <a rel="dct:creator" href="../#deelnemer">Rijksdienst voor Identiteitsgegevens</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/oorg/oorg10103">Rijksdienst voor Identiteitsgegevens</a> (TOOI link)  |
|                          | <a rel="dct:publisher" href="../#deelnemer">Rijksdienst voor Identiteitsgegevens</a> (FDS deelnemer link)  |
| Toegang                  | <a property="dct:accessRights" href="http://publications.europa.eu/resource/authority/access-right/NON_PUBLIC">Niet Publiek</a> (TOOI link)  |
| Thema                    | <a property='dcat:theme' href="http://standaarden.overheid.nl/owms/terms/Bestuur">Bestuur</a>  |

<div property="dct:description">

De Nederlandse overheid registreert persoonsgegevens in de Basisregistratie Personen (BRP). Alle overheidsinstellingen en bestuursorganen zoals de Belastingdienst, zijn verplicht voor hun taken gebruik te maken van deze gegevens.

Hergebruik: De BRP is GEEN open data, zie ook de [site van rvig](https://www.rvig.nl/brp).
</div>

</div> <!-- about #brp-dataset-->


</div> <!-- prefixes-->

--------------------------------------------

Deze pagina bevat linked data in [RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa) formaat, ook beschikbaar in [Turtle](index.ttl) en [JSONLD](index.jsonld). De linked data is opgezet volgens de [DCAT-AP-NL 3.0](https://docs.geostandaarden.nl/dcat/dcat-ap-nl30/) ontologie.
