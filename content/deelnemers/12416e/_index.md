---
title: Ministerie van BZK
date: 2024-12-11
description: >
  De deelnemerkenmerken van het Ministerie van Binnenlandse Zaken en Koninkrijksrelaties
---

<div about="#deelnemer" prefix="rdfs: http://www.w3.org/2000/01/rdf-schema# fds: http://federatief.datastelsel.nl/deelnemers/ontology/#">

| Kenmerk         | Waarde                                                                           |
|-----------------|----------------------------------------------------------------------------------|
| Rol             | <a rel="rdf:type" href="fds:admin">Deelname administrateur</a>                   |
| Label           | <span property="rdfs:label">Ministerie van Binnenlandse Zaken en Koninkrijksrelaties</span>  |
| KvK Nummer      | <span property="fds:kvk">50200097</span>                                         |
| OIN             | <span property="fds:oin">00000003502000970000</span>                             |
| TOOI            | <a rel="fds:tooi" href="https://identifier.overheid.nl/tooi/id/ministerie/mnre1034">Ministerie van Binnenlandse Zaken en Koninkrijksrelaties</a>  |
| Toegelaten door | <a rel="fds:admitted-by" href="#deelnemer">Ministerie van Binnenlandse Zaken en Koninkrijksrelaties</a>  |

</div>

Deze pagina bevat linked data in [RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa) formaat, ook beschikbaar in [Turtle](index.ttl) en [JSONLD](index.jsonld). De linked data is opgezet volgens een CONCEPT [ontologie](../ontology/).
