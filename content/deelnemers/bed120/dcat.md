---
title: FDS Aanbod Ministerie van Financiën
date: 2024-12-11
description: >
  Het FDS aanbod van het Ministerie van Financiën
---
<div about="#cat" prefix="dct: http://purl.org/dc/terms/" >

## Het aanbod <!-- Belastingdienst is vestiging of organisatie-onderdeel van Ministerie van Financiën volgens HR en ROO -->
{{% pageinfo color="info" %}}
Binnen FDS is het de bedoeling dat deze informatie door de aanbieder zelf wordt gepubliceerd. Hierop vooruitlopend
publiceren we hier alvast een gedeelte om een indruk te geven hoe deze informatie eruit zou kunnen zien.
{{% /pageinfo %}}

| Kenmerk          | Waarde                                                                           |
|------------------|----------------------------------------------------------------------------------|
| Naam             | <span property="dct:title">Het FDS aanbod van het Ministerie van Financiën</span>  |
| Aanbieder        | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/ministerie/mnre1090">Ministerie van Financiën</a> (TOOI link)  |
|                  | <a rel="dct:creator" href="../#deelnemer">Ministerie van Financiën</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/ministerie/mnre1090">Ministerie van Financiën</a> (TOOI link)  |
|                  | <a rel="dct:publisher" href="../#deelnemer">Ministerie van Financiën</a> (FDS deelnemer link)  |
| Wijzigingsdatum  | <span property="dct:modified" datatype="xsd:date">2024-12-11</span>              |
| Homepage         | <a rel="foaf:homepage" href=".">deze pagina</a>                                  |

<div property='dcat:dataset' resource="#bri-dataset"></div>

</div> <!-- about #cat-->

## De dataset(s) 

### BRI Dataset <!-- Niet aanwezig van aanbieder - wel record van KOOP -->

<div about="#bri-dataset" prefix="dct: http://purl.org/dc/terms/">

| Kenmerk                  | Waarde                                                                           |
|--------------------------|----------------------------------------------------------------------------------|
| Naam                     | <span property="dct:title">Basisregistratie Inkomen</span>                       |
| Aanbieder                | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/ministerie/mnre1090">Ministerie van Financien</a> (TOOI link)  |
|                          | <a rel="dct:creator" href="../#deelnemer">Ministerie van Financiën</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/ministerie/mnre1090">Ministerie van Financien</a> (TOOI link)  |
|                          | <a rel="dct:publisher" href="../#deelnemer">Ministerie van Financiën</a> (FDS deelnemer link)  |
| Toegang                  | <a property="dct:accessRights" href="http://publications.europa.eu/resource/authority/access-right/NON_PUBLIC">Niet Publiek</a>  |
| Thema                    | <a property='dcat:theme' href="http://standaarden.overheid.nl/owms/terms/Bestuur">Bestuur</a>  |

<div property="dct:description">

De basisregistratie inkomen (BRI) is een basisregistratie die andere overheidsorganisaties kunnen raadplegen (mits daarvoor een grondslag is) voor de hoogte van uw inkomen. Deze organisaties gebruiken het inkomen uit de BRI om bijvoorbeeld de hoogte van uw toeslag, eigen bijdrage of toelage te berekenen.

Zie ook [informatie op de site van de belastingdienst](https://www.belastingdienst.nl/wps/wcm/connect/bldcontentnl/belastingdienst/prive/werk_en_inkomen/geregistreerde_inkomen_en_de_inkomensverklaring/alles_over_geregistreerde_inkomen/alles-over-het-geregistreerde-inkomen#)
</div>

</div> <!-- about #bri-dataset-->


</div> <!-- prefixes-->

--------------------------------------------

Deze pagina bevat linked data in [RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa) formaat, ook beschikbaar in [Turtle](index.ttl) en [JSONLD](index.jsonld). De linked data is opgezet volgens de [DCAT-AP-NL 3.0](https://docs.geostandaarden.nl/dcat/dcat-ap-nl30/) ontologie.
