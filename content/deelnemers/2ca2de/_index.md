---
title: Het Waterschapshuis
date: 2024-12-11
description: >
  De deelnemerkenmerken van Het Waterschapshuis
---

<div about="#deelnemer" prefix="rdfs: http://www.w3.org/2000/01/rdf-schema# fds: http://federatief.datastelsel.nl/deelnemers/ontology/#">

| Kenmerk         | Waarde                                                                           |
|-----------------|----------------------------------------------------------------------------------|
| Label           | <span property="rdfs:label">Het Waterschapshuis</span>  |
| KvK Nummer      | <span property="fds:kvk">56683464</span>                                         |
| OIN             | <span property="fds:oin">00000003566834640000</span>                             |
| TOOI            | <a rel="fds:tooi" href="https://identifier.overheid.nl/tooi/id/so/so0652">  Het Waterschapshuis</a>  |
| Toegelaten door | <a rel="fds:admitted-by" href="../12416e/#deelnemer">Ministerie van Binnenlandse Zaken en Koninkrijksrelaties</a>  |
| FDS aanbod      | <a rel="fds:catalog" href="dcat/#cat">Het FDS aanbod van het Waterschapshuis</a>        |

</div>

Deze pagina bevat linked data in [RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa) formaat, ook beschikbaar in [Turtle](index.ttl) en [JSONLD](index.jsonld). De linked data is opgezet volgens een CONCEPT [ontologie](../ontology/).

