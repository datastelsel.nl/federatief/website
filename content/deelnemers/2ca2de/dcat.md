---
title: Aanbod van Het Waterschapshuis
date: 2024-12-11
description: >
  Het FDS aanbod van Het Waterschapshuis
---
<div about="#cat" prefix="dct: http://purl.org/dc/terms/" >

## Het aanbod
{{% pageinfo color="info" %}}
Binnen FDS is het de bedoeling dat deze informatie door de aanbieder zelf wordt gepubliceerd. Hierop vooruitlopend
publiceren we hier alvast een gedeelte om een indruk te geven hoe deze informatie eruit zou kunnen zien.
{{% /pageinfo %}}

| Kenmerk          | Waarde                                                                           |
|------------------|----------------------------------------------------------------------------------|
| Naam             | <span property="dct:title">Het FDS aanbod van Het Waterschapshuis</span>         |
| Aanbieder        | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/so/so0652">Het Waterschapshuis</a> (TOOI link)  |
|                  | <a rel="dct:creator" href="../#deelnemer">Het Waterschapshuis</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/so/so0652">Het Waterschapshuis</a> (TOOI link)  |
|                  | <a rel="dct:publisher" href="../#deelnemer">Het Waterschapshuis</a> (FDS deelnemer link)  |
| Wijzigingsdatum  | <span property="dct:modified" datatype="xsd:date">2024-12-11</span>              |
| Homepage         | <a rel="foaf:homepage" href=".">deze pagina</a>                                  |

<div property='dcat:dataset' resource="#keringen-dataset"></div>
<div property='dcat:dataset' resource="#kunstwerken-dataset"></div>
<div property='dcat:dataset' resource="#oppervlaktewateren-dataset"></div>
<div property='dcat:dataset' resource="#waterbeheergebieden-dataset"></div>
<div property='dcat:dataset' resource="#zoneringen-dataset"></div>
<div property='dcat:dataset' resource="#waterschapsgrenzen-dataset"></div>

</div> <!-- about #cat-->

## De dataset(s) 

### Keringen Dataset

<div about="#keringen-dataset" prefix="dct: http://purl.org/dc/terms/">

| Kenmerk                  | Waarde                                                                           |
|--------------------------|----------------------------------------------------------------------------------|
| Naam                     | <span property="dct:title">Waterschappen Keringen IMWA</span>                    |
| Aanbieder                | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/so/so0652">Het Waterschapshuis</a> (TOOI link)  |
|                          | <a rel="dct:creator" href="../#deelnemer">Het Waterschapshuis</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/so/so0652">Het Waterschapshuis</a> (TOOI link)  |
|                          | <a rel="dct:publisher" href="../#deelnemer">Het Waterschapshuis</a> (FDS deelnemer link)  |
| Toegang                  | <a rel="dct:accessRights" href="http://publications.europa.eu/resource/authority/access-right/PUBLIC">Publiek</a>  |
| Thema                    | <a rel='dcat:theme' href="http://standaarden.overheid.nl/owms/terms/Water">Water</a>  |

<div property="dct:description">

Zie [informatie op data.overheid.nl](https://data.overheid.nl/dataset/bf21c922-cc78-4342-81a9-df1f488626bc).
</div>

</div> <!-- about #keringen-dataset-->


### Kunstwerken Dataset

<div about="#kunstwerken-dataset" prefix="dct: http://purl.org/dc/terms/">

| Kenmerk                  | Waarde                                                                           |
|--------------------------|----------------------------------------------------------------------------------|
| Naam                     | <span property="dct:title">Waterschappen Kunstwerken IMWA</span>     |
| Aanbieder                | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/so/so0652">Het Waterschapshuis</a> (TOOI link)  |
|                          | <a rel="dct:creator" href="../#deelnemer">Het Waterschapshuis</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/so/so0652">Het Waterschapshuis</a> (TOOI link)  |
|                          | <a rel="dct:publisher" href="../#deelnemer">Het Waterschapshuis</a> (FDS deelnemer link)  |
| Toegang                  | <a property="dct:accessRights" href="http://publications.europa.eu/resource/authority/access-right/PUBLIC">Publiek</a>  |
| Thema                    | <a property='dcat:theme' href="http://standaarden.overheid.nl/owms/terms/Water">Water</a>  |

<div property="dct:description">

Zie [informatie op data.overheid.nl](https://data.overheid.nl/dataset/2f88246a-b0aa-4945-b8f4-34810376ff45).
</div>

</div> <!-- about #kunstwerken-dataset-->

### Oppervlaktewateren Dataset

<div about="#oppervlaktewateren-dataset" prefix="dct: http://purl.org/dc/terms/">

| Kenmerk                  | Waarde                                                                           |
|--------------------------|----------------------------------------------------------------------------------|
| Naam                     | <span property="dct:title">Waterschappen Oppervlaktewateren IMWA</span>     |
| Aanbieder                | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/so/so0652">Het Waterschapshuis</a> (TOOI link)  |
|                          | <a rel="dct:creator" href="../#deelnemer">Het Waterschapshuis</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/so/so0652">Het Waterschapshuis</a> (TOOI link)  |
|                          | <a rel="dct:publisher" href="../#deelnemer">Het Waterschapshuis</a> (FDS deelnemer link)  |
| Toegang                  | <a property="dct:accessRights" href="http://publications.europa.eu/resource/authority/access-right/PUBLIC">Publiek</a>  |
| Thema                    | <a property='dcat:theme' href="http://standaarden.overheid.nl/owms/terms/Water">Water</a>  |

<div property="dct:description">

Zie [informatie op data.overheid.nl](https://data.overheid.nl/dataset/5b493ca6-7b83-4ebd-8049-64127aee9e95).
</div>

</div> <!-- about #oppervlaktewateren-dataset-->

### Waterbeheergebieden Dataset

<div about="#waterbeheergebieden-dataset" prefix="dct: http://purl.org/dc/terms/">

| Kenmerk                  | Waarde                                                                           |
|--------------------------|----------------------------------------------------------------------------------|
| Naam                     | <span property="dct:title">Waterschappen Waterbeheergebieden IMWA</span>     |
| Aanbieder                | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/so/so0652">Het Waterschapshuis</a> (TOOI link)  |
|                          | <a rel="dct:creator" href="../#deelnemer">Het Waterschapshuis</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/so/so0652">Het Waterschapshuis</a> (TOOI link)  |
|                          | <a rel="dct:publisher" href="../#deelnemer">Het Waterschapshuis</a> (FDS deelnemer link)  |
| Toegang                  | <a property="dct:accessRights" href="http://publications.europa.eu/resource/authority/access-right/PUBLIC">Publiek</a>  |
| Thema                    | <a property='dcat:theme' href="http://standaarden.overheid.nl/owms/terms/Water">Water</a>  |

<div property="dct:description">

Zie [informatie op data.overheid.nl](https://data.overheid.nl/dataset/84f90c60-a32e-4841-ad86-dc139ef31818)
</div>

</div> <!-- about #waterbeheergebieden-dataset-->

### Waterschappen Waterstaatskundige zoneringen waterstaatswerk IMWA Dataset

<div about="#zoneringen-dataset" prefix="dct: http://purl.org/dc/terms/">

| Kenmerk                  | Waarde                                                                           |
|--------------------------|----------------------------------------------------------------------------------|
| Naam                     | <span property="dct:title">Waterschappen Waterstaatskundige zoneringen waterstaatswerk IMWA</span>     |
| Aanbieder                | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/so/so0652">Het Waterschapshuis</a> (TOOI link)  |
|                          | <a rel="dct:creator" href="../#deelnemer">Het Waterschapshuis</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/so/so0652">Het Waterschapshuis</a> (TOOI link)  |
|                          | <a rel="dct:publisher" href="../#deelnemer">Het Waterschapshuis</a> (FDS deelnemer link)  |
| Toegang                  | <a property="dct:accessRights" href="http://publications.europa.eu/resource/authority/access-right/PUBLIC">Publiek</a>  |
| Thema                    | <a property='dcat:theme' href="http://standaarden.overheid.nl/owms/terms/Water">Water</a>  |

<div property="dct:description">

Zie [informatie op data.overheid.nl](https://data.overheid.nl/dataset/7894d051-63f5-4705-87f1-24166da761fd).
</div>

</div> <!-- about #zoneringen-dataset-->

### Waterschapsgrenzen Dataset

<div about="#waterschapsgrenzen-dataset" prefix="dct: http://purl.org/dc/terms/">

| Kenmerk                  | Waarde                                                                           |
|--------------------------|----------------------------------------------------------------------------------|
| Naam                     | <span property="dct:title">Waterschappen Waterschapsgrenzen IMSO</span>     |
| Aanbieder                | <a rel="dct:creator" href="https://identifier.overheid.nl/tooi/id/so/so0652">Het Waterschapshuis</a> (TOOI link)  |
|                          | <a rel="dct:creator" href="../#deelnemer">Het Waterschapshuis</a> (FDS deelnemer link)  |
| Publicerende organisatie | <a rel="dct:publisher" href="https://identifier.overheid.nl/tooi/id/so/so0652">Het Waterschapshuis</a> (TOOI link)  |
|                          | <a rel="dct:publisher" href="../#deelnemer">Het Waterschapshuis</a> (FDS deelnemer link)  |
| Toegang                  | <a property="dct:accessRights" href="http://publications.europa.eu/resource/authority/access-right/PUBLIC">Publiek</a>  |
| Thema                    | <a property='dcat:theme' href="http://standaarden.overheid.nl/owms/terms/Bestuur">Bestuur</a>  |

<div property="dct:description">

Zie [informatie op data.overheid.nl](https://data.overheid.nl/dataset/98544750-60f6-42b2-b25e-9d7264eb3442).
</div>

</div> <!-- about #zwaterschapsgrenzen-dataset-->

</div> <!-- prefixes-->

--------------------------------------------

Deze pagina bevat linked data in [RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa) formaat, ook beschikbaar in [Turtle](index.ttl) en [JSONLD](index.jsonld). De linked data is opgezet volgens de [DCAT-AP-NL 3.0](https://docs.geostandaarden.nl/dcat/dcat-ap-nl30/) ontologie.
