---
title: Community
menu: {main: {weight: 40}}
---

{{% blocks/lead color="primary" %}}
federatief.datastelsel.nl is een open source project welke iedereen kan gebruiken, verbeteren en van
kan genieten. Dit is een voorwaarde voor een actieve community. Daarom zijn er meerdere manieren voor jou om actief betrokken te worden! 
{{% /blocks/lead %}}