---
title: Puzzels
menu: {main: {weight: 30}}
date: 2024-09-06
author: Kernteam FDS, Digilab
status: Concept
version: v0.1
description: >
  Puzzels die opgelost worden om tot een federatief datastelsel te komen.
---

Om te komen tot een federatief datastelsel moeten er heel wat puzzels opgelost worden. Hieronder
staan de puzzels die nu in beeld zijn. De indeling van deze puzzels volgen precies de indeling van
de [FDS (technische) stelselfuncties](/kennisbank/stelselfuncties/#technische-stelselfuncties). Op
deze wijze zijn de benodigde puzzels geplot op de capabilities van het FDS en zien we direct de
verbinding! Daarnaast worden er projecten en onderzoeken gedaan in
[Digilab](https://digilab.overheid.nl) om tot oplossingen te komen van deze puzzels die direct
bijdragen aan de verdere ontwikkeling van het FDS.

De puzzels die je hieronder kunt vinden zijn 'in onderhoud', dit betekent dat ze komende periode
inhoudelijk verder worden uitgewerkt. Dit doen we samen met de inhoudelijk eigenaren van de puzzels,
dit zijn organisaties en experts actief binnen de Digitale Overheid. Op die wijze komt de content gezamelijk
tot stand en faciliteren wij met dit platform de borging ervan.