--- 
title: Doel en aanpak van FDS
weight: 2
status: Concept
date: 2025-03-06
author: Kernteam FDS
description: >
   Achtergrond van FDS, vertaald naar ambitie van FDS. Verder een korte schets 
   van de beoogde werking van het FDS.
---
Het Federatief Datastelsel is geënt op de ‘Interbestuurlijke datastrategie‘,
d.d. oktober 2021 en op het ‘Toekomstbeeld Basisregistraties’, d.d. 16 december
2021.

## Interbestuurlijke datastrategie

Het belangrijke beleidsdoel van de interbestuurlijke datastrategie luidt als
volgt:

> ‘Als overheid benutten we het volle potentieel van data bij maatschappelijke
> opgaven, zowel binnen als tussen domeinen, op een ambitieuze manier die
> vertrouwen wekt.’

Bron: [Meerjarenaanpak Interbestuurlijke Datastrategie](
   https://realisatieibds.nl/files/view/1c9729a6-70a9-4bda-a245-49c1d6d63adb/meerjarenaanpak-interbestuurlijke-datastrategie6maart2023.pdf).

Een met maatschappelijke opgaven meegroeiend Federatief Datastelsel is een
belangrijk instrument voor het realiseren van deze beleidsdoelstelling, zoals
ook wordt weergegeven met de bijgaande afbeelding uit het rapport
‘Interbestuurlijke datastrategie’.

| ![Meegroeiend Federatief Datastelsel](images/groeiend-stelsel.png) |
|:------------------------------------------------------------------:|
|                 Meegroeiend Federatief Datastelsel                 |

## Toekomstbeeld basisregistraties

Belangrijke kernpunten uit het toekomstbeeld basisregistraties zijn onder
andere:
- FDS maakt hoogwaardige data binnen de overheid – over organisatie- en
  domeingrenzen heen – beschikbaar om:
   - dienstverlening aan burgers en bedrijven te verbeteren en 
   - sneller en beter in te spelen op maatschappelijke opgaven.
   - Op basis van formele afspraken werken deelnemers van FDS samen om
     hoogwaardige gegevens voor meervoudig gebruik beschikbaar te stellen.
- FDS stelt de volgende principes centraal:
   - “decentraal als kan, centraal als moet” en
   - “afspraken gaan boven standaarden, standaarden gaan boven voorzieningen”.
   - FDS versterkt de positie van burgers en bedrijven door het verankeren van
     principes als privacy, transparantie en accountability en
     beleidsuitgangspunten als het voeren van regie op de eigen gegevens.
- FDS stelselafspraken worden transparant ontwikkeld en vastgelegd en zijn
  daardoor toetsbaar. Dit bevordert het maatschappelijk draagvlak voor het
  gebruik van stelseldata.
- FDS ondersteunt verantwoord meervoudig gebruik van data. Dit kan met
  afspraken, standaarden en technische- en organisatorische functies worden
  ingevuld. Daarbij zijn nog implementatiekeuzes mogelijk.
- Het toekomstbeeld onderscheidt verschillende categorieën van data. Dat geeft
  richting aan de verdere ontwikkeling en positionering van basisregistraties en
  andere registraties zoals domein- of ketenregisters. Classificatie van soorten
  registers bepaalt welk niveau van stelselafspraken van toepassing moet zijn.
  Dit voorkomt dat alle stelseldata aan de strengste eisen moeten voldoen en
  houdt zo de ontwikkeling van FDS beheersbaar en betaalbaar.

## Ambitie Federatief Datastelsel

De uitdaging voor FDS is om eenvoudig en vertrouwd delen van data door de
overheid mogelijk te maken.

**Daarmee wil FDS bereiken dat:**
- De samenleving, burger en ambtenaar vertrouwen hebben in datadeling binnen het
  Federatief Datastelsel; het is verantwoord, met aandacht voor publieke waarden
  en aansluitend op de ontwikkelingen in de EU.
- De aanpak van maatschappelijke vraagstukken is effectiever doordat meer
  hoogwaardige data voor meervoudig gebruik beschikbaar zijn.

**Dit zal resulteren in:**
- (Ten opzichte van het huidige Stelsel van Basisregistraties.) Een federatie
  met meer datahouders, rijker aanbod van stelseldata, meer
  gebruikers(organisaties) en intensief gebruik van stelseldata, conformerend
  aan de aansluitvoorwaarden.
- Samenhangende stelselfuncties die zorgen voor de sturing op en de beoogde
  werking van het Federatief Datastelsel als vertrouwenssysteem van datagebruik.
- Structurele (door)ontwikkeling van het Federatief Datastelsel via
  (maatschappelijke) use cases en een innovatiewerkplaats voor experimenteren en
  beproeven.
- Structurele governance en financiering voor beheer en doorontwikkeling van het
  stelsel.
- Overzicht en inzicht in beschikbare data door een flinke uitbreiding van
  ontsloten open en beschermde data via datacatalogi.

Samenvattend is het de ambitie van FDS om Nederlandse overheidsorganisaties op
eenvoudige en verantwoorde wijze hun data met elkaar te laten delen ten bate van
maatschappelijke vraagstukken en publieke dienstverlening. Zodat de Nederlandse
overheid meer maatschappelijke waarde kan genereren uit de (data)middelen
waarover ze op grond van haar wettelijke taken beschikt.

## Hoe het FDS gaat werken

‘Het Federatief Datastelsel’ is een afsprakenstelsel in opbouw. Het project
Realisatie Federatief Datastelsel ontwikkelt functies om aanbieders en afnemers
van data uit alle beleidsdomeinen op een juridisch en ethisch verantwoorde
manier data te laten delen. De functies zijn ingebed in een afsprakenstelsel dat
waar nodig is verankerd in wetgeving. Het afsprakenstelsel kan verder worden
aangevuld met concrete standaarden en voorzieningen. Data, data-aanbieders,
data-afnemers en en datadiensten in het stelsel, kunnen zo worden voorzien van
een ‘FDS-label’ dat duidelijk maakt dat zij voldoen aan het FDS-afsprakenkader
(waaronder bijvoorbeeld FDS aansluitvoorwaarden). Stelselfuncties zoals
‘toezicht houden’ zorgen ervoor dat deelnemers structureel conformeren, ook na
eventuele wijzigingen binnen het stelsel. Dit alles levert een vertrouwensbasis
waarbinnen deelnemers aan het stelsel verantwoord data kunnen delen met andere
deelnemers.

Om data te delen maken stelseldeelnemers in de rol van data-aanbieder deze data
binnen het stelsel beschikbaar. Dat gebeurt door het leveren van datadiensten.
De data-aanbieder publiceert de metadata over de data en diensten die hij
beschikbaar stelt, zodat een (potentiële) data-afnemer kan bepalen of dit voor
hem bruikbaar is.

Voor het feitelijke datadelen zorgen de deelnemers zelf dat ze kunnen beschikken
over de benodigde infrastructurele en functionele voorzieningen. Deze
voorzieningen zijn zodanig ontwikkeld en ingericht dat ze conformeren aan de
relevante stelselafspraken en -standaarden. Gebruik van eventuele generieke
FDS-voorzieningen is geen verplichting, tenzij dat voor specifieke situaties
binnen het stelsel is afgesproken en als zodanig vastgelegd in
FDS-afsprakenkader.
