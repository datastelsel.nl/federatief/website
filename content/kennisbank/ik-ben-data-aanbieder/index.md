---
title: Ik ben FDS data-aanbieder
date: 2025-01-16
author: Logius
status: concept
description: >
  Als jij als data-aanbieder wilt deelnemen aan FDS doorloop je het 
  stappenplan [deelnemen FDS](https://realisatieibds.nl/groups/view/0056c9ef-5c2e-44f9-a998-e735f1e9ccaa/de-ontwikkeling-van-het-federatief-datastelsel/wiki/view/603a1a5f-b5fe-46d3-9f4d-d2b2f988840e/hoe-kun-je-deelnemen-aan-het-fds). 
  Als je als deelnemer bent toegetreden, ga je je data publiceren. Welke data op welke plaatsen wordt gepubliceerd, lees je hierna. 
---

Als je als deelnemer bent toegetreden, ga je je data publiceren. Welke data op welke plaatsen wordt gepubliceerd, lees je hierna. 
Er kunnen
  [informatiemodellen](/kennisbank/ik-ben-data-aanbieder/#informatiemodellen),
  [begrippenkaders](/kennisbank/ik-ben-data-aanbieder/#begrippenkaders),
  [Datasets & Dataservices](#datasets--dataservices) of
  [API`s & Repositories](/kennisbank/ik-ben-data-aanbieder/#apis--repositories)
  worden aangeboden via verschillende aanleverprocessen.



## Informatiemodellen

Informatiemodellen en gegevensbeschrijvingen worden ontsloten op de
[Stelselcatalogus](https://www.stelselcatalogus.nl).

### Voor wie is deze voorziening?

- Basisregistraties, zoals bijvoorbeeld de Basisregistratie Personen (BRP)
- Sectorregistraties, zoals bijvoorbeeld de Registatie Overheidsorganisaties (ROO)

### Wil jij informatiemodellen aanbieden?

Neem dan [contact](https://www.stelselcatalogus.nl/contact) op met de Stelselcatalogus.

### In welk formaat kunt u uw informatiemodellen aanleveren?

Informatiemodellen worden volgens semantische standaarden gepubliceerd, dus als linked data. Zie ook
[[Metadata]]().

Het heeft de voorkeur dat de aanbieder de metadata publiceert volgens deze standaarden. U kunt uw metadata ook aan ons aanleveren in Excel en dan transformeren wij dat naar linked data.

### Hoe wordt uw data gepubliceerd?

**Vraag: En als ze het zelf doen?**

Samen met experts van Stelselcatalogus vult u uw informatiemodel in. Dit model wordt omgezet in
linked data en gepubliceerd op de [stelselcatalogus.nl](https://www.stelselcatalogus.nl).

### Voorbeelden

- [www.stelselcatalogus.nl](https://www.stelselcatalogus.nl)
- [Stelselcatalogus.nl | Basisregistraties Adressen en Gebouwen](https://www.stelselcatalogus.nl/registraties/registratie?id=http://opendata.stelselcatalogus.nl/id/registratie/BAG)
- [Stelselcatalogus.nl | Register van Overheidsorganisaties](https://www.stelselcatalogus.nl/registraties/registratie?id=http://opendata.stelselcatalogus.nl/id/registratie/ROO)

### Contactgegevens van de voorziening

Je kunt contact opnemen met Stelselcatalogus via [logiuscontactstelselcatalogus@logius.nl](mailto:logiuscontactstelselcatalogus@logius.nl).

### Wat je nodig om je data aan te kunnen bieden?

Om je data aan te bieden heb je een beschrijving nodig van jouw informatiemodel. In overleg met de
experts van Stelselcatalogus nemen we het informatiemodel op in een standaard Excel. We helpen je
graag met het beschrijven van jouw informatiemodel. Neem hiervoor contact op met de voorziening (zie
[contactgegevens](#contactgegevens-van-de-voorziening))

## Begrippenkaders

### Voor wie is deze voorziening?

Deze voorziening is voor elke organisatie die een begrippenkader wil aanmaken of aanbieden.

### Wil jij een begrippenkader aanmaken?

Gebruik daarvoor onze [online editor](https://editor.stelselcatalogus.nl/). Vraag een account aan voor de online editor via [logiuscontactstelselcatalogus@logius.nl](mailto:logiuscontactstelselcatalogus@logius.nl).

### In welk formaat kan je jouw begrippenkaders maken?

Begrippenkader worden opgesteld conform de Standaard voor beschrijving van Begrippen: de
[NL-SBB](https://docs.geostandaarden.nl/nl-sbb/nl-sbb/)

### Hoe worden begrippenkaders aangemaakt?

Begrippenkaders worden aangemaakt met behulp van een online editor of kunnen als Linked Data worden
aangeboden. Hiervoor heb je een account nodig. Je kan een account aanvragen door te mailen naar
[logiuscontactstelselcatalogus@logius.nl](mailto:logiuscontactstelselcatalogus@logius.nl). Als je al beschikt over een SBB begrippenkader in Linked
Data kan je dat direct aanbieden. Als je nog geen NL-SBB begrippenkader hebt kun je dat opstellen
met behulp van onze online editor. Door middel van het invullen van velden in een webapplicatie kan
jij een begrippenkader aanmaken conform de NL-SBB standaard en wordt jouw kader automatisch
gegenereerd. Begrippenkaders worden na jouw goedkeuring gepubliceerd bij Logius. Begrippenkaders kan
je natuurlijk ook intern gebruiken.

### Wil jij begrippenkaders inzien?

Gepubliceerde begrippenkaders vind je [hier](https://begrippen.stelselcatalogus.nl/sbb/begrippenkader/list).

### Contactgegevens van de voorziening

Je kunt contact opnemen met de voorziening via [logiuscontactstelselcatalogus@logius.nl](mailto:logiuscontactstelselcatalogus@logius.nl).

### Wat heb je nodig om je data aan te bieden?

Om jouw bestaande SBB begrippenkader aan te kunnen bieden neem je contact op met
[logiuscontactstelselcatalogus@logius.nl](mailto:logiuscontactstelselcatalogus@logius.nl). Als je nog geen bestaand SBB begrippenkader hebt kan je
gebruik maken van de [online editor](https://editor.stelselcatalogus.nl/). Vraag een account aan voor de online editor via [logiuscontactstelselcatalogus@logius.nl](mailto:logiuscontactstelselcatalogus@logius.nl).

## Datasets & Dataservices

Datasets worden ontsloten op [www.data.overheid.nl](https://www.data.overheid.nl).

### Voor wie is deze voorziening?

Deze voorziening is voor data-aanbieders, bijvoorbeeld het CBS of het Kadaster.

### Wil jij jouw overheidsdata aanbieden?

Ga naar [data.overheid.nl | Dataset publiceren](https://data.overheid.nl/gebruiker/login?destination=/dataset/dataset-publiceren)

### Hoe wordt de data aangeboden?

Data kan handmatig of geautomatiseerd worden aangeboden. Ga naar
<https://data.overheid.nl/ondersteuning-donl> voor meer
informatie.

### Voorbeelden

Voorbeelden zijn op [data.overheid.nl/datasets](https://data.overheid.nl/datasets) te vinden.

### Contactgegevens van de voorziening

De contactgegevens vind je op [data.overheid.nl/ondersteuning/algemeen/contact](https://data.overheid.nl/ondersteuning/algemeen/contact).

### Wat heb je nodig om je data aan te bieden?

Afhankelijk van de grootte van jouw datasets en jouw organisatie is enige mate van technische kennis
nodig. Indien nodig helpen wij je graag met het aanbieden van jouw data. Ga hiervoor naar
[data.overheid.nl/datasets](https://data.overheid.nl/datasets)

## API's & Repositories

API's en repositories worden ontsloten op [developer.overheid.nl](https://developer.overheid.nl)

### Voor wie is deze voorziening?

Deze voorziening is voor developers die met of bij de overheid werken.

### Wil jij API's aanbieden?

Ga naar [developer.overheid.nl/apis](https://developer.overheid.nl/apis)

### Formaat Metadata

API's en Git-repositories bied je aan door het doorgeven van een webadres.

### Hoe wordt de data aangeboden?

De API's en GIT-repositories worden aangeboden door middel van het invullen van een online
formulier op [developer.overheid.nl](https://developer.overheid.nl)

### Voorbeelden

Voorbeelden zijn op [developer.overheid.nl/apis](https://developer.overheid.nl/apis) te vinden.

### Contactgegevens van de voorziening

De contactgegevens zijn op [developer.overheid.nl/contact](https://developer.overheid.nl/contact) te vinden.

### Wat heb je nodig om je data hier aan te bieden?

Je hebt een eigen API of GIT-repository nodig om aan te bieden.
