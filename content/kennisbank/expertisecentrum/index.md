---
title: Expertisecentrum
status: Voorstel
date: 2024-04-30
author: Kernteam FDS
version: v0.23
description: >
    Stelselfunctie voor het onderhouden van de stelselarchitectuur en het faciliteren van de 
    communities waarin kennis en ervaring over het stelsel wordt gedeeld. Het kennisnetwerk.
---

### Expertisecentrum

De organisatorische [stelselfunctie](../stelselfuncties/) Expertisecentrum:
- Onderhoudt stelsel-architectuur.
- Faciliteert community's waarin kennis en ervaring over het stelsel wordt gedeeld.
- Kennisnetwerk.

#### Status

- Ontwikkelingen tijdens het programma Realisatie IBDS geven inzicht in de manier waarop het
  Expertisecentrum structureel moet worden ingericht.
- Zodra de tijd rijp is, wordt een inrichtingsvoorstel opgesteld en ter goedkeuring voorgelegd via
  de Stelsel-governance.

#### Achtergrond

{{% TODO /%}}

#### FDS context & positionering

{{% TODO /%}}

#### Inhoudelijke verdieping

{{% TODO /%}}

#### Standaardisatie

{{% TODO /%}}
