---
title: Processen voor deelname aan het FDS als organisatie
date: 2024-07-17
author: Kernteam FDS
tags: []
description: |
  De operationele processen om de deelnemers van het FDS te administreren
---
{{< capabilities-diagram selected="operationeel" >}}

## Deelname aan het FDS als organisatie

De administratie van de deelnemers aan het FDS als organisatie wordt ondersteund door de
operationele deelnameprocessen:

- Toelaten tot het FDS als deelnemer
- Wijzigen of corrigeren van deelnemerkenmerken
- Beëindigen of intrekken van deelname

De deelnameprocessen borgen de beschikbaarheid van een actuele en vertrouwde lijst van deelnemers.
De uitvoering van de deelnameprocessen is de verantwoordelijkheid van een deelname-administrateur,
een deelfunctie van de stelselfunctie van [[Regisseur|regisseur]]().

Wil je deelnemen aan het FDS als organisatie? Lees dan
[hier](https://realisatieibds.nl/groups/view/0056c9ef-5c2e-44f9-a998-e735f1e9ccaa/de-ontwikkeling-van-het-federatief-datastelsel/wiki/view/603a1a5f-b5fe-46d3-9f4d-d2b2f988840e/hoe-kun-je-deelnemen-aan-het-fds)
hoe je kan deelnemen!

## Toelaten tot het FDS als deelnemer

Voordat een organisatie de rol van aanbieder of afnemer kan aannemen binnen het FDS,
dient een organisatie deelnemer te worden van het FDS. Om deel te kunnen nemen dient
een organisatie te voldoen aan de deelnamecriteria.

De deelnamecriteria zijn:

- de organisatie heeft een publieke taak. Zie ook [Identiteiten binnen het
  FDS](../identiteit/#Identiteit_binnen_het_FDS)
- de organisatie onderschrijft de uitgangspunten van het afsprakenstelsel in de
  [intentieverklaring](https://realisatieibds.nl/groups/view/0056c9ef-5c2e-44f9-a998-e735f1e9ccaa/federatief-datastelsel/wiki/view/603a1a5f-b5fe-46d3-9f4d-d2b2f988840e/hoe-kun-je-deelnemen-aan-het-fds)
  en volgt de afspraken die daaruit voortvloeien.

De deelnamecriteria worden getoetst door een deelname-administrateur. Bij toelating
wordt een deelnemer met zijn deelnemerkenmerken opgenomen op de lijst met deelnemers.
De lijst met deelnemers en de deelnemerkenmerken zijn beschikbaar als
[[Metadata]](). Zowel aanbieders als afnemers kunnen de
metadata raadplegen om te verifiëren of de andere partij deelnemer is.

Nadat een aanbieder is toegelaten als deelnemer dient de aanbieder het proces te doorlopen
om een dataset als aanbod in te brengen.

Nadat een afnemer is toegelaten kan de afnemer een een verzoek tot datadelen indienen bij
een aanbieder. De aanbieder kan in de deelnemerslijst nagaan of de afnemer een vertrouwde
partij is.

Een aantal aspecten zijn nog onderwerp van onderzoek:

- Eventuele aanvullende deelnamecriteria.
- Het hanteren van één of van verschillende niveau's van vertrouwen. Meerdere
  niveaus van vertrouwen vergt criteria om een niveau op te baseren. Daarnaast zal een deelname-
  administrateur moeten toetsen op deze criteria bij het toelaten van een deelnemer.

## Wijzigen, corrigeren, beëindigen en intrekken

Naast het toelaten ondersteunt de deelname-administrateur de processen om deelnemerkenmerken
te wijzigen of corrigeren en om een deelname te kunnen beëindigen of in te trekken.

Afnemers kunnen voor hun taken (sterk) afhankelijk zijn van door een aanbieder in
het stelsel ingebrachte data. Processen die potentieel leiden tot intrekken
en beëindigen van deelname zullen worden afgestemd op deze afhankelijkheid.

## De deelname-administrateur

De deelname-administrateur ondersteunt de processen om de deelnemers binnen het FDS te administreren.
De deelname-administrateur is een deelfunctie van de stelselfunctie van [[Regisseur|regisseur]]().

De registratie van deelnemers is zodanig opgezet dat het mogelijk is om deelnameprocessen door verschillende
deelname-administrateurs uit te laten voeren. Dit maakt het mogelijk om bijvoorbeeld voor bepaalde sectoren een
sectorspecifieke deelname-administrateur aan te wijzen. Een sectorspecifieke deelname-administrateur
kan beter ingericht zijn om vast te stellen of een organisatie een wettelijke taak heeft binnen betreffende
sector (bijvoorbeeld voor zorg- of onderwijsinstellingen). Of we binnen het FDS daadwerkelijk gebruik
gaan maken van meerdere deelname-administrateurs is nog onderwerp van onderzoek.
