---
title: Werkbank
# linkTitle: Docs
menu: {main: {}}
weight: 20
type: docs
---

Welkom!

Op deze pagina kun je het totaaloverzicht vinden van alle kennis die we hebben ontwikkeld. Deze kennis is volop in ontwikkeling, dit betekent dat het dus nog niet af is. Daar hebben we jullie voor nodig!

Deze omgeving is speciaal ingericht om content met elkaar aan te passen en te vormen op basis van de laatste ontwikkelingen, als team federatief datastelsel hebben we daarom een 0.1 versie ontwikkeld die we vervolgens op verschillende manieren met jullie willen aanvullen en verscherpen. Deze documenten kennen daarmee nog geen formele status. Mocht dit wel al het geval zijn dan kunnen jullie dit op de pagina zelf ook zien. 

Kortom, lees je iets waar je op wilt reageren of aan wilt bijdragen? Onder [welkom](/docs/) vind je meer info over [contributies](/docs/contribution/) aanleveren. Benieuwd waarom we hiervoor hebben gekozen? Lees hier de toelichting over deze [werkomgeving](/docs/werkomgeving/).
