---
title: Toezichthouder
status: Voorstel
date: 2024-04-30
author: Kernteam FDS
version: v0.23
description: >
    Stelselfunctie die toeziet op het nakomen van stelselafspraken en die klachten 
    en bezwaren daarover afhandelt.
---

### Toezichthouder
De organisatorische [stelselfunctie](../stelselfuncties/) Toezichthouder:

- Ziet toe op de nakoming van stelselafspraken.
- Handelt klachten en bezwaren af.

#### Status

Deze functie bestaat al in het huidige Stelsel van Basisregistraties. Deze rol wordt over het
algemeen ingevuld door de verantwoordelijk opdrachtgever voor de registratie. Dit wordt op termijn
en in samenspraak met omgevormd tot een passende toezichtfunctie voor het FDS.

#### Achtergrond

{{% TODO /%}}

#### FDS context & positionering

{{% TODO /%}}

#### Inhoudelijke verdieping

{{% TODO /%}}

#### Standaardisatie

{{% TODO /%}}
