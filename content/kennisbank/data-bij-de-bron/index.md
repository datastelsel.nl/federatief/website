---
title: Data bij de bron
date: 2024-05-28
author: Kernteam FDS
status: Concept
description: Gebruik van transparante en actuele gegevens
---
De zin 'data bij de bron' is veelgebruikt en kent meerdere betekenissen. Op deze pagina wordt de
uitwerking van wat data bij de bron betekent voor het Federatief Datastelsel telkens concreter
uitgewerkt én worden de relaties naar andere betekenissen onderhouden. Zo wordt en blijft hopelijk
duidelijk wat er allemaal te zeggen valt over *data bij de bron*.

## In het kort

**Data bij de bron** betekent in het kort dat data *opgehaald* wordt bij *een bron*.

*Opgehaald* duidt op het gebruik van **API's**, Application Programming Interfaces. Afnemers hebben
zelf geen kopie van de data van anderen die zij nodig hebben. In plaats daarvan wordt de data
opgehaald op het moment dat die data nodig is. En die data wordt ook niet langer bewaard dan voor
het gebruik nodig is. Dat betekent dus dat er **geen kopieën** van datasets meer nodig zijn.

*Een* of *de bron* duidt op de plek waar de data vandaan opgehaald wordt. Voor het Federatief
Datastelsel is dat de Aanbieder. Dat hoeft echter niet dezelfde te zijn als de 'Bronhouder', 'data
eigenaar' of 'inhoudelijk verantwoordelijke'. Per registratie of datasets kunnen hier specifieke
(sector/domein)afspraken over bestaan. Ook de wet stelt in sommige gevallen welke gegevens door
welke organisatie (verantwoordelijk) gebruikt dienen te worden; dit worden *authentieke gegevens*
genoemd. Van belang is dat in ieder geval *bekend* en bij voorkeur ook *traceerbaar* moet zijn **van
welke bron gegevens afkomstig zijn**.

Zowel over *opgehaald* als *data* zelf als over *de bron* zijn veel meer afwegingen te maken. Maar
bovenstaande is in het kort de richting voor het Federatief Datastelsel.

## Bronnen in het FDS

Er bestaan verschillende definities van 'bron' binnen de overheid. Voor het Federatief Datastelsel
zijn daar afwegingen te maken:

### NORA

De [[nora:Bron|NORA definieert een bron]]() als:

> *"de plaats of organisatie waar bepaalde informatie is ontstaan en/of beschikbaar wordt gesteld,
> of de documenten waarin die informatie is vervat"*.

Belangrijk element in deze definitie is dat de 'bron' zowel de plaats van *ontstaan* als de plaats
van *beschikbaarstelling* kan zijn. In het kader van een datastelsel is dat relevant, want in een
stelsel kunnen dit verschillende partijen zijn. Volgens de NORA-definitie kunnen elk van die
partijen claimen 'de bron' te zijn binnen zo’n stelsel.

{{% pageinfo color="info" %}}

**Voorbeeld van BGT**

Dit is bijvoorbeeld het geval binnen het huidige **stelsel van basisregistraties**. Zo staat in *artikel
23* van de *wet **basisregistratie grootschalige topografie*** (BGT):

> *"Indien een bestuursorgaan bij de vervulling van zijn publiekrechtelijke taak een gegeven nodig
> heeft dat krachtens deze wet als authentiek gegeven in de basisregistratie grootschalige
> topografie beschikbaar is, gebruikt het dat authentiek gegeven".*

In *artikel 2* van die wet staat:

> *"De basisregistratie grootschalige topografie wordt gehouden door de Dienst".*

Hieruit zou kunnen worden afgeleid dat met het gebruik van de door deze Dienst (het Kadaster)
beschikbaar gestelde BGT-data conform 'data bij de bron' wordt gewerkt. Met andere woorden: de
*Landelijke Voorziening* is *dé bron*.

In diezelfde wet is echter in *artikel 10* aan allerlei andere overheidsorganisaties de status van
'bronhouder' toegekend, waarbij deze bronhouder volgens *artikel 11* zorgt draagt:

> *"voor het bijhouden van de geografische gegevens in de basisregistratie grootschalige topografie
> door levering van de gegevens, bedoeld in de artikelen 7, tweede en derde lid, en 8 aan de
> Dienst"*.

Gebruik van de term 'levering' geeft aan dat de BGT-data afkomstig is uit andere registraties, dus
dat het hier in feite *kopiedata* betreft. Het juist toepassen van het werken volgens 'data bij de
bron' zou dus ook zo geïnterpreteerd kunnen worden dat niet de LV-BGT, maar de door deze partijen
bijgehouden bronregistraties moeten worden gebruikt. Dit is immers de plaats waar het gegeven voor
de eerste keer is vastgelegd. Met andere woorden: de *bronhouders* zijn *dé bron*.

{{% /pageinfo %}}

Ook andere registraties kennen dit soort toeleveringsketens. Soms kan zelfs verdedigd worden dat
burgers en bedrijven de eigenlijke bronnen zijn en dat toepassen van 'data bij de bron' dus inhoudt
dat zij altijd zélf bepaalde gegevens moeten gaan verstrekken. Dat laatste staat dan weer op
gespannen voet met een ander richtinggevend principe binnen het huidige stelsel van
basisregistraties, namelijk *eenmalige uitvraag en meervoudig gebruik* (het niet vragen naar de
bekende weg).

Als het gaat om het 'ontstaan' of inwinnen van data geldt ook nog dat een inwinnende
(overheids)organisatie veelal enige vorm van validatie toepast, zodat enige vorm van zekerheid kan
worden ontleend aan het door de overheidsorganisatie ingewonnen gegeven. Er is bijvoorbeeld een
bewijsstuk overlegd, welke is gecontroleerd door een ambtenaar. Of het betreft een eigen verklaring
van de betrokkene, maar de ambtenaar heeft wel de identiteit van de betrokkene gevalideerd.
Daarnaast zijn er gegevens die niet (extern) worden ingewonnen, maar ontstaan bij
overheidsorganisaties, bv gegevens mbt de besluiten die organisaties nemen. Zie ook
[[Concept data-ecosysteem Nederlandse overheid]]().

### Digitale Overheid

In de definitie van data bij de bron op
[digitaleoverheid.nl](https://www.digitaleoverheid.nl/data-bij-de-bron/) wordt op een andere manier
naar 'bron' gekeken:

> *"Data bij de bron houdt in dat gegevens van burgers en bedrijven alleen in bronsystemen zoals de
> basisregistraties worden geregistreerd, en niet gekopieerd of verstuurd. Zo zorgen we ervoor dat
> de overheid altijd transparante en actuele gegevens gebruikt."*

In deze definitie wordt de bron (het bronsysteem) niet gedefinieerd, maar wordt alleen een voorbeeld
gegeven en verder de nadruk gelegd op het doel (transparant en actueel) en wat dit in de weg staat
(het kopiëren of versturen van gegevens).

### Federatief Datastelsel

Binnen het Federatief Datastelsel (FDS) wordt eveneens een pragmatische aanpak voor het bepalen van
bronnen gevolgd. Het FDS hanteert daarvoor geen algemene definitie, maar vereist voor elke in het
FDS opgenomen dataset en datadienst stelselconforme metadata. Op grond van deze context kan de
data-afnemer zelf eenvoudig kan vaststellen of en in welke context, deze als 'bron' kan of moet
worden beschouwd. Daarbij geldt binnen het FDS de afspraak dat er alleen sprake is van verplicht
gebruik van een bron als in deze metadata naar een verifieerbare grondslag wordt verwezen. Dat kan
een bestuurlijke afspraak zijn die alleen bepaalde partijen bindt, maar het kan ook een wet zijn die
alle partijen in een sector verplicht, of zelfs de gehele overheid. Zo bevatten de wetten van de
huidige basisregistraties voor bepaalde 'authentieke data' een gebruiksverplichting voor alle
organisaties die een publiekrechtelijke taak uitvoeren waarbij de betreffende data een rol speelt.

{{% pageinfo color="info" %}}

De voorlopige richting van het FDS is dat het domein kiest wat moet worden beschouwd als 'de bron'.
Daarbij draagt het FDS bij aan het ondersteunen van een grote gemene deler om, in het belang van
zowel afnemer en aanbieder, eisen en bijbehorende oplossing(srichtingen) in ieder geval voor een
deel te harmoniseren/standaardiseren.

{{% /pageinfo %}}

In het FDS is 'de bron' wel altijd een expliciete keuze die gemaakt is binnen de governancestructuur
van het domein waar deze bron betrekking op heeft. Bij deze keuze staat het belang van de
data-afnemer centraal. Deze benadering betekent dat de implementatie in de tijd ook kan wijzigen. Zo
is het mogelijk dat er vanuit het oogpunt van gebruiksgemak of efficiency ervoor wordt gekozen om
data die door verschillende partijen wordt bijgehouden, in een centrale registratie of landelijke
voorziening te bundelen en deze vervolgens juridisch als 'bron' te bestempelen. Maar het is ook
mogelijk dat veranderingen in technologie het mogelijk maken om te 'ontbundelen' en data als een
voor de data-afnemers logisch geheel te presenteren terwijl deze fysiek bij verschillende partijen
staat. In de beschreven context in de metadata dient dit duidelijk te zijn voor elke dataset in het
FDS. Belangrijk daarbij is dat dubbelingen worden vermeden. Er kan binnen het FDS geen aanbod zijn
van dezelfde data met hetzelfde toepassingsbereik/zelfde gebruikscontext. Voor FDS-afnemers moet
altijd duidelijk zijn wat de ‘bron van de waarheid’ is en dat is er altijd maar één.

### Betrouwbare bronnen

Bij het gebruik van bronnen door een afnemer is het ook van belang *wat* een afnemer met die
brondata doet. Over dat gebruik dient de afnemer verantwoording af te kunnen leggen. Over het
algemeen zal dat gebruik een structureel karakter hebben en dan spelen er ook nog andere zaken een
rol bij het gebruik. Niet alleen de verantwoording van dat gebruik op een bepaald moment, maar ook
de traceerbaarheid, de uitlegbaarheid en de herhaalbaarheid daarvan. Dat stelt ook *eisen* aan
bronnen. Om betrouwbaar gebruik te kunnen maken van bronnen, zullen deze bronnen *capabilities*
moeten hebben om aan die eisen te kunnen voldoen. Welke capabilities dat precies zijn, staat nog
niet vast. Gedacht kan worden aan historische bevragingen, herstel en correctie ook terug in de
tijd, gegevens die 'in onderzoek' zijn oftewel die niet per se als waar gekenmerkt kunnen worden.
Deze capabilities dragen allemaal aan de hiervoor genoemde eisen.

In het FDS is er (dus) vrijheid voor domeinen om te kiezen wat 'de bronnen' zijn. De eisen aan al
die bronnen zullen wel generiek zijn. Welke capabilities *betrouwbare bronnen* precies moeten hebben
en hoe deze te realiseren zijn, wordt onderzocht in het project [[Uit betrouwbare bron]]().

## Overig gebruik van Data bij de Bron

Zoals hierboven al eerder is benoemd, wordt 'data bij de bron' veelvuldig gebruikt en met
verschillende 'lading'. Hieronder een opsomming en enige overwegingen hoe deze 'lading' zich
verhoudt tot het FDS.

### Door de digitale overheid

**Data bij de bron** is niet ondubbelzinnig duidelijk. [Data bij de
Bron](https://www.digitaleoverheid.nl/data-bij-de-bron/) wordt als uitgangspunt genoemd bij de
Digitale Overheid:

> Data bij de bron is een belangrijk uitgangspunt voor de digitale transformatie van de Nederlandse
> overheid. Data hoort zo veel mogelijk op één logische plek te staan, bij de eigenaar van die
> gegevens.

Eén logische plek is echter niet zo logisch als een basisregistratie een verzameling is van de
'stukjes' basisregistratie die bij meerdere bronhouders liggen. Dat is namelijk niet één plek. Dat
is wél de eigenaar. Maar wat is er nu belangrijker bij dit uitgangspunt: 'één logische plek' of 'bij
de eigenaar van die gegevens'?

Om hier een uitweg in te vinden, worden extra oplossingen ingebracht zoals Landelijke Voorzieningen.
Op zichzelf zijn deze ook nuttig en van toegevoegde waarde, maar in het kader van data bij de bron
zijn deze ingewikkeld.

- Wat is nu de bron?
- Wat is eigenaarschap in dit uitgangspunt?
- De oorspronkelijke bronhouder? Die is in ieder geval niet ontslagen van haar verantwoordelijkheid op
  de juistheid van de data
- De uitvoeringsorganisatie? Die is in ieder geval niet direct verantwoordelijkheid voor de
  juistheid, of misschien wel voor de verzameling (zie ook
  [[Concept data-ecosysteem Nederlandse overheid|cumulatie en consolidatie in concept data-ecosysteem Nederlandse overheid]]()),
  maar wel weer voor de beschikbaarheid e.d.

Dit uitgangspunt levert dan ook weer nieuwe vragen op:

- Wat is 'dé bron'?
- Is dat altijd hetzelfde vanuit elk perspectief? Of mag dat ook anders zijn vanuit data-aanbieder,
  bronhouder en data-afnemer?

**FDS overwegingen**

- Draagt bij aan het FDS, omdat het de oorsprong van data en informatie adresseert
- Draagt bij aan het FDS, omdat het zich richt op waar de verantwoordelijkheid ligt, in ieder geval wat betreft de
  oorsprong van de data
- Draagt niet bij aan het FDS, omdat geen uitsluitsel geeft over de nieuwe vragen die het opwerkt
- … <!-- numbers of pros and cons can vary -->

### Programma Data bij de Bron

Dit is een programma van MinBZK en het programma achter 'data bij de bron' van de [Digitale
overheid](#door-de-digitale-overheid).

### Programma Regie op Gegevens

[Regie op
gegevens](https://www.digitaleoverheid.nl/overzicht-van-alle-onderwerpen/regie-op-gegevens/) is ook
een programma van de Digitale Overheid. In dit programma is een voorstudie gedaan naar een [kader
voor regie op
gegevens](https://rog.pleio.nl/news/view/f0dc6a48-97e1-4102-a656-72e86a811511/voorstudie-naar-een-kader-voor-regie-op-gegevens):

- Hoofdprincipes

  - *Mens centraal*
  - *Digitale autonomie*
  - *Inclusiviteit*
- Inrichtingsprincipes

  - *Vertrouwen*
  - *Transparantie*
  - *Interoperabiliteit*
  - *Dataminimalisatie*

Al deze principes zijn erop gericht om duidelijkheid en begrip te kunnen verschaffen aan personen en
bedrijven daar waar het over hen gaat. Daarvoor is het noodzakelijk om te weten waar welke data is
en wordt gebruikt, zeker in geval van persoonsgegevens.

**FDS overwegingen**

- Draagt bij aan het FDS, omdat het aandacht vraagt voor *verantwoord datagebruik*
- Draagt bij aan het FDS, omdat het (indirect) vraagt naar *begrip* en *oorsprong* van data, maar
  maakt dat niet expliciet. Het stelt meer een voorwaarden aan de *capabilities van [betrouwbare
  bronnen](#betrouwbare-bronnen)*
- Draagt niet bij aan het FDS, omdat het gericht is op regie vanuit de *burger* en dat is niet de
  scope van FDS (voor dit moment)
- … <!-- numbers of pros and cons can vary -->

### NORA Principes

NORA onderkent het principe van [[nora:Informeer_bij_de_bron|Informeer bij de bron]]() (NAP12). Dat
stelt:

> *"Maak bij de dienst gebruik van gegevens die afkomstig zijn uit een bronregistratie."*
>
> *Voor betrouwbare dienstverlening is het hergebruik van de juiste informatie en documenten van
> cruciaal belang. Om de kwaliteit over de juistheid van een gegeven te borgen, moet duidelijk zijn
> welke organisatie bepaalt wat de juiste informatie is. Uitgangspunt is dat er voor gegevens waar
> de overheid gebruik van maakt, altijd één bron bestaat, die leidend is.*

Wat een 'bronregistratie' is, wordt hierboven al beschreven bij [Bronnen in het FDS | NORA](#nora).
Dat betreft (dus) de plaats of organisatie waar de informatie ontstaat en/of beschikbaar wordt gesteld.

Andere relevante principes uit de NORA zijn:

- IMP021 [[nora:Bevorder_hergebruik_van_gegevens| NORA: Bevorder hergebruik van gegevens]]() (IMP21)
- NAP10 [[nora:Neem_gegevens_als_fundament|NORA: Neem gegevens als fundament]]() (NAP10) ?

**FDS overwegingen**

- Draagt bij aan het FDS, als 'informeer bij de bron' uitgelegd wordt als 'maak gebruik van API's'
- Draagt bij aan het FDS, omdat het hergebruik van gegevens bevordert
- Draagt niet bij aan het FDS, omdat het onduidelijkheid over laat over wat nu een bron precies moet zijn
- … <!-- numbers of pros and cons can vary -->

### Basis Data Infrastructuur (in logistiek)

De **[Basis Data Infrastructuur (BDI)](https://bdinetwork.org/)** is het afsprakenstelsel waarmee je volledig digitaal en geautomatiseerd zaken doet in de logistiek. Op een efficiënte, veilige en vertrouwde manier. Zo zorgen we voor optimaal goederenvervoer. In Nederland en internationaal.

Eén van de kernprincipes van BDI is [data bij de bron](https://bdinetwork.org/bdi/kernprincipes/data-bij-de-bron).

> *"Als data-eigenaar bepaal je zelf wie er toegang heeft tot welke gegevens via de BDI, en onder welke voorwaarden. Dit noemen we ook wel ‘gegevenssoevereiniteit’. Zo hou je als bron volledige controle over de toegang tot data"*

Voor de BDI geldt dat zij dit principe verbinden met authenticatie en autorisatie:

> *"Het gevolg van deze ‘data bij de bron’-aanpak, is dat er een min of meer gemeenschappelijk identificatie-, authenticatie-, vertrouwensbeoordelings- en autorisatiemechanisme nodig is. Dit mechanisme moet naadloos integreren met een "Zero Trust" API of interface, wil je veilige en gecontroleerde gegevensuitwisselingen waarborgen. De BDI biedt zo’n mechanisme."*

**FDS overwegingen**

- Draagt bij aan het FDS, als in een vergelijkbaar principe in 'data bij de bron'
- Draagt bij aan het FDS, omdat het hergebruik van gegevens bevordert
- Draagt niet bij aan het FDS, omdat het specifiek logistiek betreft
- Draagt niet bij aan FDS, omdat de mechanismen te weinig gestandaardiseerd zijn
