---
title: Linked Data Design Rules
date: 2024-08-15
tags: 
description: >
  Design Rules voor het hanteren van Linked Data binnen het FDS.
---

## Design Rules
Binnen het FDS wordt linked data gebruikt voor metadata. Voor de primaire data
kan linked data ook gebruikt worden. Linked data is een familie van standaarden
en best practices. Er zijn echter vele interpretatiemogelijkheden en vrijheden 
die het effectief gebruik van linked data hinderen. Binnen het FDS hanteren
we daarom een aantal design rules voor het gebruik van Linked Data om de 
uitwisseling van linked data binnen het FDS te harmoniseren.

De design rules dienen nog te worden uitgewerkt en betreffen bijvoorbeeld:

* Follow your nose: een http-request met als url een object-id levert een 
linked data document met betekenisvolle kenmerken van het betreffende 
object (inclusief relevante verwijzingen naar andere objecten).
 * Content negotiation: het http mechanisme voor content negotiation wordt
 gebruikt om een linked data document te bieden in een beschikbare gewenste syntax.
 * Nog te bepalen welke syntaxes en bijbehorende http-content-types 
 aanbevolen worden (zoals [Turtle](https://www.w3.org/TR/turtle/), 
 [JSON-LD](https://www.w3.org/TR/json-ld11/), 
 [RDF-XML](https://www.w3.org/TR/rdf-syntax-grammar/) en/of 
 [RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa))
* Metadata wordt ontsloten via https. 
* Een http requests (op basis van een object-id) dient een redirect uit te 
voeren naar eenzelfde request met alleen het protocol gewijzigd
in https.
* Eventuele nog te bepalen richtlijnen met betrekking tot versionering 
van linked data documenten.
* Een object-id bevat http als protocol.
* Een object-id bevat een #-tag en een fragment-naam om onderscheid te maken 
tussen het aan te wijzen object en het linked data document waarin de triples 
van betreffend object zijn opgenomen.
* Een object-id bevat een hostname binnen een domein dat eigendom is van
een deelnemer van het FDS.
* Een nog te bepalen lijst met standaard prefixes die kunnen worden gebruikt
binnen linked data documenten.

## Aandachtspunten

Object-id's worden gebruikt om entiteiten over organisaties heen te 
verwijzen. Het is onwenselijk dat als gevolg van bijvoorbeeld een naamswijziging 
van de registratie, de aanbiedende organisatie of het aangewezen object het
object-id zou moeten wijzigen. Bij het definiëren van object-id's dient 
hiermee rekening gehouden te worden. Indien het toch noodzakelijk is om
de object-id's te wijzigen, kan de overgang worden gefaciliteerd door:
* gebruik te maken van [owl:sameAs](https://www.w3.org/TR/owl-ref/#sameAs-def)
om aan te geven dat de oude en nieuwe object-id hetzelfde object betreffen.
* bij een request op de oude object-id een redirect uit te voeren naar het
nieuwe object-id.

## Standaarden
* [RDF](https://www.forumstandaardisatie.nl/open-standaarden/rdf)
* [RDFS](https://www.forumstandaardisatie.nl/open-standaarden/rdfs)
* [OWL](https://www.forumstandaardisatie.nl/open-standaarden/owl)
* [SHACL](https://www.forumstandaardisatie.nl/open-standaarden/shacl)
* [RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa)
* [Turtle](https://www.w3.org/TR/turtle/)
* [JSON-LD](https://www.w3.org/TR/json-ld11/)
* [RDF-XML](https://www.w3.org/TR/rdf-syntax-grammar/)

