---
title: Handreiking FDS-datadiensten
author: FDS-werkgroep Servicekwaliteit
status: Definitief
date: 2024-06-26
version: v1.0
description: >
 Deze handreiking ondersteunt bij het opstellen van  gestandaardiseerde dienstenniveaubeschrijvingen voor FDS-datadiensten. 
---

 Deze handreiking ondersteunt bij het opstellen van  gestandaardiseerde dienstenniveaubeschrijvingen
 voor FDS-datadiensten. Deze inhoud is het resultaat van samenwerking met onze omgeving in de
 praktijk tijdens de FDS-werkgroep servicekwaliteit, hierbij werkten we samen met deelnemers vanuit
 het Kadaster, TNO, Logius en het FDS-projectteam.
  
## Context

Bij het werken conform het principe 'data bij de bron' worden data-afnemers voor hun bedrijfsvoering
afhankelijk van de datadiensten van FDS-data-aanbieders.  Afnemers zullen daarom willen weten welk
dienstenniveau (service level) zij mogen verwachten zodat ze kunnen inschatten of dat in hun geval
afdoende is. Omdat in dezelfde bedrijfstoepassing veelal datadiensten van verschillende
data-aanbieders worden gecombineerd, is het wenselijk dat data-aanbieders afspreken om hun
dienstenniveau op een uniforme manier te beschrijven. In eerste instantie gaat het daarbij om
standaardisatie van de inhoud van de dienstenniveau beschrijving. Dit bestaat uit een afspraak over
de dienstenniveau aspecten die minimaal moeten worden beschreven, aangevuld met afspraken over de
daarbij te hanteren begrippen. Dat laatste zorgt voor de noodzakelijke 'eenheid van taal’: dat we
binnen het FDS in dienstenniveaubeschrijvingen dezelfde begrippen gebruiken en we daar ook dezelfde
betekenis aan geven. Op een later moment kan deze handreiking worden uitgebreid met afspraken om het
dienstenniveau zelf te gaan standaardiseren door binnen het FDS standaard niveaus (serviceklassen)
te definiëren waarin het FDS data-aanbod kan worden ingedeeld (bijvoorbeeld basis, plus en vitaal).  

## Uitgangspunten voor het opstellen van een FDS-conforme dienstenniveaubeschrijving

- Het beoogd toepassingsdomein van de deze handreiking is het opstellen van
  dienstenniveaubeschrijvingen voor FDS-conforme datadiensten bestemd voor tot het FDS toegelaten
  data-afnemers. Deze afnemers zijn de organisaties die de aangeboden dienst daadwerkelijk in hun
  bedrijfsprocessen gaan toepassen. Deze handreiking is dus niet van toepassing op beschrijvingen
  die bedoeld zijn om aan opdrachtgevers/financiers duidelijk te maken wat er geleverd gaat worden.  
- Uitgangspunt binnen het FDS is dat in de metadata die hoort bij een datadienst er een relatie
  wordt gelegd (wordt gelinkt) met het van toepassing zijnde (standaard) dienstenniveau. Het is dus
  niet nodig om in de dienstenniveau beschrijving zelf op te nemen op welke diensten deze van
  toepassing is.
- Zoals de titel aangeeft heeft de beschrijving van het dienstenniveau betrekking op het beschrijven
  van kwaliteitsaspecten van de dienstverlening m.b.t. het afnemen van FDS-conforme datadiensten.
  Deze beschrijving is aanvullend op algemene FDS-deelnamevoorwaarden (de toetredingseisen) die
  gekoppeld zijn aan de rol die een stelseldeelnemer vervult. Deze deelnamevoorwaarden bevatten
  stelselbrede afspraken over zaken als de te implementeren standaarden, het toe te passen
  beveiligingsniveau en het in te richten kwaliteitsmanagement. Deze elementen hoeven in de
  dienstenniveaubeschrijving dus niet (opnieuw) te worden benoemd.  

## Verplichte inhoud dienstenniveaubeschrijving voor FDS-datadiensten

Hierna worden de minimaal op te nemen onderdelen beschreven. De onderdelen waarbij de exacte
betekenis van belang is, zijn in een tabel opgenomen. Deze tabel bevat de definitie van het
betreffende onderwerp aangevuld met een toelichting en een voorbeeld.  

### Bereikbaarheid data-aanbieder  

Een beschrijving van de kanalen (telefoonnummers, e-mailadressen, url’s webformulieren, url’s
chatbots/digitale assistenten, sociale media etc.) die data-afnemers kunnen gebruiken om met de
data-aanbieder contact op te nemen voor :  

- Algemene vragen.
- Het melden van verstoringen in de dienstverlening.
- Het rapporteren van data fouten (terugmeldingen).
- Het indienen van klachten.

### Actuele informatie over de dienstverlening  

Een link naar één of meerdere webpagina’s waar de data-afnemers in elk geval informatie kunnen
vinden over:  

- Actuele verstoringen.
- Gepland onderhoud / geplande onbeschikbaarheid.  
- Voorgenomen wijzigingen.  
- Het gerealiseerde dienstenniveau (de beschikbare dienstenniveaurapportage).

Het is de bedoeling om op termijn dit soort informatie te standaardiseren zodat het op FDS-niveau
kan worden gebundeld en data-afnemers eenvoudig kunnen nagaan hoe het stelsel als geheel presteert.  

### Beschikbaarheid

| Onderwerp | Toelichting | Niveau |
|-----------|-------------|--------|
| _Openingstijd_<br>Tijdvenster waarbinnen de dienst kan worden afgenomen | | Bijvoorbeeld 7*24 uur |
| _Servicetijd_<br>Tijdvenster waarbinnen gebruikersondersteuning wordt aangeboden en het functioneren van de dienst actief wordt bewaakt | Buiten de servicetijd is sprake van passieve bewaking, d.w.z. alleen automatisch herstel verstoringen (voor zover mogelijk). | Bijvoorbeeld Ma-Vr 7.00-18.00 |
| _Onderhoudsvenster_<br>Tijdvenster dat beschikbaar is voor het uitvoeren van onderhoud dat kan leiden tot tijdelijke onbeschikbaarheid van de dienst | Van tevoren aangekondigde inperking van de openingstijd.<br><br>Organisaties kunnen vaste onderhoudsvensters hanteren, maar het is vaak praktischer om onderhoud in te plannen zodra dat nodig is. Afnemers kunnen dan toch zekerheid krijgen door af te spreken dat het minimaal x dagen van tevoren wordt aangekondigd. | Bijvoorbeeld:<br>Elke 2e zaterdag van de maand van 9.00 uur tot 23.00 uur<br><br>Geplande onbeschikbaarheid (gebruik van het onderhoudsvenster) wordt uitgevoerd buiten de servicetijd en minimaal 10 werkdagen van tevoren aangekondigd. |
| _Beschikbaarheid_<br>Tijd dat de dienst binnen de servicetijd kon worden afgenomen als percentage van de totale openingstijd binnen een kalendermaand. | Gemeten op het aanbiedingspunt van de datadienst en exclusief gepland onderhoud binnen het onderhoudsvenster. | Bijvoorbeeld: 95% |
| _Robuustheid: maximale uitvalduur_ | Maximum totaal aantal minuten per kalendermaand dat de dienst gedurende de servicetijd onbeschikbaar mag zijn. | Bijvoorbeeld: 360 minuten |
| _Robuustheid: maximaal aantal langdurige verstoringen_ | Maximaal aantal verstoringen per kalendermaand met meer dan X aangesloten minuten onbeschikbaarheid binnen de servicetijd | Bijvoorbeeld maximaal 2 verstoringen van meer dan 60 minuten |

### Versie- en wijzigingsbeheer

Het versiebeheer en het daarmee verbonden wijzigingsbeheer moeten ervoor zorgen dat afnemers
zekerheid krijgen over de impact van gewijzigde datadiensten op hun eigen IT-diensten en de tijd die
voor hen beschikbaar is om deze impact te verwerken. In de dienstenniveaubeschrijving moet worden
aangegeven of op de aangeboden dienst wel of geen formeel proces van toepassing is voor het
introduceren van gewijzigde versies van de aangeboden datadienst en wat daarvan de belangrijkste
kenmerken zijn. Hierbij moet onder andere gespecificeerd worden hoe ‘versioning’ wordt ingevuld en
hoeveel (majeure) versies er gelijktijdig worden ondersteund. Voor API-gebaseerde datadiensten moet
daarbij van onderstaande standaard voor versienummering worden uitgegaan:  

- [Het Semantic Version model](https://gitdocumentatie.logius.nl/publicatie/api/adr/#versioning)
  voor het onderscheiden van verschillende versies van datadiensten.

Toepassing van dit model conform de [Nederlandse API
Strategie](https://gitdocumentatie.logius.nl/publicatie/api/adr/#introduction) is een
minimumvereiste om bij dit onderdeel een ‘ja’ te mogen invullen. Het model voorziet in
standaardisatie van de opbouw van het versienummer dat de afnemer inzicht geeft in de impact van de
wijziging en stelt aanvullende eisen aan het versiebeheer zoals het publiceren van een ‘change log’.

## Optionele onderdelen

Dit zijn onderwerpen die in de dienstenniveaubeschrijving wel moeten worden benoemd, maar waarop FDS
data-aanbieders geen actief beheer hoeven te voeren. Er hoeft alleen te worden aangegeven of er wel
of niet sprake is van een gegarandeerd niveau. Oftewel of er sprake is van meer dan een
inspanningsverplichting (‘best effort’). Aan de claim van een gegarandeerd niveau worden soms wel
voorwaarden gesteld, maar het staat de organisatie verder vrij om dit niveau naar eigen inzicht te
beschrijven. Hier is voor gekozen omdat organisaties dit zo verschillend invullen dat
standaardisatie niet mogelijk is.

### Performance- en capaciteitsbeheer

Het performancebeheer moet er voor zorgen dat de afnemers van een stabiele performance ervaren. In
de dienstenniveaubeschrijving moet worden gespecificeerd of de afnemers een gegarandeerde
performance kan worden geboden. Bij een positief antwoord is het belangrijk dat het voor de afnemer
duidelijk is wat het referentiepunt is (waar wordt er gemeten?) en of er randvoorwaarden gelden
zoals een maximale capaciteitsvraag. Verder is het van belang om te specificeren of er bij het
toekennen van capaciteit en het borgen van de performance prioritaire afnemers kunnen worden
onderkend. Dit is van belang voor afnemers die op dit gebied zekerheid zoeken, zoals de meldkamers
van politie en hulpdiensten.  

### Calamiteitenbeheer

Een beschrijving van eventuele specifieke voorzieningen die zijn getroffen om de gevolgen van
calamiteiten op te vangen. Een calamiteit wordt hierbij gedefinieerd als een zeldzaam voortkomende
gebeurtenis die kan leiden tot een omvangrijke en langdurige verstoring van de dienstverlening van
de data-aanbieder en waarvan niet van tevoren te voorspellen is of en wanneer deze gebeurtenis zich
zal voordoen. Van oorsprong waren het fysieke ‘rampen’ (in het Engels ‘acts of God’) zoals het
afbranden en overstromen van het rekencentrum met ‘uitwijk’ als maatregel, maar tegenwoordig kunnen
ook bepaalde succesvolle cyberaanvallen, zoals een ransomware attack, als een calamiteit worden
beschouwd.  
In de dienstenniveaubeschrijving moet worden aangegeven of voor de aangeboden dienst wel of geen
maatregelen zijn getroffen om de gevolgen van calamiteiten op te vangen. Bij een positief antwoord
moet in de dienstbeschrijving worden toegelicht welk niveau er wordt geboden. Bijvoorbeeld wat de
maximale uitvalduur is, wat het maximale dataverlies is en wat na een calamiteit nog de maximaal
beschikbare capaciteit is. Om te kunnen claimen dat in calamiteitenbeheer is voorzien moet de
dienstaanbieder in elk geval een functionerende ‘consignatiedienst’ hebben ingericht.

| | | |
|---|---|---|
| _Consignatiedienst<br>(synoniem 'piketdienst' of 'crisisdienst')_ | Een permanent ingericht geheel van organisatorische en technische maatregelen om ook buiten de servicetijd te kunnen constateren dat er (waarschijnlijk) een calamiteit is opgetreden en direct de van toepassing zijnde beheerprocedure te kunnen starten. | De invulling is vrij.<br>Organisaties kunnen bijvoorbeeld een specifieke IT-consignatiedienst instellen, maar kunnen ook een algemene bereikbaarheidsdienst voor noodgevallen instellen. |
