---
title: Publicatie
date: 2023-10-17
aliases: 
  - /kennisbank/capabilities/datawaarde/publicatie
tags: 
description: >
  Datawaarde | Publicatie: Publicatie- en marktplaatsdiensten
---

{{< capabilities-diagram selected="publicatie" >}}

## Beschrijving

Om het aanbod van databronnen en -diensten onder gedefinieerde voorwaarden te ondersteunen, moeten
inzichtelijk zijn wat beschikbaar is. Deze stelselfunctie ondersteunt de publicatie van het aanbod
zodat een afnemer het aanbod kan vinden en kan beoordelen of het zinvol kan en mag worden afgenomen.

## Meer lezen

De stelselfunctie Metadata is gebaseerd op het 
[Publication and Marketplace Services](https://docs.internationaldataspaces.org/ids-knowledgebase/v/open-dei-building-blocks-catalog#data-value-building-blocks)
zoals beschreven in het 
[OPEN DEI design principles position paper on Resources](https://design-principles-for-data-spaces.org/).

In de [Blueprint van het Data Spaces Support Centre](https://dssc.eu/page/knowledge-base) 
wordt publicatie beschreven in het building block 
[Publication and Discovery](https://dssc.eu/space/BVE/357076320/Publication+and+Discovery). 
Binnen OpenDEI is Discovery echter bij de stelselfunctie [[Metadata]]() ondergebracht.

## Publicatie van het FDS aanbod

Het aanbod van het FDS wordt gepubliceerd in een catalogus. Er is één officiële FDS-catalogus 
die het volledige aanbod binnen het FDS ter beschikking stelt aan geïnteresseerden. Daarnaast 
kunnen er alternatieve catalogi zijn die (delen van) het FDS aanbod publiceren, zoals sectorale 
en/of international catalogi die het FDS aanbod opgenomen hebben als onderdeel van een breder 
(informatie)aanbod.

Een catalogus is niet zelf de bron van de gepubliceerde metadata, maar verzamelt het aanbod
vanuit door aanbieders gepubliceerde linked data bestanden met metadata zoals beschreven in 
[[Aanbod (metadata)]](). De aanbieder is verantwoordelijk voor
het aanbieden van het bestand met metadata volgens de richtlijnen van het FDS (self-description).
Deze bestanden vormen de bron voor catalogi.

## De FDS-Catalogus

Via de FDS-catalogus is het aanbod inclusief de daarover beschikbare [[Metadata]]()
publiek inzichtelijk voor geïnteresseerden (voor zover het geen individuele uitwisselingen
betreft). Dit betreft het doorzoekbaar en beschikbaar maken in een voor geïnteresseerden 
passende vorm, zowel richting systemen als personen.
Ten behoeve van personen wordt een webapplicatie aangeboden. Informatie ontsloten naar personen
wordt ook machine-interpreteerbaar aangeboden (met behulp van 
[RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa)), naar behoefte al dan
niet aangevuld met alternatieve linked data formaten (zoals 
[Turtle](https://www.w3.org/TR/turtle/), [JSON-LD](https://www.w3.org/TR/json-ld11/) 
en/of [RDF-XML](https://www.w3.org/TR/rdf-syntax-grammar/))
en een [SPARQL](https://www.w3.org/TR/sparql11-query/) API ten behoeve van het vrij 
kunnen bevragen van de beschikbare metadata.

De FDS-catalogus is een vertrouwde vindplaats van FDS metadata. Dit impliceert onder 
andere het toepassen van https en een binnen het FDS vertrouwd certificaat. Daarnaast 
dient de catalogus alleen geverifieerde metadata ter beschikking te stellen vanuit 
vertrouwde bronnen. De FDS-catalogus kan ander (niet-FDS) aanbod bevatten. Het onderscheid 
met niet-FDS aanbod dient echter duidelijk aanwezig te zijn. 

De FDS-catalogus is actueel en compleet. Binnen het FDS worden afspraken gemaakt welke
metadata via de catalogus wordt gepubliceerd. De catalogus publiceert alle volgens de 
FDS-criteria valide metadata die binnen deze afspraken valt. Daarnaast worden 
binnen het FDS afspraken gemaakt met betrekking tot de actualiteit van de gepubliceerde
metadata. 

| ![Publicatie via de FDS catalogus](images/publicatie-catalogus.png) |
| :-----------------------------------------------------------------: |
| _Publicatie via de FDS catalogus_                                   |

## Standaarden

- [RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa)
- [SPARQL](https://www.w3.org/TR/sparql11-query/)
- [Turtle](https://www.w3.org/TR/turtle/)
- [JSON-LD](https://www.w3.org/TR/json-ld11/)
- [RDF-XML](https://www.w3.org/TR/rdf-syntax-grammar/)
