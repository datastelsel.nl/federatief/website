---
title: Metadata
date: 2023-10-17
aliases: 
  - /kennisbank/capabilities/datawaarde/metadata
tags: 
description: >
  Datawaarde | Metadata: Metadata in brede zin met beschrijvingen, verwijzingen en meer
---

{{< capabilities-diagram selected="metadata" >}}

## Beschrijving

Om data effectief te kunnen gebruiken moet duidelijk zijn in welke context en met welk doel de data
is ontstaan en wat de betekenis ervan is. Om te kunnen weten en begrijpen wat bepaalde data
betekent, beschrijven we deze met metadata.

De [NORA](https://www.noraonline.nl/) definieert metadata (metagegevens) als:
’Gegevens die context, inhoud, structuur en vorm van informatie en het beheer ervan door de tijd heen
 beschrijven.‘  Metadata vormt een essentieel fundament onder de werking van het Federatief Datastelsel. De metadata beschrijft onder andere:

* welke data binnen het stelsel beschikbaar zijn,
* wat de betekenis (semantiek) van die data is,
* wat de grondslag is voor het verzamelen van data,
* voor welke context de data oorspronkelijk zijn bedoeld,
* welke partijen betrokken zijn,
* wat de rollen en verantwoordelijkheden zijn van de betrokken partijen,
* hoe de data is vormgegeven (onder andere syntax),
* hoe de data beschikbaar is (API's bijvoorbeeld),
* wat de kwaliteit van de data en datadiensten is (denk aan juistheid, actualiteit, etc.),
* voor wie de data beschikbaar is en onder welke condities.

De metadata geeft potentiële afnemers inzicht in wat, voor wie en onder welke condities binnen
het stelsel beschikbaar is.

De publicatie van metadata in een catalogus is onderdeel van de stelselfunctie
[[Publicatie]]().

## Meer lezen

De stelselfunctie Metadata is gebaseerd op het
[Metadata and Discovery Protocol](https://docs.internationaldataspaces.org/ids-knowledgebase/v/open-dei-building-blocks-catalog#data-value-building-blocks)
zoals beschreven in het
[OPEN DEI design principles position paper on Resources](https://design-principles-for-data-spaces.org/).

In de [Blueprint van het Data Spaces Support Centre](https://dssc.eu/page/knowledge-base) wordt
metadata beschreven in het building block
[Data, Services and Offerings Descriptions](https://dssc.eu/space/BVE/357075789/Data%2C+Services+and+Offerings+Descriptions).
Binnen de blueprint is 'discovery' ten opzichte van OpenDEI verplaatst naar
[Publication and Discovery](https://dssc.eu/space/BVE/357076320/Publication+and+Discovery).

De [NORA](https://www.noraonline.nl/) biedt kennis over metadata in
[Wat zijn metadata en metadatamanagement](https://www.noraonline.nl/wiki/Wat_zijn_metadata_en_metadatamanagement%3F)
en de relatie tussen metadata en [[Modellen|modellen]]() (semantiek) in
[Nationaal Semantisch Vlak](https://www.noraonline.nl/wiki/Nationaal_Semantisch_Vlak).

## Metadata binnen het FDS

Binnen het FDS wordt metadata als Linked Data uitgewisseld. Linked data omvat een hele familie aan standaarden en
best practices. Om binnen het FDS effectief Linked Data toe te passen, worden binnen het FDS
[[Linked Data Design Rules]]() gehanteerd.

Op de volgende vlakken beschrijft FDS de te hanteren structuur van metadata:

* De [[Deelnemerslijst]]() van het FDS
* De [[Deelnemerkenmerken]]() van de verschillende deelnemers binnen het FDS
* Het [aanbod](../aanbod-metadata/) binnen het FDS ([DCAT](https://www.forumstandaardisatie.nl/open-standaarden/dcat))
* De begrippen gehanteerde binnen het aanbod ([SKOS](https://www.forumstandaardisatie.nl/open-standaarden/skos) en [NL-SBB](https://docs.geostandaarden.nl/nl-sbb/nl-sbb/))
* De gegevensdefinitie gehanteerd binnen een aangeboden dataset ([MIM](https://www.forumstandaardisatie.nl/open-standaarden/mim) of [OWL](https://www.forumstandaardisatie.nl/open-standaarden/owl))
* De kwaliteitskenmerken van het aanbod binnen het FDS ([DQV](https://www.w3.org/TR/vocab-dqv/))
* De datadeelrelaties die zijn gelegd binnen het FDS

| ![FDS metadata overzicht](images/metadata-overzicht.png) |
| :------------------------------------------------------: |
| _FDS metadata overzicht_                                 |

De uitwerking van metadata met betrekking tot (traceerbaarheid van) individuele gegevensleveringen
is onderdeel van de stelselfunctie [[Traceerbaarheid]]().

## Standaarden

* [SKOS](https://www.forumstandaardisatie.nl/open-standaarden/skos)
* [NL-SBB](https://docs.geostandaarden.nl/nl-sbb/nl-sbb/)
* [DCAT](https://www.forumstandaardisatie.nl/open-standaarden/dcat), zie
ook [DCAT-AP-NL 3.0](https://docs.geostandaarden.nl/dcat/dcat-ap-nl30/)
* [MIM](https://www.forumstandaardisatie.nl/open-standaarden/mim)
* [OWL](https://www.forumstandaardisatie.nl/open-standaarden/owl)
* [DQV](https://www.w3.org/TR/vocab-dqv/)
