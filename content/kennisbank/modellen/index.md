---
title: Modellen
date: 2023-10-17
aliases: 
  - /kennisbank/capabilities/interoperabiliteit/modellen
tags: 
description: >
  Interoperabiliteit | Modellen: Data- en informatiemodellen en -formaten
---

{{< capabilities-diagram selected="modellen" >}}

## Beschrijving

Deze stelselfunctie beschrijft hoe gegevens- en informatiemodellen worden gehanteerd
en geven daarmee betekenis en onderlinge samenhang aan de data die wordt uitgewisseld 
via [[Dataservices]](). De betekenis van gegevens wordt ook wel
uitgedrukt als de *semantiek* van gegevens. Gegevens- en informatiemodellen
zijn een vorm van [[Metadata]]().

## Meer lezen

De stelselfunctie Modellen is gebaseerd op het [Data Models and Formats building
block](https://docs.internationaldataspaces.org/ids-knowledgebase/open-dei-building-blocks-catalog#interoperability-building-blocks)
zoals beschreven in het [OPEN DEI design principles position paper on
Resources](https://design-principles-for-data-spaces.org/). In de 
[Blueprint van het Data Spaces Support Centre](https://dssc.eu/page/knowledge-base) 
wordt het begrip modellen beschreven in het 
Building Block [Data Models](https://dssc.eu/space/bv15e/766067670/Data+Models).

De [NORA](https://www.noraonline.nl/) biedt kennis over modellen
en semantiek binnen de visie 
[[nora:Nationaal Semantisch Vlak]]().

## Modellen binnen het FDS

Om het potentieel van gegevens optimaal te benutten dienen de betekenis en
onderlinge samenhang van gegevenselementen inzichtelijk te zijn. Het is daarom
cruciaal dat het gegevensmodel van FDS aanbod inzichtelijk is voor (potentiële) 
afnemers. Dit gaat verder dan het benoemen van de gegevenselementen en de 
onderlinge samenhang. Elk gegevenselement dient voorzien te zijn van 
een duidelijke definitie en de context waarbinnen het gegeven is ingezameld. Deze 
informatie is essentieel voor afnemers om goed te kunnen beoordelen hoe bruikbaar 
gegevens zijn voor hun beoogde doel.

Informatiemodellen zijn op te delen in een aantal beschouwingsniveaus, zo is
er onderscheid te maken tussen een model van begrippen, een conceptueel
informatiemodel, een logisch informatie- of gegevensmodel en een fysiek of
technisch gegevens- of datamodel. 

### Model van begrippen

Een model van begrippen bevat een begrippenkader waarbinnen begrippen van 
betekenis en onderlinge samenhang worden voorzien. Een begrippenkader wordt uitgedrukt 
met behulp van de standaard
[NL-SBB](https://docs.geostandaarden.nl/nl-sbb/nl-sbb/) op basis van op de 
internationale standaard [SKOS](https://www.forumstandaardisatie.nl/open-standaarden/skos).

### Conceptueel informatiemodel en logisch informatie- of gegevensmodel

Een conceptueel informatiemodel geeft een zo getrouw mogelijke beschrijving van die werkelijkheid 
en is in natuurlijke taal geformuleerd. Een conceptuele informatiemodel richt zich specifiek op 
de semantiek van dingen en hun eigenschappen. Het conceptuele informatiemodel richt zich op het 'wat'.
Een logisch informatie- of gegevensmodel beschrijft hoe concepten gebruikt worden bij de interactie
tussen systemen en hun gebruikers en tussen systemen onderling. Het gaat hierbij, in tegenstelling tot 
een conceptueel model, dus veel meer om het ‘hoe’.

De standaard [MIM](https://www.forumstandaardisatie.nl/open-standaarden/mim)
biedt handvatten om invulling te geven aan een conceptueel informatiemodel
en een logisch informatie- of gegevensmodel. Binnen 
[MIM](https://www.forumstandaardisatie.nl/open-standaarden/mim) kunnen deze
modellen worden uitgedrukt met 'klassieke' modelleertechnieken zoals 
[UML](https://nl.wikipedia.org/wiki/Unified_Modeling_Language) en 
[ERD](https://nl.wikipedia.org/wiki/Entity-relationshipmodel),
die veel worden toegepast bij objectgeoriënteerd modellen en
relationele modellen. Deze modelleertechnieken zijn echter sterk
geënt op het in isolatie modelleren van gegevens en bieden weinig handvatten
om informatiemodellen onderling met elkaar te verbinden.

Grote potentie zit echter in het onderling verbinden van datasets die 
traditioneel alleen in silo's beschikbaar zijn. Het concept van linked
data geeft invulling aan deze behoefte via een vorm van modelleren 
waarbij onderlinge verbondenheid in de basis is verwerkt. Bij linked data
loopt het conceptuele informatiemodel en het logische informatiemodel in
elkaar over. Linked data kent de volgende elkaar aanvullende standaarden 
voor het vormgeven van informatiemodellen: 
[RDF](https://www.forumstandaardisatie.nl/open-standaarden/rdf), 
[RDFS](https://www.forumstandaardisatie.nl/open-standaarden/rdfs), 
[OWL](https://www.forumstandaardisatie.nl/open-standaarden/owl) en 
[SHACL](https://www.forumstandaardisatie.nl/open-standaarden/shacl). 

[UML](https://nl.wikipedia.org/wiki/Unified_Modeling_Language) en 
[ERD](https://nl.wikipedia.org/wiki/Entity-relationshipmodel) enerzijds
en de set aan linked data standaarden anderzijds zijn niet eenvoudig
naar elkaar om te zetten. Dat maakt het een uitdaging binnen FDS om
modellen uitgedrukt in 
[UML](https://nl.wikipedia.org/wiki/Unified_Modeling_Language) of 
[ERD](https://nl.wikipedia.org/wiki/Entity-relationshipmodel)
te verbinden met elkaar of met linked data modellen. Overigens 
definieert [MIM](https://www.forumstandaardisatie.nl/open-standaarden/mim) hoe een 
MIM(UML)-model als linked data kan worden uitgedrukt en uitgewisseld. Dat maakt het 
uitgedrukte MIM-model echter nog geen linked data *model*, het betreft geen
vertaling van UML of ERD naar [OWL](https://www.forumstandaardisatie.nl/open-standaarden/owl) 
en/of [SHACL](https://www.forumstandaardisatie.nl/open-standaarden/shacl) (of vice versa). Het 
betreft een UML-model dat kan worden gelezen als linked data, zoals elk
verzameling van informatieobjecten in principe kan worden uitgedrukt als linked data.

### Technisch gegevens- of datamodel
De uiteindelijke gegevensuitwisseling vindt plaats via een API. Het gegevensmodel
dat de API hanteert is het technische gegevens- of datamodel. Voor REST-API's
wordt dit uitgedrukt met de 
[Open API Specification](https://www.forumstandaardisatie.nl/open-standaarden/openapi-specification).
Indien de REST-API gegevens uitwisselt in de vorm van 
[JSON-LD](https://www.w3.org/TR/json-ld11/), kan dit direct worden
gekoppeld aan een informatiemodel uitgedrukt in linked-data. 
Voor andere vormen van koppeling, bijvoorbeeld aan een model uitgedrukt 
in [UML](https://nl.wikipedia.org/wiki/Unified_Modeling_Language), is
verder onderzoek en/of verdere standaardisatie nodig.

## Informatiemodellen en traceerbaarheid
In de stelselfunctie [[Traceerbaarheid]]() wordt de behoefte
aan traceerbaarheid geformuleerd. Ook deze traceerbaarheid wordt uitgedrukt in 
gegevens die vormgegeven worden in een informatiemodel, uitgedrukt in aspecten 
als gebeurtenissen, correcties, verantwoording, twijfel, onderzoek en periodes 
van registratie en geldigheid. Op dit vlak is nog veel onderzoek te doen. 
Binnen het project [[Uit betrouwbare bron]]()
wordt getracht patronen en definities op te stellen die ondersteunen
bij het opzetten van een effectief informatiemodel dat recht doet aan deze
aspecten. 

## Informatiekundige kern

De ambitie van FDS is om de verschillende gegevensbronnen onderling aan elkaar te koppelen.
In feite de stap om van datasilo's naar onderling verbonden 'linked' datasets te gaan. Dit 
gaat verder dan de te hanteren modelleertechnieken. Deze verbinding komt namelijk alsnog
niet tot stand als voor identificatie benodigde sleutelwaarden zoals een BSN 
niet afdoende aanwezig zijn in de betreffende datasets.

Een belangrijke stap naar het koppelbaar maken van datasets bestaat uit de 
[[Informatiekundige kern van het FDS]]().
De mate van koppelbaarheid wordt bepaald door overeenkomstige data-elementen 
in verschillende bronnen. Hoe hoger de zekerheid dat data-elementen qua semantiek 
en feitelijke waarden overeenkomen hoe hoger de koppelbaarheid. 
De informatiekundige kern draagt hieraan bij door afspraken over te maken over
de koppelbaarheid. De informatiekundige kern stelt dat binnen het FDS wordt 
afgesproken hoe de kernobjecten persoon, organisatie en locatie worden geïdentificeerd.
Veel [sectorregistraties](https://www.noraonline.nl/wiki/Alle_sectorregistraties) 
bevatten informatie over personen, organisaties en/of locaties. Het harmoniseren van de 
identificatie van personen, organisaties en locaties ondersteunt het onderling
aan elkaar koppelen van al deze datasets.

## Standaarden

- [MIM](https://www.forumstandaardisatie.nl/open-standaarden/mim)
- [RDF](https://www.forumstandaardisatie.nl/open-standaarden/rdf)
- [RDFS](https://www.forumstandaardisatie.nl/open-standaarden/rdfs)
- [OWL](https://www.forumstandaardisatie.nl/open-standaarden/owl)
- [SHACL](https://www.forumstandaardisatie.nl/open-standaarden/shacl)
- [Open API Specification](https://www.forumstandaardisatie.nl/open-standaarden/openapi-specification)
- [JSON-LD](https://www.w3.org/TR/json-ld11/)
