---
title: Standaarden
date: 2024-10-22
author: Kernteam FDS
status: Voorstel
description: >
  Het programma realisatie FDS heeft een eerste lijst met standaarden opgesteld
  die partijen in eerste instantie moeten toepassen om federatief datadelen
  mogelijk te maken.
---
Deze notitie over standaarden in het federatief datastelsel is het vertrekpunt voor het gebruik van
standaarden. Het federatief model laat opslag en beheer van data lokaal aan bronhouders, maar
faciliteert datagebruik over bronnen heen, centraal door een interoperabel systeem van afspraken en
oplossingen op ontsluiting, toegang, annotatie en koppeling.

## Context

Op een federatieve manier data delen  is alleen maar mogelijk als partijen dezelfde standaarden
toepassen. Over de wijze waarop worden in het stelsel nadere afspraken gemaakt, alsook over de
invulling van de benodigde organisatorische stelselfuncties, financiering, etc.

Het programma realisatie-FDS heeft een eerste lijst met standaarden opgesteld die partijen in eerste
instantie moeten toepassen om federatief datadelen mogelijk te maken. deze lijst treft u aan in
bijlage 1. dit is een combinatie van bestaande standaarden recent ontwikkelde standaarden en
standaarden in ontwikkeling. er zijn meer standaarden nodig waar we in de loop van de tijd zicht op
zullen krijgen. deze eerste lijst wordt gepubliceerd om onze omgeving al vroegtijdig inzicht te
geven in relevante standaarden zodat men daar rekening mee kan houden.

## Hoe worden de standaarden vastgesteld?

Voor het vaststellen van standaarden wordt gebruik gemaakt van het bestaande besluitvormingsproces.
Forum Standaardisatie toetst open standaarden en adviseert het Overheids-breed Beleidsoverleg
Digitale Overheid (OBDO) over het voorschrijven ervan aan publieke organisaties. Dit kan als
aanbeveling of volgens een ‘Pas toe of leg uit’-verplichting. Standaarden met een 'Pas toe of leg
uit'-verplichting kunnen verzwaard worden met een streefbeeldafspraak of tot wettelijke
verplichting.

- Standaarden worden vastgesteld via de bestaande procedure van het Forum;
- De voor FDS relevante standaarden zijn ook (bestaande of beoogde) GDI-standaarden, daarvoor wordt
  de bestaande governance van de GDI gebruikt;
- De architectuurraad GDI adviseert de PGDI en IDO;
- Bespreking van gebruik van standaarden voor FDS vindt plaats in het Tactisch Overleg FDS. Indien
  relevant voor de CDO’s dan ook in de CDO-raad.
- Het IDO fungeert als ‘voorportaal’ van het OBDO op het gebied van datavraagstukken en heeft een
  regisserende en stimulerende rol (sponsor). Het IDO is geen besluitvormend overleg. Het OBDO
  bekrachtigt de standaarden;
- In het OBDO kunnen afspraken gemaakt worden over implementatie(ondersteuning) van standaarden.

![Standaardisatieproces](images/standaardenproces.png)

## Is er meer nodig dan opname van de standaarden op de ‘pas toe of leg uit’-lijst?

Het gebruik van standaarden is essentieel voor de werking van het FDS. Afnemers van data kunnen deze
alleen gebruiken als aanbieders en afnemers voldoen aan de afspraken die in het stelsel zijn
gemaakt. In veel gevallen zullen partijen zelf de meerwaarde van de toepassing van standaarden in de
eigen context zien en zullen deze in de loop der tijd gebruikt worden.

Dit zal echter niet opgaan voor standaarden waarvan de meerwaarde van het gebruik vooral tot uiting
komt op stelselniveau. Bovendien laat het verleden zien dat zonder flankerend beleid de adoptie van
dergelijke standaarden minstens 10 jaar duurt.

Voor het slagen van FDS is het van belang dat de prioriteit van het toepassen van de noodzakelijke
standaarden voor aanbieders en afnemers hoog is. Dat kan door een combinatie van maatregelen:

- Gestructureerd, aan de hand van een meerjaren implementatie agenda, afspraken maken over
  ontwikkeling en adoptie van standaarden;
- Implementatie-ondersteuning voor deelnemers in het stelsel;
- Het monitoren van de voortgang;
- Het op termijn verplichten van de minimaal noodzakelijke standaarden in de Wet Digitale overheid.

## FDS standaarden: de basis

Hieronder staan de standaarden beschreven die partijen in eerste instantie moeten toepassen om op de
FDS-manier te gaan werken. Daarnaast is een aantal standaarden benoemd die op dit moment ontwikkeld
wordt zodat daar nu al rekening mee kan worden gehouden en meegedaan kan worden bij de
totstandkoming als dat gewenst is. Standaarden die ook toegepast moeten worden maar meer generiek
van aard zijn (informatiebeveiliging) of zeer specifiek zijn (bijvoorbeeld geo-standaarden), zijn
niet opgenomen in dit document omdat die niet FDS-specifiek zijn.

### Doel: Inzicht in het data-aanbod

| Wat omvat de standaard                                                                                                                                                                                                                            | Standaard                                                                    | Status van de standaard        |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------- | ------------------------------ |
| Standaard voor het beschrijven van datasets: Welke datasets (gegevensverzamelingen) zijn beschikbaar, welk licentietype, toegangsregime is daarop van toepassing, wie biedt de dataset aan? Welke dataservices worden bij een dataset aangeboden? | DCAT                                                                         | Aanbevolen standaard           |
|                                                                                                                                                                                                                                                   | [Nederlands profiel DCAT](https://docs.geostandaarden.nl/dcat/dcat-ap-nl30/) | In goedkeuring                 |
| Standaard voor het beschrijven van dataservices (gegevensdiensten): welke dataservices zijn beschikbaar, welk licentietype, toegangsregime is daarop van toepassing, wie biedt de dataservice aan? Op welke datasets opereert de dataservice?     |                                                                              |                                |
| Inzicht in de betekenis van begrippen en relaties met andere begrippen (bijv. uit andere sectoren)                                                                                                                                                | SBB                                                                          | In goedkeuring                 |
|                                                                                                                                                                                                                                                   | SKOS                                                                         | Verplicht (pas toe of leg uit) |

### Doel: Ontsluiten van data

| Wat omvat de standaard                                                                                             | Standaard                                                                                                    | Status van de standaard        |
| ------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------ | ------------------------------ |
| API’s waarmee data direct kan worden bevraagd (dataservices) of waarmee functionaliteit wordt ontsloten (services) | REST API Design Rules                                                                                        | Verplicht (pas toe of leg uit) |
| Uitwisselen van berichten tussen applicaties over plaatsgevonden gebeurtenissen (notificaties)                     | CloudEvents                                                                                                  | Internationale standaard       |
|                                                                                                                    | NL Gov CloudEvents                                                                                           | In goedkeuring                 |
| Het veilig en geautomatiseerd kunnen koppelen van services tussen verschillende organisaties                       | [Digikoppeling REST API](https://gitdocumentatie.logius.nl/publicatie/dk/restapi/)                           | Verplicht (pas toe of leg uit) |
|                                                                                                                    | [FSC](https://gitdocumentatie.logius.nl/publicatie/dk/restapi/#federated-service-connectivity-standaard-fsc) | Verplicht (pas toe of leg uit) |
| Data uit verschillende bronnen in samenhang bevragen                                                               | IMX                                                                                                          | In ontwikkeling                |

### Doel: Verantwoord datagebruik

| Wat omvat de standaard                                                                             | Standaard                                                                                  | Status van de standaard |
| -------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------ | ----------------------- |
| Standaard om dataverwerkingen te loggen waarmee organisaties het datagebruik kunnen verantwoorden. | [Logboek dataverwerkingen](https://logius-standaarden.github.io/logboek-dataverwerkingen/) | In ontwikkeling         |

## 'Pas toe of leg uit'-beleid

Sommige belangrijke open standaarden worden te weinig gebruikt, waardoor onze digitale samenleving
kwetsbaar, inefficiënt of niet toegankelijk is voor iedereen. Daarom geldt voor deze standaarden het
'Pas toe of leg uit'-beleid. De verplichting geldt voor gemeenten, provincies, rijk, waterschappen
en alle uitvoeringsorganisaties. Voor alle andere organisaties in de publieke sector geldt het
gebruik van de ‘Pas toe of leg uit’-standaarden als een dringend advies.

### Pas toe

‘Pas toe’ betekent dat op het moment dat u ICT aanschaft (een dienst of en product), u de 'Pas toe
of leg uit'-lijst moet raadplegen. Wanneer de aanschaf valt onder een toepassingsgebied dat voorkomt
op deze lijst, moet u de standaard(en) toepassen.

### Leg uit

Afwijken van het gebruik van de voorgeschreven standaarden mag alleen als een dergelijke dienst of
product in onvoldoende mate wordt aangeboden, onvoldoende veilig of zeker functioneert of om een
andere reden die van bijzonder gewicht is. De afwijking en de reden daarvan moeten beschreven worden
in de bedrijfsvoeringparagraaf van het jaarverslag. Dit is de betekenis van ‘leg uit’.

### Streefbeeldafspraken en wettelijke verplichting

Voor sommige standaarden duurt het moment van een volgende aanschaf te lang, waardoor de publieke
dienstverlening te veel risico loopt. Er worden dan Streefbeeldafspraken gemaakt om de adoptie te
versnellen. En leiden ook deze afspraken tot onvoldoende resultaat, dan kan een standaard in
aanmerking komen voor wettelijke verplichting op grond van de Wet digitale overheid.
