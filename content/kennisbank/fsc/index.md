---
title: Federatieve Service Connectiviteit
linkTitle: FSC
description: >
  Federatieve Service Connectiviteit (FSC) Standaard
status: Concept
date: 2024-05-15
author: Kernteam FDS & Team FSC
---
{{% imgproc "**capabilities*.*" Resize "x200" /%}}

{{% pageinfo color="info" %}}

Direct naar [fsc-standaard.nl](https://fsc-standaard.nl)

{{% /pageinfo %}}

## Beschrijving

De FSC-standaard uniformeert de wijze waarop koppelingen worden gerealiseerd, waardoor deze
beheersbaar, veilig en betrouwbaar zijn. Het maakt ze eenvoudiger beheersbaar, doordat ze eenvoudig
en snel kunnen worden gerealiseerd, gewijzigd, gecontroleerd of opgeheven. Daarnaast worden
aanvullende waarborgen gerealiseerd voor de veiligheid en betrouwbaarheid van data. Denk aan het
automatiseren van controles op basis van beleids- en toegangsregels, gebaseerd op transparantie
zodat ook achteraf helder is waarom bepaalde gegevens gebruikt zijn en bepaalde beslissingen genomen
zijn. Zo speelt de FSC-standaard een cruciale rol in het realiseren van een betaalbare, beheersbare,
veilige en betrouwbare gegevensuitwisseling in de komende jaren.

## Veilige uitwisseling van gegevens

Het systeem moet garanderen dat gegevens veilig worden overgedragen en opgeslagen, met gebruik van
versleuteling, authenticatie en autorisatie, zodat de vertrouwelijkheid en integriteit van gegevens
beschermd zijn.

FSC beschrijft hoe de identiteit van een organisatie betrouwbaar kan worden vastgesteld en hoe een
organisatie op een transparante manier namens een andere organisatie kan handelen. Elke organisatie
werkt onder haar eigen identiteit. Dit maakt het voor een organisatie vele malen eenvoudiger om te
voldoen aan wetgeving als AVG. Het is de basis van een veilig vertrouwensstelsel waarin
onomstootbaar vaststaat wie, wat, wanneer en waarom uitgewisseld heeft. De identiteit wordt gebruikt
bij het ondertekenen van een FSC contract en vervolgens bij de daadwerkelijke gegevensuitwisseling
op basis van dat contract.

## Decentrale samenwerking

In plaats van afhankelijk te zijn van één centraal systeem, kunnen organisaties direct met elkaar
communiceren. Dit maakt het mogelijk om snel en flexibel samen te werken, terwijl de
verantwoordelijkheid voor de gegevens lokaal blijft.

FSC is federatief by design en heeft geen centrale autoriteit of systeem nodig om koppelingen tussen
organisaties te leggen en gegevens uit te wisselen. Iedere organisatie is daardoor alleen
verantwoordelijk voor zichzelf. Dit maakt het stelsel robuust want er is geen centrale kwetsbaarheid
die bij calamiteiten het hele stelsel lam legt. Het maakt het stelsel schaalbaar, want er is niet
één centrale hub die een bottleneck kan vormen. Centrale stelselfuncties kunnen indien wenselijk
boven op FSC geplaatst worden. Hierbij kan bijvoorbeeld gedacht worden aan een centrale
ledenadministratie die gebruikt wordt als drempel bij het vastleggen van afspraken in een FSC
contract en het uitwisselen van data.

## Beheersbaar door uniforme standaarden

Het systeem moet gebruik maken van gestandaardiseerde protocollen en systemen en er zo voor zorgen
dat elke organisatie op dezelfde manier data kan delen en ontvangen. Hierdoor kunnen systemen van
verschillende organisaties naadloos samenwerken.

De FSC standaard beschrijft hoe interne en externe koppelingen tussen afnemers en aanbieders
georganiseerd kunnen worden. FSC standaardiseert het tot stand brengen, beheren en gebruiken van
koppelingen, waardoor deze processen geautomatiseerd kunnen worden. Dit zorgt voor een efficiënte
werkwijze die de explosieve groei van het aantal API koppelingen, mede onder invloed van het ‘data
bij de bron’ principe, helpt beheersbaar te maken.

## Synergie met andere standaarden

Door de standaardisatie van koppelingen tussen organisaties biedt FSC een solide basis voor andere
standaarden die nodig zijn voor het organiseren van vertrouwd datagebruik. Door bijvoorbeeld de
transactie logging van FSC te koppelen aan de inhoudelijke logging van een applicatie, worden
audits aanzienlijk eenvoudiger en effectiever. Dit kan bijvoorbeeld door de FSC standaard te
combineren met de standaard [Logboek dataverwerkingen](https://minbzk.github.io/logboek-dataverwerkingen/).
Een ander voorbeeld is het federatief organiseren van de toegangscontrole waardoor de beslissing
om data op te vragen of uit te leveren op de juiste plek belegd kan worden. Deze
uitwisselingsstandaarden kunnen gebruik maken van wat FSC al biedt.

Deze uitwisselingsstandaarden zijn bewust gescheiden van elkaar zodat iedere standaard zich kan
richten op 1 aspect van het verantwoord gegevensdelen. Dit maakt het geheel van standaarden
flexibeler en organisaties wendbaarder. De scope van FSC is dataverkeer tussen organisaties en
machine to machine.

![FSC Consumer Provider](images/fsc-consumer-provider.png)

## Beheer van toegangsrechten

Elke organisatie bepaalt zelf wie toegang krijgt tot welke data. Dit gebeurt via federatieve
authenticatie, wat betekent dat gebruikers bij hun eigen organisatie inloggen, maar toch toegang
kunnen krijgen tot gegevens van andere partijen.

Voordat een verzoek tot data een organisatie verlaat, wordt eerst gecontroleerd of dit verzoek wel
voldoet aan de afspraken rondom deze dataset. FSC levert hieraan de eerste controle of de
organisatie überhaupt toestemming heeft om de dataset te bevragen. Door FSC te koppelen aan de
toegangsverleningsstandaard worden veel specifiekere controles mogelijk. Individueel zijn beide
standaarden waardevol, maar gezamenlijk bieden ze synergie en maken ze het verdelen van de
verantwoordelijkheid mogelijk.

## Controleerbaar

Het systeem houdt bij wie toegang heeft gehad tot welke data, wat zorgt voor transparantie en
controleerbaarheid. Dit is vooral belangrijk in overheidsomgevingen waar naleving van regels en
veiligheid essentieel is.

Iedere uitwisseling levert een FSC transactie logrecord op zowel bij de vragende organisatie als bij
de aanbieder van data. Beide logrecords hebben hetzelfde unieke kenmerk zodat de transactie bij
beide organisaties herleidbaar is. Audits binnen een organisatie kunnen hierdoor ook over de
organisatiegrenzen doorgaan in samenwerking met de andere organisatie. In samenwerking met de
logboek dataverwerkingen standaard (nog in de maak) wordt de controleerbaarheid nog krachtiger; door
het unieke transactie kenmerk op te nemen in de inhoudelijke logging is een audit nog beter uit te
voeren want het is dan niet alleen bekend welke organisaties, wat, wanneer uitgewisseld hebben, maar
de organisatie heeft ook vastgelegd welk proces de uitwisseling startte en welke medewerker daarbij
betrokken was.

Meer info is te vinden op [fsc-standaard.nl](https://fsc-standaard.nl).
