---
title: Stelselfuncties
weight: 8
date: 2025-03-06
author: Kernteam FDS
description: De stelselfuncties van FDS
---

Voor een goede werking van het stelsel onderscheiden we een beperkte set aan
organisatorische en technische stelselfuncties. Deze functies zorgen ervoor dat
gebruikers het stelsel als een geheel ervaren en dat ze het data-aanbod in FDS
zo eenvoudig mogelijk kunnen gebruiken.

De stelselfuncties worden bij voorkeur decentraal ingevuld op basis van
afspraken, standaarden en/of voorzieningen, waarbij het uitgangspunt 'Afspraken
boven standaarden boven voorzieningen' van toepassing is.

Onderstaand diagram toont de verschillende stelselfuncties binnen FDS.

<div class="stelselfunctie-blok-container container">
  <div class="stelselfunctie-blok-header row">
    <h3>Organisatorische stelselfuncties</h3>
  </div>
  <div class="stelselfunctie-blok-container row">
    <a href="../regisseur/" class="stelselfunctie-blok col">
        <img src="images/regisseur.png" alt="Regisseur"/>
        <h3>Regisseur</h3>
        <p>
        Stelselfunctie voor de coördinatie van beheer en (door)ontwikkeling van het stelsel.
        </p>
    </a>
  </div>
  <div class="stelselfunctie-blok-container row">
    <a href="../poortwachter/" class="stelselfunctie-blok col">
      <img src="images/poortwachter.png" alt="Poortwachter"/>
      <h3>Poortwachter</h3>
      <p>
      Stelselfunctie voor toetsing, autorisatie en registratie van koppelingsverzoeken. Toetst op conformiteit met de AVG en de stelsel aansluit- en gebruiksvoorwaarden en zorgt daarbij voor een transparant beslisproces.
      </p>
    </a>
    <a href="../marktmeester/" class="stelselfunctie-blok col">
      <img src="images/marktmeester.png" alt="Marktmeester"/>
      <h3>Marktmeester</h3>
      <p>Stelselfunctie voor het verbinden van vraag en aanbod.</p>
      <p>Ondersteunt bij het inzichtelijk maken van het potentieel en het realiseren van nieuwe gebruikerswensen.</p>
    </a>
    <a href="../helpdesk/" class="stelselfunctie-blok col">
      <img src="images/helpdesk.png" alt="Helpdesk"/>
      <h3>Helpdesk</h3>
      <p>Stelselfunctie voor eerstelijnsondersteuning van gebruikers en geregistreerden.</p>
      <p>Tevens meldpunt voor het melden van fouten in overheidsregistraties.</p>
    </a>
    <a href="../expertisecentrum/" class="stelselfunctie-blok col">
      <img src="images/expertisecentrum.png" alt="Expertisecentrum"/>
      <h3>Expertisecentrum</h3>
      <p>Stelselfunctie voor het onderhouden van de stelselarchitectuur en het faciliteren van de communities waarin kennis en ervaring over het stelsel wordt gedeeld.</p>
      <p>Het kennisnetwerk.</p>
    </a>
  </div>
  <div class="stelselfunctie-blok-container row">
    <a href="../toezichthouder/" class="stelselfunctie-blok col">
      <img src="images/toezichthouder.png" alt="Toezichthouder"/>
      <h3>Toezichthouder</h3>
      <p>Stelselfunctie die toeziet op het nakomen van stelselafspraken en die klachten en bezwaren daarover afhandelt.</p>
    </a>
  </div>
</div>

&nbsp;

<div class="container">
  <div class="stelselfunctie-blok-header row">
    <h3 id="technische-stelselfuncties">Technische stelselfuncties</h3>
  </div>
  <div class="row">
    {{< capabilities-diagram >}}
  </div>
</div>
