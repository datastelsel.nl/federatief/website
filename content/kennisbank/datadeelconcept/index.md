---
title: Concept data-ecosysteem Nederlandse overheid
weight: 1
date: 2025-03-06
author: Kernteam FDS
status: Concept
description: Conceptuele beschrijving van data-ecosysteem voor de Nederlandse overheid
---
## Inleiding

Deze pagina beschrijft een algemeen concept van het data-ecosysteem voor de 
Nederlandse overheid. Doel van deze pagina is een context aan te reiken om 
de positie van rollen, functies en stelsels op gebied van datadelen, te duiden 
ten opzichte van het Federatief Datastelsel in ontwikkeling.

Afhankelijk van de beschrijvingscontext gebruiken we in deze beschrijving het 
begrip‘data’ of  het begrip ‘gegevens’. Qua betekenis maakt het geen verschil, 
de begrippen zijn uitwisselbaar.

Met het begrip 'organisatie' bedoelen we op deze pagina een publieke organisatie
van de overheid. Private organisaties duiden we aan met de term 'bedrijf'.

**Data-ecosysteem** omschrijven we in deze context als een samenwerkend geheel
van actoren, technologieën en processen om data te verzamelen, op te slaan, te
verwerken, te analyseren en te delen, met het doel om waarde te creëren uit data
ten behoeve van de Nederlandse maatschappij.

De **Nederlandse overheid** vraagt een data-ecosysteem dat optimaal is afgestemd
op de wijze waarop zij is ingericht en functioneert. De inrichting in
bestuurslagen volgens het ‘huis van Thorbecke’ is de **organisatorische basis**.
De bestuurslagen, het Rijk, de Provincies, de Waterschappen en de Gemeenten,
hebben ieder hun eigen verantwoordelijkheden. Gezamenlijk vormen ze echter het
geheel van de Nederlandse overheid. De **functionele basis** komt voort uit de
beleidscycli waar de bestuurslagen individueel en gezamenlijk invulling aan
geven, van beleidsontwikkeling en -vaststelling tot beleidsuitvoering en
-evaluatie.

Naar analogie hiervan vraagt de digitale overheid een **digitale basis** die
alle bestuurslagen en alle beleidssectoren dient. Die gezamenlijke basis is
federatief opgebouwd. Elke organisatie is verantwoordelijk voor de eigen
digitale informatievoorziening en conformeert tegelijkertijd aan generieke
afspraken en standaarden.

Op hoofdlijnen bestaat de digitale basis uit:

- Een zachte infrastructuur van afspraken en standaarden waar elke organisatie,
  indien van toepassing, aan conformeert;
- Een generieke architectuur, i.c. de [[nora:NORA_Familie|NORA-familie]]() en
  de [Generieke Digitale Infrastructuur](https://pgdi.nl/) (GDI);
- Systemen en datastelsels die zich federatief kunnen ontwikkelen binnen ketens
  en domeinen;
- Een algemeen verbindend stelsel van stelsels, dat specifieke domeinstelsels in
  samenhang kan koppelen.

Zo functioneert een ecosysteem dat de beleidscyclus optimaal ondersteunt door
vertrouwd delen en gebruiken van data. Dit resulterend in effectief beleid op
complexe maatschappelijke vraagstukken en voor adequate publieke dienstverlening
die daarmee annex is.

## Zonder data geen beleid en geen uitvoering

| ![Afbeelding A](./images/typering-datastromen.png) |
| -------------------------------------------------- |
| Afbeelding A: Datastromen Nederlandse overheid     |

De afbeelding ‘Datastromen Nederlandse overheid’ is afgeleid van een klassieke
overheidsbeleidscyclus en aangevuld met typering en herkomst van de daarin
voorkomende data en datastromen.

De Nederlandse overheid heeft data nodig om invulling te kunnen geven aan haar
taken. Dit betreft zowel data voor beleidsvorming bij complexe maatschappelijke
vraagstukken als data die nodig is om uitvoering te geven aan beleid door middel
van concrete dienstverlening aan de burgers en bedrijven in de maatschappij.

Data voor uitvoering en dienstverlening is ‘operationeel’ van aard en heeft
betrekking op zaken waarin specifieke personen, bedrijven en/of objecten een rol
spelen. Bijvoorbeeld data die nodig is om een rijbewijs af te geven, of om een
door iemand aangekochte auto over te schrijven op diens naam. Voor dergelijke
operationele diensten bestaat altijd een wettelijke grondslag.

Voor beleidsvorming is data nodig over (grootschalige) populaties die een rol
spelen in het betreffende maatschappelijke beleidsvraagstuk. Denk aan data over
uitstoot van bepaalde stoffen door gemotoriseerd verkeer binnen Nederland. Het
spreekt voor zich dat statistische analyses daarin belangrijk zijn. De data voor
dergelijke analyses komen enerzijds voort uit de maatschappij, waarin
bijvoorbeeld een organisatie als CBS een belangrijke bijdrage levert. Anderzijds
vormen de eerder genoemde operationele data een belangrijke bron. De wettelijke
kaders voor beleidsvorming zijn onder andere opgetekend in de Grondwet,
institutionele wetten, de Algemene Wet Bestuursrecht, Ministeriële regelingen en
Koninklijke besluiten. Een belangrijk principe bij gebruik van data voor
beleidsvorming, is beleidsmatige rechtvaardiging en proportionaliteit.

De operationele data die de overheid verzamelt en produceert bij de uitvoerende
taken kunnen dus een belangrijke bron van informatie vormen voor
maatschappelijke vraagstukken. Tegelijkertijd kunnen operationele data die
voortkomen uit dienstverlening ook heel bruikbaar zijn bij andere vormen van
dienstverlening. De
[[nora:NORA_online|Nederlandse Overheid Referentie Architectuur]]() (NORA) stelt
[[nora:Bevorder_hergebruik_van_gegevens|expliciet]]() bij de (implicaties van)
architectuurprincipes:

> ***Maak zo veel mogelijk gebruik van gegevens die reeds beschikbaar zijn in
> plaats van deze gegevens opnieuw te verzamelen of te creëren***.

In bepaalde gevallen is zelfs sprake van verplichting. Voor efficiënte en
effectieve uitvoering van haar taken is de Nederlandse overheid derhalve enorm
gebaat bij het eenvoudig en verantwoord kunnen delen van ‘haar eigen’ data.

## Gegevens interpreteren en gebruiken in context

Organisaties gebruiken gegevens voor uitvoering van hun publieke taak. Die
gegevens ontstaan door registratie van handelingen en besluiten van de
organisatie zelf en door inwinning. Inwinnen gebeurt door gegevens op te vragen
bij betrokken personen en bedrijven, of door situaties waar te nemen en die te
registreren. Maar ook kan een organisatie gegevens voor hergebruik verkrijgen
van een of meer collega-organisaties.

Het verkrijgen en gebruiken van gegevens gebeurt altijd binnen een bepaalde
context. Omstandigheden zoals doel, achtergrond, plaats en tijd van inwinning en
gebruik, zijn van invloed op hoe de gegevens begrepen moeten worden.

De context van een gegeven kan heel specifiek zijn, maar kan ook algemener van
aard zijn. Zo worden binnen het zorgdomein gegevens gebruikt over patiënten.
Binnen het onderwijsdomein gebruikt men gegevens over studenten. In beide
gevallen gaat het om gegevens over personen. Eenzelfde persoon kan een rol
spelen in beide domeinen, bijvoorbeeld als hij student is en ziek wordt,
waardoor hij als patiënt een behandeling moet ondergaan. De gegevens van de
persoon als student en tevens patiënt hebben een meer algemene context als het
gaat om kenmerken als naam, adres en leeftijd. Een meer specifieke context geldt
voor kenmerken als ‘genoten vooropleiding’ en ‘bloeddruk gemeten voor
behandeling’.

De gegevenskenmerken met de algemenere context kunnen in beide domeinen worden
gebruikt. Ze kunnen (indien toegestaan) worden verkregen uit een domein met een
algemenere context. Het stelsel van basisregistraties kan gezien worden als een
meer ‘algemeen domein’. Gegevenskenmerken met specifieke context worden
aanvullend geregistreerd binnen het betreffende specifiekere domein.

Gegevens met een specifieke context zijn vooral of voornamelijk relevant binnen
een specifiek domein of keten, terwijl gegevens met een algemenere context
eerder geschikt zijn voor (her)gebruik in andere contexten. De algemenere
gegevens kunnen daardoor ook geschikt zijn als basisgegeven en breed gedeeld
worden.

<span id="afbeelding-b">&nbsp;<span>

| ![Afbeelding B](./images/data-zorg-onderwijs.png) |
| ------------------------------------------------- |
| Afbeelding B: Voorbeeld datagebruik in sectoren   |

Zoals hiervoor al aangegeven, kan een organisatie gegevens ook voor hergebruik
verkrijgen van een collega-organisatie. Het is een belangrijk beginsel dat
organisaties van de overheid gebruik maken van gegevens die al binnen de
overheid bekend zijn. In het algemeen wordt dit aangeduid als hergebruik of
gezamenlijk gebruik.

Hiervoor is het echter wel noodzakelijk om de context van gegevens te kennen.
Zowel de context van waaruit een gegeven is ingewonnen als de context waarin het
gegeven gebruikt gaat worden. In de algemene kenmerken van een persoon kunnen
bij de inwinning bijvoorbeeld formele aanschrijftitels zijn opgenomen. Maar in
correspondentie met die persoon kunnen afhankelijk van de omstandigheden
(context) andere minder formele aanschrijftitels wenselijk zijn. Zonder kennis
van de context kan het bijvoorbeeld gebeuren dat bij hergebruik van het gegeven
in een informele context, de persoon ongewenst wordt aangeschreven met zijn
formele titulatuur.

De context plaatst een gegeven dus in het juiste perspectief waardoor het
mogelijk wordt de ***(her)bruikbaarheid*** ervan te interpreteren in een
andere context. Bij het bepalen van de herbruikbaarheid gaat het op hoofdlijnen
om drie vragen:

- Is het gegeven in semantische zin bruikbaar in de nieuwe context?
- Is het juridisch toegestaan om het gegeven in de nieuwe context te gebruiken?
- Is het ethisch verantwoord om het gegeven in de nieuwe context te gebruiken?

## Patronen van gegevens delen

Bij gezamenlijk gebruik worden doorgaans gegevens uit verschillende bronnen
gebundeld tot een bruikbaar geheel. Herkenbare patronen daarbij zijn cumulatie,
consolidatie en compilatie.

### Cumulatie en consolidatie

Cumulatie en consolidatie komen voor als organisaties dezelfde type gegevens
verzamelen. Ze leggen deelverzamelingen aan die elkaar aanvullen maar elkaar
misschien ook overlappen. Dit zien we bijvoorbeeld bij lokale of regionale
organisaties. Elke lokale organisatie beheert een deelverzameling van het
landelijke totaal.

Cumulatie is het samenbrengen of optellen van afzonderlijke deelverzamelingen.
Het benadrukt de ‘opeenstapeling van data’ zonder dat er een specifieke nadruk
ligt op consistentie of integratie.

Consolidatie gaat verder dan cumulatie, omdat het niet alleen gaat om het 
samenvoegen van data, maar ook om het creëren van een ‘geïntegreerd en consistent 
geheel’. Bij consolidatie worden deelverzamelingen geschoond, gecombineerd en 
gestructureerd om een uniforme verzameling te vormen.
Dit proces zorgt voor consistentie en integriteit van de data en verbetert daarmee de bruikbaarheid ervan.
Inconsistenties kunnen ook achteraf tijdens gebruik van de data nog worden opgemerkt 
en opgelost.

### Compilatie

Bij compilatie gaat het om samenbrengen van data uit verschillende bronnen voor
een specifiek doel, bijvoorbeeld het maken van een rapport of het voorbereiden
van een analyse. Afhankelijk van de noodzaak om data samen te brengen, te
structureren en/of te verrijken, kan het proces van compilatie gebruik maken van
cumulatie en consolidatie, naast aanvullende regels voor het afleiden van
gegevens. Bij compilatie ligt de nadruk vaak op het ‘organiseren en verrijken
van de data in een samenhangende vorm’ die kan worden gepresenteerd of verder
geanalyseerd.

Een gecompileerde verzameling bestaat vaak uit data van verschillende soorten,
welke aan de hand van overeenkomstige sleutelgegevens worden samengevoegd en
vaak verrijkt met afgeleide gegevens. Binnen ketens is dit een veel toegepast
patroon voor **operationele data** in de uitvoering van publieke
dienstverlening.

Een andere belangrijke vorm van compilatie betreft **statistische data** ten
behoeve van beleidsontwikkeling. Hiervoor worden gegevens over bepaalde
populaties samengevoegd, geaggregeerd en voorzien van gemeten eenheden.
Bijvoorbeeld aggregatie van voertuigdata op het kenmerk ‘soort voertuig’. Daar
kan vervolgens het aantal voorkomens per soort voertuig aan worden toegevoegd.

### Samenhang patronen voor gezamenlijk gebruik van data

Samenvattend vormen cumulatie, consolidatie en compilatie een logische volgorde
in de patronen bij gezamenlijk gebruik van data:

- Cumulatie kan de eerste stap zijn waarbij gegevens uit meerdere bronnen worden
  verzameld.
- Consolidatie zorgt ervoor dat de gecumuleerde gegevens worden geïntegreerd in
  een consistent en bruikbaar geheel, waarbij inconsistenties worden opgelost.
- Compilatie komt vaak na consolidatie en houdt in dat de gestructureerde
  gegevens worden samengesteld voor een specifieke toepassing, zoals gebruik in
  een andere context of voor analyse en rapportage.

## Datadomeinstelsel

Delen van data vergt afspraken tussen aanbieders en afnemers. Vaak begint dat
met een paar organisaties die samen willen werken. Er ontstaat een klein
afsprakenstelseltje. Meer organisaties krijgen interesse en sluiten aan op het
afsprakenstelseltje. Verdere uitbreiding gepaard met professionalisering op
gebied van interoperabiliteit, standaardisatie, organisatorische inrichting,
generieke functies en anderszins, laten prille datadeelrelaties zo uitgroeien
tot een datadomeinstelsel (**DDS**).

De afbeelding toont in samenhang de procesmatige en organisatorische componenten
van een datadomeinstelsel in het algemeen.

| ![Afbeelding C](./images/domeinstelsel.png) |
| ------------------------------------------- |
| Afbeelding C: Concept datadomeinstelsel     |

Een datadomein bestaat bij de gratie van organisaties die de data inzamelen,
produceren en bewaren. Zij zijn bronhouder. De
bronhouder is degene die toegang verleent tot de data. Elke
dataverzameling valt onder de verantwoordelijkheid van de bronhouder binnen wiens
context de verzameling is/wordt opgebouwd en onderhouden.

Organisaties met de rol van ‘data-aanbieder’ zorgen ervoor dat data van 
bronhouders worden verwerkt tot dataproducten waar
concrete behoefte aan bestaat. De aanbieder 
cumuleert, consolideert en/of compileert gegevens tot een dataproduct binnen
het domein. De aanbieder zorgt eveneens voor het maken, publiceren
en aanbieden van de datadiensten waarmee het dataproduct kan worden afgenomen.

Organisaties met de rol van ‘data-afnemer’ kunnen op de datadienst
aansluiten. De onderhavige data laten ze op verantwoorde wijze binnen
de eigen context gebruiken.

Merk op dat bronhouder, data-aanbieder en data-afnemer
rollen zijn waar organisaties invulling aan kunnen geven. Een organisatie kan
een of meerdere rollen vervullen m.b.t. zowel dezelfde data als verschillende
data.

M.b.t. de totaalverantwoordelijkheid voor het datadomein kunnen generieke
afspraken worden gemaakt over de (formele) inrichting ervan. Zo'n 
inrichting kan ook bij wet worden bestendigd, zoals dat bij landelijke
voorzieningen wel het geval is.

### Stelsel van stelsels

Een datadomeinstelsel is, zoals de term het al aangeeft, bedoeld voor de
deelnemers in het betreffende datadomein. Om data te kunnen delen tussen
domeinen, moeten de datadomeinstelsels interoperabel zijn met elkaar. Deze
interoperabiliteit is echter niet vanzelfsprekend. Er is een stelsel nodig dat
de datadomeinstelsels met elkaar verbindt.

| ![Afbeelding D](./images/stelsel-van-stelsels.png)   |
| ---------------------------------------------------- |
| Afbeelding D: Concept Stelsel van Datadomeinstelsels |

De datadomeinen beschikken over de kennis om data binnen het domein te
verbinden, te delen en te gebruiken. Een verbindend stelsel heeft niet de taak
en niet het vermogen om specifieke sector- of domeinvraagstukken op te lossen.
Het zorgt in plaats daarvan voor een generieke zachte infrastructuur, bestaande
uit afspraken en standaarden die datadomeinstelsels kunnen toepassen om
onderling vertrouwd data te kunnen delen. Het stelsel van stelsels ondersteunt
dit met generieke besturingsfuncties en eventueel generiek toepasbare
voorzieningen. Zo maakt het domein- en sector-overstijgend delen van data
mogelijk en vervult daarmee een brugfunctie.

De huidige bij wet bepaalde basisregistraties vormen al een verbindende factor
tussen verschillende overheidstaken, -processen en -registraties. Ze kennen elk
een ingericht stelsel van partijen en afspraken over inwinnen, vastleggen,
kwaliteit, toezicht, rolverdeling, beschikbaar stellen, etc. Ze vormen daarmee
een ‘algemeen’ datadomeinstelsel, zie ook [afbeelding B](#afbeelding-b) op deze
pagina.

De ontwikkeling van een ‘stelsel van stelsels’ kan daarmee zeker voortgaan op
wat reeds voorhanden is binnen het stelsel van basisregistraties, maar het zal
zich daar niet toe beperken. De basisregistraties vormen een belangrijke basis
bij het benutten van het datapotentieel voor maatschappelijke opgaven. Maar
daarnaast is er nog veel onbenut potentieel in sector- en ketenregistraties. De
NORA beschrijft sowieso al zo'n 
[[nora:Sectorregistraties|145 sectorregistraties]](). Om de potentie van deze
data ook te benutten zal veel meer over sectoren en domeinen heen ontsloten
moeten worden.

## Federatief Datastelsel (FDS)

De interbestuurlijke datastrategie is gericht op het effectiever laten
functioneren van het data-ecosysteem van de Nederlandse overheid. Daarvoor is
het volgende beleidsdoel geformuleerd:

> ‘Als overheid benutten we het volle potentieel van data bij maatschappelijke
> opgaven, zowel binnen als tussen domeinen, op een ambitieuze manier die
> vertrouwen wekt.’

Bron: [Meerjarenaanpak Interbestuurlijke
Datastrategie](https://realisatieibds.nl/files/view/1c9729a6-70a9-4bda-a245-49c1d6d63adb/meerjarenaanpak-interbestuurlijke-datastrategie6maart2023.pdf).

Als één van de middelen om dat doel te bereiken benoemt de strategie de
systeemfunctie ‘Federatief Datastelsel’ (FDS). Een federatief ingericht stelsel
past goed in het ‘Huis van Thorbecke’.

Een data-federatie beschouwen we als een samenwerking tussen de leden op basis
van wetten en collectieve afspraken over het delen en gebruiken van elkanders
data. De leden van de federatie zijn en blijven verantwoordelijk voor de
kwaliteit van de data die ze inbrengen. Het hergebruik van ingebrachte data valt
onder de verantwoordelijkheid van de hergebruikende organisatie. De leden
conformeren aan het collectieve kader van stelselafspraken en relevante
wetgeving.

De systeemfunctie ‘Federatief Datastelsel’ positioneren we in het concept
data-ecosysteem als ‘Stelsel van stelsels’. Het realisatieprogramma FDS (rFDS)
is gericht op ontwikkeling en implementatie van de ‘zachte infrastructuur’ die
nodig is om de datadomeinstelsels te verbinden en daarmee het data-ecosysteem te
laten functioneren. 

Dit wordt verder toegelicht in het [[Basismodel|Basismodel FDS]]().
