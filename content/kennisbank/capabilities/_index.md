---
title: Capabilities
# menu: {main: {weight: 20}}
# weight: 10
description: >
   De capabilities (bekwaamheden) van het federatief datastelsel met relaties (verwijzingen)
   naar de **bouwblokken** die voor de realisatie nodig zijn
status: Voorstel
date: 2023-11-28
author: Kernteam FDS
version: v0.22
---

{{% pageinfo color="warning" %}}

Deze pagina is verouderd, stelselfuncties en capabilities zijn samengevoegd tot stelselfuncties.

Zie de pagina [[Stelselfuncties]]() voor de samengevoegde stelselfuncties. Zie ook het 
besluit [[00006 Capabilities en Stelselfuncties]]().

{{% /pageinfo %}}

Een bekwaamheid, vaak aangeduid met de Engelse term 'capability', is een vermogen dat iets (bijv.
een systeem) in staat stelt om bepaalde doelstellingen te realiseren. Voor het realiseren van de
doelstellingen van het Federatief Datastelsel, zijn bepaalde bekwaamheden nodig bij de deelnemers in
het stelsel en er zijn algemene bekwaamheden die het stelsel als geheel laten functioneren. In deze
uitwerking ligt de focus bij het laatstgenoemde. De bekwaamheden die een individuele deelnemer nodig
heeft om effectief deel te kunnen nemen beschouwen we als voorwaarden die FDS stelt aan deelname.
Het stellen van die voorwaarden is dan weer een bekwaamheid van het stelsel. FDS benoemt vier basis
bekwaamheden. Deze vormen de kapstok voor het dieper uitwerken naar detailniveaus. Deze bekwaamheden
zijn afgeleid uit het OPEN DEI raamwerk, zoals beschreven in paragraaf 2.3. FDS onderscheidt de
volgende vier basis bekwaamheden:

- Stelsel-governance: Het vermogen van het stelsel om gedragingen en activiteiten van de deelnemende
  partijen te laten conformeren aan de geldende stelselafspraken.
- Interoperabiliteit: Het vermogen van het stelsel om de deelnemende partijen data met elkaar te
  laten delen en/of uit te wisselen.
- Vertrouwen: Het vermogen van het stelsel om vertrouwen te genereren voor verantwoord gebruik van
  data door de deelnemende partijen.
- Data waarde: Het vermogen van het stelsel om data zichtbaar van waarde te laten zijn voor de
  deelnemende partijen.

Elke hoofdbekwaamheid is verder onderverdeeld in deel-bekwaamheden. Hiervoor gebruiken we het model
dat we kort hebben toegelicht bij de beschrijving van
[de relatie met de Europese datastrategie](/kennisbank/scope-van-fds/#samenwerking-en-afbakening-binnen-europa)
. Het model is weergegeven in figuur 3.

Klik op de betreffende capability om te lezen waar deze over gaat, welke componenten,
afhankelijkheden en relaties deze heeft.

{{< capabilities-diagram >}}

Het federatief datastelsel gebruikt een [Raamwerk voor ontwikkeling in
samenwerking](/kennisbank/raamwerk/). Hieronder is de uitwerking van de capabilities die daar
benoemd worden.

Voor meer informatie over [Capabilities](/besluiten/00001-basisstructuur/#capabilities) en de
context van het federatief datastelsel, lees onze [[Strategie van samenwerken]]() en het
[[Raamwerk|Raamwerk voor ontwikkeling in samenwerking]](). In de [[Werkomgeving]]() kun je lezen hoe je
kunt bijdragen en hoe de samenwerkomgeving is opgezet. Wil je meer lezen over achterliggende
overwegingen (besluiten), lees onze DR [[00001 Basisstructuur]]() en
DR [[00004 Capabilities NL]]().
