---
title: Beheer
weight: 3
date: 2023-10-17
tags: 
description: >
  Governance | Beheer: Continuïteit en doorontwikkeling van het stelsel
---

{{< capabilities-diagram selected="beheer" >}}

## Beschrijving

Continuïteit en doorontwikkeling van het stelsel ... Regie van het stelsel, etc, etc

Het continuïteitsmodel beschrijft de processen voor het beheer van wijzigingen, versies en releases
voor normen en overeenkomsten. Dit omvat ook het bestuursorgaan voor besluitvorming en
conflictoplossing.

Elke deelnemer aan het stelsel kan deelnemen aan de beheer processen van het stelsel.

## Beheer processen binnen het FDS

Binnen het FDS onderkennen we de volgende groepen beheer processen:

* Processen voor het inbrengen van data-spaces / sub-stelsels (nog uit te werken)
* Processen voor het inbrengen en tijdig oplossen van geschillen (nog uit te werken)
* Processen voor het beheer van stelsel veranderingen (nog uit te werken)

## Bouwblokken

- [...]
