---
title: Operationeel
weight: 1
date: 2023-10-17
tags: []
description: >
  Governance | Operationeel: Operationele overeenkomsten en afspraken
---

{{< capabilities-diagram selected="operationeel" >}}

## Beschrijving

Operationele overeenkomsten reguleren beleid dat wordt afgedwongen in de operatie. Ze reguleren
bijvoorbeeld de beschikbaarheid in een dienstniveau-overeenkomst (DNO, ook wel servicelevelagreement
of SLA, zie in dit kader ook de [[Handreiking FDS-datadiensten|handreiking servicekwaliteit]]().) of
bevatten voorwaarden als gevolg van de
[AVG](https://www.autoriteitpersoonsgegevens.nl/themas/basis-avg/avg-algemeen/de-avg-in-het-kort)/GDPR
of de [Payment Services Directive
2](https://www.rijksoverheid.nl/onderwerpen/financiele-sector/nieuwe-wetgeving-voor-betalingsverkeer-psd2)
(PSD2).

Daarbij ondersteunen operationele processen van het FDS de dagelijkse werking, zoals
het aansluiten op aanbod of het melden van een storing.

## Meer lezen

De capability Operationeel is gebaseerd op het [Organisational/Operational Management building
block](https://docs.internationaldataspaces.org/ids-knowledgebase/v/open-dei-building-blocks-catalog/)
zoals beschreven in het [OPEN DEI design principles position paper on
Resources](https://design-principles-for-data-spaces.org/).

In de [Blueprint van het Data Spaces Support Centre](https://dssc.eu/page/knowledge-base) zijn
de Governance Building Blocks aangepast ten opzichte van Open DEI. In de context van operationele
processen is het Building Block [Participation Management](https://dssc.eu/space/BVE/357074624/Participation+Management)
opgenomen.

## Operationele processen binnen het FDS

Binnen het FDS onderkennen we de volgende groepen operationele processen:

* [[Processen voor deelname aan het FDS als organisatie]]()
* Processen voor het inbrengen van datasets als aanbod (nog uit te werken)
* Processen voor het transparant rapporteren van gebruik (nog uit te werken)

In [[Processen voor deelname aan het FDS als organisatie]]() wordt het deelnameproces om als
aanbieder deel te nemen toegelicht (inclusief het inbrengen van aanbod).

## Standaarden

Er zijn geen aangewezen standaarden voor operationele processen. Een aantal best-practices om richting te geven aan
de inrichting van operationele processen is gebundeld in
[ITIL](https://nl.wikipedia.org/wiki/Information_Technology_Infrastructure_Library).
