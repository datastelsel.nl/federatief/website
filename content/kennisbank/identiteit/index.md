---
title: Identiteit
date: 2023-10-17
aliases: 
  - /kennisbank/capabilities/vertrouwen/identiteit
tags: 
description: >
  Vertrouwen | Identiteit
---

{{< capabilities-diagram selected="identiteit" >}}

## Beschrijving

De stelselfunctie Identiteit betreft het vermogen om de identiteit van bijvoorbeeld een systeem,
persoon of organisatie met afdoende zekerheid vast te stellen. Dit vindt plaats door gebruik te
maken van identificerende kenmerken (zoals een identificatienummer) en een identificatiemiddel
(zoals een digitaal versleuteld certificaat). Identificatiemiddelen worden gebruikt om de identiteit
van iets of iemand met zekerheid vast te kunnen stellen.

Identificerende kenmerken en identificatiemiddelen kunnen worden beheerd in een Identity Management
(IM) oplossing. Voorbeelden van open-source oplossingen zijn de Keycloak-infrastructuur, het Apache
Syncope IM-platform, het open-source IM-platform van het Shibboleth Consortium en het Fiware
IM-framework.

Het binnen Europa uitgeven van publieke identificatiemiddelen valt onder de
[eIDAS](https://eidas.ec.europa.eu/) regulering.

## Meer lezen

De stelselfunctie Identiteit is gebaseerd op het [Identity Management building
block](https://docs.internationaldataspaces.org/ids-knowledgebase/v/open-dei-building-blocks-catalog#data-sovereignty-and-trust-building-blocks)
zoals beschreven in het [OPEN DEI design principles position paper on
Resources](https://design-principles-for-data-spaces.org/).

In de [Blueprint van het Data Spaces Support Centre](https://dssc.eu/page/knowledge-base) wordt het
bewijzen van de identiteit met een [Verifiable Credential
(VC)](https://www.w3.org/TR/vc-data-model-2.0/) beschreven in het Building Block [Identity and
Attestation Management](https://dssc.eu/space/BVE/357075352/Identity+and+Attestation+Management).
Dit houdt verband met het toegekende vertrouwen aan een beweerde identiteit in het Building Block
[Trust Framework](https://dssc.eu/space/BVE/357075461/Trust+Framework).

Binnen de NORA is informatie te vinden over Identiteit onder 
[[nora:Identity & Access Management (IAM)]]()

## Identiteit binnen het FDS

Binnen het FDS vindt uitwisseling van gegevens plaats tussen een aanbieder en een afnemer. Bij een
uitwisseling binnen het FDS zijn zowel de afnemer als de aanbieder deelnemer van het FDS. Een
deelnemer van het FDS is een organisatie met een publieke taak. Een organisatie kan als deelnemer
worden toegelaten tot het FDS als deze is ingeschreven in het handelsregister als publiekrechtelijk
of privaatrechtelijke rechtspersoon. Daarbij dient een privaatrechtelijke rechtspersoon een
expliciet benoemde wettelijke taak te hebben. Zowel publiekrechtelijke als privaatrechtelijke
rechtspersonen zijn ingeschreven in het [handelsregister](https://www.kvk.nl/zoeken/).

Binnen het FDS kan de deelnemer de daadwerkelijke uitwisseling plaats laten vinden door een
verwerker. Een verwerker is een door de deelnemer aan te wijzen organisatie die de daadwerkelijke
uitwisseling uitvoert. Een verwerker is een Nederlandse rechtspersoon, maar hoeft geen deelnemer te
zijn aan het FDS en hoeft geen publieke taak te vervullen. Een verwerker wordt door de deelnemer
aangewezen in een overeenkomst. Het aanwijzen vindt plaats door het benoemen van een uniek identificerend
kenmerk dat kan worden vastgesteld bij het leggen van de verbinding voor de gegevensuitwisseling. 

Bij  het uitwisselen van gegevens dient over en weer betrouwbaar vastgesteld te kunnen 
worden wat de identiteit van de andere uitwisselende deelnemer is. Indien een verwerker optreed 
namens een deelnemer, dient daarbij zowel de identiteit van de verwerker als de identiteit van de 
deelnemer vastgesteld te kunnen worden. Een uitzondering hierop vormt Open Data waarbij de 
afnemer niet door de aanbieder wordt geïdentificeerd. Of, en in welke mate, binnen het FDS sprake
kan of moet zijn van het uitwisselen van data als anoniem benaderbare Open Data (bijvoorbeeld bij 
metadata) dient nog te worden bepaald in te maken afspraken.

Het deelnemen aan het FDS door een organisatie-onderdeel van een rechtspersoon is nog onderwerp van
onderzoek (bijvoorbeeld de Belastingdienst is een organisatie-onderdeel van de publiekrechtelijke
rechtspersoon Ministerie van Financiën).

## Identificerende kenmerken

Binnen het FDS toepasbare identificerende kenmerken voor een deelnemer zijn:

* het [KvK-nummer](https://www.kvk.nl/starten/kvk-nummer-alles-wat-je-moet-weten/)
* het [RSIN](https://www.kvk.nl/over-het-handelsregister/rsin-nummer/)
* het [OIN](https://www.logius.nl/domeinen/toegang/organisatie-identificatienummer), 
  eventueel gebaseerd op een KvK-nummer of RSIN.
* het FDS [[Deelnemer-ID]]().

De in een uitwisseling te hanteren identificerende kenmerken worden bepaald door de aanbieder.
Binnen het stelsel kunnen echter afspraken worden gemaakt om dit te beperken. Een
deelnemer kan binnen het FDS verschillende identificerende kenmerken hanteren, zolang ze maar
dezelfde organisatie identificeren. Bijvoorbeeld een RSIN, een KvK-nummer, een OIN gebaseerd op
een RSIN en een OIN gebaseerd op een KvK-nummer. Een deelnemer heeft altijd (ook) een
FDS [[Deelnemer-ID]](). Binnen het stelsel is een functie voorzien om vanuit
een identificerend kenmerk de [[Deelnemerkenmerken]]() op te vragen,
waaronder overige door de deelnemer gehanteerde identificerende kenmerken. 

Het faciliteren van verschillende identificerende kenmerken maakt het mogelijk om verschillende 
soorten identificatiemiddelen binnen het FDS toe te passen. Dit maakt ook bijvoorbeeld migratie 
naar nieuw opkomende identificatiemiddelen mogelijk. 

In een overeenkomst kunnen andere identificerende kenmerken worden toegepast om de deelnemer of een
door de deelnemer aangewezen verwerker te identificeren, zoals een hostname, public-key thumbprint
of een certificaat-thumbprint.

## Certificaten als Identificatiemiddel

Een deelnemer identificeert zich binnen het FDS met behulp van een identificatiemiddel.
Potentieel binnen het FDS toepasbare identificatiemiddelen bij het leggen van een verbinding zijn:

* een [PKIoverheid](https://www.logius.nl/domeinen/toegang/pkioverheid) Services [X.509
  certificaat](https://www.forumstandaardisatie.nl/open-standaarden/x509) met een OIN tbv
  [TLS](https://www.forumstandaardisatie.nl/open-standaarden/tls).  
* een [eIDAS QWAC](https://eidas.ec.europa.eu/efda/discover/qwac) [X.509
  certificaat](https://www.forumstandaardisatie.nl/open-standaarden/x509) met een OIN of KvK-nummer.

Momenteel worden vooral PKIoverheid certificaten gebruikt met een alleen binnen Nederland gehanteerde 
methode om een OIN op een certificaat op te nemen. Binnen Europa begint de generieke standaard 
[ETSI EN 319 412-1](https://www.etsi.org/deliver/etsi_en/319400_319499/31941201/01.05.01_60/en_31941201v010501p.pdf)
meer toegepast te worden. Deze methode is ook binnen PKIoverheid toegestaan. Mogelijk is een
migratie naar [ETSI EN 319 412-1](https://www.etsi.org/deliver/etsi_en/319400_319499/31941201/01.05.01_60/en_31941201v010501p.pdf) een gewenste beweging om binnen FDS te maken.

Een eIDAS QWAC certificaat wordt standaard vertrouwd door de browser. Voor toepassing binnen het FDS 
is het wel van belang dat het certificaat een geschikt identificerend kenmerk bevat (zoals het OIN of een 
Kvk-nummer). Hierdoor biedt een eIDAS QWAC certificaat mogelijkheden om content hybride
aan te bieden: content die vertrouwd machineleesbaar is, maar ook via een standaard browser 
te benaderen is. Dit maakt bijvoorbeeld mogelijk om 
[[Metadata]]() hybride (zowel interpreteerbaar door mensen als door machines)
aan te bieden via de [RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa) standaard. 

Mogelijk worden ook binnen PKIoverheid in de toekomst eIDAS QWAC certificaten opgenomen. 
[PKIoverheid](https://www.pkioverheid.nl/) kent een transparante besturing en programma van
eisen. Hoe dit is vormgegeven bij andere QWAC certificaten die hierbuiten vallen is nog
volop in beweging, en daarmee ook de mate van vertrouwen die eraan kan worden ontleend. 

Binnen het FDS is het mogelijk gegevens uit te wisselen onder een digitale (leverings)overeenkomst.
In een digitale (leverings)overeenkomst is aangegeven wie namens een afnemer of aanbieder gegevens
uitwisselt binnen het FDS. Daarbij kan eventueel een alternatief identificerend kenmerk zoals 
een hostname, public-key thumbprint of een certificaat-thumbprint worden toegepast om een deelnemer 
te identificeren. Zie hierover ook de stelselfunctie [[Veiligheid]]().

Mogelijke identificatiemiddelen voor het ondertekenen van digitale (leverings)overeenkomsten:

* een [PKIoverheid](https://www.logius.nl/domeinen/toegang/pkioverheid) Services [X.509
  certificaat](https://www.forumstandaardisatie.nl/open-standaarden/x509) met een OIN tbv het
  ondertekenen van elektronische documenten.
* een eIDAS [Qualified eSeal
  (QES)](https://ec.europa.eu/digital-building-blocks/sites/display/DIGITAL/eSignature+FAQ) [X.509
  certificaat](https://www.forumstandaardisatie.nl/open-standaarden/x509) met een OIN of KvK-nummer.

Een PKIoverheid Services certificaat tbv ondertekening van digitale documenten is altijd een 
eIDAS Qualified eSeal. Andersom is dit niet altijd het geval, er worden (Nederlandse) eIDAS 
Qualified eSeals uitgegeven die géén PKIoverheid certificaat zijn. 

Of bij het leggen van een verbinding, als een (leverings)overeenkomst wordt toegepast, naast 
[eIDAS QWAC](https://eidas.ec.europa.eu/efda/discover/qwac) of 
[PKIoverheid](https://www.logius.nl/domeinen/toegang/pkioverheid) Services 
[X.509 certificaat](https://www.forumstandaardisatie.nl/open-standaarden/x509) alternatieve,
minder vertrouwde certificaten kunnen worden ingezet is nog onderwerp van onderzoek. Dit
geldt ook voor het toepassen van het 
[ACME Protocol](https://digilab.overheid.nl/projecten/beproeving-acme-protocol/) voor
het geautomatiseerd afgeven van certificaten.

## Alternatieve identificatiemiddelen

Certificaten bieden een vertrouwde en beproefde methode om partijen te identificeren. Naast 
het toepassen van certificaten om te identificeren zijn er alternatieve 
methodes denkbaar:

* het toepassen van een API key om toegang te verlenen. 
* het toepassen van een Self-Souvereign Identity (SSI).

Een API key biedt een authenticatiemechanisme, maar omhelst nog geen standaard
methode om deze te koppelen aan een identiteit. Dit zou kunnen worden ondervangen door
bij het uitgeven gebruik te maken van bijvoorbeeld [eHerkenning](https://www.eherkenning.nl/nl)
om een identiteit te koppelen aan een API key. Deze toepassing biedt echter minder 
vertrouwenswaarborgen dan de toepassing van een certificaat. Of binnen het 
FDS gebruik gemaakt kan worden van API-keys (al dan niet voor bepaalde categorieën 
gegevens) is nog niet beoordeeld.

Bij Self-Souvereign Identities (SSI) controleert een deelnemer zelf zijn digitale identiteit. 
Dit principe wordt bijvoorbeeld toegepast bij de 
[EU Wallet](https://ec.europa.eu/digital-building-blocks/sites/display/EUDIGITALIDENTITYWALLET/What+is+the+wallet).
Naast wallets voor privé-personen worden in deze context ook wallets voor organisaties 
gereguleerd. Het [EDI programma](https://edi.pleio.nl/) houdt zich bezig met de
implementatie in Nederland.

Een voorbeeld van een veel toegepaste standaarden voor SSI is de 
[Decentralized Identifier (DID)](https://www.w3.org/TR/did-core/) in combinatie 
met [Verifiable Credentials (VC)](https://www.w3.org/TR/vc-data-model-2.0/) en
[OpenID for VC](https://openid.net/sg/openid4vc/). 
Binnen de DID standaard dient een (authenticatie)methode te worden geselecteerd
zoals [did:web](https://w3c-ccg.github.io/did-method-web/) waarbij de houder via
het internet een publieke sleutel ter beschikking stelt of 
[did:ebsi](https://hub.ebsi.eu/vc-framework/did/legal-entities)
waarbij de Europese publieke blockchain 
[EBSI](https://ec.europa.eu/digital-building-blocks/sites/display/EBSI/Home) wordt 
gebruikt om de houder te authenticeren. 

Alleen authenticeren met een DID is echter niet voldoende om een deelnemer binnen
het FDS te identificeren. Daarvoor dient de deelnemer een bewijs 
te presenteren met een aangewezen identificerend kenmerk zoals een KvK-nummer
of OIN, bijvoorbeeld als [Verifiable Credential (VC)](https://www.w3.org/TR/vc-data-model-2.0/). 
Dit bewijs dient te zijn afgegeven door een binnen het FDS vertrouwde partij. Het 
afgeven en presenteren van een [Verifiable Credentials (VC)](https://www.w3.org/TR/vc-data-model-2.0/)
vindt bijvoorbeeld plaats via [OpenID for VC](https://openid.net/sg/openid4vc/). Mogelijk 
kunnen de binnen het FDS te hanteren [[Deelnemerkenmerken]]()
zo worden opgezet dat ze als [did:web](https://w3c-ccg.github.io/did-method-web/) en/of
als [Verifiable Credentials (VC)](https://www.w3.org/TR/vc-data-model-2.0/) voor een
identificerend kenmerk kunnen dienen. 

Er is veel ontwikkeling op het gebied van Self-Souvereign Identities (SSI), in
het bijzonder als gevolg van de vernieuwing van eIDAS om 
[Wallets](https://ec.europa.eu/digital-building-blocks/sites/display/EUDIGITALIDENTITYWALLET/What+is+the+wallet) 
te reguleren. Een koploper hierbinnen is de ontwikkeling van het Europese 
digitale rijbewijs 
([NEN-ISO/IEC 18013-5](https://www.nen.nl/nen-iso-iec-18013-5-2021-en-288068)).

Deze ontwikkelingen bieden in de toekomst wellicht mogelijkheden om SSI ook
toe te passen binnen FDS. Dit vergt echter enerzijds dat er voldoende vertrouwen 
is in de veiligheid van de authenticatiemethode, en anderzijds dat er een infrastructuur 
van vertrouwde partijen ontstaat die een bewijs met een binnen het FDS 
bruikbaar identificerend kenmerk kunnen afgeven. 


## Standaarden

* [X.509](https://www.forumstandaardisatie.nl/open-standaarden/x509)
* [TLS](https://www.forumstandaardisatie.nl/open-standaarden/tls)
* [RDFa](https://www.forumstandaardisatie.nl/open-standaarden/rdfa)
* [Decentralized Identifier (DID)](https://www.w3.org/TR/did-core/)
* [Verifiable Credentials (VC)](https://www.w3.org/TR/vc-data-model-2.0/)
* [OpenID for VC](https://openid.net/sg/openid4vc/)
* [PKIoverheid](https://www.pkioverheid.nl/) via [Digikoppeling](https://www.forumstandaardisatie.nl/open-standaarden/digikoppeling)
