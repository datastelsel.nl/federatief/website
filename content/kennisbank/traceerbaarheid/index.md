---
title: Traceerbaarheid
date: 2023-10-17
aliases: 
  - /kennisbank/capabilities/interoperabiliteit/traceerbaarheid
tags: 
description: >
  Interoperabiliteit | Traceerbaarheid: Traceerbaarheid en herleidbaarheid
---

{{< capabilities-diagram selected="traceerbaarheid" >}}

## Beschrijving

De stelselfunctie Traceerbaarheid biedt de middelen voor tracering en tracking in het proces van 
dataverstrekking en dataverbruik/gebruik. Het biedt daardoor de basis voor een aantal belangrijke 
functies, van identificatie van de lijn van gegevens tot auditbestendige logboekregistratie van 
transacties. 

In de context van traceerbaarheid worden ook termen als lineage en provenance gebruikt. Deze drie
termen hebben subtiel verschillende betekenissen die allen betrekking hebben op de inzichtelijkheid
in de herkomst, totstandkoming en het gebruik van data. Traceerbaarheid richt zich op het volgen van
van gegevens door ketens, lineage richt zich meer op de bewerkingen die de data hebben ondergaan en
provenance richt zich meer op de betrouwbaarheid en authenticiteit van gegevens. Binnen FDS maken
we geen onderscheid tussen traceerbaarheid, lineage en provenance en vatten we elk van deze vormen
onder de stelselfunctie Traceerbaarheid.

Een standaard voor traceerbaarheid (gericht op provenance) is de [PROV](https://www.w3.org/TR/prov-overview/) 
familie van standaarden.

## Meer lezen

De stelselfunctie Traceerbaarheid is gebaseerd op het 
[Data Provenance and Traceability Building Block](https://docs.internationaldataspaces.org/ids-knowledgebase/open-dei-building-blocks-catalog#interoperability-building-blocks)
zoals beschreven in het [OPEN DEI design principles position paper on
Resources](https://design-principles-for-data-spaces.org/).

In de [Blueprint van het Data Spaces Support Centre](https://dssc.eu/page/knowledge-base) is
traceerbaarheid ondergebracht in het Building Block 
[Provenance and Traceability](https://dssc.eu/space/bv15e/766068145/Provenance+&+Traceability).

## Traceerbaarheid binnen FDS

Afnemers binnen het FDS maken gebruik van gegevens die ze verkrijgen via FDS om wettelijke taken
uit te voeren. Om te kunnen verantwoorden welke gegevens zijn gebruikt, dient uitwisseling binnen 
het FDS traceerbaar te zijn. Concreet kan dit betekenen dat voor een verantwoorde verwerking
nodig is dat:

* Inzichtelijk is wanneer welke gegevens zijn afgenomen en voor welke verwerking de afnemer deze 
gegevens heeft gebruikt. 
* De aanbieder inzichtelijk maakt hoe geleverde gegevens tot stand zijn gekomen en welke zekerheden 
er aan de gegevens zijn te ontlenen, bijvoorbeeld dat geboortegegevens van een persoon zijn ontleend 
aan een Nederlandse geboorteakte of aan een eigen verklaring door de betrokkene. 
* Inzichtelijk is welke eerder geleverde gegevens zijn gecorrigeerd. Indien gegevens bij een 
aanbieder worden gecorrigeerd, dient een afnemer effectief te kunnen bepalen of hij betreffende
gegevens heeft gebruikt in handelingen (in het bijzonder handelingen met rechtsgevolgen). De 
afnemer dient deze handelingen immers mogelijk te herzien. Zie ook notificeren onder de stelselfunctie 
[[Dataservices]]().
* Voor een betrokken persoon dient inzichtelijk te zijn welke (persoons)gegevens 
wanneer door een aanbieder aan welke afnemer is geleverd, in welke verdere verwerkingen de 
gegevens zijn gebruikt, voor welk doel (doelbinding) en onder welke 
[grondslag](https://www.autoriteitpersoonsgegevens.nl/themas/basis-avg/avg-algemeen/grondslagen-avg-uitgelegd).

De mate van het benodigde inzicht is afhankelijk van het soort verwerking. Voor welke termijn 
het genoemde inzicht beschikbaar dient te blijven is afhankelijk van het gebruik.
Aanbieder en afnemers stemmen dit met elkaar af daar waar ze van elkaar afhankelijk zijn. 
Mogelijk kunnen binnen FDS afspraken worden gemaakt om bijvoorbeeld gradaties of typeringen 
van traceerbaarheid te onderscheiden en/of de mate van traceerbaarheid te standaardiseren.

Daar waar sprake is van de inzet van een verwerker zoals benoemd in de stelselfunctie 
[Identiteit](../identiteit/#identiteit-binnen-het-fds) is dit inzichtelijk.

Voor traceerbaarheid benodigde gegevens worden, onder andere, geregistreerd in logboeken. 
Beschikbare standaarden voor deze logboeken zijn:

* [FSC logging](https://commonground.gitlab.io/standards/fsc/logging/draft-fsc-logging-00.html) als
transactielog
* [Standaard logboek dataverwerkingen](https://logius-standaarden.github.io/logboek-dataverwerkingen/)

Een centrale rol binnen de traceerbaarheid vormt de datadeelrelatie die wordt
gebruikt om de gegevens uit te wisselen:

| ![Traceerbaarheid binnen FDS](images/traceerbaarheid.png) |
| :-------------------------------------------------------: |
| _Traceerbaarheid binnen FDS_                              |

Om beschikbaar te hebben welke gegevens zijn gebruikt in een verwerking kunnen
afnemers een kopie bijhouden van door een aanbieder geleverde gegevens, bijvoorbeeld
in een berichtenlog. Veelal biedt een aanbieder niet de mogelijkheid om gegarandeerd 
exact eerder geleverde gegevens opnieuw op te kunnen vragen, bijvoorbeeld 
omdat er geen afdoende gegarandeerde bewaartermijn is of omdat na een correctie
eerder geleverde gegevens niet meer reproduceerbaar zijn. Indien daar behoefte
aan bestaat kunnen binnen FDS mogelijk afspraken worden gemaakt om inzichtelijk te 
maken of en hoe dataservices van de aanbieder kunnen worden ingezet om 
eerder geleverde gegevens te reproduceren.

## Standaarden

- [PROV](https://www.w3.org/TR/prov-overview/) 
- [FSC logging](https://commonground.gitlab.io/standards/fsc/logging/draft-fsc-logging-00.html)
- [Standaard logboek dataverwerkingen](https://logius-standaarden.github.io/logboek-dataverwerkingen/)
