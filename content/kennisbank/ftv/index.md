---
title: Federatieve Toegangsverlening
linkTitle: FTV
date: 2025-01-02
author: Team FTV
status: concept
description: >
  Het project Federatieve Toegangsverlening (FTV) werkt aan een standaard voor toegangsverlening in het FDS.
---

## Wat is toegangsverlening?

Toegangsverlening gaat over het controleren of verwerkingen toegestaan zijn, en onder welke
voorwaarden. Denk hierbij aan een gemeentemedewerker die met een zaaksysteem persoonsgegevens
opvraagt uit de Basisregistratie Personen (BRP) en daarin een aanpassing maakt. Dat mogen alleen
medewerkers van burgerzaken, met een geldige bevoegdheid, alleen vanaf een gemeentewerkplek, en
alleen van burgers die gevestigd zijn in de eigen gemeente.

Toegangsverlening, ook wel autorisatie, volgt op de processen van identificatie en authenticatie. In
deze processen worden zekerheden bepaald rondom 'wie ben jij'. Toegangsverlening maakt hier gebruik
van en bouwt daarop verder om te bepalen 'wat mag jij'.

## Waarom dit project?

Toegangsverlening bestaat natuurlijk al: er wordt nu ook gecontroleerd voordat gegevens verwerkt
worden. Wat dit project beoogt is:

* Een standaard werkwijze, de FTV-standaard (werktitel), beschrijven. Er is nu geen standaard,
  waardoor elk systeem zijn eigen methode kan bedenken of kiezen, hetgeen extra werk oplevert en het
  lastiger maakt om bij elkaar aan te sluiten.
* Toegangsregels expliciet maken. Waar deze nu veelal verborgen zijn in software worden deze in FTV
  op een standaardwijze buiten de software gebracht. De regels kunnen daardoor makkelijker
  geschreven, beheerd en gecontroleerd worden.
* Fijnmaziger toegang mogelijk maken. Door naast statische informatie zoals groepen en rechten ook
  dynamische informatie zoals apparaat en tijdstip mee te nemen kunnen preciezere regels
  geformuleerd worden.

## Scope en context

In het kader van het FDS richt FTV zich specifiek op machine-naar-machinecommunicatie, en minder op
mens-naar-machine.

FTV hangt nauw samen met twee andere standaarden: Federatieve Service Connectiviteit
([[FSC]]()) beschrijft hoe verbindingen tot stand komen en beveiligd worden,
en Logboek Dataverwerkingen ([LDV](https://minbzk.github.io/logboek-dataverwerkingen/)) waarmee de
toegestane verwerkingen vastgelegd worden als juridische verantwoording.

FTV zal een uitwerking zijn van bredere begrippen als externalized access management en Policy Based
Access Control (PBAC). Andere projecten die hieraan vooraf gingen zijn het position paper
[[Policy Based Access Control]]() en [[Lock-Unlock]]()

## Status

Het vooronderzoek is afgerond en er wordt nu geschreven aan de FTV-standaard. In deze fase is er
behoefte aan betrokkenheid van projecten en programma’s die voor uitdagingen staan rondom toegang.
Dat kan zijn in de vorm van ervaring delen, meeschrijven en pilot projecten die de standaard willen
beproeven.

## Links

* [FTV projectsite](https://federatieve-toegangsverlening-digilab-overheid-n-5d4b9badc9bcfa.gitlab.io/)
* [FTV-standaard](https://ftv-standaard-2f223b.gitlab.io/) (werktitel en in ontwikkeling)
* [[Federatieve Service Connectiviteit]]() ([[FSC]]())
* [[Logboek Dataverwerkingen]]() ([[LDV]]())
