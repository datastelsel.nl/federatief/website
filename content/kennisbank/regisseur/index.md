---
title: Regisseur
status: Voorstel
date: 2024-04-30
author: Kernteam FDS
version: v0.23
description: >
    Stelselfunctie voor de coördinatie van beheer en (door)ontwikkeling van het stelsel.
---

### Regisseur
De organisatorische [stelselfunctie](../stelselfuncties/) Regisseur:

- coördineert beheer en (door)ontwikkeling van het stelsel.

#### Status

Ontwikkelingen tijdens het programma Realisatie IBDS geven inzicht in de manier waarop de
Regisseur-functie structureel moet worden ingericht.

Zodra de tijd rijp is, wordt een inrichtingsvoorstel opgesteld en ter goedkeuring voorgelegd via de
Stelsel-governance.

#### Achtergrond

{{% TODO /%}}

#### FDS context & positionering

{{% TODO /%}}

#### Inhoudelijke verdieping

{{% TODO /%}}

#### Standaardisatie

{{% TODO /%}}
