---
title: Stelselrollen
weight: 7
status: Concept
date: 2025-03-06
author: Kernteam FDS
description: >
     Op hoofdlijnen hoe de rollen m.b.t. het aanbieden en afnemen van data
     binnen het Federatief Datastelsel zijn ingericht.
---

## Aanbod en vraag

FDS verbindt de vraag naar stelseldata met het aanbod ervan.

De vraag naar stelseldata heeft betrekking op data die is ingewonnen ten behoeve
van de uitvoering van een wettelijke taak. De inwinnende partij heeft in het
huidige stelsel van basisregistraties de rol van 'bronhouder'. FDS heeft geen
invloed op de organisatorische en procesmatige inrichting van het inwinnen en
bijhouden van de data. In de beschrijving van het [Data-ecosysteem van de
Nederlandse overheid](../datadeelconcept) positioneren we de bronhouder in de
domeinen waarbinnen de data wordt ingezameld en gebruikt ([paragraaf
'Datadomeinstelsel'](../datadeelconcept/#Datadomeinstelsel)).

FDS positioneren we als [stelsel van
stelsels](../datadeelconcept/#Stelsel-van-stelsels) waarbinnen data
domeinoverstijgend wordt aangeboden. Vanuit die rol stelt FDS [eisen aan de
data](../stelseldata/) die onderdeel van het stelselaanbod zijn of worden.

De primaire rollen in FDS zijn de data-aanbieder en data-afnemer. Aan de
primaire rollen koppelt FDS rol-specifieke taken, verantwoordelijkheden en
bevoegdheden. De toekenning hiervan is afgestemd op de specifieke doelen van FDS
en is alleen van toepassing op handelingen binnen het FDS afsprakenkader. Een
organisatie kan als deelnemer in het stelsel, zowel aanbieder zijn als afnemer.
FDS eist dat het altijd volkomen duidelijk is vanuit welke rol een
stelseldeelnemer handelt.

### Data-aanbieder

De data-aanbieder is een stelselrol die wordt ingevuld door een organisatie met
een [wettelijke taak](../scope-van-fds/#Wettelijke-grondslag). De data-aanbieder is
deelnemer in het stelsel. Hij maakt de data die tot het stelsel is toegelaten,
beschikbaar en bruikbaar door er dataservices op te ontwikkelen en aan te
bieden, zodat data-afnemers ze kunnen gebruiken. Deze rol is vergelijkbaar met
de rol van 'verstrekker' in het huidige stelsel van basisregistraties.

De organisatie die de rol vervult kan tegelijkertijd ook de rol van bronhouder
hebben, maar het kan ook een andere organisatie zijn. Voor FDS geldt het
uitgangspunt dat de data-aanbieder verantwoordelijk is voor een bepaald
data-aanbod, ongeacht of hij wel of niet de bronhouder is. De data-aanbieder
staat garant voor het waarmaken van de opgegeven specificaties van de
datadiensten (en de daarin vrijkomende data) die hij aanbiedt.

De data-aanbieder biedt de data aan op grond van een wettelijke taak. Hiermee is
het verantwoord datadelen vanuit het publiek belang juridisch afdwingbaar.

### Data-afnemer

De data-afnemer is een organisatie met een wettelijke taak, welke als deelnemer
is aangesloten op het stelsel. De data-afnemer creëert maatschappelijke waarde
door het toepassen van FDS- en niet-FDS-datadiensten voor het uitvoeren van zijn
publieke taak: het leveren van digitale diensten aan burgers, bedrijven en
overheden.

De data-afnemer moet kunnen aantonen dat hij de af te nemen gegevens binnen de
kaders van zijn wettelijke taak gebruikt.
