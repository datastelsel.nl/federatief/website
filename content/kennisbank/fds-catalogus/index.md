---
title: FDS Catalogus
description: Inzicht bieden in het aanbod van data en dataservices.
date: 2024-04-26
author: Kernteam FDS
---

## Achtergrond

De in- en overzichtfunctie is één van de stelselfuncties van het te realiseren Federatief
Datastelsel (FDS). Deze stelselfunctie maakt het mogelijk om vanuit verschillende perspectieven een
totaalbeeld te krijgen van de inhoud van het FDS: welke data conform het FDS-afsprakenkader wordt
gedeeld, waarvoor deze data kan worden gebruikt, welke partijen betrokken zijn bij dit gebruik en
hoe deze zich binnen de federatie tot elkaar verhouden.

## FDS context & positionering

Dit wordt mogelijk omdat is afgesproken dat alle componenten van het FDS (deelnemers, datasets en
datadiensten) van betekenisvolle beschrijvende gegevens (metadata) worden voorzien die op een
gestandaardiseerde manier openbaar beschikbaar worden gesteld. Hiermee ontstaat een virtuele open
(meta)dataset, een ‘catalogus’, die op verschillende manieren kan worden ontsloten en doorzocht.

Zo kan bijvoorbeeld worden opgezocht welke gegevens er in de basisregistratie WOZ zijn opgenomen,
wat de betekenis en de kwaliteit daarvan is en met welke datadiensten dit aanbod is ontsloten.
Hoewel de in- en overzicht functie op zich alleen zaken passief toont (overzicht biedt), levert dit
wel het inzicht dat nodig is om op een verantwoorde manier actief met het datapotentieel aan de slag
te kunnen gaan.  

Omdat de gekozen afspraken en standaarden het mogelijk maken om ‘alles met alles’ te verbinden en de
FDS-metadata zelf ook een volgens de FDS-standaarden ontsloten (virtuele) dataset is, kan iedereen
naar eigen behoefte met de open FDS-metadata aan de slag. Zo is het bijvoorbeeld mogelijk om een
catalogus te bouwen waarin wetgevingsjuristen eenvoudig kunnen zoeken of een bepaald begrip al in
bestaande FDS-data voorkomt, welke definitie het daar heeft en aan welke juridische grondslag (wet,
regeling) deze definitie is ontleend. Gebruikers die bijvoorbeeld met het thema energietransitie aan
de slag willen, kunnen op basis van daarmee verbonden trefwoorden zoeken welke data daarvoor in het
FDS beschikbaar is en of deze voor hen bruikbaar zijn (qua toegang, kwaliteit, betekenis etc.) Dat
zoeken kan in de eigen catalogus van het FDS, maar het is ook mogelijk om de FDS-metadatadiensten in
een eigen energiecatalogus te integreren die ook niet-FDS-data ontsluit om zo de gebruikers een
totaalbeeld van het energiedata-aanbod te geven. Omdat de FDS afspraken en standaarden voor het
ontsluiten van meta data het mogelijk maken om op begrippen te zoeken, ondersteunen ze ook het
oppakken van semantische harmonisatie en andere acties om verschillende databronnen beter op elkaar
te laten aansluiten en om wetgeving en data beter met elkaar te verbinden. Ze maken het namelijk
mogelijk om een overzicht te krijgen van begrippen met een verschillende naam, maar dezelfde
betekenis (synoniemen), van begrippen met dezelfde naam maar juist een verschillende betekenis
(homoniemen), of van begrippen die zijn opgebouwd uit een combinatie van een aantal andere begrippen
(concepten).  
De in- en overzichtfunctie is daarmee een belangrijk instrument voor het creëren van waarde met de
in het FDS opgenomen data.  

De in- en overzichtfunctie is ook vanuit een ander perspectief van belang. Omdat iedereen de
metadata kan raadplegen (het is immers open data), wordt de inhoud van het FDS volledig transparant,
waardoor dit ook onderwerp kan zijn van publiek debat. Dit is een bewuste keuze omdat bij het FDS de
publieke waarden centraal staan. Deze openheid betreft uiteraard alleen de beschrijvende data (de
metadata) en niet de data zelf. De individuele gegevens binnen de datasets zijn en blijven alleen
toegankelijk voor degenen die daartoe zijn geautoriseerd.  

## Beoogde werking

Voor het aanbieden van de metadata gelden dezelfde principes als voor andere FDS-data. Dit betekent
dat degene die data in het stelsel aanbiedt zelf de kenmerken van deze data beschrijft en zelf
diensten levert om deze metadata ‘bij de bron’ te kunnen bevragen. Door aan te sluiten bij gangbare
wereldwijde, Europese en nationale standaarden voor het generen, vastleggen en ontsluiten van
metadata wordt deze meervoudig bruikbaar. Data-aanbieders hoeven dit in principe slechts één keer te
doen om hun aanbod in alle voor hen relevante datastelsels (dataspaces) voor mensen en machines
vindbaar te maken. Het op het FDS toegesneden data-aanbod kan hieruit gedestilleerd worden door in
de metadata een ‘FDS-label’ op te nemen als in het aansluitproces is vastgesteld dat aan de
FDS-kwaliteitseisen wordt voldaan. Door voor dit labelen een stelseloverstijgende standaard af te
spreken, kunnen data-aanbieders op uniforme wijze in de metadata laten zien aan welke
afsprakenstelsels ze voldoen, dus in welke datastelsels hun data gebruikt kunnen worden. Met deze
aanpak wordt de variëteit in het datagebruik ondersteund en worden tevens groeipaden gefaciliteerd.
Open overheidsdata waarvan de metadata al wel op orde is, maar die bijvoorbeeld alleen nog maar als
download beschikbaar is, kan al meteen op data.overheid.nl worden aangeboden terwijl deze pas later,
nadat bijvoorbeeld door het toevoegen van een API het FDS-label is verdiend, ook in de FDS-catalogus
verschijnt.  

## Van visie naar werkelijkheid: het realisatietraject

In de kop van de vorige paragrafen werd het woord “beoogd” gebruikt. Dat illustreert dat er nog het
nodige te doen valt voordat we zover zijn. Soms betreft dit ontwikkeling omdat de benodigde metadata
standaarden er nog niet zijn, soms betreft dit het kiezen uit verschillende concurrerende
standaarden en soms is de standaard duidelijk maar moet deze nog op de Nederlandse situatie worden
toegespitst of moet er worden afgesproken hoe je de standaard precies gaat gebruiken.

Zelfs als dit alles achter de rug is, moet er nog ervaring worden opgedaan met het in onderlinge
samenhang toepassen van deze standaarden om het verlangde inzicht en overzicht op FDS-(totaal)niveau
te krijgen. Als elke FDS-deelnemer zelf het wiel uit moet vinden, zijn we vele jaren verder voordat
deze essentiële stelselfunctie voldoende bruikbaar is. Het FDS-realisatieproject zal daarom
ondersteuning bieden om de realisatie en implementatie te versnellen. Daarbij wordt de volgende
aanpak gehanteerd:  

- Vertrekpunt is het denkwerk dat al binnen de Nora-community is gedaan (zie de bijlage onderaan dit
  hoofdstuk).  

- Het FDS-project onderneemt in samenwerking met de verantwoordelijke beheerders van bestaande
  metadata voorzieningen en de (potentiële) data-aanbieders actie om de hierna genoemde
  voorzieningen verder te ontwikkelen. Dit als tijdelijke overbrugging naar de uiteindelijke
  oplossing van een op afspraken en standaarden gebaseerd federatief werkend virtueel geheel, met
  voor het FDS mogelijk nog een eigen ‘catalogus’ ingang om eenvoudig een overzicht te krijgen van
  het data aanbod dat aan de eisen van het FDS-afsprakenstelsel voldoet.

- Voor het beschrijven van datasets is dit nu data.overheid.nl. De relevante standaard hiervoor is
  DCAT.  

- Voor het beschrijven van de in data opgenomen gegevens is dit nu de stelselcatalogus. Relevante
  standaarden hiervoor zijn SKOS, MIM en de standaard voor het beschrijven van begrippen (SBB).

- Voor het beschrijven van dataservices (API’s) is dit developer.overheid.nl.  

- Voor nieuwe elementen zoals het beschrijven van de stelseldeelnemers en de kwaliteit van data en
  -diensten (het inhoud geven aan het FDS-label) ontwikkelt het FDS-project in samenwerking met
  kennisinstituten zoals Geonovum nieuwe standaarden die in het Digilab worden beproefd om te
  verifiëren of ze praktisch werkbaar zijn.  

- Het FDS zorgt voor het ontwikkelen van afspraken en standaarden voor het betekenisvol verbinden
  van de verschillende metadatabronnen, waarbij met in het Digilab te ontwikkelen
  referentietoepassingen beproefd wordt of deze op een werkbare manier het beoogde totaalbeeld
  opleveren. Hierbij wordt aansluiting gezocht bij proeven en pilots die voor de EU of sectorale
  dataspaces worden gedaan.  

- Als is aangetoond dat het werkt en werkbaar is, worden koplopers gezocht die de componenten van de
  FDS in- en overzichtfunctie voor (een deel van) hun aanbod willen implementeren. Hiermee wordt
  duidelijk wat er nodig is om het samenhangend geheel van bestaande en nieuwe meta data standaarden
  in de uitvoeringspraktijk toe te passen en komen er ervaringscijfers beschikbaar waarmee geraamd
  kan worden hoeveel tijd en geld er nodig is om ze stelselbreed toe te gaan passen.  

- De opgedane praktijkervaring wordt vervolgens benut om de formele besluitvorming over de
  opschaling in gang te zetten, met behandeling van de voorstellen in het Interbestuurlijk Data
  Overleg (IDO) en het Overheidsbreed Beleidsoverleg Digitale Overheid (OBDO) en een openbare
  consultatie van volgens de bestaande procedures van het Forum Standaardisatie.  

- Als de formele besluitvorming succesvol is afgerond zal het FDS-project de adoptie aanjagen door
  de stelselpartijen te ondersteunen bij de implementatie van de (nieuwe) metadata-afspraken en
  -standaarden. Hierbij kan gedacht worden aan het beschikbaar stellen van validators, het
  faciliteren van proefimplementaties en het delen van voorbeelden en best practices.

## Standaardisatie

In de NORA wordt gewerkt aan het beter in samenhang duiden van onderwerpen die nu afzonderlijk zijn
beschreven. Daartoe is in de NORA gebruikersraad van september 2023 een opzet besproken van
onderwerpen die raken aan het thema Semantiek en Gegevensmanagement.  
