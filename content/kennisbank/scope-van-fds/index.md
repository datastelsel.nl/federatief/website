---
title: Scope van FDS
weight: 3
status: Concept
date: 2025-03-06
author: Kernteam FDS
description: Het Federatief Datastelsel (FDS) richt zich op publieke gegevens
 uitwisseling op nationaal niveau, ingebed in Europese kaders.
---

FDS ontwikkelt zich tot een afsprakenstelsel dat het datapotentieel van
verschillende sector-, domein- of thematisch georiënteerde datastelsels in
overeenstemming met publieke waarden voor publieke doelen beschikbaar maakt.

## Stelsel van stelsels

Met de beschrijving van het Concept data-ecosysteem Nederlandse overheid
schetsen we de algemene context waarin het FDS zal functioneren als het
verbindende stelsel van datadomeinstelsels (DDS).

Het Stelsel van Basisregistraties kunnen we beschouwen als een samenstel van
DDS-en. Daarnaast kennen we een variëteit aan keten- en sectorale registraties
die we ook als (producten van) DDS-en kunnen beschouwen. Ook de ontwikkelingen
op dit gebied staan niet stil. Zo lopen er diverse initiatieven die zijn gericht
op ontwikkeling van nieuwe datadomeinstelsels, o.a. in domeinen als GEO,
mobiliteit, zorg, etc.

FDS is gericht op de uitwisselbaarheid (of interoperabiliteit) van data tussen
deze bestaande en zich ontwikkelende DDS-en. Hiermee voorkomt FDS dat de DDS-en
de nieuwe silo’s gaan worden.

Het realisatieprogramma (R-FDS) ontwikkelt daarvoor de (zachte) infrastructuur
van afspraken, standaarden en werkwijzen die de interoperabiliteit (of
uitwisselbaarheid) tussen de DDS-en en hun participanten moet waarborgen. FDS
vervult daarmee een brugfunctie en is het verbindende stelsel van stelsels.

De domeinoverstijgende datadeel-relaties worden ondersteund door adequate
beschrijving van de data (de metadata) waaronder de benoeming van de
sleutelgegevens die nodig zijn om gegevens uit verschillende registraties te
kunnen combineren.

## Wettelijke grondslag

Uitgewisselde gegevens binnen FDS moeten juridisch en ethisch verantwoord
meervoudig gebruikt kunnen worden in publiek belang. FDS borgt daarmee de
publieke waarden en biedt tegelijk de data-afnemers het vertrouwen en de
zekerheid dat zij hun publieke dienstverlening op data uit het stelsel kunnen en
mogen baseren. Data in het stelsel moet daarom een wettelijke grondslag hebben.

FDS is derhalve gericht op publieke databronnen en hergebruik van de data door
Nederlandse organisaties met een wettelijke taak. Met het oog op dat hergebruik
moet gezorgd worden dat stelseldata door afnemers voor hún wettelijke taken
gebruikt kunnen worden. Momenteel wordt onderzocht welke aanpassingen hiervoor
nodig zijn in wet- en regelgeving.
