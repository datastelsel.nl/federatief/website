---
title: Veiligheid
date: 2023-10-17
aliases: 
  - /kennisbank/capabilities/vertrouwen/veiligheid
tags: 
description: >
  Vertrouwen | Veiligheid: Vertrouwde uitwisseling
---

{{< capabilities-diagram selected="veiligheid" >}}

## Beschrijving

Deze stelselfunctie faciliteert de vertrouwde data-uitwisseling onder deelnemers, waardoor deelnemers in
een data-uitwisselingstransactie geruststellen dat die andere deelnemers echt zijn wie zij beweren
te zijn en dat zij voldoen aan gedefinieerde regels/overeenkomsten. Dit kan worden bereikt door
organisatorische maatregelen (bijv. certificering of geverifieerde referenties) of technische
maatregelen (bijvoorbeeld attestatie op afstand).

## Meer lezen

De stelselfunctie Veiligheid is gebaseerd op het [Trusted Exchange building
block](https://docs.internationaldataspaces.org/ids-knowledgebase/v/open-dei-building-blocks-catalog#data-sovereignty-and-trust-building-blocks)
zoals beschreven in het [OPEN DEI design principles position paper on
Resources](https://design-principles-for-data-spaces.org/).

In de [Blueprint van het Data Spaces Support Centre](https://dssc.eu/page/knowledge-base) wordt het
bewijzen van de identiteit met een [Verifiable Credential
(VC)](https://www.w3.org/TR/vc-data-model-2.0/) beschreven in het Building Block [Identity and
Attestation Management](https://dssc.eu/space/BVE/357075352/Identity+and+Attestation+Management).
Dit houdt verband met het toegekende vertrouwen aan een beweerde identiteit in het Building Block
[Trust Framework](https://dssc.eu/space/BVE/357075461/Trust+Framework).

Er zijn daarnaast diverse bronnen die informatie geven over eisen en maatregelen
met betrekking tot veilige uitwisseling van gegevens, bijvoorbeeld:

* [Baseline Informatiebeveiliging Overheid (BIO)](https://www.digitaleoverheid.nl/overzicht-van-alle-onderwerpen/cybersecurity/bio-en-ensia/baseline-informatiebeveiliging-overheid/)
* Het [Nationaal Cyber Security Centrum (NCSC)](https://www.ncsc.nl/),
zoals de [ICT-beveiligingsrichtlijnen voor webapplicaties](https://www.ncsc.nl/documenten/publicaties/2019/mei/01/ict-beveiligingsrichtlijnen-voor-webapplicaties)
* Het [Centrum Informatiebeveiliging en Privacybescherming (CIP)](https://www.cip-overheid.nl/),
zoals [grip op Secure Software Development (SSD)](https://www.cip-overheid.nl/productcategorieen-en-workshops/producten?product=Grip-op-SSD)
* [Nationaal innovatie centrum privacy-enhancing technologies (nicpet)](https://nicpet.pleio.nl/)

## Veiligheid binnen FDS

Binnen het FDS worden gegevens uitgewisseld over een beveiligde verbinding (zoals
[HTTPS](https://www.forumstandaardisatie.nl/open-standaarden/https-en-hsts)) waarbij zowel de
aanbieder als de ontvanger worden geïdentificeerd met behulp van een betrouwbaar
[identificatiemiddel](../identiteit/#identificatiemiddelen-binnen-het-fds).

Voor zowel de aanbieder als de ontvanger geldt dat deze een verwerker de daadwerkelijke uitwisseling
kan laten uitvoeren. Deze verwerker dient in dat geval een eigen
[identificatiemiddel](../identiteit/#identificatiemiddelen-binnen-het-fds) te gebruiken die
betreffende verwerker identificeert. Een verwerker hoeft geen deelnemer te zijn binnen FDS. Wel
dient betrouwbaar navolgbaar te zijn namens welke deelnemer de verwerker gegevens uitwisselt.
Daarnaast dient er een overeenkomst te bestaan waaruit onomstotelijk kan worden afgeleid dat de
deelnemer aangeeft dat de verwerker onder verantwoordelijkheid van de deelnemer gegevens mag
uitwisselen.

Of binnen het FDS ook informatie wordt uitgewisseld waarbij de afnemer anoniem kan blijven (Open
Data) is nog onderwerp van onderzoek. Indien gegevens worden aangeboden aan zich identificerende
afnemers, sluit dit niet uit dat deze aanbieder de gegevens ook als Open Data aanbiedt. Het is
echter nog onderwerp van onderzoek of dit aanbod als Open Data binnen het FDS kan vallen.

Het netwerk dat de aanbieder gebruikt om gegevens aan te bieden dient inzichtelijk zijn in de
[[Aanbod (metadata)|metadata van het aanbod]](). Een aanbieder kan binnen het FDS gegevens aanbieden
via het internet, via DigiNetwerk, of beide.

## Veiligheid met FSC

Bij [[FSC]]() worden zowel verzender als ontvanger geïdentificeerd met een digitaal
certificaat (mutual [TLS](https://www.forumstandaardisatie.nl/open-standaarden/tls)). Daarbij vindt
binnen [[FSC]]() uitwisseling plaats onder een digitaal contract. In dit contract zijn
identificerende kenmerken van beide uitwisselende partijen opgenomen, voor de aanroepende partij via
een 'public key thumbprint' en voor de aangeroepen partij via een 'hostname' van het API endpoint.

In de context van FDS wordt het FSC-contract digitaal ondertekend door zowel de aanbieder als de
afnemer. Dit identificeert de aanbieder en de afnemer op een betrouwbare manier. In het contract
zijn identificerende kenmerken opgenomen die (via de gebruikte certificaten) leiden tot het
betrouwbaar identificeren van de feitelijk uitwisselende partijen. Dit betreft de aanbieder of
afnemer zelf of de door de aanbieder of afnemer ingeschakelde verwerker.

| ![Veiligheid binnen FDS met FSC](images/veiligheid-fsc.png) |
| :---------------------------------------------------------: |
| _Veiligheid binnen FDS met FSC_                             |

## Standaarden

* [[FSC]]()
* [HTTPS](https://www.forumstandaardisatie.nl/open-standaarden/https-en-hsts)
* [TLS](https://www.forumstandaardisatie.nl/open-standaarden/tls)
