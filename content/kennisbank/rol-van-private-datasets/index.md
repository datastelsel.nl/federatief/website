---
title: Rol van private datasets
date: 2024-09-12
author: Kernteam FDS
status: Voorstel
description: >
  Wat is voor het FDS een private dataset 
---
Het Federatief Datastelsel (FDS) ontsluit alleen publieke datasets en geen private datasets.
Hieronder wordt uitgelegd wat hiervoor de reden is en wat precies het verschil tussen beide is.  

## Algemeen kader

Het FDS is een afsprakenstelsel dat voor de overheid het domeinoverstijgend (cross sectoraal)
datadelen faciliteert. Het FDS biedt de aangesloten overheidsafnemers toegang tot de data die ze
nodig hebben voor het uitvoeren van hun wettelijke taken. Kortom een stelsel van en voor de
overheid. Het FDS verschilt daarin van de EU datastelsels / dataspaces die zijn gericht op het
creëren van economische waarde: het mogelijk maken van een “data markt”, of het vereenvoudigen van
het economisch verkeer. Het FDS ondersteunt bovendien alleen het toepassen van het FDS-aanbod. Het
FDS bevat geen afspraken over de totstandkoming van daarvan: de inrichting van de relaties tussen de
(keten)partijen die betrokken zijn bij de inwinning en bijhouding.  
Vanwege deze kenmerken biedt het FDS alleen publieke datasets aan en valt het aanbieden van private
datasets buiten de scope van het FDS.  
Zie voor een uitleg van het verschil tussen private en publieke datasets de volgende hoofdstukken en
voor meer informatie over het FDS-kader [de pagina wat is het FDS](/kennisbank/wat-is-het-fds/).

## Wat is in de FDS-context een private dataset?

Een private dataset is een verzameling gegevens die voor rekening en risico van een private partij
is ingewonnen en waarvan een private partij daarom het eigendomsrecht kan claimen. Deze
data-eigenaar moet zich aan algemene kaders zoals de AVG houden, maar kan verder helemaal zelf
bepalen wat hij met zijn data doet en dus ook hoe hij deze aanbiedt. Hij kan zijn data onder een
standaard gebruikslicentie beschikbaar stellen, of hij kan met afnemers specifieke afspraken over
het beschikbaar stellen van de data maken. Deze afspraken zijn bij een private dataset de uitkomst
van een onderhandelingsproces tussen in formele zin gelijkwaardige partijen. De aanbieder is niet
verplicht om zijn data aan te bieden, de afnemer is niet verplicht om deze af te nemen. Wat ze
onderling willen regelen bepalen ze zelf en leggen ze in een overeenkomst vast. Bij datastelsels
waarin private data tussen private partijen wordt gedeeld vormen privaatrechtelijke contracten
tussen de betrokken stelseldeelnemers het formele kader voor de rechten en plichten van
data-aanbieders en data-afnemers  

## Wat is in de FDS-context een publieke dataset?

In een publiek datadeelstelsel zoals het FDS werkt het anders. Als hoeder van het publieke belang
hebben we als overheid de wet als instrument om zeggenschap over het datadelen af te dwingen. Met
wetgeving kunnen we de private data die we nodig hebben in het publiek domein trekken. Een voorbeeld
hiervan is de wettelijke verplichting voor netwerkbeheerders om gegevens over de ligging van hun
netwerk aan de door het Kadaster beheerde kabel- en leidingenregistratie te leveren. Zo kunnen we
als overheid ook gegevens die we niet zelf hebben ingewonnen (private gegevens) onderdeel van bij
wet ingestelde registratie maken. Het zijn daarmee publieke gegevens geworden. Bij het ontwerp van
het  het FDS is aangenomen dat we als overheid het wettelijk instrumentarium toepassen om relevante
data voor overheidsgebruik te verkrijgen. Via wet- en regelgeving kan vervolgens het delen worden
beperkt, of juist worden verruimd (aanmerken als open data) en kunnen er eisen worden gesteld aan in
de dataset opgenomen gegevens, zoals het verplicht toepassen van voor de overheid belangrijke
identifiers (zoals bsn's, kvk'nrs, bag-id's). Het FDS ontsluit dus datasets die op grond van een wet
worden ontsloten, waarbij in wetgeving de rechten en plichten van data-aanbieders en data-afnemers
zijn vastgelegd. Bij die datasets is er geen sprake van de voor private data zo kenmerkende
contractvrijheid.  
Belangrijk hierbij is dat het FDS-afsprakenkader alleen het domeinoverstijgend, datadelen tussen
overheden reguleert en niet de inwinning en bijhouding van de onder het FDS-regime aangeboden
datasets. Het is heel gebruikelijk dat een FDS data-aanbieder voor het vullen van een vanuit zijn
wettelijke taak aangeboden publieke dataset gebruik maakt van door private partijen ingewonnen
gegevens. Het inrichten van de daarvoor benodigde gegevensuitwisseling tussen private bronhouders en
de overheidsorganisatie die als FDS-data aanbieder optreedt, valt echter buiten het
FDS-afsprakenkader aangezien het hier sector/domeinspecifieke ketenprocessen betreft. Het reguleren
daarvan is de taak van het voor het domein verantwoordelijke beleidsdepartement.

## Rationale: waarom worden private, niet op grond van een wettelijke taak aangeboden, datasets buiten de FDS-scope gehouden?

Privaat aanbod faciliteren vergroot de complexiteit en leidt tot extra kosten:

- Bij het ontbreken van een wettelijke taak kan het onder het FDS-afsprakenstelsel ontsluiten van de
  private dataset als een marktactiviteit worden beschouwd waarvoor de gedragsregels uit de Wet
  Markt en Overheid gelden. In dat geval is de FDS-data-aanbieder bijvoorbeeld verplicht om alle met
  de datadienst gemoeide kosten,inclusief BTW, aan de FDS data-afnemers door te berekenen.
- Bij datadelen op grond van een publieke taak kan conformiteit met het FDS-afsprakenkader vanuit de
  overheid verplicht worden gesteld. Daarmee worden rechten en plichten van data-aanbieders en
  data-afnemers overkoepelend geregeld. Bij een zuiver privaatrechtelijk aanbod zal elke
  data-afnemer zelf een contract met de data-aanbieder moeten afsluiten om de betreffende data te
  kunnen afnemen.
- Vanuit governance perspectief is een privaatrechtelijk gereguleerde ontsluiting ook ingewikkeld.
  Er zijn dan geen beleidsopdrachtgevers die als stelselregisseur sturend kunnen optreden en
  gezamenlijk bindende generieke stelselafspraken kunnen maken, bijvoorbeeld over het toepassen van
  een nieuwe standaard of over de verrekening van de kosten. Zonder wettelijk basis kunnen private
  data-aanbieders ook niet aan het stelseltoezicht worden onderworpen.
- Bij een privaat, niet wettelijk gereguleerd, aanbod, zijn er ook meer continuïteitsrisico’s.
  Contracten zijn eindig en de aanbiedende partij kan er mee stoppen als het voor hem niet meer
  aantrekkelijk is en als de private partij failliet gaat, houdt het sowieso op.

Er wordt vooralsnog van uitgegaan dat het meeste data-aanbod dat gekwalificeerd is voor verantwoord
meervoudig, cross sectoraal datadelen voor het uitvoeren van overheidstaken voortkomt uit een door
de overheid gereguleerde registratie en dat de restgroep van zuiver privaat aanbod te klein is om de
meerkosten van opname in het FDS te rechtvaardigen.

Deze scopebeperking heeft overigens alleen betrekking op het data-aanbod vanuit het FDS (de
datasets). De interoperabiliteitsafspraken en -standaarden die in het FDS worden toegepast zijn ook
bruikbaar voor het binnen sectoren/ketens delen van (private) datasets die niet aan de
FDS-deelnamevoorwaarden voldoen.
