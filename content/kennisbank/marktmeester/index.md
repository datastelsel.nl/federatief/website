---
title: Marktmeester
status: Voorstel
date: 2024-04-30
author: Kernteam FDS
version: v0.23
description: >
    Stelselfunctie voor het verbinden van vraag en aanbod. Ondersteunt bij het inzichtelijk 
    maken van het potentieel en het realiseren van nieuwe gebruikerswensen. 
---

### Marktmeester
De organisatorische [stelselfunctie](../stelselfuncties/) Marktmeester:

- verbindt vraag en aanbod van data voor sectoroverstijgend, 
  meervoudig gebruik.
- Helpt het gebruikspotentieel van het Federatief Datastelsel inzichtelijk te maken en ondersteunt
  stelselgebruikers bij het inbrengen van hun eisen en wensen omtrent data voor maatschappelijke
  opgaven.

#### Status

- Er is een inrichtingsvoorstel gemaakt voor de invulling van de Marktmeester functie tijdens de
  projectfase waarbij er een centraal aanspreekpunt is. Dit voorstel is besproken in het [Tactisch
  Stelseloverleg van 30 mei
  2023](https://realisatieibds.nl/groups/view/0056c9ef-5c2e-44f9-a998-e735f1e9ccaa/federatief-datastelsel/wiki/view/27e82cd2-669b-45e2-8a29-59d192141128/agenda-notulen-tactisch-overleg-30-mei).

- Met deze tijdelijke invulling wordt de komende tijd ervaring opgedaan in samenwerking met het
  project Data in maatschappelijke opgaven. Deze praktijkervaring wordt gebruikt om later een
  voorstel voor de definitieve inrichting van deze stelselfunctie te doen.

#### Achtergrond

{{% TODO /%}}

#### FDS context & positionering

{{% TODO /%}}

#### Inhoudelijke verdieping

{{% TODO /%}}

#### Standaardisatie

{{% TODO /%}}
