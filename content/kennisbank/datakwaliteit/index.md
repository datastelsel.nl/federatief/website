---
title: Afspraken datakwaliteit
date: 2025-02-25
author: kernteam FDS met werkgroep datakwaliteit, leden Tactisch Overleg
status: Concept
version: v0.92
description: >
  Op deze pagina gaan we specifiek in op de afspraken die betrekking hebben op kwaliteit van de data.
---

## 1. Aanleiding

Het stelsel van basisregistraties wordt doorontwikkeld tot een Federatief Datastelsel. Dit is een
afsprakenstelsel waarin afspraken vastgelegd worden. In dit document gaan we specifiek in op de
afspraken die betrekking hebben op de kwaliteit van de data. Deze zijn gebaseerd op het geleerde
vanuit het stelsel van basisregistraties aangevuld met hetgeen hierover sindsdien in NORA-verband is
uitgewerkt.

Deze afspraken gaan over zowel de kwaliteit van de data, de metadata als de dataset en worden in de
afspraken benoemd als ‘data’ Uit de afspraak kan afgeleid worden of het om de volledige dataset of
een specifiek onderdeel van de dataset gaat.  

Opgemerkt wordt dat de kwaliteit van de services, die zorgen dat de data gedeeld worden op een
verantwoorde wijze, buiten scope van dit document vallen. Deze wordt behandeld in een separaat
traject.  

Deze afspraken zullen in de toekomst worden uitgebreid met onderwerpen die nog uitgewerkt moeten
worden, bijvoorbeeld het toezicht op de datakwaliteit binnen het stelsel.

Er worden in deze kwaliteitsafspraken geen afspraken gemaakt over collectieve normen op het stelsel
of verhogen van kwaliteit in het stelsel omdat deze vaak des sectors zijn.

## 2. Doel

Doel van dit document is tweeledig:  

1. Vastleggen van de FDS-afspraken over datakwaliteit;

2. Duiding geven aan de veranderingen ten opzichte van de kwaliteitsvoorschriften in het stelsel van
   basisregistraties.  

Zodra deze nieuwe afspraken vastgesteld zijn, kan de Baseline kwaliteitszorg Stelsel van
Basisregistraties, waarin de afspraken momenteel vastgelegd zijn, vervallen.

## 3. Essentie

De kwaliteit van data gebruikt in (overheids)processen is bepalend voor de kwaliteit van de
dienstverlening.  

De tijdgeest heeft het belang van datakwaliteit voor breder gebruik en dienstverlening een grote
impuls gegeven. Daar waar er bij de start van het stelsel van basisregistraties een aantal
(kwaliteits)processen en functies verplicht ingericht moesten worden, kan er in deze tijd meer
uitgegaan worden van een algemeen en breder belang van goede kwaliteit waar partijen, graag aan
bijdragen en ieder passend bij hun positie haar verantwoordelijkheid kent en neemt.  

Het belangrijkste uitgangspunt in dit beleid is gebaseerd op het streven dat de datakwaliteit
geschikt moet zijn voor bovensectoraal gebruik (fit for purpose). Voor de juiste balans tussen vraag
naar kwaliteit door een data-afnemer en het bieden van kwaliteit door een data-aanbieder moeten een
tweetal functies worden ingericht:

1. Inzicht en transparantie over datakwaliteit Daarmee zijn de data-afnemers in staat een goede
   inschatting te maken in hoeverre het aanbod geschikt is voor gebruiksbehoeften en doelen.

2. Behouden en verbeteren van datakwaliteit

Daarmee zijn de data-afnemers in staat het gebruik blijvend te baseren op het aanbod. Ook wordt
hiermee duidelijk op welke wijze gewenste verbeteringen van de datakwaliteit kenbaar gemaakt en
opgepakt worden.

## 4. Rollen, taken en verantwoordelijkheden

In het basisconcept FDS is een data-aanbod- en datavraagkant beschreven. Deze worden verbonden door
stelselmechanismen.

Voor datakwaliteit zijn de volgende rollen van belang, deze zijn niet allemaal benoemd in het
basisconcept:

1. Bronhouder: een partij of een samenwerkingsverband van partijen die verantwoordelijk is voor het
   inwinnen,  bijhouden en consolidateren van de data en de borging van de kwaliteit daarvan (deze
   rol ligt buiten het basisconcept); gegevensuitwisseling verloopt tussen de data-aanbieder en
   data-afnemer. Deze rol is vergelijkbaar met die uit het stelsel van basisregistraties.

2. Data-aanbieder: is verantwoordelijk voor het ontsluiten van aangeboden data middels dataservices
   en het volgens de FDS-afspraken en -standaarden publiceren van de te verwachten en feitelijk
   gerealiseerde datakwaliteit. Deze rol lijkt grotendeels op de rol verstrekker uit het stelsel van
   basisregistraties.

3. Data-afnemer: is verantwoordelijk voor het juiste gebruik en het bij de data-aanbieder melden van
   kwaliteitsissues (‘terugmelden’). Deze rol is vergelijkbaar met die van afnemer uit het stelsel
   van basisregistraties.

4. Beleidsopdrachtgever voor de dataset: is verantwoordelijk voor de formele vaststelling van de
   kwaliteitsspecificaties van de aangeboden data en de eisen voor monitoring en rapportage. Deze
   rol zorgt ervoor dat daarbij de FDS-afspraken en eisen worden meegenomen. Deze rol is
   vergelijkbaar met die uit het stelsel van basisregistraties.  

5. Toezichthouder van de dataset: houdt toezicht op de kwaliteitseisen zoals benoemd in wet- en
   regelgeving van deze dataset/sector.  

6. Toezichthouder FDS: houdt toezicht op het Federatief Datastelsel als geheel. Deze rol wordt nog
   uitgewerkt en zal zich met betrekking tot de kwaliteitsafspraken richten op nakoming van de
   afspraken benoemd in dit document.

Toegelicht met een voorbeeld:

Dit betekent bijvoorbeeld voor het delen van data over civieltechnische kunstwerken in de waterketen
dat de rollen als volgt ingevuld worden:

| Rol                     | Invulling           |
| ----------------------- | ------------------- |
| Bronhouders:            | 21 Waterschappen    |
| Data-aanbieder:         | Het Waterschapshuis |
| Data-afnemer:           | Provincie Y         |
| Beleidsopdrachtgever:   | Ministerie I&W      |
| Toezichthouder dataset: | Het Waterschapshuis |

Organisaties kunnen zowel de rollen bronhouder als data-aanbieder hebben afhankelijk van de
inrichting van taken voor een bepaalde dataset. Daarnaast kan deze organisatie ook een data-afnemer
zijn.  

De afspraken over kwaliteit zoals in dit document beschreven, worden vooralsnog jaarlijks
geëvalueerd met de werkgroep Datakwaliteit die advies uitbrengt aan het Tactisch Overleg. Op termijn
gaan deze afspraken over in de datafederatie overkoepelende organisatorische stelselrollen
‘toezichthouder’ en ‘regisseur’.  

## 5. Afspraken datakwaliteit gewenste situatie

Voor het inzicht in de kwaliteit en de kwaliteitsborging van data in het FDS worden afspraken op het
niveau van het stelsel gemaakt. Deze zijn hierna per onderwerp beschreven.

### 5.1. Inzicht en transparantie datakwaliteit

Data-afnemers krijgen op een uniforme wijze inzicht in de datakwaliteit van het data-aanbod waarmee
ze in staat worden gesteld om invulling te geven aan het juiste gebruik. De data-aanbieder is
verantwoordelijk voor de publicatie.

#### Uniformering beschrijving

De datakwaliteit voor FDS wordt eenduidig beschreven zodat het voor
een data-afnemer duidelijk is om te bepalen wat dit voor zijn toepassing betekent, bijvoorbeeld als
data van verschillende aanbieders gecombineerd wordt of om eisen te stellen aan verbeterde
datakwaliteit. De standaard om dit op een uniforme wijze te doen is het toepassen van het
NORA-raamwerk Gegevenskwaliteit.

De afspraken die het Tactisch Overleg hierover op 2 juli 2024 geaccordeerd heeft zijn:

1. Data-aanbieders gebruiken in externe communicatie de terminologie uit het NORA-kwaliteitsraamwerk
   voor zover relevant voor de dataset.
2. Data-aanbieders passen in externe communicatie zoveel als mogelijk de door de werkgroep
   Datakwaliteit ontwikkelde zinsjablonen toe, voor zover relevant en toepasbaar op de dataset.  

Als minimumeis gaat het om de kwaliteitsdimensies Compleet, Juistheid en Actualiteit. Daarnaast
worden waar relevant en mogelijk de andere (voorgeschreven) dimensies en/of attributen binnen de
dimensies beschreven. Hierbij wordt in inzicht gegeven in zowel de gerealiseerde kwaliteit als de
streefnorm.  

*Voorbeelden van enkele basisregistraties zijn te vinden op de website van
[NORA](https://www.noraonline.nl/wiki/Voorbeelden_Raamwerk_Gegevenskwaliteit). De tabel met
dimensies, attributen* *en sjabloonzinnen zijn vindbaar op deze
[pagina](https://www.noraonline.nl/wiki/Raamwerk_gegevenskwaliteit#Tabel_met_alle_attributen)*

#### Transparantie datakwaliteit, rapportage

De rapportagefrequentie waarbij inzicht wordt gegeven in
de kwaliteit is afhankelijk van de dataset, het soort gegeven en eerdere kwaliteit.  

- Over identificerende gegevens in het stelsel met betrekking tot personen, organisaties en locaties
wordt jaarlijks gerapporteerd. Dit sluit aan op de frequentie zoals deze in het Stelsel van
basisregistraties in gebruik is.

- Voor gegevens met een koppeling met identificerende gegevens over personen, organisaties en
locaties wordt de kwaliteit jaarlijks gerapporteerd. Wanneer de gerealiseerde kwaliteit structureel
meerdere jaren zeer hoog is mag volstaan worden met elke 3 jaar. Jaarlijks rapporteren sluit aan op
de huidige praktijk, het minder frequent rapporteren is nieuw ten opzichte van het stelsel van
basisregistraties en is enkel bedoeld als verlichting van de werkzaamheden.

- Voor gegevens zonder koppeling naar identificerende gegevens over personen, organisaties en
 locaties worden vanuit FDS geen verplichtende afspraken gemaakt. Dat ligt binnen de scope van de
 sector, waarbij het aan de data-aanbieder en data-afnemer te bepalen is wat gepast en wenselijk is.

- Datasets die nieuw deelnemen in het federatief datastelsel geven bij toetreding inzicht in de
  datakwaliteit van de dataset met nadruk op de koppeling met identificerende gegevens van personen,
  organisaties en locaties.

Jaarlijks worden de resultaten stelselbreed gebundeld en gepubliceerd. Het ministerie van BZK heeft
hierin een coördinerende rol.  

### 5.2. Behouden en verbeteren datakwaliteit

Met de hiervoor opgenomen afspraken over uniformering van de beschrijving wordt op uniforme wijze
inzicht gegeven in de normen, beschrijvingen en behaalde resultaten. Deze kwaliteit moet behouden
blijven en waar mogelijk verbeterd worden. Hiervoor zijn de hierna volgende afspraken gemaakt
waardoor afnemers kunnen vertrouwen op de kwaliteit en bekend zijn met de wijze waarop gewenste
verbeteringen en de afwegingen daarvoor gemaakt worden.

#### Bepalen norm kwaliteitscriteria

Data-afnemers doen voorstellen voor (aanpassing van) de beoogde
kwaliteitscriteria van de dataset.  De beleidsopdrachtgever stelt in overleg met de andere betrokken
rollen (bronhouder(s) en data-aanbieder) de normen vast en ‘organiseert’ de haalbaarheid van de
nakoming.

*Dit kan bijvoorbeeld door een gebruiksoverleg te voeren. Andere manieren van afstemming zijn
mogelijk.*

#### Monitoring op de kwaliteitsborging d.m.v. periodieke controles en gebaseerd op proportionaliteit

Met betrekking tot datakwaliteit is de bronhouder de partij die verantwoordelijk is voor de
kwaliteit van de data. Zij bewaakt en monitort deze, waarbij ze gebruik kan maken van door de
data-aanbieder beschikbaar gestelde signalen, analyses en rapportages.

*Dit kan bijvoorbeeld door op basis van patronen in terugmeldingen of geconstateerde omissies de
volledige dataset hierop te bevragen of door (gerichte) bestandscontroles of kwaliteitsmetingen,*
*al dan niet met een ketenpartner. Uitgangspunt hierbij is een sterker collectief stelsel. Daarnaast
kunnen audits als instrument ingezet worden (geen verplichting).*

#### Verbeteren van datakwaliteit middels terugmelden gerede twijfel over de juistheid van een gegeven in de dataset

Afnemers en gebruikers kunnen bij toepassing van de data vermoeden en/of concluderen
dat data in de datasets niet juist zijn en geven hierover een signaal aan de data-aanbieder om de
kwaliteit van de datasets hoog te houden. Dit doen zij middels een terugmelding gerede twijfel over
de juistheid van een gegeven (hierna terugmelding). Ook burgers en ondernemers kunnen een melding
doen dat hun gegevens niet juist zijn.  
De afspraken over terugmelden zijn:

- Data-aanbieders bieden de (technische) mogelijkheid aan data-afnemers en data-gebruikers om
  terugmeldingen te kunnen doen. ​Hoe zij dit doen mogen de data-aanbieders zelf bepalen.  

- Data-afnemers doen een terugmelding bij gerede twijfel over de juistheid van een gegeven; het is
  afhankelijk van de geldende wet- en regelgeving of dit verplicht is voor deze dataset.  

- Een terugmelding wordt gedaan bij de data-aanbieder die de data heeft geleverd. Deze levert de
  melding door aan de bronhouder.  

- De bronhouder maakt voor afnemers inzichtelijk dat er een terugmelding is gedaan; het is
  afhankelijk van afspraken in de governance danwel wet- en regelgeving op welke wijze daar
  invulling aan gegeven wordt.  

- Bronhouders borgen dat de terugmeldingen binnen redelijke termijn afgehandeld worden.​

- De data-aanbieder rapporteert in afstemming met de bronhouder(s) het aantal signaleringen en de
  afhandeltermijnen.

- De data-aanbieder en bronhouder maken afspraken welke partij de terugmeldingen op oorzaken en
  patronen analyseert, zodat het mogelijk is procesafspraken ter verbetering van minder goed lopende
  processen op te kunnen stellen en daarmee de kwaliteit van het stelsel te behouden en verbeteren.​

- Burgers en ondernemers die fouten constateren in hun eigen gegevens geven dit door op de wijze die
  door de data-aanbieder is aangegeven. Dit betreft het uitvoering geven aan het correctierecht dat
  in de AVG is opgenomen.

## Bijlage 1

Samenvatting afspraken en standaarden datakwaliteit

### Doel: Inzicht en transparantie over datakwaliteit

Daarmee zijn de data-afnemers in staat een
goede inschatting te maken in hoeverre het aanbod geschikt is voor gebruiksbehoeften en doelen.

#### Subdoel: Uniformering beschrijving

Afspraken:

- Data-aanbieders beschrijven in externe communicatie de terminologie uit het [NORA-
  kwaliteitsraamwerk](https://www.noraonline.nl/wiki/Voorbeelden_Raamwerk_Gegevenskwaliteit) voor
  zover relevant voor de dataset.
- Data-aanbieders passen in externe communicatie zoveel als mogelijk de door de werkgroep
  Datakwaliteit ontwikkelde
  [zinsjablonen](https://www.noraonline.nl/wiki/Raamwerk_gegevenskwaliteit#Tabel_met_alle_attributen.)
  toe, voor zover relevant en toepasbaar op de data- set.
- Dit geldt minimaal voor de kwaliteitsdimensies Compleet, Juistheid en Actualiteit. Waar relevant
  en mogelijk worden ook de andere (voorgeschreven) dimensies en/of attributen binnen de dimensies
  beschreven.  
- Er wordt voor zover mogelijk en relevant inzicht gegeven in zowel de gerealiseerde kwaliteit als
  de streefnorm.

#### Subdoel: Transparantie datakwaliteit, rapportage

De rapportagefrequentie waarbij inzicht wordt
gegeven in de kwaliteit is afhankelijk van de dataset, het soort gegeven en eerdere kwaliteit.  

Afspraken:

- Over identificerende gegevens in het stelsel met betrekking tot personen, organisaties en locaties
  wordt jaarlijks gerapporteerd. Dit sluit aan op de frequentie zoals deze in het Stelsel van
  basisregistraties in gebruik is.
- Voor gegevens met een koppeling met identificerende gegevens over personen, organisaties en
  locaties wordt de kwaliteit jaarlijks gerapporteerd. Wanneer de gerealiseerde kwaliteit
  structureel meerdere jaren zeer hoog is mag volstaan worden met elke 3 jaar. Jaarlijks rapporteren
  sluit aan op de huidige praktijk, het minder frequent rapporteren is nieuw ten opzichte van het
  stelsel van basisregistraties en is enkel bedoeld als verlichting van mogelijk administratieve
  lasten.
- Voor gegevens zonder koppeling naar identificerende gegevens over personen, organisaties en
  locaties worden vanuit FDS geen verplichtende afspraken gemaakt. Dat ligt binnen de scope van de
  sector, waarbij het aan de data-aanbieder en data-afnemer te bepalen is wat gepast en wenselijk
  is.
- Datasets die nieuw deelnemen in het federatief datastelsel geven bij toetreding inzicht in de
  datakwaliteit van de dataset met nadruk op de koppeling met identificerende gegevens van personen,
  organisaties en locaties.

Jaarlijks worden de resultaten stelselbreed gebundeld en openbaar gepubliceerd. Het ministerie van
BZK heeft hierin een coördinerende rol.  

### Doel: Behouden en verbeteren van datakwaliteit

Daarmee zijn de data-afnemers in staat het
gebruik blijvend te baseren op het aanbod. Ook wordt hiermee duidelijk op welke wijze gewenste
verbeteringen van de datakwaliteit kenbaar gemaakt en opgepakt worden.

#### Subdoel: Bepalen norm kwaliteitscriteria

Afspraken:

- Data-afnemers doen voorstellen voor (aanpassing van) de beoogde kwaliteitscriteria van de dataset.
- De beleidsopdrachtgever stelt in overleg met de andere betrokken rollen (bronhouder(s) en
  data-aanbieder) de normen vast en ‘organiseert’ de haalbaarheid van de nakoming.

#### Subdoel: Monitoring op de kwaliteitsborging d.m.v. periodieke controles en gebaseerd op proportionaliteit

Afspraken: De bronhouder bewaakt en monitort de datakwaliteit, waarbij ze gebruik
kan maken van door de data-aanbieder beschikbaar gestelde signalen, analyses en rapportages.

#### Subdoel: Verbeteren van datakwaliteit middels terugmelden gerede twijfel over de juistheid van een gegeven in de dataset

Afspraken:

- Data-aanbieders bieden de (technische) mogelijkheid aan data-afnemers en data-gebruikers om
  terugmeldingen te kunnen doen. ​ Hoe zij dit doen mogen de data-aanbieders zelf bepalen.
- Data-afnemers doen een terugmelding bij gerede twijfel over de juistheid van een gegeven; het is
  afhankelijk van de geldende wet- en regelgeving of dit verplicht is voor deze dataset.
- Een terugmelding wordt gedaan bij de data-aanbieder die de data heeft geleverd. Deze levert de
  melding door aan de bronhouder.  
- De bronhouder maakt voor afnemers inzichtelijk dat er een terugmelding is gedaan; het is
  afhankelijk van afspraken in de governance danwel wet- en regelgeving op welke wijze daar
  invulling aan gegeven wordt.  
- Bronhouders borgen dat de terugmeldingen binnen redelijke termijn afgehandeld worden.
- De data-aanbieder rapporteert in afstemming met de bronhouder(s) het aantal signaleringen en de
  afhandeltermijnen.
- De data-aanbieder en bronhouder maken afspraken welke partij de terugmeldingen op oorzaken en
  patronen analyseert, zodat het mogelijk is procesafspraken ter verbetering van minder goed lopende
  processen op te kunnen stellen en daarmee de kwaliteit van het stelsel te behouden en verbeteren.​
- Burgers en ondernemers die fouten constateren in hun eigen gegevens geven dit door op de wijze die
  door de data-aanbieder is aangegeven. Dit betreft het uitvoering geven aan het correctierecht dat
  in de AVG is opgenomen.

## Bijlage 2

### Beleid huidige situatie (achtergrond)

Zoals aangegeven vervalt de baseline kwaliteitszorg Stelsel van Basisregistraties na het vaststellen
van de nieuwe afspraken. Deze zijn hier enkel vermeld om verschillen te duiden.

### Baseline kwaliteitszorg Stelsel van Basisregistraties

In de [baseline kwaliteitszorg Stelsel van
Basisregistraties](https://www.digitaleoverheid.nl/overzicht-van-alle-onderwerpen/stelsel-van-basisregistraties/kwaliteit-en-terugmelden/#:~:text=Een%20overzicht%20van%20bestaande%20afspraken%2C%20spelregels%20en%20instrumenten,borgen%20is%20beschreven%20in%20de%20Baseline%20Kwaliteitszorg%20Basisregistraties.)
zijn de afspraken binnen het stelsel gebundeld. Deze baseline is enkele jaren geleden opgesteld met
als doel een handzaam overzicht van de afspraken, spelregels en instrumentarium die gehanteerd
worden te bieden. Daarnaast is deze baseline bedoeld om als groeidocument te fungeren als er nieuwe
afspraken gemaakt worden.  

De baseline is onder andere gebaseerd op de voor kwaliteit relevante eisen uit de 12 eisen van de
basisregistraties:  

- eis 2 De afnemers hebben een terugmeldplicht  
- eis 9 Er is een stringent regime van kwaliteitsborging  

### Rol- en taakverdeling

In de stelselarchitectuur van het heden wordt gesproken over rol- en taakverdeling. Ten aanzien van
de kwaliteit is hierin afgesproken dat de bronhouder de kwaliteit van de gegevens borgt en daartoe
op eigen initiatief onderzoek doet of naar aanleiding van terugmeldingen van afnemers. Daarnaast is
de rolomschrijving van de toezichthouder als volgt gedefinieerd: “De toezichthouder is er
verantwoordelijk voor dat wordt toegezien of de basisregistraties conform eisen, afspraken en
wetgeving opereert”.

Daarnaast is over toezicht ook het volgende in de stelselarchitectuur opgenomen: Over het algemeen
is de registratiehouder verantwoordelijk voor het toezicht op de naleving van de bepalingen die in
de wet voor de basisregistratie zijn opgenomen. Een deel van deze verantwoordelijkheid kan ook bij
de bronhouders en/of verstrekker belegd zijn of bij een externe partij zoals een ministerie.  

In de praktijk is de monitoring en het toezicht hierop meestal sectoraal ingericht vanuit de
bronhouder en/of het verantwoordelijke ministerie.  

### Kwaliteitstoetsing

Er wordt op verschillende manieren invulling gegeven aan de kwaliteitstoetsing. Dit vindt minimaal
intern plaats, bijvoorbeeld door een ENSIA-zelfevaluatie. In de stelselarchitectuur wordt de interne
kwaliteitstoetsing als volgt omschreven: “Om het vertrouwen van afnemers te verkrijgen en behouden,
is het noodzakelijk dat er ten aanzien van de authentieke registratie een expliciete, interne
regeling voor kwaliteitstoetsing bestaat. Deze interne regeling dient uiteraard ook regelmatig te
worden toegepast.”  

Ten aanzien van externe kwaliteitstoetsing is in de architectuur het volgende opgenomen: “(…) neemt
niet weg dat wenselijk is regelmatig externe toetsing van de kwaliteit van de authentieke
registratie te laten plaatsvinden. Een dergelijke externe toetsing zal meestal plaatsvinden in de
vorm van een audit. Verantwoordelijk voor het laten uitvoeren van een audit is in principe de houder
van een registratie. Daarnaast kan bij de inrichting van de gebruikersorganisatie ten aanzien van
het beheer, ook aan de beheersorganisatie het recht worden verleend om (al dan niet op regelmatige
basis) audits op kwaliteit van de authentieke registratie te laten uitvoeren.” De term audit wordt
hierin breed gebruikt en wordt in een aantal registraties als daadwerkelijke audit uitgevoerd. Bij
andere registraties wordt hieraan invulling gegeven door een meting door een externe partij te laten
uitvoeren, bijvoorbeeld het CBS.  

#### Kwaliteit per registratie

Binnen het Stelsel van Basisregistraties is per registratie beschreven wat de kwaliteitsnormen zijn.
Deze zijn met de gebruikers van de registratie bepaald. Ten aanzien van eis 9 (streng regime van
kwaliteitsborging) is in de architectuur het volgende opgenomen: Kwaliteitsborging dient transparant
te zijn over de volgende zaken: - wat de gerealiseerde kwaliteit is van een basisregistratie,
uitgedrukt met behulp van duidelijke indicatoren en afgezet tegen vastgestelde normen; - hoe de
basisregistratie de gerealiseerde kwaliteit heeft bepaald; - welke maatregelen een basisregistratie
treft om de kwaliteit te garanderen en verbeteren.  

De beschrijving van kwaliteit blijkt tussen registraties maar beperkt vergelijkbaar en ook de normen
kunnen verschillen. Er zijn behoudens een enkele registratie geen normen voor koppelingen tussen de
registraties.  

Verder zien we verschillen in de wijze waarop er per registratie met kwaliteit wordt omgegaan. Zo
zien we in een aantal registraties dat er centraal signaleringen gedaan worden om bronhouders te
attenderen op potentiële fouten. In sommige registraties wordt ook aandacht gegeven aan controle van
de feitelijke werkelijkheid (ten opzichte van de geregistreerde gegevens). Verder blijkt dat hoe
gemakkelijker de registratie het gebruikers maakt om potentiële fouten terug te melden, dit ook in
grotere mate gedaan wordt.  

#### Kwaliteit in beeld

Jaarlijks worden er kwaliteitsmetingen gedaan door de registraties op de eigen normen. Deze worden
gecombineerd
[gepubliceerd](https://www.digitaleoverheid.nl/overzicht-van-alle-onderwerpen/stelsel-van-basisregistraties/kwaliteit-en-terugmelden/).
Daarnaast wordt in opdracht van het ministerie van BZK, de kwaliteit van de koppelingen tussen
registraties jaarlijks gemeten door het CBS. Ook hier wordt over
[gerapporteerd](https://dashboards.cbs.nl/v4/kwaliteitsmeting_basisregistraties_2022/). Indien nodig
worden verbeteringen voorgesteld.
