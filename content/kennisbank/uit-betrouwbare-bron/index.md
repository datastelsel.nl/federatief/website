---
title: Uit betrouwbare bron
status: Voorstel
date: 2024-09-19
version: v0.1
author: Kernteam FDS
description: >
    Onderzoek naar de _capabilities_ van een 'bron' om een betrouwbare _datadienst_
    te zijn in een federatief datastelsel.
---

{{% imgproc "**capabilities*.*" Resize "x200" /%}}

In een federatief datastelsel wordt data aangeboden en afgenomen / gebruikt. Over het gebruik van de
data dient verantwoording te worden afgelegd. Niet alleen dát het gebruikt is en waarvoor, maar ook
ter onderbouwing van genomen besluiten. Als een afnemer een besluit baseert op een ‘extern gegeven’
is het wenselijk (noodzakelijk?) dat nagegaan kan worden welk gegeven dat was, welke zekerheid er
aan dat gegeven hangt, dat deze te reproduceren is, dat de oorspronkelijke herkomst duidelijk is,
eventuele correcties, etc, etc. Dit vraagt om uitgebreide capabilities van de datadienst die als
bron wordt gebruikt. Dit dient een **betrouwbare bron** te zijn. Wat maakt dat een bron als
betrouwbaar gekenmerkt kan worden? Welke capabilities zijn dat?

## Project Uit betrouwbare bron

Het Project [Uit betrouwbare bron](https://uitbetrouwbarebron.nl) is een project van VNG Realisatie
om uit te vinden welke capabilities een betrouwbare bron nodig heeft en wanneer we dus kunnen
spreken van een betrouwbare bron. [[Stelselfuncties]]() als [[Modellen]](), [[Dataservices]]() en
[[Traceerbaarheid]]() zullen op een bepaalde manier ingevuld moeten worden om aan te sluiten op te
verwachten bevindingen vanuit betrouwbare bron. En dat zal ook inzicht geven in de richting waarin
de stelselfuncties van FDS (datadiensten) zich het beste kunnen ontwikkelen om aan te sluiten bij
behoeften van afnemers. Daarom is nauwe samenwerking met en opnemen van de uitkomsten van het
project Uit betrouwbare bron van grote waarde voor de vorming van het FDS.

## Relatie tot FDS

Nauwe samenwerking en volgen van het *Project Uit betrouwbare bron* door het kernteam FDS.

1. 2024 Q3 Project Uit betrouwbare bron uitnodigen voor presentatie in inhoudelijk blok
1. 2024 Q3/Q4 actieve samenwerking met Project Uit betrouwbare bron door kennisdeling en
   gezamenlijke sessies (met name FDS Architectenteam)
1. 2025 Q1/Q2 update van Project Uit betrouwbare bron in inhoudelijk blok
