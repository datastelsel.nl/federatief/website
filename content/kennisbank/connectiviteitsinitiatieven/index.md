---
title: Vergelijk van initiatieven voor connectiviteit
date: 2024-11-13
author: Kernteam FDS
tags:
  - Interoperabiliteit
status: Concept
description: "Uitkomsten van het onderzoek in Digilab naar het koppelen van
  datatoepassingen aan dataservices voor het uniformeren van de uitwisseling. "
---
Het Federatief Datastelsel (FDS) wordt ontwikkeld als een 'data-ecosysteem' met als belangrijkste doel om maatschappelijke waarde te genereren uit het verantwoord delen van meervoudig bruikbare hoogwaardige data. 

Voor de technische functies van het FDS geldt dat het stelsel deze zoveel als mogelijk invult met afspraken en standaarden en alleen als het niet anders kan, met (centraal verplichte) voorzieningen. Het FDS maakt gebruik van standaarden en ontwikkelt deze niet zelf.

### Onderzoek

Er loopt momenteel het 'connectiviteit' onderzoek in Digilab naar het koppelen van datatoepassingen aan dataservices voor het uniformeren van de uitwisseling (ook bekend als de FDS demonstrator datadelen). 

In het onderzoek is gekozen dit te doen met 3 ontwikkelingen die hierop aansluiten en in een praktische FDS demonstrator te tonen zijn.

1. FSC - "Federated Service Connectivity"
2. iShare - "Trust Framework for Data Spaces"
3. TSG - "TNO Secure Gateway"
 
Ieder van deze 3 heeft een eigen perspectief en raakt een aantal van de vraagstukken die spelen bij het FDS qua interoperabiliteit, vertrouwen en datawaarde. Uitgangspunt voor een vergelijk is de Nederlandse API strategie, in het bijzonder in relatie tot API-management. 

Het vergelijk gaat uit van een toepassing die een dataservice gebruikt met vraagstukken zoals deelnemer registratie, controle op toegang en gebruik, pbac, delegatie, technische interoperabiliteit, beheer, service publicatie en logboeken.

### Aspecten

Vanuit het perspectief van het FDS kijken we naar ontwikkelingen rond de volgende aspecten:

* Samenwerking: De federatieve architectuur moet organisaties in staat stellen direct samen te werken, waarbij gegevens efficiënt en veilig tussen systemen kunnen worden uitgewisseld, liefst zonder afhankelijkheid van een centrale tussenpartij. We zoeken de grootste overlap met de FDS doelgroep.

* Uniformering: Gegevensuitwisseling moet gebaseerd zijn op uniforme standaarden en liefst protocollen, zodat verschillende systemen op een consistente en voorspelbare manier kunnen communiceren, ongeacht de infrastructuur van de betrokken partijen.

* Verdeling van verantwoordelijkheid: Elke partij blijft verantwoordelijk voor het beheer van zijn eigen data, met duidelijke afspraken over wie welke gegevens kan opvragen en delen. Dit bevordert autonomie en controle over data bij alle betrokken partijen.

* Veiligheid: Het systeem moet garanderen dat gegevens veilig worden overgedragen en opgeslagen, met gebruik van versleuteling, authenticatie en autorisatie, zodat de vertrouwelijkheid en integriteit van gegevens beschermd zijn.

* Beheerbaarheid: Toegang tot en gebruik van gegevens moet beheersbaar zijn, met heldere processen om toegangsrechten te verlenen en in te trekken. Dit vereist een duidelijk toegangsbeheer voor alle betrokken partijen.

* Controleerbaarheid: Het systeem moet de mogelijkheid bieden om gedetailleerde logs bij te houden van wie toegang heeft gehad tot welke gegevens en wanneer, zodat audits en naleving van regelgeving mogelijk zijn.

* Governance: Waar is (door)ontwikkeling belegd, hoe hebben gebruikers invloed. Welke governanceprocessen zijn er die bepalen hoe de systemen worden beheerd, wie verantwoordelijk is voor besluitvorming, en hoe wijzigingen en naleving van afspraken worden bewaakt. 

### Naburige initiatieven

#### Standaardisatie vanuit Europa

Europees is er sinds kort het initiatief tot Europese standaardisatie [JTC 25](https://www.cencenelec.eu/news-and-events/news/2024/brief-news/2024-09-25-jtc-25/) ‘Data management, Dataspaces, Cloud and Edge. In Nederland verzorgt het [NEN](https://www.nen.nl/data-act) de Nederlandse normcommissie ‘Datamanagement, dataspaces, cloud en edge’. 

#### Simpl

[Simpl](https://digital-strategy.ec.europa.eu/en/policies/simpl#1712822729753-2) is een open-source middleware-platform dat gegevens toegang en interoperabiliteit ondersteunt binnen Europese dataspaces. 

Het initiatief komt vanuit het DIGITAL Europe Work programma. Simpl speelt een rol bij de oprichting van de Gemeenschappelijke Europese Dataspaces. 

#### Solid

[Solid](https://solidproject.org/) Solid is een specificatie waarmee mensen hun data veilig kunnen opslaan in gedecentraliseerde datastores, zogenaamde Pods. Pods fungeren als veilige persoonlijke webservers voor data. 

Deelnemers bepalen zelf wie toegang krijgt tot hun data in hun Pod. Ze beslissen welke data gedeeld wordt en met wie (individuen, organisaties, applicaties, enz.), en kunnen deze toegang op elk moment intrekken.

Solid-compatibele applicaties gebruiken standaard, open en interoperabele dataformaten en -protocollen om data in een Pod op te slaan en te benaderen.

#### Gaia-X

[Gaia-X](https://gaia-x.eu/) is een Stichting met een initiatief dat, gebaseerd op Europese waarden, een digitale governance ontwikkelt die toepasbaar is op elke bestaande cloud- of edge-technologiestack om transparantie, controleerbaarheid, overdraagbaarheid en interoperabiliteit over data en diensten te waarborgen. 

### FSC

#### Techniek

Standaardiseert de koppeling tussen organisaties door voor te schrijven hoe het managementverkeer en het dataverkeer verloopt tussen API's van organisaties. Het is een peer-to-peer netwerk, waarbij het gebruik van de "directory" als centrale component optioneel is. 

[[FSC]]() biedt een veilige manier voor organisaties om met andere organisaties te delegeren. Het delegatie mechanisme maakt het mogelijk namens een organisatie data op te halen bij een bron en om API's namens een andere organisatie aan te bieden. Daarnaast voorziet FSC standaardisatie voor audit en technische logging.

FSC werkt nu op basis van PKIOverheid certificaten en biedt ruimte voor verifiable credentials. 

#### Gebruik

FSC is bij gemeente Utrecht in gebruik, Kadaster en gemeente den Bosch ondernemen proeven. In Digilab zijn er diverse koppelingen mee opgezet. 

#### Vorm

FSC is een standaard gericht op federatieve verbindingen. De standaard richt zich op interoperabiliteit van REST APIs onafhankelijk van een implementatie. De standaard komt voort uit de common ground visie en heeft een open source referentie implementatie die productie-waardig is. 

#### Federatief

Deze standaard wijzen we aan als standaard voor het Federatief Datastelsel. Het voornemen is om FSC op te nemen in het Digikoppeling REST API profiel. 

### iShare

#### Techniek

[iShare](https://framework.ishare.eu/) heeft een hub en spoke model.
Per dataspace is er een satellite, die in verbinding staat met een parent satellite.
De satellite is de plek waar alle deelnemers staan geregistreerd.

Machtigen/Delegeren gaat door middel van policies.
Een Entitled Party kan daarin aangeven welke service rechten hij delegeert aan een specifieke service consumer.
Deze policies kunnen in een authorization registry worden vastgelegd.

Het technische framework is nog in ontwikkeling evenals de test-tooling. De software op github is een voorbeeld, in de praktijk zijn verschillende versies in gebruik. De satellite is niet openbaar, de broncode is beschikbaar voor deelnemers aan iShare.

iShare biedt ondersteuning voor certificaten onder eIDAS en heeft aaknopingspunten voor andere authenticatiemechanismen.

#### Gebruik

Meest toonaangevende voorbeeld is DVU rond energiegebruik. RVO laat een omgeving door DICTU beheren met het beheer van de satelliet door KPN.

Andere voorbeelden in ontwikkeling zijn BDI, DMI en DSGO. In Digilab is het werkend gekregen, het vroeg echter veel eigen implementatie-keuzes.

#### Vorm

iShare is een op zich staand legal framework, het maakt gebruik van verschillende standaarden en heeft een focus op het protocol met verschillende implementatie scenarios. Er is een streven naar Europese adoptie. 

iShare voert certificeringen uit van de (iShare) autorisatie registers. Op zichzelf is het geen standaard. Deelnemers betalen voor de certificering. 

#### Federatief

Aangezien iShare geen standaard is maar wel als oplossing voor data-delen gebruikt maakt van standaarden kan het een platform zijn dat een stelsel gebruikt om aansluiting te vinden bij een FDS. 

### TSG

#### Techniek

De TNO Security Gateway ([TSG](https://tsg.dataspac.es/)) is een open-source IDS-connectorimplementatie, ontwikkeld door TNO. De beproefde connector is sinds mei 2024 beschikbaar. De TSG faciliteert de koppeling tussen organisaties door voor te schrijven hoe het managementverkeer en het dataverkeer verloopt tussen API's van organisaties.

Specificatiewerk gestart eind 2022 als subwerkgroep van de WG Architecture van IDSA. Governance is overgedragen aan de Eclipse stichting.

De TSG-componenten (als IDS implementatie) stellen je in staat deel te nemen aan een IDS-dataspace om informatie uit te wisselen met andere organisaties, met aandacht voor data-soevereiniteit. De broncode is open-source, beschikbaar onder de Apache 2.0-licentie.

De TSG richt zich voor authenticatie op het gebruik van verifiable credentials.

#### Gebruik

De TSG is meer een onderzoeksproject, niet gericht op productie en recent ontwikkeld. Binnen Digilab is de TSG in een uur werkend opgezet met hulp van TNO. 

De IDS beschrijving geeft geen garantie dat een implementatie technische interoperabiliteit krijgt tussen verschillende IDS-connectoren. Er is veel keuzevrijheid bij de invulling van de IDS protocollen. 

#### Vorm

IDS bestaat uit drie afzonderlijke protocollen, de TSG volgt dit in de implementatie:

* Catalogusprotocol
* Protocol voor contractonderhandelingen
* Protocol voor gegevensoverdracht

Niet gespecificeerd in het Dataspace Protocol:

* Identificatie, Authenticatie & Autorisatie
* Intermediaire diensten

#### Federatief

Deze [IDSA connector](https://internationaldataspaces.org/data-connector-report/) sluit aan bij het internationale initiatief. Het voornemen is het dataspace protocol te verheffen tot een standaard. Vanuit de Eclipse stichting werkt men hier aan. Het is geen initiatief vanuit een Europese organisatie, dit is een initiatief van de Eclipse stichting.
