---
title: Dataservices
date: 2023-10-17
aliases: 
  - /kennisbank/capabilities/interoperabiliteit/dataservices
tags: 
description: >
  Interoperabiliteit | Dataservices: API's voor data uitwisseling
---

{{< capabilities-diagram selected="dataservices" >}}

## Beschrijving

Deze stelselfunctie beschrijft hoe de uitwisseling van gegevens via services kan
worden gestandaardiseerd. Dit heeft betrekking op de wijze waarop Application
Programming Interfaces (API's) worden vormgegeven. Het standaardiseren van
API's vergemakkelijkt het uitwisselen van gegevens, in het bijzonder voor 
afnemers die gegevens van verschillende aanbieders combineren.

## Meer lezen

De stelselfunctie Dataservices is gebaseerd op het 
[Data Exchange API's building block](https://docs.internationaldataspaces.org/ids-knowledgebase/open-dei-building-blocks-catalog#interoperability-building-blocks)
zoals beschreven in het [OPEN DEI design principles position paper on
Resources](https://design-principles-for-data-spaces.org/). In de 
[Blueprint van het Data Spaces Support Centre](https://dssc.eu/page/knowledge-base) 
wordt het begrip dataservices beschreven in het 
Building Block [Data Exchange](https://dssc.eu/space/bv15e/766067960/Data+Exchange). Daarnaast
onderscheid DSSC een apart Building Block 
[Value Creation Services](https://dssc.eu/space/bv15e/766070344/Value+creation+services)
voor services ten behoeve van aanvullende diensten om waarde te creëren uit data.

## Dataservices binnen FDS

Als een afnemer processen uitvoert die gegevens vergen van de aanbieder, kan de afnemer een bevraging 
uitvoeren naar een dataservice van de aanbieder. Als antwoord op deze bevraging ontvangt de afnemer 
de gevraagde gegevens zodat de afnemer deze kan verwerken. Voor het uitvoeren van sommige processen zullen 
echter afnemers een signaal nodig hebben dat aangeeft dat er een gebeurtenis heeft plaatsgevonden,
zoals de geboorte van een kind. Een afnemer kan periodiek bevragen of er wijzigingen hebben plaatsgevonden ('polling'), maar over het algemeen is het effectiever als de aanbieder een signaal verstuurt naar 
de afnemer. Het door de aanbieder aan de afnemer versturen van een signaal noemen we notificeren. De 
patronen bevragen en notificeren zijn de basisuitwisselpatronen binnen FDS. 

## Bevragen binnen FDS

Bevraging heeft betrekking op het ophalen van kenmerken van entiteiten zoals personen, organisaties, locaties
etc, maar ook op kenmerken van gebeurtenissen zoals een geboorte, een correctie of de vaststelling 
van een jaarinkomen. Vanuit het oogpunt van dataminimalisatie dient de afnemer in te kunnen (laten) perken welke 
entiteiten en welk kenmerken van deze entiteiten het ontvangt. Inperking kan door deze in de
gestelde vraag op te nemen, door de inperking in de [[Toegang]]() in te richten, of
door een combinatie van deze twee methodes.

Bevragen kan plaatsvinden via het 
[WUS Profiel](https://gitdocumentatie.logius.nl/publicatie/dk/wus/) van 
[Digikoppeling](https://www.forumstandaardisatie.nl/open-standaarden/digikoppeling). 
Het [REST Profiel](https://gitdocumentatie.logius.nl/publicatie/dk/restapi/) is echter 
moderner en daarmee meer toekomstvast. Voor dit profiel zijn 
[REST-API Design Rules](https://www.forumstandaardisatie.nl/open-standaarden/rest-api-design-rules)
beschikbaar. De structuur van een REST API wordt beschreven via een
[Open API Specification](https://www.forumstandaardisatie.nl/open-standaarden/openapi-specification).

Bevraging kent diverse verschijningsvormen, zoals:
* 'sleutelbevraging': kenmerken van een entiteit ophalen op basis van een sleutel (zoals het bsn van een persoon).
* 'zoekbevraging': het vinden van een entiteit op basis van kenmerken die het object (potentieel)
niet uniek identificeren (bijvoorbeeld de voornamen, de geslachtsnaam en de geboortedatum van een persoon).
* 'bulkbevraging': het ophalen van kenmerken van verschillende entiteiten in één keer. Een variant van
een bulkbevraging is de 'selectie' waarbij in één keer de kenmerken van een (potentieel omvangrijke) 
groep entiteiten worden opgehaald. Vanwege de benodigde verwerkingstijd om een selectie samen te stellen
kan het noodzakelijk zijn een 'selectie' asynchroon samen te stellen. In dat geval kan de aanbieder de afnemer
notificeren zodra de selectie is samengesteld en kan worden opgehaald, bijvoorbeeld via de
[Digikoppeling Koppelvlakstandaard Grote Berichten](https://gitdocumentatie.logius.nl/publicatie/dk/gb/).

## Notificeren binnen FDS

Naast het bevragen van gegevens is het van belang dat de afnemer pro-actief op de hoogte kan worden 
gebracht van gebeurtenissen. Dit pro-actief op de hoogte brengen noemen we notificeren. Een notificatie kent
een soort gebeurtenis (wat is er gebeurd) en een subject (wie of wat betreft het). Denk aan de geboorte
van een kind, of de correctie van een belastbaar inkomen. De standaard 
[NL GOV Profile voor Cloudevents](https://www.logius.nl/domeinen/gegevensuitwisseling/nl-gov-profile-cloudevents)
is geschikt als standaard voor het notificeren.

Bijzondere aandacht verdient het notificeren van correcties. Een afnemer kan rechtsgevolgen verbinden
aan opgevraagde gegevens. In dat geval is het van belang dat de afnemer op de hoogte wordt gebracht van 
correcties op deze gegevens, zodat het de rechtsgevolgen kan herzien. Dit betekent enerzijds dat de 
aanbieder notificaties moet kunnen sturen van correcties op eerder geleverde gegevens, en anderzijds dat 
de afnemer op basis van deze notificaties moet kunnen bepalen welke verwerkingen met rechtsgevolgen 
potentieel herzien moeten worden.

Een variant van notificeren is het leveren. Bij leveren worden berichten veelal voorzien van de (gewijzigde) 
gegevens zelf. Dit levert echter een kopie van de bron op die ten tijde van verwerking inmiddels kan zijn 
verouderd. Zuiverder is daarom om de notificatie alleen te gebruiken als 'trigger' van een verwerkingsproces, 
en de voor de verwerking benodigde *actuele* gegevens via bevraging op te halen. Dit vereenvoudigt tevens de 
dataminimalisatie doordat per verwerkingsstap om minimale data kan worden gevraagd, in plaats dat bij een 
aanbieder op voorhand bekend dient te zijn wat de minimale set gegevens is die de afnemer nodig heeft voor 
de verwerking. 

Ten behoeve van leveren is de 
[ebMS](https://gitdocumentatie.logius.nl/publicatie/dk/ebms/) standaard in 
[Digikoppeling](https://www.forumstandaardisatie.nl/open-standaarden/digikoppeling) opgenomen. EbMS is 
echter een redelijk 'zware' en complexe standaard om te implementeren. Met de introductie van
[NL GOV Profile voor Cloudevents](https://www.logius.nl/domeinen/gegevensuitwisseling/nl-gov-profile-cloudevents)
komt er een modernere en eenvoudigere standaard beschikbaar om leveren 
te vervangen door notificeren.

Een aanbieder stuurt notificaties vanuit een 'abonnement' dat aangeeft voor welke soorten gebeurtenissen
en voor welke populatie een afnemer notificaties wenst te ontvangen. Vanuit het principe van
dataminimalisatie is het onwenselijk dat een afnemer notificaties ontvangt waarop de afnemer geen 
verwerking hoeft te laten volgen. Dit raakt aan het inperkende functionaliteit zoals omschreven in de 
stelselfunctie [[Toegang]]().

## Standaarden

- [Digikoppeling](https://www.forumstandaardisatie.nl/open-standaarden/digikoppeling)
- [Open API Specification](https://www.forumstandaardisatie.nl/open-standaarden/openapi-specification)
- [REST-API Design Rules](https://www.forumstandaardisatie.nl/open-standaarden/rest-api-design-rules)
- [NL GOV Profile voor Cloudevents](https://www.logius.nl/domeinen/gegevensuitwisseling/nl-gov-profile-cloudevents)
