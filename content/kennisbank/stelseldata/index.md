---
title: Stelseldata
weight: 6
status: Concept
date: 2025-03-06
author: Kernteam FDS
description: >
    Op hoofdlijnen beschrijving van data als begrip en de eisen die het stelsel aan het data-aanbod verbindt.
---

‘Data’ is afkomstig uit het Latijn en betekent zoveel als ‘gegevens’ of
‘feiten’. Het Latijnse enkelvoud is ‘datum’. De verwarring die hierbij kan
ontstaan met het welbekende tijdsbegrip is evident. We gebruiken daarom de term
‘data’ voor zowel het enkelvoud als voor het meervoud van ‘gegeven’. Uit de
context moet blijken of het om het meervoud gaat of om het enkelvoud.

Om data als begrip te omschrijven gebruiken we de definitie zoals omschreven in
<a
href="https://eur-lex.europa.eu/legal-content/NL/TXT/HTML/?uri=CELEX:32022R0868#d1e969-1-1"
target="_blank">artikel 2 lid 1 van de Europese Data Governance Act</a>:

***Elke digitale weergave van handelingen, feiten of informatie en elke
compilatie van dergelijke handelingen, feiten of informatie, ook in de vorm van
geluidsopnames of visuele of audiovisuele opnames.***

De datastrategie is gericht op het (beter) dienen van het publiek belang, door
het datapotentieel waar de overheid al over beschikt, breder in te zetten voor
maatschappelijke opgaven. Stelseldata (her)gebruiken voor verschillende doelen
in verschillende contexten is daarom een belangrijk doel. Om herbruikbaarheid te
bevorderen, stelt FDS eisen aan de data die in het stelsel worden opgenomen.
Deze eisen hebben invloed op de inhoud van de datadiensten waarmee het
data-aanbod in het stelsel voor de data-afnemers wordt ontsloten. De
belangrijkste eisen die FDS stelt aan stelseldata zijn:

- Passend binnen de [[Scope van FDS]]()
- De stelseldata zijn [adequaat beschreven](#data-zijn-adequaat-beschreven)
- De stelseldata zijn [in betekenisvolle samenhang
  ontsluitbaar](#data-zijn-in-betekenisvolle-samenhang-ontsluitbaar)

## Data zijn adequaat beschreven

In deze paragraaf gaat het om herbruikbaarheid in semantische zin. De
beschrijvingen van data en datadiensten moeten inzicht geven in de mogelijke
herbruikbaarheid van de betreffende data. Om verantwoord hergebruik van data in
een andere context te kunnen bepalen, is minimaal inzicht vereist in:

- Betekenis, context en doel van de her te gebruiken data;
- Condities voor (her)gebruik;
- Context en doel van het nieuwe gebruik.

Zo moet bijvoorbeeld duidelijk zijn wat de betekenis is van de data en waarvoor
deze oorspronkelijk is ingezameld en gebruikt. Lees voor meer [[Metadata]](),
[[Aanbod (metadata)|Metadata van aanbod]]() en de [[Publicatie|Publicatie van
metadata]](). 

## Data zijn in betekenisvolle samenhang ontsluitbaar

Voor het betekenisvol combineren (of compileren) van data moet deze voorzien
zijn van uniek identificerende kenmerken, ook wel sleuteldata genoemd. Relaties
tussen data worden gelegd via deze sleuteldata. Met sector-overstijgend data
delen als uitgangspunt is het noodzakelijk dat data uit verschillende sectoren
is voorzien van overeenkomstige sleuteldata. FDS onderhoudt een afsprakenkader
dat beschrijft welke sleuteldata minimaal aanwezig moet zijn, om te kunnen
koppelen met andere data. Dit afsprakenkader is bekend onder de naam
‘Informatiekundige Kern’ (IK). De relaties tussen sectoren blijken vooral gelegd
te kunnen worden op basis van ‘wie’ en/of ‘waar’, ofwel de identificerend
kenmerken van natuurlijke personen, organisaties en locaties. Binnen sectoren
zijn de onderwerpen en objecten waar de sector over gaat belangrijke factoren
voor het leggen van relaties. Denk aan voertuigen in de mobiliteitssector,
studieprogramma’s in de onderwijssector, etc. De IK bestaat uit afspraken over
de identificatie van natuurlijke personen, niet-natuurlijke personen en
locaties. Dit betreft de identificerende eigenschappen van BRP, HR en BAG.
Doordat data-aanbieders in FDS conformeren aan de IK-afspraken kunnen ze
datadiensten aanbieden die op informatiekundig betrouwbare manier data
combineren. Als twee verschillende datasets dezelfde unieke sleutel gebruiken
(bijv. BSN 999.99.999 van een persoon), is het simpel vast te stellen dat het
gaat om data over dezelfde persoon. De bij deze sleutel behorende datakenmerken
uit beide datasets zijn dan eenvoudig te combineren tot een samengesteld beeld
over de betreffende persoon. Naast afspraken over standaard identificerende
sleutels voorziet FDS ook in afspraken over alternatieve sleutels. Stel dat een
data-aanbieder niet mag of kan beschikken over een BSN, maar de data die hij wil
aanbieden leent zich wel voor het leggen van relaties op persoonsniveau. In dat
geval kan wellicht een alternatieve sleutel worden toegepast. Denk aan postcode
en huisnummer voor een adres, of geboortedatum, voornamen en achternamen voor
een persoon. Bij gebruik van alternatieve sleutels kan altijd een risico bestaan
dat een sleutel niet 100% uniek is. Ook over het mitigeren van dit risico zijn
afspraken nodig, evenals over hoe te handelen bij eventuele maatschappelijke
gevolgen bij ongewenst koppeling van data. De IK zal ook nog afspraken uitwerken
over de invulling van ondersteunende functies. Dergelijke functies moeten de
implementatie van de afspraken eenvoudiger maken. Te denken valt aan:

- Opzoeken van sleutels;
- Wisselen van sleutels naar alternatieve sleutels. Denk aan het omwisselen van
  identificatie op basis van KVK-nummer naar identificatie op basis van RSIN
  voor een niet-natuurlijk persoon.
- Gebruik van pseudoniemen bij persoonsgegevens.
