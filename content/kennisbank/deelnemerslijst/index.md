---
title: Deelnemerslijst
date: 2024-08-19
tags: 
description: >
  Registratie van de deelnemers door de deelnameadministrateur(s).
---

## Mechanisme
De deelnemerslijst bevat verwijzingen naar alle deelnemers van het FDS. De
deelnemerslijst wordt door de deelname-administrateur bijgehouden bij het
uitvoeren van de [deelnameprocessen](../deelnameprocessen/). De deelnamelijst
is metadata en wordt beschikbaar gesteld als linked data. Hierbij zijn
de [Linked Data Design Rules](../linked-data-design-rules/) van toepassing.

De deelnemerslijst bestaat uit twee delen:

- alle wijzigingen (events) op de deelnemerslijst die hebben plaatsgevonden
- de volledige lijst met deelnemers die uit deze wijzigingen voortvloeit

Elke administrateur houdt alle wijzigingen bij die deze administrateur doorvoert
(opname en verwijdering). Vervolgens stelt elke administrateur een resulterende
volledige lijst met deelnemers op gebaseerd op de lijsten met wijzigingen van 
alle administrateurs in het stelsel.

Voor beide soorten lijsten wordt een versiehistorie bijgehouden.

| ![Deelnemerslijst mechanisme](images/deelnemerslijst-mechanisme.png) |
| :------------------------------------------------------------------: |
| _Het mechanisme van de deelnemerslijst_                              |

Zowel de deelnamekenmerken van de administrateur verwijzen naar zowel de lijst met
deelnamewijzigingen als de deelnemerslijst.

## De deelnamewijzigingen

Elke administrateur houdt bij welke deelnemers hij opneemt en verwijderd. Deze
wijzigingen worden toegevoegd aan een nieuwe versie van de lijst met
deelnamewijzigingen. 

De allereerste wijziging bestaat uit het zichzelf toevoegen van de eerste
administrateur (bootstrapping). Deze eerste administrateur kan vervolgens
andere administrateurs en deelnemers (aanbieders en afnemers) toevoegen.

## De deelnemerslijst

Uit de verschillende lijsten met deelnamewijzigingen van de verschillende
administrateurs wordt vervolgens de lijst met deelnemers samengesteld. De deelnemerslijst bevat verwijzingen naar de (versies) van de lijsten met
deelnamewijzigingen die zijn gebruikt om de deelnemerslijst samen te stellen.

Elke administrateur verzamelt periodiek de laatste versie(s) van de lijsten met 
deelnamewijzigingen van alle administrateurs. Indien één van de lijsten
met deelnamewijzigingen is aangepast maakt elke administrateur 
een nieuwe versie van  de deelnemerslijst aan. 

| ![De deelnemerslijst](images/deelnemerslijst-metadata.png) |
| :----------------------------------------------------------: |
| _De deelnemerslijst_                                         |

Welke organisatie(s) binnen het FDS deelnameadministrateur worden en of binnen
het FDS daadwerkelijk gebruik gemaakt gaat worden van meerdere administrateurs 
is nog onderwerp van onderzoek.

## Voorbeelden

Een voorbeeld van een lijst met deelnamewijzigingen (in 
[Turtle](https://www.w3.org/TR/turtle/)):

```
@prefix fds: <http://fds.example.org/ontology#>
@base <http://admin-a.example.org/fds/>

<deelnamewijzigingen>
   fds:versionInfo 'v4' ;
   fds:priorVersion <deelnamewijzigingen/v3> .

(
  [ a fds:participant-addition ;
    fds:of-participant <deelnemers/self#deelnemer> ; 
    fds:by-admin <deelnemers/self#deelnemer> ; 
    fds:at-timestamp "2025-04-09T10:00:00"^^xsd:dateTime ; 
    fds:in-version <deelnamewijzigingen/v1> 
  ]

  [ a fds:participant-addition ;
    fds:of-participant <http://admin-b.example.org/fds/deelnemers/self#deelnemer> ; 
    fds:by-admin <deelnemers/self#deelnemer> ; 
    fds:at-timestamp "2025-04-09T10:00:00"^^xsd:dateTime ; 
    fds:in-version <deelnamewijzigingen/v1> 
  ]

  [ a fds:participant-addition ;
    fds:of-participant <deelnemers/organisatie-x/#deelnemer> ; 
    fds:by-admin <deelnemers/self#deelnemer> ; 
    fds:at-timestamp "2025-04-09T10:00:00"^^xsd:dateTime ; 
    fds:in-version <deelnamewijzigingen/v1> 
  ]

  [ a fds:participant-addition ;
    fds:of-participant <deelnemers/organisatie-y/#deelnemer>; 
    fds:by-admin <deelnemers/self#deelnemer> ; 
    fds:at-timestamp "2025-05-12T10:00:00"^^xsd:dateTime; 
    fds:in-version <deelnamewijzigingen/v2> ]

  [ a fds:participant-addition
    fds:of-participant <deelnemers/organisatie-z/#deelnemer> ; 
    fds:by-admin <deelnemers/self#deelnemer> ; 
    fds:at-timestamp "2025-06-24T10:00:00"^^xsd:dateTime ; 
    fds:in-version <deelnamewijzigingen/v3> ]

  [ a fds:participant-removal
    fds:of-participant <deelnemers/organisatie-y/#deelnemer> ; 
    fds:by-admin <deelnemers/self#deelnemer> ; 
    fds:at-timestamp "2025-10-08T10:00:00"^^xsd:dateTime ; 
    fds:in-version <deelnamewijzigingen/v4> ]

....

)

```

Een voorbeeld van de resulterende deelnemerslijst (in 
[Turtle](https://www.w3.org/TR/turtle/)):

```
@prefix fds: <http://fds.example.org/ontology#>
@base <http://admin-a.example.org/fds/>

<deelnemerslijst>
   fds:versionInfo 'v12' ;
   fds:priorVersion <deelnemerslijst/v11> ;
   fds:created-from <deelnamewijzigingen/v4> ,
                    <http://admin-b.example.org/fds/deelnamewijzigingen/v8> , 
                    <http://admin-c.example.org/fds/deelnamewijzigingen/v5> .

(
  <deelnemers/self#deelnemer> 
  <http://admin-b.example.org/fds/deelnemers/self#deelnemer> 
  <deelnemers/organisatie-x#deelnemer>
  <deelnemers/organisatie-z#deelnemer>

.... aangevuld met deelnemers vanuit andere administrateurs .....

)

```

De te gebruiken linked data ontologie (semantiek) en de daarbij te hanteren
[OWL](https://www.forumstandaardisatie.nl/open-standaarden/owl) en/of 
[SHACL](https://www.forumstandaardisatie.nl/open-standaarden/shacl) definities 
is nog onderwerp van onderzoek.

## Standaarden
* [RDF](https://www.forumstandaardisatie.nl/open-standaarden/rdf)
* [RDFS](https://www.forumstandaardisatie.nl/open-standaarden/rdfs)
* [OWL](https://www.forumstandaardisatie.nl/open-standaarden/owl)
* [SHACL](https://www.forumstandaardisatie.nl/open-standaarden/shacl)
