---
title: Informatiekundige kern van het FDS
linkTitle: Informatiekundige kern
author: Kernteam FDS
status: Concept
date: 2024-08-22
description: > 
  Uitleg concept informatiekundige kern
---
## Uitleg van het doel en de inhoud van de informatiekundige kern van het FDS

Het onderkennen van een Informatiekundige kern is een belangrijk element binnen het ontwerp van het
Federatief Datastelsel (FDS). Het is echter ook een nieuw concept met het risico van
begripsverwarring en verkeerde toepassing. Hieronder wordt in verschillende stappen uitgelegd waarom
we een informatiekundige kern binnen het FDS onderkennen, wat het concreet inhoudt en hoe
stelselpartijen er mee om moeten gaan.  

## Wat maakt een stelsel tot stelsel?

Een setje losse datasets (registraties) wordt een datastelsel doordat gegevens in die datasets
betrouwbaar aan elkaar kunnen worden gerelateerd. ​ Informatiekundig is het dan belangrijk dat je
zeker weet dat je het over hetzelfde of dezelfde hebt.​ Wat dan erg helpt is dat je afspreekt om
gebruik te maken van dezelfde identificerende gegevens. Het handigst zijn dan unieke nummers (‘id’s’
of ‘sleutels’). ​ Er zijn immers heel veel Janssens, zelfs heel veel Jan Janssens en misschien zelfs
wel meerdere Jan Janssens die op 4 oktober 1956 zijn geboren, maar er is maar één Jan Janssen met
het BSN 123456789. ​

## Hoe zit het nu? Identificerende nummers in het stelsel van basisregistraties​

![Stelselplaat 2020 met realiteit van koppelingen](images/stelselplaat-2020.png)

Binnen elk domein komen identificerende nummers voor van kadastrale object id’s tot kentekennummers
maar zoals de ‘Stelselplaat’ laat zien, worden vooral de identificerende nummers uit de
basisregistratie personen (BRP), het Handelsregister (HR) en de basisregistratie adressen en
gebouwen (BAG) gebruikt om relaties tussen registraties in verschillende domeinen te leggen. De
basisregistraties BRP, HR en BAG vormen daarmee in de praktijk de kern van het huidige stelsel.

## Van veel gebruikte basisregistraties naar de informatiekundige kern van het FDS

Waarom zijn de BRP, BAG en HR ook de kern van het Federatief Datastelsel?

- Bij het Federatief datastelsel (FDS), de opvolger van het stelsel van basisregistraties, willen we
  meer data verbinden om die te gebruiken bij maatschappelijke opgaven. Daarvoor is het vrijwel
  altijd nodig om te weten over wie het gaat en/of waar het zich afspeelt.
- BRP en HR leveren de gegevens om eenduidig naar die ‘wie’ te verwijzen:  de identificerende
  gegevens van natuurlijke en niet-natuurlijke personen (organisaties).
- De BAG levert de gegevens om eenduidig naar het  ‘waar’ te verwijzen: de identificerende gegevens
  van een locatie.  

BRP, BAG en HR bevatten veel gegevens die wat zeggen over personen, organisaties en locaties, maar
slechts een beperkt deel daarvan is nodig om ze te kunnen identificeren. Die ‘identificerende
gegevens’, vormen **de informatiekundige kern** van het FDS.

## Inhoud van de informatiekundige kern – de essentiële identificerende gegevens

De informatiekundige kern bestaat in elk geval uit de identificerende nummers die specifiek bedoeld
zijn om personen, organisaties en locaties te identificeren: BSN, KVK-nummer en het adresseerbaar
object-id. Deze vormen de essentiële inhoud van de informatiekundige kern. We bouwen een datastelsel
als we afspreken dat elke daarin op te nemen dataset (registratie) in elk geval één van deze nummers
gebruikt voor het vastleggen van gegevens over het wie en waar, oftewel een koppeling legt met de
informatiekundige kern. Binnen het huidige stelsel van basisregistraties is dit al de praktijk. Er
zijn echter meer dan 100 potentiële registraties met waardevolle gegevens over personen en locaties.
Dat zijn allemaal kandidaten voor het nieuwe Federatief Datastelsel. Maar deze registraties bevatten
nog niet allemaal deze identificerende nummers en kunnen of mogen die soms ook niet opnemen (geldt
voor het BSN). Hoe zorg je ervoor dat je ze dan toch al meteen in samenhang kunt gebruiken? Dit
 kunnen we oplossen door een beperkt aantal andere gegevens toe te voegen die het mogelijk maken om
 de inhoud van deze registraties toch al zinvol te verbinden terwijl ze de essentiële gegevens (nog)
 niet kunnen toepassen.  

## De inhoud van de informatiekundige kern – aanvullende identificerende gegevens

Datasets (registraties) die niet de standaard identificerende nummers bevatten, bevatten vaak wel
andere gegevens uit de BRP, het HR en de BAG waarmee we personen, organisaties en locaties ook
betrouwbaar kunnen identificeren. Hier kunnen we gebruik van maken door deze gegevens ook toe te
passen om binnen het Federatief Datastelsel te koppelen. Je realiseert dan al waarde met deze data
zonder dat je hoeft te wachten tot overal ‘de essentie’ is geïmplementeerd. Om dit goed te laten
werken, moeten we wel duidelijk afspreken wat we precies willen gaan gebruiken: welke gegevens uit
BRP, HR en BAG vormen in eerste instantie samen vanuit informatiekundig perspectief **de kern** van
het Federatief Datastelsel?
Deze vraag is in 2023 opgepakt door de FDS-werkgroep Informatiekundige Kern. De volgende paragraaf
bevat hun voorstel voor de inhoud van deze kern.

## Toepassing van de informatiekundige kern bij het realiseren en benutten van het FDS

### Inhoudelijk vertrekpunt

De informatiekundige kern van het Federatief Datastelsel is dus in essentie een afspraak over een
set minimaal toe te passen identificerende gegevens. Daarbij wordt vooralsnog uitgegaan van de
volgende inhoud van deze set:

 (Voorstel eind 2023 van de FDS-werkgroep Informatiekundige Kern)

 ![Tabel met FDS Kern](images/tabel-fds-kern.png)

### Het werkend krijgen van het concept van de informatiekundige kern

Om te zorgen dat alle stelseldeelnemers de verplicht te gebruiken identificerende gegevens goed
kunnen benutten heeft de werkgroep een aantal ondersteunende functies benoemd.

Dit zijn in elk geval functies voor:

- Het opzoeken van sleutels
- Specifiek bij persoonsgegevens: het gebruik van pseudoniemen

En mogelijk ook functies voor het wisselen van sleutels, bijvoorbeeld van postcode/huisnummer naar
verblijfsobject-id en vice versa.

De afspraken over de invulling van deze ondersteunende functies moeten nog verder worden uitgewerkt.
Dat geldt ook voor enkele andere zaken die nodig zijn om de kern goed te kunnen toepassen
(bijvoorbeeld over de volledigheid en de kwaliteit van de daarin op te nemen gegevens). Hier gaat
het FDS-project de komende jaren verder mee aan de slag.
