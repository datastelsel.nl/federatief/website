---
title: Basismodel
weight: 5
status: Concept
date: 2025-03-06
aliases: 
  - /kennisbank/basisconcept
author: Kernteam FDS
description: Het basismodel van het Federatief Datastelsel
---

## Inleiding

Deze pagina beschrijft het basismodel van het Federatief Datastelsel (FDS), dat
als _**stelsel van stelsels**_ deel uitmaakt van het [[Concept data-ecosysteem Nederlandse overheid]]().
Het vervangt de eerder opgestelde
['houtskoolschets'](https://realisatieibds.nl/file/download/21091f37-326c-4383-a60a-e04fb09d5c83/220030-fds-houtskoolschets.pdf)
die is gepubliceerd tijdens de stelseldag in november 2022.

Dit basismodel vormt het vertrekpunt voor de verdere uitwerking van FDS in een
doelarchitectuur. Het is niet 'in beton gegoten', maar tegelijkertijd is het wel
een ankerpunt dat niet voortdurend kan veranderen. Daarom is dit een abstract en
zo kort mogelijk verhaal met doorverwijzingen naar diepgaandere uitwerkingen.

Concreet bestaat het basismodel uit een compact diagram met een korte
aanvullende beschrijving. Eventuele wijzigingen aan het concept moeten
weloverwogen worden aangebracht, rekening houdend met de gevolgen ervan voor de
diepere uitwerkingen.

## Context

Complexe maatschappelijke opgaven vragen steeds vaker om samenwerking binnen en
tussen sectoren, in ketens en netwerken waarin gegevens gezamenlijk worden
gebruikt. De [Interbestuurlijke Datastrategie](https://realisatieibds.nl/)
beschrijft een strategie gericht op een beter gebruik van het datapotentieel van
en door de Nederlandse overheid. Niet alleen beter gebruik, maar ook juridisch
en ethisch verantwoord gebruik. Een effectief functionerend Federatief
Datastelsel (FDS) wordt daarbij gezien als een essentieel middel op nationaal
niveau, ingebed in Europese kaders.

Meer lezen over:

- [[Doel en aanpak van FDS]]() 
- [[Scope van FDS]]() 
- [[Uitgangspunten van FDS]]()

## Diagram en korte beschrijving basismodel

| ![FDS Basismodel](images/fds-basismodel.png) |
|:--------------------------------------------:|
|                FDS Basismodel                |

Het basismodel kent de volgende componenten:

### Data-vraag
  
  De vraag naar stelseldata wordt ingevuld doordat een **Data-afnemer** een of
  meer datadiensten afneemt van een of meer data-aanbieders. Voor de afnemer
  geldt evenals voor de aanbieder dat hij structureel en aantoonbaar voldoet aan
  de FDS-deelnamevoorwaarden.

  Om aan te sluiten op datadiensten van een data-aanbieder, maakt de
  data-afnemer gebruik van een **toepassing** die conform de afspraken en
  standaarden van het stelsel is ontworpen en geïmplementeerd.
  
  Meer lezen over:

  - [Stelselrol van afnemer](../stelselrollen/#data-afnemer)

### Data-aanbod

  Het data-aanbod krijgt gestalte doordat **Data** in het stelsel wordt
  ingebracht vanuit (organisaties in) **datadomeinstelsels** (zie ook
  [[Concept data-ecosysteem Nederlandse overheid|Data-ecosysteem Nederlandse overheid]]().
  Voor de aanbieder geldt evenals voor de afnemer dat hij structureel en
  aantoonbaar voldoet aan de FDS-deelnamevoorwaarden.
  
  Beschikbare data wordt door een **Data-aanbieder** aangeboden door middel van
  datadiensten.

  Meer lezen over:

  - [[Stelseldata]]()
  - [Stelselrol van aanbieder](../stelselrollen/#data-aanbieder)
  - [[Dataservices|Datadienst]]()

### Stelselmechanismen

  FDS heeft mechanismen om te borgen dat data betrouwbaar en vertrouwd in het
  stelsel kan worden gedeeld en dat dit juridisch is toegestaan en ethisch is
  verantwoord. Daarvoor wordt het stelsel opgebouwd aan de hand van een
  **architectuurraamwerk**, waaruit de inrichting van de **stelselfuncties**
  wordt afgeleid. De stelselfuncties bestaan uit afspraken over data,
  data-aanbod en data-vraag, met waar nodig nadere afspraken over het toepassen
  van standaarden en het gebruiken van stelselvoorzieningen.

  Meer lezen over:

  - [[Raamwerk|Architectuurraamwerk]]()
  - [[Stelselfuncties]]()
