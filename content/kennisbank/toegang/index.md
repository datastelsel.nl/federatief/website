---
title: Toegang
date: 2023-10-17
aliases: 
  - /kennisbank/capabilities/vertrouwen/toegang
tags: 
description: >
  Vertrouwen | Toegang: Toegangscontrole en gebruikscontrole
---

{{< capabilities-diagram selected="toegang" >}}

## Beschrijving

Deze stelselfunctie bepaalt of, en in welke mate, toegang wordt verleend tot aangeboden gegevens.
Zowel de aanbieder als de afnemer implementeert mechanismen voor datatoegang om misbruik van
gegevens te voorkomen. Toegang vertrouwt op betrouwbaar geïdentificeerde partijen zoals omschreven
in de stelselfuncties [[Identiteit]]() en [[Veiligheid]]().

## Meer lezen

De stelselfunctie Identiteit is gebaseerd op het
[Access and Usage Control/Policies building block](https://docs.internationaldataspaces.org/ids-knowledgebase/v/open-dei-building-blocks-catalog#data-sovereignty-and-trust-building-blocks)
zoals beschreven in het
[OPEN DEI design principles position paper on Resources](https://design-principles-for-data-spaces.org/).
 In de [Blueprint van het Data Spaces Support Centre](https://dssc.eu/page/knowledge-base) wordt
 toegang beschreven in het Building Block
[Access and Usage Policies Enforcement](https://dssc.eu/space/BVE/357075567/Access+%2526+Usage+Policies+Enforcement).

Meer informatie over het toepassen over policies is beschreven in het
[[PBAC|Position paper Policy Based Access Control]]() en de resultaten van de projecten [[Lock-Unlock]]()
en [[Federatieve Toegangsverlening]]().

## Toegang binnen het FDS

De stelselfunctie toegang ondersteunt de organisatorische stelselfunctie van
[[Poortwachter|poortwachter]]().

Toegangsverlening kent beperkingen op verschillende niveau's:

* Het bepalen of een verbinding wordt aangegaan
* Het bepalen of een gegevensvraag mag worden gesteld
* Het bepalen of gegevens betreffende een entiteit worden geleverd
* Het bepalen welke gegevens van een entiteit worden geleverd

Implementatie van deze beperkingen kan worden uitgedrukt in bijvoorbeeld [[PBAC|Policies]](). Voor
de [[FSC]]() standaard is een extensie in ontwikkeling om policies met [[FSC]]() te combineren.
Zowel de aanbieder als de afnemer hebben een verantwoordelijkheid bij een zorgvuldige levering.
Beide partijen dienen daar waar nodig beperkingen toe te passen. Wie welke beperkingen toepast is
onderhevig aan onderlinge en stelselbrede te maken afspraken.

In het bijzonder bij persoonsgegevens worden beperkingen dusdanig ingericht dat alleen de minimaal
ten behoeve van de grondslag benodigde gegevens worden uitgewisseld (dataminimalisatie).

Er is een beperkt aantal standaarden met betrekking tot toegangsverlening, bijvoorbeeld
[ODRL](https://www.w3.org/TR/odrl-model/) en
[XACML](https://docs.oasis-open.org/xacml/3.0/xacml-3.0-core-spec-os-en.html). Welke standaarden
geschikt zijn in de context van FDS is nog onderwerp van onderzoek. Zie in dit kader ook de
resultaten van de projecten [[Lock-Unlock]]() en [Federatieve
toegangsverlening](https://digilab.overheid.nl/projecten/toegangsverleningmethodiek-api/).

## Standaarden

* [ODRL](https://www.w3.org/TR/odrl-model/)
* [XACML](https://docs.oasis-open.org/xacml/3.0/xacml-3.0-core-spec-os-en.html)
* [[FSC]]()
