---
title: Aanbod (metadata)
date: 2024-08-19
tags: 
description: >
  Registratie van het aanbod binnen het FDS met behulp van DCAT.
---

## Mechanisme

Het aanbod beschrijft de aangeboden datasets en de dataservices die
toegang bieden tot deze datasets. Het aanbod is metadata en wordt 
beschikbaar gesteld als linked data volgens de 
[DCAT](https://www.forumstandaardisatie.nl/open-standaarden/dcat) standaard.
Hierbij zijn de [Linked Data Design Rules](../linked-data-design-rules/) 
van toepassing.

Elke aanbieder publiceert via self-description zelf een bestand met
het aanbod van betreffende aanbieder. Dit bestand bevat een
[DCAT](https://www.forumstandaardisatie.nl/open-standaarden/dcat) 
catalog met van daaruit verwezen alle door de aanbieder aangeboden
datasets en dataservices die voldoen aan de FDS criteria.

Elke deelname-administrateur publiceert een bestand met het volledige
aanbod binnen het FDS. Dit bestand bevat een
[DCAT](https://www.forumstandaardisatie.nl/open-standaarden/dcat) 
catalog met van daaruit verwezen alle vanuit deelnemerkenmerken
verwezen [DCAT](https://www.forumstandaardisatie.nl/open-standaarden/dcat) 
catalogs van aanbieders.

Indien beschikbaar worden vanuit de door de aanbieder beschreven
datasets en dataservices aanvullende metadata verwezen zoals
kwaliteitskenmerken (op basis van [DQV](https://www.w3.org/TR/vocab-dqv/)), 
begrippen (op basis van 
[SKOS](https://www.forumstandaardisatie.nl/open-standaarden/skos) en
[NL-SBB](https://docs.geostandaarden.nl/nl-sbb/nl-sbb/)) 
en het gehanteerde informatiemodel (op basis van 
[MIM](https://www.forumstandaardisatie.nl/open-standaarden/mim) of 
[OWL](https://www.forumstandaardisatie.nl/open-standaarden/owl)). 

| ![Aanbod metadata via DCAT](images/metadata-aanbod.png) |
| :-----------------------------------------------------: |
| _Aanbod metadata via DCAT_                              |

## Voorbeelden

Een voorbeeld van het volledige aanbod vanuit een administrateur
(in [Turtle](https://www.w3.org/TR/turtle/)):

```
@prefix dcat: <http://www.w3.org/ns/dcat#>
@prefix dct: <http://purl.org/dc/terms/>
@prefix fds: <http://fds.example.org/ontology#>
@base <http://admin-a.example.org/fds/dcat/>

<fds-catalog>
   dcat:contactPoint 'info@admin-a.example.org' ;
   dct:title 'Het FDS aanbod'@nl-nl ;
   dct:description 'Het volledige FDS aanbod van alle aanbieders binnen het FDS.'@nl-nl ;
   dct:publisher <http://admin-a.example.org/fds/deelnemers/self#deelnemer> ;
   dcat:catalog <http://deelnemer-x.example.org/fds/dcat#catalog> ,
                <http://deelnemer-z.example.org/fds/dcat#catalog> .

```

Een voorbeeld van het het aanbod vanuit een aanbieder
(in [Turtle](https://www.w3.org/TR/turtle/)):

```
@prefix dcat: <http://www.w3.org/ns/dcat#>
@prefix dct: <http://purl.org/dc/terms/>
@prefix fds: <http://fds.example.org/ontology#>
@base <http://deelnemer-x.example.org/fds/dcat#>

<catalog>
   dcat:contactPoint 'info@deelnemer-x.example.org' ;
   dct:title 'Het FDS aanbod van Deelnemer X.'@nl-nl ;
   dct:description 'Het aanbod van Deelnemer X binnen het FDS.'@nl-nl ;
   dct:publisher <http://admin-a.example.org/fds/deelnemers/organisatie-x#deelnemer> ;
   dcat:service <service-a> ,
                <service-b> ;
   dcat:dataset <dataset-a> .

.... definitie van datasets en dataservices ....

```

Binnen FDS verwachten we gebruik te gaan maken van 
[DCAT-AP-NL 3.0](https://docs.geostandaarden.nl/dcat/dcat-ap-nl30/). Op
basis van dit generieke applicatieprofiel wordt naar verwachting verder
aangescherpt in een FDS applicatieprofiel in de vorm van een aanvullende
[OWL](https://www.forumstandaardisatie.nl/open-standaarden/owl) en/of 
[SHACL](https://www.forumstandaardisatie.nl/open-standaarden/shacl) 
definities.

## Standaarden
* [SKOS](https://www.forumstandaardisatie.nl/open-standaarden/skos)
* [NL-SBB](https://docs.geostandaarden.nl/nl-sbb/nl-sbb/)
* [DCAT](https://www.forumstandaardisatie.nl/open-standaarden/dcat), zie 
ook [DCAT-AP-NL 3.0](https://docs.geostandaarden.nl/dcat/dcat-ap-nl30/)
* [MIM](https://www.forumstandaardisatie.nl/open-standaarden/mim)
* [OWL](https://www.forumstandaardisatie.nl/open-standaarden/owl)
* [DQV](https://www.w3.org/TR/vocab-dqv/)
* [RDF](https://www.forumstandaardisatie.nl/open-standaarden/rdf)
* [RDFS](https://www.forumstandaardisatie.nl/open-standaarden/rdfs)
* [OWL](https://www.forumstandaardisatie.nl/open-standaarden/owl)
* [SHACL](https://www.forumstandaardisatie.nl/open-standaarden/shacl)
