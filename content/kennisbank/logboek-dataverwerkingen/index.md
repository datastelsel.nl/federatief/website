---
title: Logboek Dataverwerkingen
linkTitle: LDV
date: 2024-12-19
author: Projectteam Logboek Dataverwerkingen / MinBZK
status: voorstel
description: >
  Hier lees je meer informatie over het project Logboek Dataverwerkingen (LDV)
---

De standaard [Logboek Dataverwerkingen](https://logius-standaarden.github.io/logboek-dataverwerkingen/)
biedt een basis om te zorgen dat de overheid precies de data logt die zij nodig heeft om
verantwoording af te leggen over haar taken. Niet meer, maar ook niet minder. En om te zorgen dat
organisaties data zodanig loggen dat zij zich niet alleen over een eigen handelen kunnen
verantwoorden, maar ook over hun gezamenlijk handelen als “de overheid”. Het project Logboek
Dataverwerkingen is onderdeel van het ministerie van BZK. De
Vereniging van Nederlandse Gemeenten (VNG) en Geonovum werken hieraan mee.

Snelle links naar Logius | Logboek Dataverwerkingen: 
[Standaard](https://logius-standaarden.github.io/logboek-dataverwerkingen/) |
[Praktijkrichtlijn](https://logius-standaarden.github.io/logboek-dataverwerkingen_Inleiding/) |
[Juridische beleidskader](https://logius-standaarden.github.io/logboek-dataverwerkingen_Juridisch-beleidskader/)

## Waarom een standaard voor logging van gegevensverwerking?

De overheid wil voor burgers en bedrijven zo transparant mogelijk zijn in de omgang met hun
gegevens. Daarom is het bij de informatieverwerking in datasets belangrijk om voor elke mutatie of
raadpleging vast te leggen wie deze actie wanneer uitvoert, en waarom. Deze herleidbaarheid speelt
zowel een rol in het kader van de wetgeving op het gebied van privacy als ook het streven naar
openheid en transparantie bij de overheid. Voor een optimale samenwerking over organisaties en
bronnen heen is voor deze logging een algemene standaard nodig: de standaard
[Logboek Dataverwerkingen](https://logius-standaarden.github.io/logboek-dataverwerkingen/).

## De juridische basis

Het [juridisch beleidskader](https://logius-standaarden.github.io/logboek-dataverwerkingen_Juridisch-beleidskader/)
biedt het overzicht van de (juridische) verantwoording die de overheid over haar handelen moet
afleggen en is opgesteld ten behoeve van de standaard Logboek Dataverwerkingen. Er wordt toegelicht
hoe de standaard is opgebouwd vanuit het perspectief van verantwoording. Het doel daarvan is dat het
Logboek Dataverwerkingen een basis biedt om te zorgen dat de overheid precies de gegevens logt die
zij nodig heeft om verantwoording af te leggen over haar taken. Niet meer, maar ook niet minder. En
om te zorgen dat organisaties gegevens zodanig loggen dat zij zich niet alleen over een eigen
handelen kunnen verantwoorden, maar ook over hun gezamenlijk handelen als “de overheid”.

## Aanleiding en context

Informatiehuishouding van de overheid moet op orde worden gebracht. De overheid werkt ten dienste
van burgers en bedrijven. De overheid verwerkt daarvoor informatie van deze burgers en bedrijven.
Het is belangrijk dat de informatiehuishouding van de overheid op orde is, zodat de overheid
transparant en aanspreekbaar is, en zich daarover goed kan verantwoorden. Eenduidige en integrale
verantwoording over dataverwerkingen door de overheid. Belangrijk is dat overheidsorganisaties op
een eenduidige manier met informatie omgaan en op een eenduidige manier informatie met elkaar
uitwisselen.

Het project Logboek Dataverwerkingen maakt deel uit van het actieplan
[Data bij de Bron](https://www.digitaleoverheid.nl/data-bij-de-bron/) en onderzoekt met Digilab
in samenwerking met diverse overheidspartijen (ministeries, uitvoeringsorganisaties en gemeentes) of
we op basis van de tot nu toe opgedane inzichten een overheidsbrede standaard kunnen vaststellen.

## Verbetering informatiehuishouding

Een belangrijk instrument om verbetering van de informatiehuishouding te bereiken is
standaardisatie. Op diverse aspecten is daarom standaardisatie nodig en worden deze ontwikkeld. Een
van deze aspecten is de wijze waarop overheden zich verantwoorden. Standaardisatie daarvan vormt
daarmee een puzzelstuk in het bredere geheel. Hiermee kunnen overheden hun dataverwerkingen op
dezelfde wijze verantwoorden en deze verantwoording onderling relateren, zodat de keten van
dataverwerkingen tussen organisaties compleet inzichtelijk kan worden gemaakt.

## Globale opbouw

De standaard Logboek Dataverwerkingen beschrijft een manier om technisch interoperabele
functionaliteit voor het loggen van dataverwerkingen te implementeren, door voor de volgende
functionaliteit de interface en het gedrag voor te schrijven:

* het vastleggen van logs van dataverwerkingen
* het aan elkaar relateren van logs van dataverwerkingen
* het aan elkaar relateren van dataverwerkingen over de grenzen van systemen

Door Dataverwerkingen te loggen volgens de standaard kunnen organisaties het datagebruik
verantwoorden. De standaard is gericht op verantwoording van Dataverwerkingen door Nederlandse
(overheids)organisaties, gelet op onder meer de Algemene Verordening Gegevensbescherming en de
Algemene Wet Bestuursrecht.

## Doelgroep

De standaard heeft als doelgroep iedereen die zich bezighoudt met het implementeren van logging rond
dataverwerkingen en beschrijft alleen wat relevant is voor de implementatie.

## De informatiekundige kern en de relatie met FDS

Dataverwerkingen kunnen over applicatiegrenzen heengaan. In sommige gevallen worden gegevens
opgevraagd bij andere applicaties of bij andere organisaties. Om grote en complexe dataverwerkingen
te overzien en uiteindelijk terug te herleiden, is het van belang om traceringsmetadata te loggen
naast de reguliere data die verwerkt is. Het model van traceringsmetadata in de standaard is
gebaseerd op de internationale standaard [OpenTelemetry](https://opentelemetry.io/).

De logging van verwerking van data vindt plaats op basis van zogenaamde ‘value pairs’. Dit betekent
dat het data-element genoemd wordt en daarbij de waarde (bijvoorbeeld postcode = 1234AB). De
benaming van de te loggen data moet in overeenstemming zijn met de FDS-filosofie. Meer
achtergrondinformatie en voorbeelden over de Logius Praktijkrichtlijn is te vinden op
[Algemene inleiding - Logboek Dataverwerkingen](https://logius-standaarden.github.io/logboek-dataverwerkingen_Inleiding/).
