---
title: Deelnemer-ID
date: 2024-08-19
tags: 
description: >
  De identificatie van een deelnemer binnen het FDS.
---

## Identificatie van de deelnemer

Een deelnemer van het FDS heeft een Deelnemer-ID. Een deelnemer-ID kan als URL worden gebruikt om
een linked data ([RDF](https://www.forumstandaardisatie.nl/open-standaarden/rdf)) document op te
halen met [[Deelnemerkenmerken]]() zoals het KvK-nummer, RSIN en/of OIN. 
Het Deelnemer-ID is een [LinkedData](https://en.wikipedia.org/wiki/Linked_data)
[URI](https://www.forumstandaardisatie.nl/open-standaarden/uri-en-iri). Op de deelnemer-ID 
zijn de [[Linked Data Design Rules]]() van toepassing. Zie voor meer informatie over
identiteiten de stelselfunctie [[Identiteit]]().

Mogelijk kan het Deelnemer-ID ook als [Distributed Identity (DID)](https://www.w3.org/TR/did-core/)
gebruikt worden om uitwisseling als [Verifiable Credentials (VC)](https://www.w3.org/TR/vc-data-model-2.0/)
mogelijk te maken. In dat geval zou het document met [[Deelnemerkenmerken]]() ook als
DID Document conform de standaard [did-web](https://w3c-ccg.github.io/did-method-web/)
kunnen dienen. Dit is echter vooralsnog onvoldoende uitgewerkt om al concreet toe 
te kunnen passen. Daarnaast is het nog onzeker of betreffende standaarden
passend zijn voor toepassing binnen FDS.

## Standaarden
* [RDF](https://www.forumstandaardisatie.nl/open-standaarden/rdf)
* [URI](https://www.forumstandaardisatie.nl/open-standaarden/uri-en-iri)
* [DID](https://www.w3.org/TR/did-core/)
* [did-web](https://w3c-ccg.github.io/did-method-web/)
* [VC](https://www.w3.org/TR/vc-data-model-2.0/)
