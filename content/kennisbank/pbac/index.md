---
title: Policy Based Access Control
linkTitle: PBAC
date: 2024-05-15
author: Kernteam FDS
status: Geaccepteerd
description: |
  Position paper Policy Based Access Control voor een Federatief Datastelsel
---

{{% imgproc "**capabilities*.*" Resize "x200" /%}}

| <span class="h2">It’s PBAC-time</span>                     |
|:----------------------------------------------------------:|
|![PBAC voorblad](images/PBAC_5_voorblad.jpg)                |

## Belang en scope

### Identity- and Access Management

Identity- and Access Management (IAM) is vrij vertaald het beheer om ervoor te zorgen dat de juiste
identiteiten, vooral personen of systemen, voor de juiste redenen en op het juiste moment toegang
krijgen tot de juiste faciliteiten. IAM is zowel nodig voor het kunnen leveren van diensten als om
te voorkomen dat er aan de verkeerde persoon wordt geleverd, en is van belang voor zowel de
dienstaanbieder als de -afnemer. IAM zorgt voor drie belangrijke voorwaarden voor digitale
dienstverlening[^1]:

1. Identificatie zorgt er voor dat we weten wie je bent. Dit is je identiteit of een van je digitale
   identiteiten (accounts);
2. Authenticatie zorgt er voor dat we met een bepaalde zekerheid weten dat je ook echt degene bent
   die je zegt te zijn;
3. Autorisatie zorgt er voor dat we weten wat je dan mag (toegang) of juist niet mag. I n dit
document bespreken we Autorisatie.

### Scope

Dit document focust op autorisatie en bespreekt de onderwerpen identificatie en authenticatie alleen
in context van autorisatie. Bijvoorbeeld wanneer het autorisatiemechanisme eisen stelt aan deze twee
onderwerpen. Voor uiteindelijke implementatie is een goede uitwerking van al deze onderwerpen in samenhang een
vereiste.

De scope van dit document is vastgesteld op datasamenwerking tussen overheden (overheidsorganen).
Voor deze position paper is dat voldoende. Bij uiteindelijke besluitvorming over autoriseren (en de
ander zaken omtrent identity- and access management) wordt ook de uitwisseling tussen private
partijen en tussen overheid en private partijen meegenomen.

Tot slot is de reikwijdte van dit paper datasamenwerking tussen overheidsorganen om datatoegang te
verlenen voor het uitvoeren van eigen werkzaamheden. De data-uitwisseling tussen een overheidsorgaan
en haar verwerker behandelen we niet. Hiermee bedoelen we: partij A kan zelf (om wat voor reden dan
ook) zelf geen data verwerken, geeft partij B toegang tot hun data en laat partij b deze verwerken.
De uitwisseling tussen deze partij en verwerker hoort niet bij dit autorisatiedeel.

### Belang voor een Federatief Datastelsel

Het doel van deze position paper over Policy Based Access Control (PBAC) voor het Federatief
Datastelsel is om deze manier van autoriseren te onderzoeken en evalueren. Voor het Federatief
Datastelsel is een schaalbare, beheerbare en beheersbare wijze van toegang verlenen tot
data(-diensten) nodig. De traditionele methode en de manier waarop er momenteel wordt geautoriseerd,
voldoen niet aan de eisen en standaarden die het Federatief Datastelsel stelt, zoals het principe
dat [Data bij de Bron](/kennisbank/data-bij-de-bron/) blijft. Ook sluit PBAC beter dan andere
alternatieven aan bij het bestuursrecht en uitgangspunten die gelden voor de uitwisseling van
gegevens tussen overheden. PBAC lijkt daarom een kansrijke methode om datatoegang in een Federatief
Datastelsel te regelen.

## Autorisatiemechanismen en PBAC

### Verschillende autorisatiemechanismen

Toegangscontroles kunnen op meerdere manieren plaatsvinden. Het kiezen van de juiste methode is
afhankelijk van bijvoorbeeld de complexiteit van het I-landschap, de beheersbaarheid en de
schaalbaarheid. In alle gevallen geldt: een subject (medewerker, burger, organisatie) wil toegang
tot een object (databron, systeem, applicatie). Om een beeld te geven van verschillende manieren van
autoriseren worden hier kort de populairste methoden uitgelegd, op volgorde van simpel tot
geavanceerd:

**Access Control List (ACL)**. Deze methode kun je vergelijken met een gastenlijst. Staat jouw
(digitale) identiteit op de controlelijst? Dan krijg je toegang, anders niet. Deze methode kost
weinig moeite om op te zetten (voor één databron of applicatie) en vraagt vooral een goed
identiteitenbeheer. Deze methode is moeilijk schaalbaar (want elke databron heeft een aparte
controlelijst nodig) en lastig beheersbaar. Daarnaast is er geen onderverdeling in soorten toegang
te maken: eenmaal binnen, kun je overal bij.

| ![Access Control List](images/PBAC_1_ACL.jpg) |
| :-------------------------------------------: |
| _Figuur 1: Access Control List_               |

**Role Based Access Control (RBAC)**. Dit is de 'klassieke' manier van autoriseren waarbij
toegangsrechten worden gekoppeld aan rollen en aan deze rollen worden gebruikers toegewezen.
Meerdere toegangsrechten worden dus geclusterd tot 1 rol. De logica van het toekennen van rollen is
vooral gebaseerd op de functie van de medewerker. Een voorbeeld van een toegangsregel is: alle
medewerkers met de rol 'Functioneel beheerder Sharepoint' mogen bepaalde toegangsrechten in de
applicatie Sharepoint. Het beheer voor deze autorisatiemethode is op meerdere plaatsen belegd. Aan
de kant van de organisatie moeten de juiste mensen worden toegewezen aan de juiste rollen, en aan de
kant van de applicatie moeten de juiste rechten worden gekoppeld aan de juiste rollen. RBAC is veel
beter beheersbaar en schaalbaar dan ACL, en is een goed systeem voor toegang van medewerkers tot
applicaties en databronnen binnen dezelfde organisatie of hetzelfde I-landschap. Dit wordt lastiger
met identiteiten van buiten de organisatie. Daarnaast kan het aantal rollen enorm toenemen naarmate
de tijd vordert, en blijft het grootste gedeelte van deze methode handmatig werk (toevoegen van
medewerkers aan rollen).

| ![Role Based Access Control](images/PBAC_2_RBAC.jpg) |
| :--------------------------------------------------: |
| _Figuur 2: Role Based Access Control_                |

**Attribute Based Access Control (ABAC)**. Attributen zijn stukjes informatie over een persoon,
object of situatie. Gebruikersattributen geven context over een medewerker, zonder dat de volledige
identiteit wordt gegeven. Dit kunnen bijvoorbeeld haar afdeling, rang, rol, of afgeronde opleidingen
zijn. Bronattributen geven context over een databron, zoals wie de eigenaar is, het type bestand, of
rubriceringsniveau. Bij ABAC worden specifieke attributen van de medewerker gekoppeld aan specifieke
attributen van de databron of applicatie, en deze combinatie geeft wel of geen toegangsrechten.
Naast attributen van een persoon of object, bestaan er ook omgevingsattributen zoals tijd, locatie
en apparaattype, die gebruikt kunnen worden als extra restrictie in de toegangsregel. Je kunt
bijvoorbeeld de autorisatieregel krijgen: "elke medewerkers van afdeling X mag toegang tot data met
attribuut afdeling X, op werkdagen tussen 08.00 en 20.00 uur". In vergelijking met RBAC is ABAC
flexibel, nog beter beheersbaar en schaalbaar, en daarnaast privacyvriendelijker, omdat alleen de
attributen van een medewerker gebruikt worden, die nodig zijn voor de toegangsregel. Ook neemt ABAC
handwerk weg: in plaats van handmatig medewerkers aan rollen toe te moeten voegen (RBAC), krijgt de
medewerker automatisch (proactief) toegang als deze de juiste attributen heeft. Voor ABAC is wel een
geavanceerder landschap nodig, met verschillende registraties voor de attributen, met goed geregeld
eigenaarschap en een geavanceerd autorisatieregelmechanisme.

**Policy Based Access Control (PBAC)[^2]**. RBAC en ABAC maken gebruik van ‘statische’ elementen,
namelijk de rollen en attributen. Deze elementen staan in registraties in het landschap en worden
alleen veranderd als er iets wijzigt bij het subject (de medewerker) of het object (applicatie of
databron). PBAC gebruikt een dynamische, contextgebaseerde aanpak. Net als ABAC gebruikt PBAC ook
attributen voor het maken van toegangsregels, maar PBAC gaat hierbij nog een stap verder door deze
te combineren met policies, oftewel beleidsregels. Door het gebruik van policies wordt het systeem
dynamischer, omdat het sneller kan worden aangepast aan veranderende omstandigheden. Daarnaast
zorgt PBAC (specifiek: het gebruik van policies) ervoor dat het gemakkelijker is om
omgevingsattributen mee te nemen in de autorisatiebeslissing. Je krijgt bijvoorbeeld alleen toegang
als je via een geregistreerd device op een bepaalde manier toegang verzoekt. Naleving van
regelgeving wordt op deze manier ingebouwd. Het grootste voordeel van PBAC is dat het goed werkt in
een complexe structuur van verschillende organisaties. Zie hiervoor het hoofdstuk hierna [Hoe werkt
PBAC](#hoe-werkt-pbac).

PBAC maakt het mogelijk om alle voorwaarden voor toegang op één plaats inzichtelijk te hebben (in
het Policy Administration Point, afgekort PAP). Bij ABAC kan dat ook, maar implementaties zijn vaak
beperkt tot een beperkt aantal attributen. Bij RBAC is het vaak niet meer inzichtelijk of er nog
contextgerelateerde beperkingen zijn.

Kortom, het gebruik van PBAC in een samenwerking met meerdere organisaties biedt een gestroomlijnde,
veilige en goed beheerde aanpak voor het beheren van toegangsrechten en het handhaven van
beveiligingsstandaarden over alle organisatorische eenheden heen. Door deze afspraken te publiceren
wordt de samenwerking tevens transparant voor alle belanghebbenden gemaakt.

### Hoe werkt PBAC

| ![Een autorisatieaanvraag in PBAC (XACML)](images/PBAC_3_PBAC_1org.jpg) |
| :---------------------------------------------------------------------: |
| _Figuur 3: Een autorisatieaanvraag in PBAC (XACML)_                     |

Datatoegang wordt via PBAC als volgt geregeld. Een gebruiker (het subject) wil graag toegang tot de
data (het object). Een autorisatieverzoek wordt ontvangen door het Policy Enforcement Point (PEP).
Dit punt is de slagboom van het systeem en verleent toegang of niet. Het PEP stuurt het verzoek naar
het Policy Decision Point (PDP). Dit punt neemt de uiteindelijke beslissing of het verzoek leidt tot
toegang of weigering. Om deze beslissing te maken heeft het punt informatie nodig: de
autorisatieregels die gelden voor deze situatie en de informatie over subject (wie doet het verzoek),
object (waartoe wordt toegang verzocht) en omgeving (onder welke omstandigheden wordt toegang
verzocht). Het PDP stuurt het verzoek door naar het PIP en het PAP. Het Policy Administration Point
(PAP) levert de juiste voorwaarden voor datatoegang en het Policy Information Point (PIP) de juiste
informatie over subject, object en omgeving. Het PDP clustert de informatie en maakt de beslissing
of de geleverde informatie volstaat om aan de voorwaarden van toegang tot de data te voldoen. Indien
ja, dan geeft het PDP een signaal aan het PEP om de toegang te verlenen. Zo nee, dan geeft het PDP
een signaal aan het PEP dat de slagboom dicht blijft. Zie bovenstaande figuur voor een schematische
weergave.

Samengevat:

- Policy Enforcement Point (PEP)
- Policy Decision Point (PDP)
- Policy Administration Point (PAP)
- Policy Information Point (PIP)

### Hoe werkt PBAC over verschillende organisaties in een Federatief Datastelsel

Wanneer PBAC als autorisatiemechanisme wordt toegepast over verschillende organisaties ziet dat er
ongeveer zo uit als in figuur 4. Het verzoek tot data van het subject wordt eerst getoetst bij de
eigen organisatie. Deze bekijkt voor dit specifieke toegangsverzoek aan de hand van het vooraf
afgesproken afsprakenstelsel welke policies er gelden (PAP) en welke informatie van het subject en
de omgevingsfactoren van toepassing zijn (PIP). De PDP (nog steeds bij de datavragende organisatie)
bepaalt of het verzoek wordt goedgekeurd op basis van de beschikbare informatie en stuurt het
goedgekeurde verzoek door via de eigen PEP naar de PEP van de dataleverende partij[^3]. In deze
goedkeuring staat beschreven dat er volgens de datavragende partij wordt voldaan aan de vooraf
opgestelde eisen voor datatoegang, en voor welke vooraf afgesproken data dit van toepassing is.
Wanneer de PDP van de datavragende partij het verzoek van het subject afkeurt, wordt er geen verzoek
naar de dataleverende partij gestuurd.

De PEP van de dataleverende partij ontvangt de informatie van de datavragende partij en bepaalt of
deze goedkeuring nog steeds geldt met de informatie die de datavragende partij zelf heeft. Het
verzoek gaat langs via de PEP naar de PDP en deze vult het verzoek aan met eigen informatie, vooral
informatie en context over de data. De PDP legt de vooraf afgesproken policies uit de PAP, de
(data)informatie uit de PIP naast het binnengekomen verzoek en besluit of de datatoegang wordt
toegekend. Indien ja, dan krijgt de PEP het signaal dat het subject van de datavragende partij
toegang mag krijgen tot het object van de dataleverende partij. Zo niet, dan wordt het
oorspronkelijke verzoek tot data alsnog afgekeurd en wordt er geen toegang verleend.

Zowel datavragende als dataleverende partij voeren policies uit om het verzoek goed of af te keuren.
Hoewel de policies onderdeel uit maken van het afsprakenstelsel tussen beiden partijen, zijn dit
doorgaans niet dezelfde policies. De datavragende partij voert de checks uit aan de subjectkant,
bijvoorbeeld: behoort de medewerker tot de mensen die data mag opvragen, of mag de organisatie deze
data bij de dataleverende partij opvragen? De dataleverende partij voert policies uit als check aan
de datakant, bijvoorbeeld: onder welke omstandigheden mag deze data worden opgevraagd, of: is deze
data gelabeld als data die gedeeld mag worden?

Hier ligt ook een sterke relatie met de andere onderdelen van IAM, voornamelijk Identity Management.
Welke gegevens moet een organisatie vastleggen om met dit stelsel te kunnen werken en welke gegevens
hiervan komen terug in de policies? Hoe de identiteit[^4] eruit komt te zien is een ander vraagstuk
dat verdere uitwerking vraagt.

| ![PBAC over meerdere organisaties](images/PBAC_4_PBAC_2org.jpg) |
| :-------------------------------------------------------------: |
| _Figuur 4: PBAC over meerdere organisaties_                     |

De poortwachter van het Federatief Datastelsel heeft ook een belangrijke rol in dit mechanisme.
Vanuit het Federatief Datastelsel worden de stelselpolicies gedeeld met de organisaties. Deze
stelselpolicies bestaan uit de standaarden vanuit het Federatief Datastelsel en de
samenwerkafspraken tussen de organisaties. Zie hoofdstuk [De poortwachter binnen het Federatief
Datastelsel](#de-poortwachterfunctie-binnen-het-Federatief Datastelsel) met meer informatie over
deze Poortwachter.

Tot slot, deze situatie beschrijft een tussenfase. Uiteindelijk, bij een goed opgezet en
functionerend stelsel, en bij bevraging tussen twee overheidsorganen, zal het zwaartepunt van het
uitvoeren van de checks met behulp van policies gaan liggen bij de datavragende partij.

## Uitgangspunten

### Eigenaarschap en verantwoordelijkheid op de juiste plaats

Wanneer datatoegang op de traditionele manier, zoals het veelgebruikte RBAC, is ingericht, wordt er
veel gevraagd van de aanbieders van data. Deze partij moet immers kunnen toetsen of degene die
toegang vraagt ook daadwerkelijk toegang mag krijgen, en moet daardoor allerlei checks doen op basis
van eigen objectinformatie (de informatie over de data) en de subjectinformatie van de aanvrager (de
informatie over degene die toegang wil, als deze al op de juiste manier wordt gedeeld). In de
praktijk betekent dit, dat een  groot gedeelte van het werk om de data-aanvraag te toetsen voor
rekening van de dataleverende partij komt.

Door gebruik te maken van een generiek systeem, opgesteld vanuit het Federatief Datastelsel, op
basis van PBAC, is het mogelijk om de beheerlast te verschuiven naar de vrager van data. De
datavragende partij laat dan zien, op basis van eigen informatie en vooraf afgesproken informatie
van de data, dat deze voldoet aan de policy om toegang te krijgen tot de data van de dataleverende
partij.

Het lijkt dan misschien alsof er veel verantwoordelijkheid bij de afnemer gelegd wordt, maar de
dataleverende partij heeft de verantwoordelijkheid om te zorgen dat de voorwaarden compleet zijn.
PBAC heeft het voordeel dat het toegang verlenen beheersbaar, uitbreidbaar, en responsief houdt,
vanuit een dataeigenaar-perspectief. Op deze manier wordt de werklast voor datatoegang verdeeld over
de datavragende en dataleverende partij en ligt de verantwoordelijkheid voor het aantonen van
verschillende delen informatie bij de juiste partijen.

Kortom, door gebruik te maken van PBAC worden eigenaarschap en verantwoordelijkheid op de juiste
plek belegd.

### Zero trust

PBAC voldoet aan het Zero trust[^5] principe, dat uitgaat van het grondbeginsel _never trust, always
verify_. Vroeger was de beveiliging primair gericht op het plaatsen van beveiligingsmaatregelen op
de “de buitenste” laag van de infrastructuur. Het zero trust principe gaat daarentegen uit van
segmentering. Er ontstaat dus een opdeling van bijvoorbeeld meerdere kleine beveiligde netwerken
genaamd _implied trust zones_. Deze _implied trust zones_ helpen bij het structureren van een of
meerdere functionaliteiten door het implementeren van een set aan beveiligingseisen. Als eenmaal is
voldaan aan deze eisen, volgt toegangsverlening. Toegang tot deze _implied trust zones_ gaat gepaard
met sterke authenticatie en autorisatie en het monitoren hierop. Door de structurering in PBAC is
dit autorisatiemechanisme een manier om aan deze autorisatie-eis te voldoen. Dit impliceert ook dat
er in dit systeem dezelfde controles worden gedaan bij zowel interne als externe bevragingen van
data, in plaats van alleen op externe bevragingen in traditionele niet-zero-trust-systemen.

### Datasoevereiniteit

PBAC voldoet aan het principe Datasoevereiniteit. Doordat er met PBAC gebruik wordt gemaakt van een
afsprakenstelsel met geautomatiseerde besluitvorming, wordt er dus vooraf aan de samenwerking
besloten aan welke voorwaarden beide partijen moeten voldoen om data te kunnen uitwisselen. De
dataleverende partij hoeft op het moment van de aanvraag niet meer te besluiten of de
datavragende partij toegang krijgt of niet. Dit gebeurt namelijk aan de voorkant in plaats van op
het moment van data-aanvraag en de datatoegang wordt automatisch geregeld. Dit maakt het vooraf
transparant wanneer data tussen deze partijen mag worden gedeeld en biedt tegelijkertijd de vrijheid
om deze afspraken flexibel te wijzigen wanneer er een verandering in de samenwerking ontstaat.

Vooraf toetsen van afspraken gebeurt tegenwoordig al in veel gevallen waarin persoonsgegevens en de
AVG een rol spelen. Een afnemer kan bij een aanbieder persoonsgegevens aanvragen om te mogen
gebruiken, waarvoor de afnemer een wettelijke taak heeft als grondslag. De aanbieder toetst hierbij
ook vooraf of dit legitiem is en neemt een autorisatiebesluit. Het verschil tussen de huidige
situatie en de toekomstige situatie met PBAC, is dat er een structureel systeem wordt neergezet
waarbij er veel kan worden geautomatiseerd. Het handmatige werk van het checken van legitimiteit en
het nemen van een autorisatiebesluit, wordt door PBAC aan de voorkant goed geregeld zodat er
real-time geen lange doorlooptijden meer zijn en er minimaal handwerk is. Het autorisatiebesluit is
geen handmatig besluit meer, wat bijdraagt aan datasoevereiniteit.

### Policies vooraf en audits achteraf

De datavragende partij controleert aan de hand van de policies (vooraf gemaakte afspraken) en de
context van de aanvraag of zij deze aanvraag moet goedkeuren of afkeuren. De dataleverende partij is
géén toezichthouder en zal niet checken of het subject aan alle eisen voor het aanvragen van data
voldoet (en mag dat ook vaak niet). De dataleverende partij ontvangt een goedgekeurd verzoek en
controleert vervolgens of de context van de data ook voldoet aan de gestelde policies. Dit betekent
niet dat de datavragende partij ‘zomaar’ al hun eigen dataverzoeken kan goedkeuren! Of de vooraf
gemaakte afspraken daadwerkelijk worden nageleefd kan achteraf worden gecontroleerd op elke plaats
waar keuzes worden gemaakt (zie paragraaf [Zero Trust](#zero-trust)). Via een logboek
dataverwerkingen kan worden aangetoond of de datavragende partij correct heeft gehandeld. Deze audit
kan worden uitgevoerd in opdracht van een toezichthouder op dezelfde manier als dat momenteel al
wordt gedaan. De grondslag voor de datatoegang is de combinatie van de policies van beide partijen.

Door autorisatie op deze manier in te regelen wordt de focus van de standaarden (van het Federatief
Datastelsel) gericht op de datavragende partij: zij moeten kunnen verantwoorden waarom ze toegang
tot de data moeten krijgen. Het correct handelen van de datavragende partij is geen
verantwoordelijkheid van de dataleverende partij.

### Privacy

Bij het gebruik van Role Based Access Control (RBAC) moet voor elke datatoegang een identiteit
gekoppeld worden aan een rol. Deze rol wordt vervolgens gekoppeld aan rechten voor datatoegang. Dit
betekent dat als een medewerker van partij A toegang wil tot data van partij B, er in de organisatie
van partij B een digitale identiteit (account) aangemaakt moet worden zodat deze gekoppeld kan
worden aan rollen. Voor partij B is dan precies inzichtelijk wie deze persoon is en dit voldoet niet
aan de vereiste van dataminimalisatie.

Bij het gebruik van Attribute Based Access Control (ABAC) wordt het besluit voor datatoegang gemaakt
door de autorisatieregels en bijbehorende attributen te vergelijken. Dit betekent dat als een
medewerker van partij a toegang wil tot data van partij b, dat de attributen van deze medewerker
(subjectattributen) moeten worden gedeeld met partij b. Hoewel dit minder informatie geeft dan de
volledige identiteit die wordt gedeeld bij RBAC, geven deze attributen nog steeds informatie over de
identiteit van de medewerker. Voor partij B is dan deels inzichtelijk wie deze persoon is en ook
dit voldoet niet aan de vereiste dataminimalisatie.

Bij PBAC worden geen identiteiten of attributen gedeeld met de partij B. Er wordt slechts gedeeld
aan welke policies het subject van partij A voldoet en voor welke data van partij B deze policies
van toepassing zijn. Op deze manier wordt er minimale informatie uitgewisseld over de
identiteit van de medewerker en voldoet de autorisatie aan het vereiste van dataminimalisatie.

### PBAC is in lijn met kaderstellers

Diverse partijen binnen de overheid omarmen deze manier van autoriseren. De VNG geeft aan dat
gemeenten steeds meer op deze manier zouden moeten autoriseren. Ook de architecten vanuit CIO Rijk
bewegen in de richting van PBAC en stellen steeds meer standaarden op om hieraan op een goede manier
te voldoen. In het domein Zorg geeft Nictiz, het Nederlandse kenniscentrum voor landelijke
toepassingen van ICT in de zorg, ook aan dat PBAC de gewenste manier van autoriseren is. Daarnaast
werken er steeds meer Europese projecten met PBAC in plaats van de traditionele
autorisatiemechanismen en ook RVO geeft het signaal dat dit steeds meer de gewenste manier van
autoriseren is.

## Implementatie

### De poortwachterfunctie[^6] binnen het Federatief Datastelsel

De poortwachterfunctie zorgt voor een transparant beslisproces. Het toetst, autoriseert en
registreert koppelingsverzoeken tussen bronnen, toetst op conformiteit met de AVG en de aansluit- en
gebruiksvoorwaarden van het stelsel. De toetsing zal veelal neerkomen op het afwegen van strijdige
belangen, waarbij bijvoorbeeld noodzaak, privacy, ethiek en conformiteit met aansluit- en
gebruiksvoorwaarden van het Federatief Datastelsel een rol spelen. Vooral dat laatste heeft een
sterke relatie met PBAC. Het toetsen gebeurt in een transparant beslisproces met mogelijkheden voor
bezwaar en beroep. De uitkomst van dit proces wordt vastgelegd in een openbare registratie
(gegevensstroom-boekhouding). Deze registratie heeft de volgende functies. Per functie wordt de
relatie met PBAC uitgelegd:

1. Opname van een datadeelrelatie in het register legitimeert de datadeling door daarvoor een
   juridische grondslag te bieden. Deze grondslag is een verwijzing naar een toepasselijk
   wetsartikel of een positief besluit van het toetsingscomité.

   PBAC: Voor elke autorisatieregel, dus een beperking op datadeling, geldt dat deze herleidbaar
   moet zijn naar een juridische grondslag. Deze autorisatieregels inclusief grondslagen worden
   vastgelegd in de policies van PBAC (zie [Policy Administration Point (PAP)](#hoe-werkt-pbac)).

2. Het maakt op één plek inzichtelijk welke stelseldata, met welke partijen, voor welk doel mogen
   worden gedeeld en wat daarvoor de juridische grondslag is. Hiermee kan ook periodiek worden
   getoetst of eerder genomen beslissingen nog steeds valide zijn. Daarnaast ontstaat inzicht op
   welke gebieden aanpassing van wetgeving nodig is om oordelen van het toetsingscomité juridisch
   sterker te verankeren.

   PBAC: De policies inclusief herleiding naar juridische grondslag worden vastgelegd in de Policy
   Administration Point (PAP). Door deze transparant te publiceren en te zorgen dat die via één plek
   inzichtelijk is, kan hieruit het inzicht verkregen worden op welke gebieden aanpassing van
   wetgeving nodig is.

3. Op het Federatief Datastelsel aangesloten data-aanbieders kunnen op basis van de gegevens in het
   register eenvoudig, geautomatiseerd, vaststellen of ze een datadeel-verzoek van een tot het
   stelsel toegelaten afnemer mogen honoreren. Ze hoeven dus niet meer zelf een juridische toets uit
   te voeren.

   PBAC: Door voor het autoriseren het mechanisme PBAC te gebruiken, worden er voorafgaand aan de
   datasamenwerking voorwaarden gesteld aan het delen van data. Het maken van het besluit of iemand
   toegang krijgt tot data wordt geautomatiseerd gedaan door het Policy Decision Point (PDP) op
   basis van de opgestelde policies. De juridische toets, maar ook afwegingen op het vlak van
   ethiek, privacy, maatschappelijke wenselijkheid en belang, zijn vooraf al gedaan en hoeven niet
   plaats te vinden tijdens het proces van datadelen.

4. Vastlegging in het centrale register van datadeelrelaties, in combinatie met de toelating tot
   het stelsel (het voldoen aan de aansluitvoorwaarden) vervangt de bilaterale contracten die nu nog
   vaak nodig zijn om de datadeling tussen partijen te legitimeren.

   PBAC: Door PBAC als voorwaarde vanuit het Federatief Datastelsel te stellen wordt op een eenduidige en
   gestructureerde manier datadeling geregeld. Een datavragende partij kan gestandaardiseerd toegang
   verkrijgen en er hoeft dus geen bilateraal contract nodig te zijn.

Hoe PBAC precies een deel van de poortwachterfunctie kan invullen vereist een uitvoerige
vervolgstudie, bij voorkeur aan de hand van Use Cases.

### Voorbeelden

SUWINET is een goed voorbeeld om te kunnen laten zien hoe het gebruik van PBAC uitdagingen kan
oplossen en verbeteren. Bij dit voorbeeld heeft de dataleverende kant veel verantwoordelijkheid en
daardoor wordt er informatie buiten het systeem om uitgewisseld. Dit is niet de gewenste situatie en
door het gebruik van PBAC kan dit worden verbeterd.

Vanuit CIO-Rijk zijn enkele voorbeelden aangeleverd van policies:

1. Deze dienst mag alleen via een door ons vertrouwd netwerk gebruikt worden.
2. De persoon moet geauthentiseerd zijn door een door ons vertrouwde IDP op het juiste niveau
3. De persoon moet handelen namens een door ons vertrouwd bedrijf.
4. De persoon mag alleen data zien voor het bedrijf namens welke de persoon werkt.

### Uitdagingen

Naast de voordelen van het gebruik van PBAC bestaan er ook enkele uitdagingen in de huidige
context van het Federatief Datastelsel en de overheid:

1. Vooraf weet je niet _precies_ door wie of waarom de data gebruikt gaat worden, dus bij het
   aansluitproces van een afnemer zal er constant gecheckt moeten worden of de policies die er zijn
   wel toereikend zijn én of ze geen gaten laten vallen bij de nieuw situatie. Dit hoort bij het
   principe [Zero Trust](#zero-trust).
2. Binnen de overheid wordt PBAC (volgens CIO-Rijk) nog niet echt ergens geïmplementeerd. Bij grote
   Tech bedrijven of financiële instellingen is het gebruikelijker.
3. Dit document geeft een eerste concept voor hoe de structuur van PBAC binnen een Federatief
   Datastelsel eruit kan komen te zien. De technische implementatie kent echter nog veel
   verschillende smaken. Hier zullen goede afwegingen in gemaakt moeten worden.
4. Complexiteit vanuit een landschapsperspectief: het overheidsorgaan is eigenaar van veel plekken
    waar de data staat:

    1. Er zijn meerdere plekken waar de data staat, want de data staat bij de bron;
    1. Er zijn meerdere afnemers van de data;
    1. Het hele landschap wijzigt ook voortdurend.

    Daarnaast moet toegang tot data eenvoudig en snel toegekend te worden. Dit is waarom er vaak een
    voorkeur is voor policies en niet een RBAC-model.

## Stappen voor vervolg

Naast een discussiestuk (dit document) hebben het project Federatief Datastelsel en
[Digilab](https://digilab.overheid.nl) het voornemen om PBAC in een concrete proefopstelling te
tonen, om tastbaar te maken wat policies zijn en wat ervoor ingeregeld moet worden. Vervolgstappen
worden in een separaat document uitgewerkt en zal de volgende onderwerpen bevatten:

1. Casusbeschrijving en uitwerking van een use case met concrete policies en attributen;
2. Onderzoek naar of XACML (of ODRL) nog steeds de standaard is;
3. Verhouding PBAC met mechanismen van iSHARE, en met andere technieken;
4. Uitwerking relatie PBAC en de poortwachterfunctie;
5. Onderzoek naar het maken van specifieke endpoints voor specifieke klanten;
6. Gebruik van [[FSC]]() (VNG), verbinding moet verder worden uitgewerkt;

[^1]: Noraonline.nl – Nederlandse Overheid Referentie Architectuur
[^2]: NIST 800-95 Guide to Secure Web Services
[^3]: Om de twee organisaties te koppelen is er een transactielog nodig. Het Digilab is voornemens
    dit met FSC in november 2024 op te zetten.
[^4]: FCS beschrijft in de certificaten een identiteit. Verder onderzoek is nodig om te kijken of
    dit de gewenste identiteit is.
[^5]: NCSC: [What about zero
    trust?](https://www.ncsc.nl/actueel/weblog/weblog/2020/what-about-zero-trust)
[^6]: Zie [[Poortwachter]]()
