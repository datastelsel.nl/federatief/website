---
title: Helpdesk
status: Voorstel
date: 2024-04-30
author: Kernteam FDS
version: v0.23
description: >
    Stelselfunctie voor eerstelijnsondersteuning van gebruikers en geregistreerden.
    Tevens meldpunt voor het melden van fouten in overheidsregistraties.
---

### Helpdesk

De organisatorische [stelselfunctie](../stelselfuncties/) Helpdesk:
- biedt eerstelijnsondersteuning van gebruikers en geregistreerden.

#### Status

Deze functie wordt samen met het kenniscentrum van de Interbestuurlijke Datastrategie (IBDS)
vormgegeven en groeit mee met de behoeften van aanbieders en gebruikers.

#### Achtergrond

{{% TODO /%}}

#### FDS context & positionering

{{% TODO /%}}

#### Inhoudelijke verdieping

{{% TODO /%}}

#### Standaardisatie

{{% TODO /%}}
