---
title: Poortwachter
status: Voorstel
date: 2024-04-30
author: Kernteam FDS
version: v0.23
description: >
    Stelselfunctie voor toetsing, autorisatie en registratie van koppelingsverzoeken. Toetst op 
    conformiteit met de AVG en de stelsel aansluit- en gebruiks-voorwaarden en zorgt daarbij voor 
    een transparant beslisproces.
---

### Poortwachter
De organisatorische [stelselfunctie](../stelselfuncties/) Poortwachter:

- Toetst, autoriseert en registreert koppelingsverzoeken tussen bronnen. Toetst op conformiteit met
  de AVG en de stelsel aansluit- en gebruiksvoorwaarden.
- Zorgt voor een transparant beslisproces.

#### Status

- Er is een startnotitie gemaakt.
- In de tweede helft van 2023 is de beschrijving van de werking verdiept langs een aantal dimensies:
  functies, standaarden, voorzieningen en wettelijk inbedding. Dit als opmaat naar demonstrators en
  experimenten in het Digilab om de feitelijke werking zo concreet mogelijk te maken.

#### Achtergrond

{{% TODO /%}}

#### FDS context & positionering

Kernelement van het federatief datastelsel is dat datagebruik in overeenstemming is met publieke
waarden. Om dit waar te maken, is het nodig dat ontwerp en realisatie van het FDS vanuit dit
perspectief worden gestuurd. Daartoe zijn de volgende ontwerpelementen passend:

- Poortwachtersfunctie.
- Dataminimalisatie.
- Regie op Gegevens.

Het stelsel voorziet in gecontroleerde toegang tot gevoelige data. Voor het toegang bieden en
afnemen van data die niet gevoelig zijn, volstaan veelal algemene toegangsregels, bijvoorbeeld bij
een bron die als open data is aangemerkt. Voor gevoelige stelseldata zoals persoonsgegevens, waarbij
gebruikt wordt gemaakt van het BSN als identificatie en koppelingssleutel, is meer nodig. Deze data
passeren eerst de stelselfunctie Poortwachter.  
De poortwachters is de functie binnen het Federatief Datastelsel die op basis van een formeel
vastgesteld toetsingskader bepaalt of de combinatie data - gebruikersgroep - gebruiksdoel kan worden
toegestaan. Dit is een drempel om ervoor te zorgen dat er een zorgvuldige afweging wordt gemaakt
tussen de vaak tegenstrijdige maatschappelijke belangen bij nieuwe vormen van sector overstijgend
datagebruik.

Door de verantwoordelijkheid voor deze toetsing op stelselniveau te beleggen, kan kennis over
verantwoord datagebruik worden gebundeld en ontstaan er meer mogelijkheden om de afweging
transparant en controleerbaar te maken. Dit maakt het proces efficiënter doordat individuele
data-aanbieders worden ontzorgd omdat ze op uitvoeringsniveau geen belangenafweging meer hoeven te
maken die eigenlijk op het politieke niveau thuishoort.

Onderdeel van het toetsingskader is het benutten van de verschillende opties die het stelsel biedt
om verantwoord data te delen. Zo ondersteunt het toekomstig stelsel dataminimalisatie waarbij de
verstrekkers van stelseldata _privacy enhancing technologies_ toepassen of intelligente
bevragingsdiensten leveren die gericht antwoord geven op een gestelde informatievraag (bijvoorbeeld
is deze persoon jonger dan 18?).

In een aantal gevallen is het dan niet meer nodig om (privacy)gevoelige data te delen. Een andere
mogelijkheid die het stelsel ondersteunt is het faciliteren dat de burger of het bedrijf zelf
toestemming geeft om persoonlijke of bedrijfsdata via het federatief datastelsel met anderen te
delen.

Zie ook [[PBAC|Position paper: Policy Based Access Control]]().

#### Inhoudelijke verdieping

De Poortwachtersfunctie zorgt voor toetsing, autorisatie en registratie van verzoeken om data te
delen1 . De toetsing zal veelal neerkomen op het afwegen van strijdige belangen, waarbij de volgende
zaken een rol spelen:  

- Noodzaak (er zijn geen alternatieven, digitaal werken is nodig, het gaat om tot de persoon
  herleidbare of concurrentiegevoelige gegevens).  
- Het maatschappelijke belang  
- Het recht op bescherming van de persoonlijke levenssfeer (bescherming privacy).
- Juridische toelaatbaarheid.
- Ethische toelaatbaarheid.
- Maatschappelijke wenselijkheid.
- Conformiteit met aansluit- en gebruiksvoorwaarden van het federatief datastelsel.

Dit gebeurt in een transparant beslisproces met mogelijkheden voor bezwaar en beroep. De uitkomst
van dit proces wordt vastgelegd in een openbare registratie (gegevensstroom boekhouding).  Deze
registratie heeft de volgende functies:  

- Opname van een datadeelrelatie in het register legitimeert de datadeling door daarvoor een
  juridische grondslag te bieden. Deze grondslag is een verwijzing naar een toepasselijk wetsartikel
  of een positief besluit van het toetsingscomité.  
- Het maakt op één plek inzichtelijk welke stelseldata, met welke partijen, voor welk doel mogen
  worden gedeeld en wat daarvoor de juridische grondslag is. Hiermee kan ook periodiek worden
  getoetst of eerder genomen beslissingen nog steeds valide zijn. Daarnaast ontstaat inzicht op
  welke gebieden aanpassing van wetgeving nodig is om oordelen van het toetsingscomité juridisch
  sterker te verankeren.  
- Op het federatief datastelsel aangesloten data aanbieders kunnen op basis van de gegevens in het
  register eenvoudig, geautomatiseerd, vaststellen of ze een datadeel verzoek van een tot het
  stelsel toegelaten afnemer mogen honoreren. Ze hoeven dus niet meer zelf een juridische toets uit
  te voeren.  
- Vastlegging in het centrale register van datadeel relaties, in combinatie met de toelating tot het
  stelsel (het voldoen aan de aansluitvoorwaarden) vervangt de bilaterale contracten die nu nog vaak
  nodig zijn om de datadeling tussen partijen te legitimeren.  

De Poortwachtersfunctie is één van de structurele stelselfuncties die het project Federatief
Datastelsel de komende jaren zal realiseren. Bij de realisatie wordt gebruik gemaakt van de
resultaten van andere onderdelen van het programma Realisatie Interbestuurlijke Datastrategie
(Adviesfunctie verantwoord datagebruik, het organiseren van datadialogen) en de uitkomsten van
relevante externe trajecten zoals het wetgevingswerk van de Regeringscommissaris
Informatiehuishouding.

De eerste stap is het ontwerp van deze functie, vergelijkbaar met de wijze waarop de inrichting van
de stelselfunctie [[Marktmeester]]() is beschreven. Daarna zal de feitelijke inrichting via use
cases / praktijksituaties plaatsvinden. Het tempo hangt daarmee af van de snelheid waarmee geschikte
use cases zich voordoen. Parallel wordt de technische functionaliteit van de Poortwachtersfunctie
beproefd (m.b.v. de Integrale Gebruiksoplossing, de IGO) in de Innovatiewerkplaats.

#### Standaardisatie

{{% TODO /%}}

