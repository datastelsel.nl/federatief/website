---
title: Deelnemerkenmerken
date: 2024-08-19
tags: 
description: >
  Registratie van kenmerken van deelnemers aan het FDS.
---

## Mechanisme

De deelnemerkenmerken bestaan uit voor het FDS relevante kenmerken van een deelnemer. 
De deelnemerkenmerken worden door de deelname-administrateur geverifieerd en 
geregistreerd bij het uitvoeren van de [deelnameprocessen](../deelnameprocessen/). De 
deelnemerkenmerken zijn metadata en worden beschikbaar gesteld als linked data. Hierbij zijn
de [[Linked Data Design Rules]]() van toepassing.

## De deelnemerkenmerken

De volgende deelnemerkenmerken worden geregistreerd:

* Een naam (label) van de organisatie
* Aanvullende 
[identificerende kenmerken](../identiteit/#identificerende-kenmerken) 
van de organisatie (zoals het OIN, KvK-nummer en het RSIN)
* De grondslag waarop de organisatie is toegelaten tot het FDS. 
* De administrateur die de deelnemer heeft opgenomen op de [[Deelnemerslijst]]()

Indien het een aanbieder betreft, een verwijzing naar: 
* Het aanbod van de deelnemer (als 
[DCAT](https://www.forumstandaardisatie.nl/open-standaarden/dcat) catalog)

Indien het een administrateur betreft, de volgende verwijzingen: 
 * De [deelnemerslijst](../deelnemerslijst/#de-deelnemerslijst)
 * De [deelnamewijzigingen](../deelnemerslijst/#de-deelnamewijzigingen)
 * Een verwijzing naar het [[Aanbod (metadata)]]() binnen het FDS (als 
 [DCAT](https://www.forumstandaardisatie.nl/open-standaarden/dcat) catalog)

De deelnemer wordt binnen de resource geïdentificeerd op basis van een 
[[Deelnemer-ID]]().

Van de deelnemerkenmerken wordt een versiehistorie bijgehouden.

| ![De deelnemerkenmerken](images/metadata-deelnemerkenmerken.png) |
| :--------------------------------------------------------------: |
| _De deelnemerkenmerken_                                          |

## De grondslag

De grondslag op basis waarvan de organisatie is toegelaten tot het FDS betreft de 
wettelijke taak op grond waarvan de organisatie gegevens kan aanbieden of afnemen 
binnen het FDS. Deze grondslag bestaat uit een of meer verwijzingen naar het 
[BWB](https://www.forumstandaardisatie.nl/open-standaarden/bwb).

## Betrouwbaarheid

De deelnemerkenmerken worden beschikbaar gesteld door de administrateur die
betreffende deelnemer heeft toegelaten. Dit impliceert dat de 'hostname' binnen 
het [[Deelnemer-ID]]() een hostname binnen een domein van deze
administrateur is. Door deelnemerkenmerken beschikbaar te stellen via een 
domein van de administrateur verleent de administrateur autoriteit aan de
geleverde deelnemerkenmerken. 

Een alternatief model is het door de deelnemer zelf ter beschikking stellen
van de deelnemerkenmerken. Dit vergt echter een bewijs van bijvoorbeeld
een administrateur dat de deelnemerkenmerken zijn geverifieerd. Dit 
zou bijvoorbeeld kunnen door het mechanisme van 
[Verifiable Credentials (VC)](https://www.w3.org/TR/vc-data-model-2.0/)
toe te passen, al dan niet in combinatie met een 
[did-web](https://w3c-ccg.github.io/did-method-web/) als 
[Identificatiemiddel](../identiteit/#alternatieve-identificatiemiddelen). 
In dat geval zouden de deelnemerkenmerken zo opgezet 
kunnen worden dat ze de vorm krijgen van DID document zoals 
voorgeschreven binnen de [did-web](https://w3c-ccg.github.io/did-method-web/)
standaard. Dit is echter vooralsnog onvoldoende uitgewerkt om al concreet toe 
te kunnen passen. Daarnaast is het nog onzeker of betreffende standaarden
passend zijn voor toepassing binnen FDS.

## Voorbeelden

Een voorbeeld van een lijst met deelnemerkenmerken (in 
[Turtle](https://www.w3.org/TR/turtle/)):

```
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
@prefix fds: <http://fds.example.org/ontology#>
@base <http://admin-a.example.org/fds/deelnemers/>

<organisatie-x>
   fds:versionInfo 'v8' ;
   fds:priorVersion <organisatie-x/v7> .

<organisatie-x#deelnemer>
   rdfs:label 'Deelnemer-X'@nl-nl ;
   fds:kvk '50200097' ;
   fds:oin '00000003502000970000' ;
   fds:admitted-by <http://admin-a.example.org/fds/deelnemers/self#deelnemer> ;
   fds:catalog <http://deelnemer-x.example.org/fds/dcat/#catalog> .
```

## Standaarden
* [RDF](https://www.forumstandaardisatie.nl/open-standaarden/rdf)
* [RDFS](https://www.forumstandaardisatie.nl/open-standaarden/rdfs)
* [OWL](https://www.forumstandaardisatie.nl/open-standaarden/owl)
* [SHACL](https://www.forumstandaardisatie.nl/open-standaarden/shacl)
