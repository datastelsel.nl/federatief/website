---
title: IMX
date: 2024-08-28
author: Kernteam FDS
status: Concept
description: IMX staat voor een crossdomain informatiemodel, model-mapping
  syntax en orkestratie engine
---
IMX, oorspronkelijk [IMX-geo](https://www.geonovum.nl/geo-standaarden/imx-geo-semantisch-model-basis-en-kernregistraties), of model-gedreven orkestratie, verbindt geo-basisregistraties zonder wijzigingen aan de bron te eisen. Het doel is gegevens te optimaliseren voor specifieke doelgroepen en deze in de taal van de gebruiker aan te bieden, terwijl de gegevens herleidbaar blijven naar hun authentieke bron(nen).

IMX maakt gebruik van een model-mapping concept, waarbij gegevens uit meerdere bronmodellen worden vertaald naar een productmodel. Dit gebeurt op een declaratieve manier, wat inhoudt dat de mappingregels en orkestratie "stapelbaar" zijn en machine-leesbaar worden gespecificeerd.

De [IMX-beproeving in Digilab](https://digilab.overheid.nl/projecten/imx-modelgedreven-orkestratie/) heeft aangetoond dat de technologie effectief is in het orkestreren van data uit verschillende bronnen zonder de noodzaak van data-kopieën.

Het vervolgonderzoek richt zich op de uitbreiding van de IMX-methodologie naar niet-geo-registraties. Doel is om door middel van model-gedreven orkestratie gegevens uit verschillende registraties te combineren zonder de noodzaak voor data-kopieën.

Belangrijke aspecten zijn het verbeteren van beveiligingsstandaarden (zoals authenticatie en autorisatie) en het aansluiten bij linked data. Dit onderzoek moet bijdragen aan bredere toepasbaarheid en draagvlak binnen de overheid, wat een toekomstbestendige oplossing biedt voor datadeling en integratie binnen het FDS.

Dit vervolgonderzoek dat loopt vanaf augustus '24 tot begin volgend jaar (2025), omvat onder andere een proof of concept om de beveiligingsarchitectuur te testen en om te bepalen hoe bestaande metadata-standaarden zoals DCAT en *lineage* modellen gebruikt kunnen worden voor niet-geo-registraties.
