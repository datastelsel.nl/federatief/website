---
title: Samenwerking andere programma's
date: 2024-10-07
author: Kernteam FDS
status: Concept
version: v0.1
description: >
   Samenwerken met andere overheidsprogramma's is integraal onderdeel van onze
   veranderstrategie om tot het Federatief Datastelsel te komen. 
---

Hieronder maken we een begin met het beschrijven van andere overheidsprogramma's waar wij mee
samenwerken. Deze pagina zullen we met de tijd aanvullen en verscherpen op basis van de laatste
inzichten.

### Kennisplatform API's en FDS

**Doel**: Het Kennisplatform API’s is opgericht door Geonovum in samenwerking met het Bureau Forum
Standaardisatie, Kamer van Koophandel, VNG Realisatie, Logius en het Kadaster. Het doel van het
platform is  samen te werken aan strategische en tactische vraagstukken rond het ontwikkelen van
API’s door de overheid . Dit resulteert in gedeelde kennis, afstemming, afspraken en standaarden.  

**Raakvlakken**: Het FDS ziet het ontsluiten van data middels API’s als een belangrijk middel om
vertrouwd data te delen in het stelsel en richt zich onder andere op uitwisselstandaarden die
aansluiten op het inzetten van API’s maar minder op de spelregels rondom API’s zelf. Daar richt het
Kennisplatform zich op. Het FDS werkt nauw samen met het Kennisplatform zodat beiden zich in lijn
met elkaar blijven ontwikkelen.  

**Samenwerking**: Kennisplatform API's en FDS hebben momenteel gesprekken lopen om de samenwerking
verder te concretiseren. Samen met Digilab en Developer Overheid is er al bepaald welke doelgroepen
elk programma heeft en hoe we elkaar kunnen ondersteunen in het informeren van die doelgroepen via
alle kanalen.

Neem vooral een kijkje op de website van [Kennisplatform API's](https://www.geonovum.nl/themas/kennisplatform-apis)

### Developer.Overheid.NL en FDS

**Doel**: Developer.overheid.nl biedt een platform voor ontwikkelaars die voor of met de overheid
werken. Het platform biedt toegang tot hoogwaardige API's en Open Source repositories waarover
inzicht gegeven wordt in een aantal kwaliteitskenmerken met behulp van testtools. De community
faciliteert de uitwisseling van kennis en  stimuleert de samenwerking tussen overheidsinstanties.

**Raakvlakken**: Het FDS heeft meerdere dimensies en een daarvan is de technische interoperabiliteit
welke ondersteund wordt door het kiezen van uitwisselingsstandaarden. Binnen het FDS is niet veel
ontwikkelkennis beschikbaar omdat er gekozen is om het ontwikkelen van standaarden buiten de FDS
scope te plaatsen. Tegelijk is die ontwikkelkennis wel belangrijk om ook als FDS bepaalde puzzels te
onderzoeken en keuzes te maken rondom de uitwisselingsstandaarden. Hier raken het FDS en het
platform elkaar: ontwikkelkennis. Het platform biedt ruimte voor datasets (API’s) om zichtbaar te
zijn voor de ontwikkelaars zodat deze best practices kunnen delen, vragen aan elkaar en de dataset
eigenaar te kunnen stellen en indien wenselijk feedback ter verbetering kunnen geven.

**Samenwerking**: De technische keuzes die door het FDS en haar stakeholders gemaakt worden, worden op
het platform gepubliceerd. Hierdoor bereikt het FDS een grote bron aan ontwikkelkennis waaruit
enerzijds feedback gegeven wordt en FDS haar richting kan aanscherpen. Anderzijds geeft het FDS
richting aan de ontwikkelaars zodat deze geholpen worden in het maken van eigen keuzes.  

Neem vooral een kijkje op de website van [Developer Overheid](https://developer.overheid.nl/)