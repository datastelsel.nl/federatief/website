---
title: Welkom
# linkTitle: Docs
menu: {main: {weight: 20}}
weight: 20
---

Welkom bij federatief.datastelsel.nl, het  kennisplatform voor ons allemaal!

Het Federatief Datastelsel is een open source project dat voor iedereen toegankelijk is om te
gebruiken en te verbeteren. De ontwikkeling hiervan doen we juist graag met elkaar, dus we kijken
uit naar je betrokkenheid.

Deze omgeving biedt verdieping op wat het Federatief Datastelsel inhoudt en is daarmee geschikt voor lezers die al 
kennis hebben van het FDS. Ben je nog helemaal nieuw in dit onderwerp? Welkom! De eerste stap is naar de [IBDS-website](https://realisatieibds.nl/page/view/564cc96c-115e-4e81-b5e6-01c99b1814ec/de-ontwikkeling-van-het-federatief-datastelsel)
te gaan omdat daar de informatie begrijpelijk is uitgelegd met ondersteuning van animaties en videos. Word dan direct
lid van de [FDS-groep](https://realisatieibds.nl/groups/view/0056c9ef-5c2e-44f9-a998-e735f1e9ccaa/federatief-datastelsel), zodat je alle updates direct ontvangt! Vervolgens kan je op deze website de verdieping op alle subonderwerpen vinden.

In die verdieping op de onderwerpen leggen we leggen graag uit hoe we met elkaar willen samenwerken. In 
[[Strategie van samenwerken|strategie van samenwerken]]() staat hoe we de samenwerking voor ons zien.
Bij [[Werkomgeving|werkomgeving]]() hopen we jullie inzicht en duidelijkheid te geven in
hoe we  die samenwerking dan het liefste willen realiseren.

Heb je vragen? Gaan dan direct naar [MatterMost](https://digilab.overheid.nl/chat/fds) (onze chat)!
Bij het kanaal [~algemeen](https://digilab.overheid.nl/chat/fds/channels/town-square) in MatterMost
kun je al je vragen stellen. Mocht je dan willen aanhaken bij een specifiek thema dan zorgen we dat
je geattendeerd wordt op het juiste kanaal!
