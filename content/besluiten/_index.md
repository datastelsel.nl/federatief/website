---
title: Keuzes
menu: {main: {weight: 40}}
type: besluiten
weight: 80
description: Lijst van uitgangspunten met afwegingen en onderbouwing
---

Dit is de sectie waar onze 'DRs', Decision Records worden vastgelegd. Decision Records zijn een
methode om gestructureerd besluiten te maken op onderwerpen en deze vast te leggen. We noemen dit
echter met opzet keuzes om verwarring in onze omgeving te voorkomen. Deze DRs betekenen namelijk
niet dat er formele besluiten genomen zijn in officiële overleggen, maar dat dit nu ontwerp- en
inrichtingsuitgangspunten zijn die we als programma hanteren. Als er over deze DRs vragen zijn dan
kun je te allen tijde daar nog vragen over stellen.  

Hieronder vind je het overzicht van alle uitgangspunten, Decision Records, in chronologische
volgorde. Meer toelichting over Decision Records vind je in het besluit om deze methode toe te
passen: [[00002 Gebruik van Markdown Decision Records in Git]]()
