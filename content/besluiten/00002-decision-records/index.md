---
title: 00002 Gebruik van Markdown Decision Records in Git
type: besluiten
date: 2023-10-01
author: Kernteam FDS
tags: 
status: Voorstel
description: >
  Voor het vastleggen en ondersteunen van discussies leggen we besluiten en voorgenomen besluiten vast
  als 'decision records'. Dit is onze eigen afleiding van 'Architecture Decision Records'.
---

## Context en probleemstelling

In het algemeen zijn er veel termen, woorden, begrippen, principes, concepten die niet eenduidig
vastliggen. Om discussies te kunnen beslechten, vast te leggen en (ook) later op terug te kunnen
komen en naar te verwijzen, is een methode nodig om dat vast te leggen. Wat is een goede methode?
Hoe en waar leggen we dat dan vast?

## Overwogen opties

- [NORA Wiki](#nora-wiki)
- [Markdown in enige vorm](#markdown-in-enige-vorm)
- [MADR](https://adr.github.io/madr/) 2.1.2 with Log4brains patch
- [MADR](https://adr.github.io/madr/) 2.1.2 – The original Markdown Architectural Decision Records
- [Michael Nygard's
  template](http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions) – The
  first incarnation of the term "ADR"
- [Sustainable Architectural
  Decisions](https://www.infoq.com/articles/sustainable-architectural-design-decisions) – The
  Y-Statements
- Other templates listed at <https://github.com/joelparkerhenderson/architecture_decision_record>
- Formless – No conventions for file format and structure

## Besluit

We maken gebruik van **Decision Records** volgens onze eigen, in het Nederlands vertaalde template
`decision-record.md`. We hanteren daarbij het gebruik van Markdown aangezien dat (redelijk)
mensvriendelijk is en tegelijkertijd machine leesbaar. We maken daarbij gebruik van Git
versiebeheer, aangezien dit subliem is en de facto standaard.

Decision records hebben een uniek, oplopend nummer. De volgorde is niet van belang. Het is een
nummer uit alleen cijfers en uit vijf karakters: `00000`

Decision records doorlopen het door [Log4brains](https://github.com/thomvaill/log4brains) bedachte
proces:

![Decision record workflow](images/adr-workflow.png)

```mermaid
flowchart LR
  s1([Concept])
  s2([Voorstel])
  s3([Geaccepteerd])
  s4([Verouderd])
  s5([Vervangen])
  s6([Verworpen])
  s1 --> s2
  s2 --> s3
  s3 --> s4
  s3 --> s5
  s2 --> s6
```

## Legenda labels

Voor wie nog minder bekend is met het gebruik van labels en Decision records, lichten we hieronder toe wat de status van een pagina is. De eerste drie labels zullen in de huidige fase van eht FDS het meest voorkomen.

- Concept: dit betekent dat de pagina informatie biedt die in concept is. Kortom, deze
  informatie behoeft juist nog input en expertise van een ieder die zich daartoe geroepen voelt. Een
  draft kan al meerdere versienummers hebben, omdat ook de draft in ontwikkeling kan zijn.

- Voorstel:  dit betekent dat het stuk is voorgesteld aan het Tactisch Overleg van het FDS en is
  besproken. Hierdoor heeft er een inhoudelijke discussie heeft kunnen plaatsvinden met eventuele
  inhoudelijke aanvullingen en/of wijzigingen. Deze feedback kan ook leiden tot een nieuwere versie
  van het voorstel OF, afhankelijk van de feedback, gaat weer terug naar de conceptfase als het gaat
  om grote vraagstukken. Nieuwere versies kunnen dan ten alle tijden op dit platform gevonden
  worden.

Inhoudelijke verwerking van expertise kan plaatsvinden, maar kent een lagere frequentie dan de pagina's die in concept zijn. 

- Definitief: dit betekent dat de content afgerond is en voor nu geen verdere aanvulling
  behoeft. Vragen stellen is altijd mogelijk, maar deze informatie is al door meerdere
  iteratieslagen heen gegaan waarbij er voldoende ruimte was voor inspraak vanuit de omgeving. Op
  dit moment is er nog weinig content formeel vastgesteld en daarmee definitief. Het wordt pas echt
  definitief wanneer een nog nader te bepalen stuurgroep deze status kan vaststellen.

- Verouderd: dit betekent dat het stuk verouderde informatie bevat. Het is ooit
  definitief geweest (of misschien zelfs niet eens) maar het verliest relevantie. Toch kan het
  nuttige informatie bevatten over hoe er eerder naar zaken gekeken werd. Vaak zijn er aanleidingen
  of andere beslisnotities die de reden zijn dat informatie als verouderd aangeduid kan worden. Als
  dat zo is, dan wordt dat boven in de pagina toegevoegd. Verouderde informatie kan nog steeds
  actueel geldend zijn, maar voor nieuwe oplossingen niet meer relevant. Als er echt vervangende
  oplossingen zijn, dan is de status Superseded/Vervangen passender.

- Vervangen: dit betekent dat het stuk vervangen is door andere beslisnotities en keuzes. Dit is
  niet alleen verouderd (zie vorige status) maar ook nog eens opgevolgd door een nieuwe versie of
  vervolg beslisnotitie. Deze status betekent dat de opvolgende beslisnotities de actueel te volgen
  richtingen zijn en dat dit stuk komt te vervallen. Het is alleen nog terug te vinden als
  archiefstuk. Dit in tegenstelling tot de status Deprecated/Verouderd welke nog steeds geldend kan
  zijn, maar voor nieuwe oplossingen niet meer relevant.

Decision records hebben de volgende indeling, naar aanleiding en geïnspireerd van de overige
[overwogen opties](#overwogen-opties).

- Context en probleemstelling
- Beslissingsfactoren
- Overwogen opties
- Besluit
  - Positieve gevolgen
  - Negatieve Consequences
- Pros en Cons van de oplossingen
  - Optie 1
  - Optie 2
- Links (naar andere decision records)

Deze template is als Markdown template beschikbaar in de repository:
[archetypes/decision-record.md](https://gitlab.com/datastelsel.nl/federatief/website/-/blob/main/archetypes/decision-record.md?ref_type=heads)

Het gebruik van Markdown betekent dat de template er ongeveer zo uit zal zien:

```markdown
## Context en probleemstelling

## Beslissingsfactoren

## Overwogen opties

## Besluit

### Positieve gevolgen

### Negatieve Consequences

## Pros en Cons van de oplossingen

### Optie 1

### Optie 2

## Links
```

### Positieve gevolgen

- Impliciete aannames zouden expliciet gemaakt moeten zijn Ontwerp documentatie is belangrijk voor
  mensen om besluiten later te kunnen begrijpen. Zie ook [A rational design process: How and why to
  fake it](https://doi.org/10.1109/TSE.1986.6312940).
- Markdown is klein, compact en eenvoudig en tegelijk rijk aan mogelijkheden
- Bovenstaande structuur is eenvoudig, krachtig en faciliteert gebruik en onderhoud
- Oorspronkelijke basis hiervan, namelijk Markdown Architecture Decision Records (MADR), is een
  actieve en sterk groeiende methode van vastlegging van besluiten
- Het proces (workflow) is eenvoudig en duidelijk
- Een nieuw decision record heeft status `draft`, wat de mogelijkheid biedt voor anderen om
  suggesties te doen en bijdragen te leveren
- Markdown decision records hebben het bestandsformaat `NNNNN-decision-naam`
- Decision records zijn met hun nummer te refereren ... hoewel met naam meer context geeft (en dus
  beter is / voorkeur heeft)

## Pros en Cons van de oplossingen

### NORA Wiki

Een wiki is een manier om online content te publiceren én tegelijk dat te beheren met meerdere
mensen. Het biedt de mogelijkheid om door content te bladeren, te zoeken en links tussen pagina's te
maken. Een 'What You See Is What You Get' (WYSIWYG) editor ondersteunt niet-technische gebruikers.
Dit is waar [Wikipedia](https://www.wikipedia.org/) mee ontstaan en groot geworden is.

De Nederlandse Overheids Referentie Architectuur, NORA, maakt gebruik van dezelfde technologie als
Wikipedia: [Mediawiki](https://www.mediawiki.org/wiki/MediaWiki). Het doel is vergelijkbaar als met
Wikipedia, namelijk dat het beheer van content eenvoudig is en door iedereen gedaan kan worden.

- Goed, omdat gepubliceerde content en het wijzigen van content dicht bij elkaar staan en te vinden
  zijn
- Goed, omdat niet-technische gebruikers gemakkelijk kunnen bijdragen (na aanmaken van een account,
  dat zelfstandig gedaan kan worden)
- Slecht, omdat versiebeheer per pagina niet zo duidelijk is
- Slecht, omdat versiebeheer van meerdere en samenhangende pagina's eigenlijk niet goed ondersteund
  worden (er zijn wel wat mogelijkheden ... maar allemaal suboptimaal)
- Vergelijkbaar met Markdown, omdat het onderliggende formaat waarmee een wiki werkt, ondanks de
  WYSIWYG editor, gebaseerd is op 'wikitekst'. Dat is de facto standaard van Mediawiki en lijkt op
  Markdown. Wikitekst is minder gestandaardiseerd dan Markdown en alleen (in deze vorm) ondersteund
  door Mediawiki.

### Markdown in enige vorm

Bij de ontwikkeling van content, het zij op internet, het zij in documenten, is er behoefte aan
opmaak. Opmaak in Microsoft Word is specifiek en direct afhankelijk van de tool MS Word. Op internet
is de afhankelijkheid naar een tool er niet. Hoogstens van de HTML (+ CSS + JavaScript) standaarden
voor visualisatie mogelijkheden.

Om voor de ontwikkeling van content op internet niet afhankelijk te zijn van technische experts die
HTML+ kunnen schrijven, wordt er al lang gezocht naar manieren om met 'pseudo codes' opmaak aan te
geven in platte tekst. Wikitekst, zoals in de [NORA Wiki](#nora-wiki), is daar een voorbeeld van.
Het vroegere Wordperfect had daar ook mogelijkheden voor. Voor software documentatie is Markdown de
de facto standaard.

Markdown is een vrij eenvoudige specificatie van speciale codes die opmaak betekenis hebben. Door
een conversiestap van bron (source) document naar HTML kan een mooie HTML website gepubliceerd
worden. Doordat Markdown redelijk gestandaardiseerd is, zijn er vele (😳) tools beschikbaar die een
website kunnen genereren. Hier is enorme keuze om een geschikte tool te vinden afhankelijk van de
situatie.

Doordat Markdown bestanden platte tekst bestanden zijn, is versiebeheer mbv Git makkelijk, logisch
en veel toegepast. Daarom is dit ook een prettige oplossing voor software developers documentatie.
Tools als GitHub en GitLab hebben standaard ook ondersteuning voor Markdown incl. presentatie als
HTML.

- Goed, omdat Markdown eenvoudige codes gebruikt, zodat ook minder-technische mensen er mee uit de
  voeten kunnen
- Goed, omdat Markdown redelijk gestandaardiseerd is
- Goed, omdat versiebeheer per Markdown bestand zeer mogelijk is mbv Git
- Goed, omdat versiebeheer over een collectie Markdown bestanden ook zeer mogelijk is door de
  toepassing van Git
- Goed, omdat de brondocumenten in Markdown echt ontkoppeld zijn van de tool voor publicatie in HTML
- Slecht, omdat de brondocumenten op enige afstand kunnen staan door de ontkoppeling met de
  publicatie tool
- Slecht, omdat het versiebeheer geen onderdeel is van Markdown en extra toegevoegd moet worden. Git
  is daarvoor zeer geschikt ... maar ook zo uitgebreid, dat minder-technische mensen daar moeite mee
  hebben. Dit verhoogt de barrière voor minder-technische mensen om Markdown (plus Git) te gebruiken
- Vergelijkbaar met Wikitekst, omdat het platte tekst is waarin met codes opmaak wordt geproduceerd

## Links

- [...]
