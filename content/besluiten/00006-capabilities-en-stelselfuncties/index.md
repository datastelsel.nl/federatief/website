---
title: 00006 Capabilities en Stelselfuncties
date: 2024-11-07
author: R-FDS Team
tags: []
status: voorstel
description: >
  We gebruiken de OpenDEI building blocks als capabilities, maar hoe laten we
  dat aansluiten op FDS en de stelselfuncties
---

## Probleemstelling en context

Momenteel hanteren we op Open DEI gebaseerde [[Capabilities|capabilities]]()
en vanuit FDS gedefinieerde [[Stelselfuncties|stelselfuncties]](). De capabilities
en stelselfuncties hebben voor een belangrijk hetzelfde doel (structureren van functies binnen het
stelsel). Dit werkt verwarrend en is lastig uit te leggen.

In de Decision Records [[00001 Basisstructuur]]() en [[00004 Capabilities NL]]() bepalen we dat we
met capabilities gaan werken volgens de building blocks van OpenDEI. Hoe laten we de capabilities
goed aansluiten op het FDS en de daarin benoemde organisatorische en technische stelselfuncties.

## Overwogen opties

- [Technisch OpenDEI en Organisatorisch FDS](#technisch-opendei-en-organisatorisch-fds)
- [Capabilities naast stelselfuncties](#technisch-opendei-en-organisatorisch-fds)

aansluitende (deel)opties met betrekking tot capability 'verantwoording':

- [We schrappen capability verantwoording](#schrappen-van-verantwoording)
- [We hernoemen capability verantwoording naar bekostiging](#verantwoording-wordt-bekostiging)
- [We hernoemen capability verantwoording naar financiële verantwoording](#verantwoording-wordt-bekostiging)
- [We handhaven capability verantwoording](#handhaven-capability-verantwoording)

aansluitende (deel)opties met betrekking tot kwaliteit:

- [We voegen capability kwaliteit toe](#kwaliteit-toevoegen)
- [We verwijzen kwaliteit vanuit capability metadata](#verwijs-kwaliteit-vanuit-metadata)

aansluitende (deel)opties met betrekking tot dssc capability 'value added services':

- [We voegen capability waardediensten toe](#waardediensten-toevoegen)
- [We voegen capability waardetoevoegende diensten toe](#waardetoevoegende-diensten-toevoegen)
- [We beschrijven value added services onder dataservices](#value-added-services-onder-dataservices)

aansluitende (deel)opties met betrekking tot een alternatieve betekenis van 'verantwoording':

- [We voegen capability verantwoording toe onder vertrouwen](#verantwoording-onder-vertrouwen)
- [Geen nieuwe capability vertrouwen](#verantwoording-niet-opnemen)

## Besluit

We laten uitlegbaarheid, inhoudelijke consistentie en eenvoud prevaleren boven vormgeving
of het volgen van OpenDEI.

Daarmee kiezen we voor:

- [Technisch OpenDEI en Organisatorisch FDS](#technisch-opendei-en-organisatorisch-fds)
- [We schrappen capability verantwoording](#schrappen-van-verantwoording)
- [We voegen capability kwaliteit toe](#kwaliteit-toevoegen)
- [We beschrijven value added services onder dataservices](#value-added-services-onder-dataservices)
- [Geen nieuwe capability vertrouwen](#verantwoording-niet-opnemen)

### Positieve gevolgen <!-- optional -->

- We hebben een goed uitlegbaar stelselfunctiemodel dat ruimte biedt voor
  alle belangrijke aspecten van FDS.

### Negatieve gevolgen <!-- optional -->

- We moeten de vormgeving en content van de huidige organisatorische stelselfuncties
  en OpenDEI capabilities herzien (ook bij Digilab) om de nieuwe indeling te accommoderen.

## Algemene beslissingsfactoren <!-- optional -->

- Context:
  - Doelstellingen [Realisatie Interbestuurlijke Datastrategie (IBDS)](https://realisatieibds.nl/)
  - Doelstellingen [Federatief Datastelsel (FDS)](/kennisbank/doel-en-aanpak-van-fds/)
- Decision Records:
  - DR [[00001 Basisstructuur]]()
  - DR [[00004 Capabilities NL]]()
- Overwegingen:
  - Building blocks (onze capabilities) zijn in het OpenDEI paper opgedeeld in 'Technical Building
    Blocks' en 'Governance Building Blocks'
  - De OpenDEI indeling wordt niet onderhouden in nieuwere revisies.
  - De [dssc blueprint 1.0](https://dssc.eu/page/knowledge-base) is een doorontwikkeling van het
    OpenDEI model.
  - In het dssc model zijn de OpenDEI 'Technical Building Blocks' grotendeels gehandhaafd.
  - In het dssc model zijn de OpenDEI 'Governance Building Blocks' vervangen door andere building
    blocks en aangevuld tot 'Business and Organisational Building Blocks'.
  - De FDS organisatorische stelselfuncties lijken te overlappen met de OpenDEI 'Governance'
    building blocks, maar lijken niet direct goed op elkaar te passen.
  - De FDS organisatorische stelselfuncties hebben naamsbekendheid en zijn voor een deel (bv
    poortwachter) al vrij ver uitgewerkt / uitgedacht.
  - De FDS technische stelselfuncties lijken goed onder te brengen binnen de OpenDEI 'technical
    building blocks' met uitzondering van de 'terugmeldfunctie' (of datakwaliteit in het algemeen).
  - Het OpenDEI building block 'Verantwoording' is in OpenDEI bedoeld als financiële verantwoording
    ten behoeve van het in rekening brengen van dataleveringen.
  - Het dssc model voegt de capability 'value added services' toe onder de capabilitygroep
    'datawaarde'.
  - Onder value added services verstaan we het toevoegen bij het beschikbaar stellen van gegevens,
    bijvoorbeeld aggregratie, compilatie, translatie of bv het verhogen van beschikbaarheid,
    responsetijden, zoekmogelijkheden etc.
  - Geen van de OpenDEI capabilities lijkt betrekking te hebben op kwaliteit (servicekwaliteit en/of
    datakwaliteit). Daarmee heeft bv terugmelding ook geen logische plek.
  - GDI werkt in de
    [Domeinarchitectuur Gegevensuitwisseling](https://minbzk.github.io/gdi-gegevensuitwisseling/2.0/content/index.html)
    met capabilities gerelateerd aan (groepen van) business functions.

## Voordelen en nadelen van de oplossingen <!-- optional -->

### Technisch OpenDEI en Organisatorisch FDS

- We handhaven de de technische capabilities (building blocks) van Open DEI en verlaten de
  technische FDS stelselfuncties door de inhoud onder te brengen bij de technische capabilities:
  - FDS Verantwoordingsfunctie onderbrengen bij OpenDEI Traceerbaarheid
  - FDS Catalogusfunctie onderbrengen bij OpenDEI Publicatie
  - FDS Monitoringsfunctie onderbrengen bij OpenDEI Metadata
  - FDS Notificatiefunctie onderbrengen bij OpenDEI Dataservices
  - FDS Terugmeldfunctie onderbrengen bij **nieuwe** capability Kwaliteit onder Datawaarde of alleen
    verwijzen vanuit capability Metadata.
  - FDS Toegang onderbrengen bij OpenDEI Identiteit, Toegang, Veiligheid en Traceerbaarheid
- Voor organisatorische aspecten verlaten we de OpenDEI indeling en hanteren we de FDS
  organisatorische stelselfuncties.
- Daarmee hanteren we technische OpenDEI capabilities icm organisatorische FDS stelselfuncties.
- We hernoemen de technische OpenDEI capabilities naar technische stelselfuncties zodat we alleen
  nog (organisatorische en technische) stelselfuncties hebben en geen capabilities meer.

**Voordelen**

- We hoeven niet te definiëren wat het verschil is tussen een capability en een stelselfunctie.
- We hoeven niet te definiëren wat een capability is.
- Het resulterende architectuurmodel is eenvoudiger en daarmee eenvoudiger te volgen en te
  communiceren.

**Nadelen**

- We verlaten de herkenbaarheid van OpenDEI voor organisatorische diensten. Dit nadeel is beperkt
  tot architecten met kennis van OpenDEI.
- We moeten (wellicht) wel uitleggen wat de verhouding is van FDS stelsel functies ten opzichte van
  capabilities en building blocks in andere architecturen.
- We kunnen in de architectuur geen gebruik maken van verschillen in betekenis tussen een capability
  en een stelselfunctie.

### Capabilities naast stelselfuncties

- We stellen een definitie op die duiden wat het verschil is tussen een stelselfunctie en een
  capability.
- We creëren stelselfuncties als niveau onder/naast de OpenDEI Capabilities:
  - FDS Regisseur onder OpenDEI Bestuurlijk?, OpenDEI Operationeel? en OpenDEI Beheer? 
  - FDS Poortwachter onder OpenDEI Operationeel?
  - FDS Marktmeester onder ?
  - FDS Helpdesk onder Beheer?
  - FDS Expertisecentrum onder ? 
  - FDS Toezichthouder onder Beheer?
  - FDS Verantwoordingsfunctie onder OpenDEI Traceerbaarheid
  - FDS Catalogusfunctie onder OpenDEI Publicatie
  - FDS Monitoringsfunctie onder OpenDEI Metadata
  - FDS Notificatiefunctie onder OpenDEI Dataservices
  - FDS Terugmeldfunctie onder *nieuwe* capability Datakwaliteit onder Datawaarde
  - FDS Toegang(sfunctie) onder OpenDEI Identiteit, Toegang, Veiligheid en Traceerbaarheid

**Voordelen**

- We handhaven de herkenbaarheid van OpenDEI voor organisatorische diensten.
- We kunnen verschillen in betekenis toekennen aan een capability en een
  stelselfunctie.

**Nadelen**

- De architectuurbeschrijving wordt complexer en daarmee moeilijker uit te leggen.
- We moeten definiëren wat een capability is en uitleggen wat het verschil is met een
  stelselfunctie.
- We moeten de relatie gaan leggen tussen individuele stelselfuncties en capabilities.
- Het totaal aan stelselfuncties en capabilities levert een groter aantal te beschrijven definities
  op met meer niet direct zichtbare of begrijpelijke verschillen.
- We moeten uitleggen wat de verhouding of relatie is tussen de stelselfuncties en de OpenDEI
  governance building blocks. Deze verhouding is voor organisatorische stelselfuncties niet
  eenvoudig te maken of toe te lichten.

### Schrappen van verantwoording

- We schrappen (financiële) verantwoording als capability onder datawaarde
- We leggen in het overzicht van capabilities uit waarom we de capability niet hebben opgenomen.

**Voordelen**

- De term 'verantwoording' kan niet meer tot verwarring met de capability 'traceerbaarheid' leiden.
- We maken ruimte voor een andere capability zonder het frame van 3x3 of 4x3 capabilities te hoeven
  aanpassen.

**Nadelen**

- We hebben geen plaats om eventueel nu nog niet voorziene eigenschappen van FDS tbv de
  verantwoording van financiële verrekening onder te brengen.
- De capabilities zijn enigzins minder herkenbaar als OpenDEI voor architecten met kennis van
  OpenDEI.

### Verantwoording wordt bekostiging

- We hernoemen (financiële) verantwoording als capability naar bekostiging

**Voordelen**

- De term 'verantwoording' kan niet meer tot verwarring met de capability 'traceerbaarheid' leiden.
- 'Bekostiging' bestaat uit één woord en is daarmee in lijn met de overige capabilities als één
  woord.
- We blijven aansluiten bij de OpenDEI indeling en het 4x3 of 3x3 frame van capabilities.

**Nadelen**

- Bekostiging dekt de lading nog niet heel goed: in OpenDEI is het bedoeld als verantwoording ten
  behoeve van van bekostigings- of verrekeningsconstructies. Bekostiging(sconstructies) zou beter
  onder de organisatorische / governance capabilities vallen.
- Bekostiging wordt als technische capability waarschijnlijk leeg: er is (vooralsnog) waarschijnlijk
  technisch gezien weinig af te spreken binnen FDS.

### Verantwoording wordt financiële verantwoording

- We hernoemen (financiële) verantwoording als capability naar bekostiging

**Voordelen**

- De term 'financiële verantwoording' kan niet meer tot verwarring met de capability
  'traceerbaarheid' leiden.
- 'Financiële verantwoording' blijft heel dicht bij de OpenDEI betekenis.
- We blijven aansluiten bij de OpenDEI indeling en het 4x3 of 3x3 frame van capabilities.

**Nadelen**

- 'Financiële verantwoording' bestaat uit twee woorden waar de overige capabilities uit één woord
  bestaan. Dit levert uitdagingen in de vormgeving.
- 'Financiële verantwoording' wordt als technische capability waarschijnlijk leeg: er is
  (vooralsnog) waarschijnlijk technisch gezien weinig af te spreken binnen FDS.

### Handhaven capability verantwoording

- We handhaven de naam 'verantwoording'
- We lichten in de text duidelijk toe dat dit 'financiële verantwoording betreft'

**Voordelen**

- We blijven aansluiten bij de OpenDEI indeling en het 4x3 of 3x3 frame van capabilities.

**Nadelen**

- We dienen bij herhaling en in bij de weergave van de set capabilities moeten blijven uitleggen wat
  het verschil is met 'traceerbaarheid'.
- 'Financiële verantwoording' wordt als technische capability waarschijnlijk leeg: er is
  (vooralsnog) waarschijnlijk technisch gezien weinig af te spreken binnen FDS.

### Kwaliteit toevoegen

- We beschrijven kwaliteitsaspecten en kwaliteitsverbeteringsintiatieven zoals terugmelding onder
  een nieuwe capability Kwaliteit onder Datawaarde.
- De capability metadata beschrijft het ontsluiten van kwaliteitskenmerken.

**Voordelen**

- Dit creeert een goed uit te leggen en prominente plek voor datakwaliteit en servicekwaliteit in
  het OpenDEI model.

**Nadelen**

- We doorbreken het 4x3 of 3x3 frame van capabilities (eventueel te mitigeren met
  [We schrappen capability verantwoording](#schrappen-van-verantwoording)).

### Verwijs kwaliteit vanuit metadata

- We verwijzen vanuit de capability metadata (via het ontsluiten van kwaliteitskenmerken) naar
  content met betrekking tot kwaliteit binnen het FDS.

**Voordelen**

- We blijven aansluiten bij de OpenDEI indeling en het 4x3 of 3x3 frame van capabilities.

**Nadelen**

- We een binnen het FDS belangrijk onderwerp van datakwaliteit enigszins buiten ons
  architectuurmodel. 


### Waardediensten toevoegen

- We voegen onder datawaarde de capability Waardediensten toe tbv dssc 'value added diensten'.

**Voordelen**

- 'Waardediensten' bestaat uit één woord en is daarmee in lijn met de overige capabilities als één
  woord.

**Nadelen**

- 'Waardediensten' is een enigszins 'gekunstelde' vertaling voor 'value added services'.
- We doorbreken het 4x3 of 3x3 frame van capabilities (eventueel te mitigeren met
  [We schrappen capability verantwoording](#schrappen-van-verantwoording)).
- Hoe 'value added services' te concretiseren is naar afspraken en standaarden binnen FDS is nog
  niet helder. Daarmee is ook nog niet duidelijk wat we zouden willen beschrijven.

### Waardetoevoegende diensten toevoegen

- We voegen onder datawaarde de capability Waardetoevoegende diensten toe tbv dssc 'value added
  diensten'.

**Voordelen**

- 'Waardetoevoegende diensten' blijft dicht bij het oorspronkelijke 'value added services'.

**Nadelen**

- 'Waardetoevoegende diensten' is een relatief lang en bestaat uit twee woorden. Dit levert
  uitdagingen in de vormgeving.
- We doorbreken het 4x3 of 3x3 frame van capabilities (eventueel te mitigeren met
  [We schrappen capability verantwoording](#schrappen-van-verantwoording)).
- Hoe 'value added services' te concretiseren is naar afspraken en standaarden binnen FDS is nog
  niet helder. Daarmee is ook nog niet duidelijk wat we zouden willen beschrijven.
  
### Value added services onder dataservices

- We beschrijven 'value added services' onder 'dataservices' onder interoperabiliteit voor zover we
  daar duidelijkheid over hebben. We kunnen altijd nog op een later moment deze beschrijvingen
  lostrekken en verplaatsen naar een eigen capability.

**Voordelen**

- We blijven aansluiten bij de OpenDEI indeling en het 4x3 of 3x3 frame van capabilities.

**Nadelen**

- De kern van 'value added services' heeft geen betrekking op interoperabiliteit, maar het toevoegen
  van waarde aan bestaande gegevens en staat daarmee in de verkeerde kolom.
- Als we op een later moment 'value added services' toch los willen trekken leidt tot aanpassingen
  in het hoofdmodel.

### Verantwoording onder vertrouwen

- We voegen 'verantwoording' toe onder 'vertrouwen'. Dit betreft vertrouwen in de zin van
  onderbouwing van registratie, niet (financiële) verantwoording zoals opgenomen in OpenDEI.

**Voordelen**

- We vergroten de herkenbaarheid van het belang van verantwoording als onderdeel van het
  vertrouwensraamwerk.

**Nadelen**

- Verschillende aspecten van verantwoording en de daarbij horende afspraken en standaarden zijn al
  goed te plaatsen onder 'traceerbaarheid', 'identiteit', 'toegang' en 'veiligheid'. Dit leidt tot
  dubbeling van benoeming van aspecten.

### Verantwoording niet opnemen

- We voegen 'verantwoording' niet toe als capability. Om het belang te onderstrepen kunnen we
  eventueel een thematische (overzichts)beschrijving opnemen. Daarin zouden we dan toelichten in
  welke onderdelen verantwoording uiteenvalt en onder welke capabilities / stelselfuncties dat is
  beschreven.

**Voordelen**

- We krijgen geen (minder) dubbele beschrijving van standaarden en afspraken met betrekking tot
  verantwoording.

**Nadelen**

- Verantwoording als belangrijk onderdeel van FDS is niet direct herkenbaar in het functiemodel.

## Links <!-- optional -->

- DR [[00001 Basisstructuur]]()
- DR [[00004 Capabilities NL]]()
- [[Capabilities]]()
- [[Stelselfuncties]]()
