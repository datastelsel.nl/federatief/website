---
title: 00005 Gebruik van Versies en Labels
type: besluiten
date: 2024-10-30
author: Kernteam FDS
tags: 
status: voorstel
description: >
  Bij het bijhouden en aanmaken van nieuwe pagina's werken we met labels en versionering, om ervoor
  te zorgen dat iedereen dit op dezelfde manier doet hebben we besloten hoe we dit de komende tijd
  willen inregelen.
---

## Context en probleemstelling

Momenteel komen er onwijs veel nieuwe pagina's bij in deze samenwerkingsomgeving en dat is goed
nieuws! Daarmee wordt het gebruik van versie nummers en labels extra belangrijk en met name het
uniform toepassen van de juiste updates in de versionering. Geconstateerd werd dat er verschillen
waren in het gebruik van versie updates, waardoor een nieuw besluit is genomen om dit te voorkomen.
We leggen hier uit hoe we hiermee om gaan.

## Overwogen opties

Eerst is er gekeken of git geautomatiseerd de versienummers kan aanpassen, dit is helaas niet
mogelijk. Vervolgens is na analyse geconcludeerd dat versionering met name is berust op updates in
de software volgens het  major.minor principe. Deze gedachtegang passen we nu handmatig toe bij
bepaalde pagina's.

Aangezien automatisering niet mogelijk is hebben we ervoor gekozen om een proces te bepalen waarin
we de randomness van het aanpassen van versienummers verwijderen. Dit leidt tot de besluiten
hieronder.

## Besluit gebruik versienummers

Vanaf heden maken we niet gebruik van versienummers, alleen bij specifiek gekozen pagina's waaronder
het basisconcept.  Bij deze pagina's is het goed om te zien welke wijzigingen we doen. Dit doen we
in de versionering van major.minor wijzigingen. Alle grote inhoudelijke wijzigingen zijn major
wijzigingen, alle kleinere inhoudelijke aanpassingen zijn minor aanpassingen. Spelfouten en
grammaticale verbeteringen leiden niet tot een nieuw versie nummer.

Voor alle andere pagina's is een versienummer niet nodig, in Git loggen we namelijk automatisch alle
changes en kunnen we ten alle tijden zien welke versies en acties er in het verleden zijn geweest.

## Besluit gebruik labels in het Nederlands

In [[00002 Gebruik van Markdown Decision Records in Git|decision record 0002]]() is er een visuele
weergave gemaakt van de labels en staat er een uitgebreide legenda waarin alle labels worden
gedefinieerd.

We werken met de volgende labels:

```mermaid
flowchart LR
    draft([concept])
    style draft fill:#fff

    proposed([voorstel])
    style proposed fill:#fff
    
    rejected([afgewezen])
    style rejected fill:#fff,stroke-dasharray: 5 5
    
    accepted([definitief])
    style accepted fill:#fff,stroke-dasharray: 5 5
    
    deprecated([verouderd])
    style deprecated fill:#fff,stroke-dasharray: 5 5
    
    superseded([vervangen])
    style superseded fill:#fff,stroke-dasharray: 5 5
    
    draft --> proposed
    proposed --> accepted
    accepted --> deprecated
    accepted --> superseded
    proposed ---> rejected
```

Op dit moment is het besluit genomen om alleen met de volgende twee labels te werken:

1. Concept: dit betekent dat de pagina informatie biedt die in concept is. Kortom, deze informatie
   behoeft juist nog input en expertise van een ieder die zich daartoe geroepen voelt!
2. Voorstel: dit betekent dat het stuk is voorgesteld aan het Tactisch Overleg van het FDS en is
   besproken. Hierdoor heeft er een inhoudelijke discussie heeft kunnen plaatsvinden met eventuele
   inhoudelijke aanvullingen en/of wijzigingen.

Deze labels gaan we vanaf heden in het Nederlands toevoegen aan een nieuwe pagina. Bij bestaande
pagina's zullen we komende tijd de wijzigingen doorvoeren. In de toekomst zullen ook de labels
geaccepteerd en geweigerd worden toegevoegd, dit hangt ook samen met de verdere inrichting van het
Federatief Datastelsel.

## Pros en cons van de oplossingen

- Op deze wijze is het gebruik van versienummers uniform afgesproken, dit voorkomt verwarring tussen
  de auteurs
- Op deze wijze is het helder welke labels we in de huidige fase van het FDS gebruiken
- Dit biedt ruimte om in de toekomst ook de andere labels toe te passen, bij verscherping en
  aanvulling op het besluitvormingsproces rondom 'FDS producten'
- We zijn sneller in staat om vragen te beantwoorden van buitenaf welke status bepaalde
  kennispagina's hebben en hoe we daarmee om gaan
