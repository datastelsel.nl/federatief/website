---
title: federatief.datastelsel.nl
---

{{< blocks/cover title="Welkom bij het samenwerkingsplatform van het federatief datastelsel! " image_anchor="top" height="full" >}}
<div class="column">
  <a class="btn btn-lg btn-info me-3 mb-4" href="/kennisbank/">
    Werkbank</i>
  </a>
  <a class="btn btn-lg btn-info me-3 mb-4" href="/puzzels/">
    Puzzels</i>
  </a>
  <a class="btn btn-lg btn-info me-3 mb-4" href="/community/">
    Community</i>
  </a>
  <a class="btn btn-lg btn-primary me-3 mb-4" href="https://realisatieibds.nl/" target="_blank">
    Interbestuurlijke datastrategie <i class="fa fa-globe ms-2 "></i>
  </a>
</div>
<div class="column">
<a class="btn btn-lg btn-primary me-3 mb-4" href="/kennisbank/ik-ben-data-aanbieder/">
  <svg id="datawaarde-svg" xmlns="http://www.w3.org/2000/svg" class="capability-icon" viewBox="0 0 24 24">
    <g fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
      <path d="M6 5h12l3 5l-8.5 9.5a.7.7 0 0 1-1 0L3 10l3-5" />
      <path d="M10 12L8 9.8l.6-1" />
    </g>
  </svg>
  Ik ben een data-aanbieder
</a>
</div>

<div class="container row p-3" >&nbsp;</div>

{{% blocks/section color="primary" %}}
Hier werken we samen aan de ontwikkeling van het data-ecosysteem van de Nederlandse overheid. Om goed samen te kunnen werken, hebben we dit platform opgezet, tegelijkertijd zijn we nog druk aan het werk om dit platform verder te ontwikkelen.

Het succes van het federatief datastelsel is een optelsom van de bijdragen van de deelnemers. Samen
geven we vorm aan de meerjarige ontwikkeling van het stelsel. Op deze samenwerkingsomgeving vind je
de laatste versie van het fds. We nodigen je uit om te reageren, vragen te stellen of direct
aanvullingen op de content te doen middels [Git](/docs/contribution/). Lees meer hierover in onze [strategie van
samenwerken](/docs/strategie-van-samenwerken/) of duik direct onze
[werkomgeving](/kennisbank/) in.

De ontwikkeling van het fds wordt ondersteund door het programma realisatie Interbestuurlijke
Datastrategie (IBDS). De IBDS is opgezet om de kansen van verantwoord datagebruik te benutten en om
de knelpunten aan te pakken. Wat mag? Wat kan? Wat helpt? Wat inspireert?

Meer informatie vind je op de [IBDS-website](https://realisatieibds.nl/). Hier vind je ook een speciale pagina over [fds](https://realisatieibds.nl/groups/view/0056c9ef-5c2e-44f9-a998-e735f1e9ccaa/federatief-datastelsel).

{{% /blocks/section %}}

{{< /blocks/cover >}}

{{% blocks/section color="white" type="row" %}}
{{% blocks/feature icon="fds-icons-chat" title="Mattermost" %}}

We begrijpen dat je vragen hebt over de ontwikkeling van het federatief datastelsel, kom daarom
rechtstreeks met ons in contact.

Via deze chat op [digilab.overheid.nl/chat/fds](https://digilab.overheid.nl/chat/fds) kun je met
alle betrokkenen in contact komen!

{{% /blocks/feature %}}

{{% blocks/feature icon="fab fa-gitlab" title="Contributies zijn welkom" url="/docs/contribution/" %}}

We nodigen je uit mee te bouwen aan het stelsel, ga naar GitLab en dien je aanvullingen in.

Meer weten over hoe?

{{% /blocks/feature %}}

{{% blocks/feature icon="fds-icons-linkedin" title="Volg ons" %}}

Wil je vooral nu up to date blijven over de nieuwste ontwikkelingen, volg Realisatie IBDS dan op
[LinkedIn](https://www.linkedin.com/company/realisatie-ibds/)

{{% /blocks/feature %}}

{{% /blocks/section %}}
