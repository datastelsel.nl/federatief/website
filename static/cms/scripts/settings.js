CMS.registerRemarkPlugin({ settings: { bullet: '-', rule: '-' } });

console.defaultError = console.error;
console.error = (...args) => {
    if (
        args.length > 0 && 
        args[0] != undefined &&
        args[0].message != undefined &&
        args[0].message.includes("does not have access to this repo")
    ) {
        alert(`
        Uw Gitlab account heeft nog geen toegang tot het CMS. 
        Er zal een nieuw tabblad geopend worden die ervoor zorgt dat er toegang wordt gevraagd. 
        Nadat die pagina is geladen kunt u het tabblad sluiten.
        `);
        window.open("https://gitlab.com/datastelsel.nl/federatief/website/-/project_members/request_access", '_blank');
    }

    console.defaultError(args);
}
