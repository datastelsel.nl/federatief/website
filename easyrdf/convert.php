<?php
require 'vendor/autoload.php';

function getDirContents($dir, &$results = array()) {
    $files = scandir($dir);

    foreach ($files as $key => $value) {
        $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
        if (is_dir($path) && $value != "." && $value != ".." && $value != "_print") {
            getDirContents($path, $results);
        } else if ( str_ends_with($value, '.html') ) {
            $results[] = $path;
        }
    }

    return $results;
}


# Find all html file locations

foreach (getDirContents('/src') as $src) {
    # For each source file location, create target files and a base uri

    $dst = str_replace('/src/', '/dst/', $src);
    $dst_dir = dirname($dst);
    $uri = str_replace('/dst', 'http://federatief.datastelsel.nl', $dst_dir)."/";
    $dst_ttl = str_replace('.html', '.ttl', $dst);
    $dst_jsonld = str_replace('.html', '.jsonld', $dst);

    # Dump the created locations to the terminal

    var_dump($src);
    var_dump($dst_dir);
    var_dump($dst_ttl);
    var_dump($dst_jsonld);
    var_dump($uri);

    # Read the Hugo generated rdfa file 

    $graph = new \EasyRdf\Graph();
    $graph->parseFile($src, 'rdfa', $uri);
    
    # Cleanup Hugo generated triples

    $graph->delete($uri, 'og:url');
    $graph->delete($uri, 'og:site_name');
    $graph->delete($uri, 'og:title');
    $graph->delete($uri, 'og:description');
    $graph->delete($uri, 'og:locale');
    $graph->delete($uri, 'og:type');
    $graph->delete($uri, 'article:section');
    $graph->delete($uri, 'article:published_time');
    $graph->delete($uri, 'article:modified_time');

    # Create the turtle content

    $output_ttl = $graph->serialise("turtle");
    if (!is_scalar($output_ttl)) {
        $output_ttl = var_export($output_ttl, true);
    }

    # Create the jsonld content
    
    $output_jsonld = $graph->serialise("jsonld", [JSON_PRETTY_PRINT]);
    if (!is_scalar($output_jsonld)) {
        $output_jsonld = var_export($output_jsonld, true);
    }
    // replace /r/n for just /n
    $search   = "\\n";
    $replace = "\\n";
    $output_jsonld = str_replace($search, $replace, strval($output_jsonld));
    // pretty print jsonld
    $json_temp = json_decode($output_jsonld);
    $output_jsonld = json_encode($json_temp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

    # Create a folder (if not present)

    if (!file_exists($dst_dir)) {
        mkdir($dst_dir);
    }

    # Write the turtle and jsonld files

    file_put_contents($dst_ttl, $output_ttl);
    file_put_contents($dst_jsonld, $output_jsonld);
}



